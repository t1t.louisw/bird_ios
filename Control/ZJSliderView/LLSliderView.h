//
//  LYFSliderView.h
//  MingMen
//
//  Created by 罗云飞 on 2017/3/9.
//  Copyright © 2017年 罗云飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LLSliderView : UIView
//执行执行此方法之前必须要设定ZJSliderView的frame
-(void)setViewControllers:(NSArray *)viewControllers owner:(UIViewController *)parentViewController;

//获取顶部的控制滚动视图
-(UIView *)topControlViewWithFrame:(CGRect)frame titleLabelWidth:(CGFloat)titleLabelWidth;

@property(nonatomic,copy)void (^NewLLSlider)(int index);

@end
