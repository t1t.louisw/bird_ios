//
//  ZJSliderView.m
//  MyDamai
//
//  Created by mac on 14-10-18.
//  Copyright (c) 2014年 zhang jian. All rights reserved.
//

#import "LLSliderView.h"

#define TOP_TAG 200
#define CONTENT_TAG 201


@interface LLSliderView ()<UIScrollViewDelegate>
{
    UIScrollView *_topScrollView;
    NSMutableArray *_titleLabelArray;
    UIImageView *_topIndicatorView;
    
    UIScrollView *_contentScrollView;
    UILabel *label1;
    
    
}
@property (strong,nonatomic) NSArray *viewControllers;
@property (weak,nonatomic) UIViewController *parentViewController;
@property (nonatomic) float titleLabelWidth;
@property(nonatomic,retain)NSArray *vcArr;

@end

@implementation LLSliderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setViewControllers:(NSArray *)viewControllers owner:(UIViewController *)parentViewController;
{
    self.parentViewController = parentViewController;
    self.viewControllers = viewControllers;
    
    
    //创建view
    _contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _vcArr = viewControllers;
    
    [self lazyLoadVcFromIndex:0];
    
    _contentScrollView.pagingEnabled = YES;
    _contentScrollView.tag = CONTENT_TAG;
    _contentScrollView.showsHorizontalScrollIndicator = NO;
    _contentScrollView.showsVerticalScrollIndicator = NO;
    _contentScrollView.delegate = self;
    _contentScrollView.bounces = NO;
    
    [self addSubview:_contentScrollView];
    /*
     for (int i=0; i<viewControllers.count; i++) {
     
     UIViewController *vc = viewControllers[i];
     vc.view.frame = CGRectMake(i*_contentScrollView.frame.size.width, 0, _contentScrollView.frame.size.width, _contentScrollView.frame.size.height);
     
     [_contentScrollView addSubview:vc.view];
     
     //非常关键的一句
     [self.parentViewController addChildViewController:vc];
     }
     */
    _contentScrollView.contentSize = CGSizeMake(_contentScrollView.frame.size.width * viewControllers.count, _contentScrollView.frame.size.height);
    
    //显示第0页
    [self contentScrollViewShowPage:0];
}
//懒加载策略
-(void)lazyLoadVcFromIndex:(NSInteger )index
{
    UIViewController *vc = _vcArr[index];
    vc.view.frame = CGRectMake(_contentScrollView.frame.size.width*index,0, _contentScrollView.frame.size.width,_contentScrollView.frame.size.height);
    [_contentScrollView addSubview:vc.view];
    [self.parentViewController addChildViewController:vc];
}

//获取顶部的控制滚动视图
-(UIView *)topControlViewWithFrame:(CGRect)frame titleLabelWidth:(CGFloat)titleLabelWidth;
{
    
    //设置label宽度
    if(titleLabelWidth == 0)
    {
        titleLabelWidth = 80;
    }
    _titleLabelWidth = titleLabelWidth;
    
    
    //滚动视图
    _topScrollView = [[UIScrollView alloc] initWithFrame:frame];
    _topScrollView.tag = TOP_TAG;
    _topScrollView.showsHorizontalScrollIndicator = NO;
    _topScrollView.showsVerticalScrollIndicator = NO;
    _topScrollView.delegate = self;
    [self addSubview:_topScrollView];
    
    
    
    //添加view
    _titleLabelArray = [[NSMutableArray alloc] init];
    for (int i=0; i<_viewControllers.count; i++) {
        
        UIViewController *vc = _viewControllers[i];
        
        label1 = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelWidth * i, 0, titleLabelWidth, _topScrollView.frame.size.height)];
        label1.text = vc.title;
        label1.userInteractionEnabled = YES;
        label1.tag = i+100;
        label1.font = [UIFont systemFontOfSize:14];
        label1.textAlignment = NSTextAlignmentCenter;
        [_titleLabelArray addObject:label1];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(titleLabelWidth * i-.7, 10, .6, _topScrollView.frame.size.height-20)];
        line.backgroundColor = NewBlackColor;
        [_topScrollView addSubview:line];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dealTap:)];
        [label1 addGestureRecognizer:tap];
        [_topScrollView addSubview:label1];
    }
    _topScrollView.contentSize = CGSizeMake(titleLabelWidth * _viewControllers.count, _topScrollView.frame.size.height);
    
    //    //添加提示视图
    //    _topIndicatorView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, _topScrollView.frame.size.height)];
    //    _topIndicatorView.image = [UIImage imageNamed:@"red_line_and_shadow1.png"];
    //    [_topScrollView addSubview:_topIndicatorView];
    //
    
    //添加提示视图
    _topIndicatorView = [[UIImageView alloc] init];
    CGFloat indicatorViewW = label1.frame.size.width/2;
    CGFloat indicatorViewCenterX = label1.center.x;
    
    _topIndicatorView.frame = CGRectMake(indicatorViewCenterX - indicatorViewW / 2, 42, indicatorViewW, 2);
    _topIndicatorView.backgroundColor = NewNavigationColor;
    [_topScrollView addSubview:_topIndicatorView];
    
    
    [self topScrollViewShowPage:0];
    
    
    
    return _topScrollView;
}


#pragma mark - 处理动画结束
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView.tag == CONTENT_TAG)
    {
        //注意0除
        int page = scrollView.contentOffset.x / self.frame.size.width;
        [self topScrollViewShowPage:page];
        [self lazyLoadVcFromIndex:page];
        _NewLLSlider(page);
    }
}

-(void)dealTap:(UITapGestureRecognizer *)tap
{
    int page = tap.view.tag - 100;
    [self topScrollViewShowPage:page];
    [self contentScrollViewShowPage:page];
    [self lazyLoadVcFromIndex:page];
    _NewLLSlider(page);
}
-(void)topScrollViewShowPage:(int)page
{
    for (UILabel *label in _titleLabelArray) {
        label.textColor = [UIColor blackColor];
    }
    UILabel *selectLabel = _titleLabelArray[page];
    selectLabel.textColor = NewNavigationColor;
    
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat indicatorViewW = selectLabel.frame.size.width/2;
        CGFloat indicatorViewCenterX = selectLabel.center.x;
        _topIndicatorView.frame = CGRectMake(indicatorViewCenterX - indicatorViewW / 2, 42, indicatorViewW, 2);
        // _topIndicatorView.frame = selectLabel.frame;
    }];
    
    //计算偏移量
    if (label1.center.x<SCREEN_WIDTH/2) {
        
        [_topScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }else if (label1.center.x>_topScrollView.frame.size.width-SCREEN_WIDTH/2)
    {
        [_topScrollView setContentOffset:CGPointMake(_topScrollView.contentSize.width-SCREEN_WIDTH, 0) animated:YES];
    }else
    {
        [_topScrollView setContentOffset:CGPointMake(label1.center.x-SCREEN_WIDTH/2, 0) animated:YES];
    }
    
    
}
-(void)contentScrollViewShowPage:(int)page
{
    [_contentScrollView setContentOffset:CGPointMake(page * _contentScrollView.frame.size.width, 0) animated:YES];
    [self lazyLoadVcFromIndex:page];
    
}



/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
