//
//  NewRollButton.m
//  MingMen
//
//  Created by NewProject on 2017/5/13.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewRollButton.h"
#import "NewRollButtonCell.h"


@interface NewRollButton ()<UICollectionViewDataSource,UICollectionViewDelegate>{
    NSMutableArray *dataArray;
}

@end

@implementation NewRollButton


- (id)initWithFrame:(CGRect)frame
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0; //列间距
    flowLayout.minimumLineSpacing = 0;      //行间距
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    self = [super initWithFrame:frame collectionViewLayout:flowLayout];
    if (self) {
        dataArray = [NSMutableArray array];
        //隐藏滑块
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        //设置代理
        self.delegate = self;
        self.dataSource = self;
        //设置背景颜色（默认黑色）
        self.backgroundColor = [UIColor clearColor];
        //注册单元格
        [self registerClass:[NewRollButtonCell class] forCellWithReuseIdentifier:@"Cell"];
    }
    return self;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize itemSize = CGSizeMake(0.0, 0.0);
    
    if (self.buttonType == rollButtonAndTitle) {
        itemSize.width = SCREEN_WIDTH/6;
        itemSize.height = self.frame.size.height;
    }
    else if (self.buttonType == rollButtonAndTitle) {
        itemSize.width = SCREEN_WIDTH/6;
        itemSize.height = self.frame.size.height;
    }
    return itemSize;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewRollButtonCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    if (dataArray.count>0) {
        [cell assignment:dataArray[indexPath.row] type:self.buttonType];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.NewRollButtonCallback?self.NewRollButtonCallback(dataArray[indexPath.row]):nil;
}


- (void)reloadDatas:(NSMutableArray *)array
{
    [dataArray removeAllObjects];
    [dataArray addObjectsFromArray:array];
    [self reloadData];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
