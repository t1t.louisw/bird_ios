//
//  NewRollButtonCell.m
//  MingMen
//
//  Created by NewProject on 2017/5/14.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewRollButtonCell.h"

@implementation NewRollButtonCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        
        self.img = [[UIImageView alloc] init];
        self.img.frame = CGRectMake((self.frame.size.width-fitScreenHeight(33))/2, (self.frame.size.height/2)-(fitScreenHeight(33)/2)-(fitScreenHeight(20)/2)-1.5, fitScreenHeight(33), fitScreenHeight(33));
        [self addSubview:self.img];

        self.tilte = [[UILabel alloc]init];
        self.tilte.frame = CGRectMake(0, self.img.frame.origin.y+self.img.frame.size.height+3, self.frame.size.width, fitScreenHeight(20));
        self.tilte.font = NewFont(12);
        self.tilte.textColor = NewBlackColor;
        self.tilte.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.tilte];
        
    }
    return self;
}
- (void)assignment:(NSDictionary *)model type:(NewRollButtonType)type
{
    if (type == rollButtonAndTitle) {
//        [self.img sd_setImageWithURL:NewURL([model safeObjectForKey:@"classifyIcon"]) placeholderImage:NewImageError_C];
//        self.tilte.text = [model safeObjectForKey:@"classifyName"];
        self.img.backgroundColor = NewRedColor;
        self.tilte.text = @"哈哈";
    }else {
//        [self.img sd_setImageWithURL:NewURL([model safeObjectForKey:@"classifyIcon"]) placeholderImage:NewImageError_C];
//        self.tilte.hidden = YES;
        self.img.backgroundColor = NewRedColor;
        self.tilte.text = @"哈哈";
    }
}

@end
