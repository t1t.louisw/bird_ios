//
//  NewRollButton.h
//  MingMen
//
//  Created by NewProject on 2017/5/13.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    rollButtonAndTitle = 0,       //按钮+文字
    rollButton                    //纯按钮模式
}NewRollButtonType;

@interface NewRollButton : UICollectionView

@property (assign,nonatomic)NewRollButtonType buttonType;
@property (nonatomic,copy)void (^NewRollButtonCallback)(NSDictionary *dictionary);
- (void)reloadDatas:(NSMutableArray *)array;

@end
