//
//  NewRollButtonCell.h
//  MingMen
//
//  Created by NewProject on 2017/5/14.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewRollButton.h"

@interface NewRollButtonCell : UICollectionViewCell

@property(strong,nonatomic) UIImageView *img;
@property(strong,nonatomic) UILabel *tilte;

- (void)assignment:(NSDictionary *)model type:(NewRollButtonType)type;

@end
