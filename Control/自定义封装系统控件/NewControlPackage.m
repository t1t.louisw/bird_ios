//
//  NewControlPackage.m
//  XPH
//
//  Created by NewProject on 16/7/7.
//  Copyright © 2016年 YAY. All rights reserved.
//

#import "NewControlPackage.h"


@implementation NewControlPackage

#pragma mark - UIButton按钮
+(UIButton *)buttonInitWithTitle:(NSString *)title Frame:(CGRect)frame backgroundImage:(UIImage *)backgroundImage backgroundImageHighlighted:(UIImage *)backgroundImageHighlighted backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor textAlignment:(UIControlContentHorizontalAlignment)textAlignment font:(UIFont *)font tag:(int)tag target:(id)target action:(SEL)action hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled
{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:frame];
    [btn setUserInteractionEnabled:userInteractionEnabled];
    [btn setTag:tag];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn.titleLabel setFont:font];
    [btn setTitleColor:textColor forState:UIControlStateNormal];
    [btn setContentHorizontalAlignment:textAlignment];
    [btn setBackgroundColor:backgroundColor];
    [btn setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    [btn setBackgroundImage:backgroundImageHighlighted forState:UIControlStateHighlighted];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [btn setHidden:hidden];
    return btn;
}


#pragma mark - UILabel标签
+(UILabel *)labelInitWithFrame:(CGRect)frame text:(NSString *)text numberOfLines:(int)numberOfLines  backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font tag:(int)tag hidden:(BOOL)hidden adjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth userInteractionEnabled:(BOOL)userInteractionEnabled
{
    
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    [label setNumberOfLines:numberOfLines];
    [label setBackgroundColor:backgroundColor];
    [label setText:text];
    [label setTextColor:textColor];
    [label setTextAlignment:textAlignment];
    [label setFont:font];
    [label setTag:tag];
    [label setHidden:hidden];
    [label setAdjustsFontSizeToFitWidth:adjustsFontSizeToFitWidth];
    [label setUserInteractionEnabled:userInteractionEnabled];
    return label;
}


#pragma mark - UIScrollView
+(UIScrollView *)scrollViewInitWithFrame:(CGRect)frame scrollEnabled:(BOOL)scrollEnabled contentSize:(CGSize)contentSize backgroundColor:(UIColor *)backgroundColor showsHorizontalScrollIndicator:(BOOL)showsHorizontalScrollIndicator showsVerticalScrollIndicator:(BOOL)showsVerticalScrollIndicator pagingEnabled:(BOOL)pagingEnabled tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:frame];
    [scrollView setScrollEnabled:scrollEnabled];
    [scrollView setContentSize:contentSize];
    [scrollView setBackgroundColor:backgroundColor];
    [scrollView setShowsHorizontalScrollIndicator:showsHorizontalScrollIndicator];
    [scrollView setShowsVerticalScrollIndicator:showsVerticalScrollIndicator];
    [scrollView setPagingEnabled:pagingEnabled];
    [scrollView setTag:tag];
    [scrollView setHidden:hidden];
    [scrollView setUserInteractionEnabled:userInteractionEnabled];
    return scrollView;
}


#pragma mark - UIImageView
+(UIImageView *)imageViewInitWithFrame:(CGRect)frame image:(UIImage *)image highlightedImage:(UIImage *)highlightedImage backgroundColor:(UIColor *)backgroundColor tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    [imageView setImage:image];
    [imageView setHighlightedImage:highlightedImage];
    [imageView setBackgroundColor:backgroundColor];
    [imageView setTag:tag];
    [imageView setHidden:hidden];
    [imageView setUserInteractionEnabled:userInteractionEnabled];
    return imageView;
}


#pragma mark - UITextField
+(UITextField *)textFieldInitWithFrame:(CGRect)frame backgroundImage:(UIImage *)backgroundImage backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor placeholder:(NSString *)placeholder hidden:(BOOL)hidden tag:(int)tag font:(UIFont *)font textAlignment:(NSTextAlignment)textAlignment clearButtonMode:(UITextFieldViewMode)clearButtonMode clearsOnBeginEditing:(BOOL)clearsOnBeginEditing adjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth secureTextEntry:(BOOL)secureTextEntry keyboardType:(UIKeyboardType)keyboardType returnKeyType:(UIReturnKeyType)returnKeyType userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UITextField *textField = [[UITextField alloc]initWithFrame:frame];
    [textField setBackground:backgroundImage];
    [textField setBackgroundColor:backgroundColor];
    [textField setTextColor:textColor];
    [textField setPlaceholder:placeholder];
    [textField setHidden:hidden];
    [textField setTag:tag];
    [textField setFont:font];
    [textField setTextAlignment:textAlignment];
    [textField setClearButtonMode:clearButtonMode];
    [textField setClearsOnBeginEditing:clearsOnBeginEditing];
    [textField setAdjustsFontSizeToFitWidth:adjustsFontSizeToFitWidth];
    [textField setSecureTextEntry:secureTextEntry];
    [textField setKeyboardType:keyboardType];
    [textField setReturnKeyType:returnKeyType];
    [textField setUserInteractionEnabled:userInteractionEnabled];
    return textField;
}


#pragma mark - UISearchBar
+(UISearchBar *)searchBarInitWithFrame:(CGRect)frame barStyle:(UIBarStyle)barStyle delegate:(id)delegate backgroundImage:(UIImage *)backgroundImage backgroundColor:(UIColor *)backgroundColor tintColor:(UIColor *)tintColor placeholder:(NSString *)placeholder hidden:(BOOL)hidden tag:(int)tag showsCancelButton:(BOOL)showsCancelButton keyboardType:(UIKeyboardType)keyboardType returnKeyType:(UIReturnKeyType)returnKeyType userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UISearchBar *searchBar = [[UISearchBar alloc]initWithFrame:frame];
    [searchBar setBarStyle:barStyle];
    [searchBar setDelegate:delegate];
    [searchBar setBackgroundImage:backgroundImage];
    [searchBar setBackgroundColor:backgroundColor];
    [searchBar setTintColor:tintColor];
    [searchBar setPlaceholder:placeholder];
    [searchBar setHidden:hidden];
    [searchBar setTag:tag];
    [searchBar setShowsCancelButton:showsCancelButton];
    [searchBar setKeyboardType:keyboardType];
    [searchBar setReturnKeyType:returnKeyType];
    [searchBar setUserInteractionEnabled:userInteractionEnabled];
    return searchBar;
}


#pragma mark - 自定义 输入框底图1
+(UIImageView *)inputBoxInitWithFrame:(CGRect)frame lconImage:(UIImage *)lconImage backgroundImage:(UIImage *)backgroundImage backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    [imageView setUserInteractionEnabled:YES];
    [imageView setImage:backgroundImage];
    
    UIImageView *lcon = [[UIImageView alloc]initWithFrame:CGRectMake(12, (frame.size.height-20)/2, 14, frame.size.height-(frame.size.height-20))];
    [lcon setImage:lconImage];
    [imageView addSubview:lcon];
    
    return imageView;
}

#pragma mark - 自定义 输入框底图2
+(UIView *)customView1InitWithFrame:(CGRect)frame text:(NSString *)text textColor:(UIColor *)textColor textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden lineHidden:(BOOL)lineHidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    [view setBackgroundColor:backgroundColor];
    [view setHidden:hidden];
    [view setTag:tag];
    [view setUserInteractionEnabled:userInteractionEnabled];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 65, frame.size.height)];
    [label setBackgroundColor:backgroundColor];
    [label setText:text];
    [label setTextColor:textColor];
    [label setTextAlignment:textAlignment];
    [label setFont:font];
    [view addSubview:label];
    
    UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, frame.size.height-.6, frame.size.width-15, .6)];
    [lineImageView setBackgroundColor:NewCellLineColor];
    [lineImageView setHidden:lineHidden];
    [view addSubview:lineImageView];
    
    return view;
}


#pragma mark - 自定义 协议规则框
+(UIView *)protocolRuleBarInitWithFrame:(CGRect)frame title:(NSString *)title textColor:(UIColor *)textColor textAlignment:(UIControlContentHorizontalAlignment)textAlignment font:(CGFloat)font backgroundColor:(UIColor *)backgroundColor backgroundImage:(UIImage *)backgroundImage backgroundImageSelected:(UIImage *)backgroundImageSelected target:(id)target action:(SEL)action  hidden:(BOOL)hidden agreeBTTag:(int)agreeBTTag agreementBTTag:(int)agreementBTTag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    [view setBackgroundColor:backgroundColor];
    [view setHidden:hidden];
    [view setUserInteractionEnabled:userInteractionEnabled];

    UIButton *agreeBT = [UIButton buttonWithType:UIButtonTypeCustom];
    agreeBT.frame = CGRectMake(20, (frame.size.height-16)/2, 16, 16);
    [agreeBT setImage:backgroundImageSelected forState:UIControlStateNormal];
    [agreeBT setImage:backgroundImage forState:UIControlStateSelected];
    [agreeBT addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [agreeBT setTag:agreeBTTag];
    [view addSubview:agreeBT];
    
    
    CGFloat width = [NewUtils heightforString:[NSString stringWithFormat:@"%@",title] andHeight:20 fontSize:font];
    UIButton *agreement = [UIButton buttonWithType:UIButtonTypeCustom];
    agreement.frame = CGRectMake(43, (frame.size.height-20)/2, width+10, 20);
    [agreement setTitle:title forState:UIControlStateNormal];
    [agreement setTitleColor:textColor forState:UIControlStateNormal];
    [agreement.titleLabel setFont:[UIFont systemFontOfSize:font]];
    [agreement setContentHorizontalAlignment:textAlignment];
    [agreement addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [agreement setTag:agreementBTTag];
    [view addSubview:agreement];
    
    return view;
}


#pragma mark - UISwitch
+(UISwitch *)switchInitWithFrame:(CGRect)frame isOn:(BOOL)on hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UISwitch *isSwitch = [[UISwitch alloc]initWithFrame:frame];
    [isSwitch setOn:on];
    [isSwitch setHidden:hidden];
    [isSwitch setTag:tag];
    [isSwitch setUserInteractionEnabled:userInteractionEnabled];
    //[isSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
    return isSwitch;
}


#pragma mark - UIView
+(UIView *)viewInitWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    [view setBackgroundColor:backgroundColor];
    [view setHidden:hidden];
    [view setTag:tag];
    [view setUserInteractionEnabled:userInteractionEnabled];
    return view;
}


#pragma mark - UITextView
+(UITextView *)textViewInitWithFrame:(CGRect)frame scrollEnabled:(BOOL)scrollEnabled backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor text:(NSString *)text font:(UIFont *)font textAlignment:(NSTextAlignment)textAlignment autoresizingMask:(UIViewAutoresizing)autoresizingMask keyboardType:(UIKeyboardType)keyboardType returnKeyType:(UIReturnKeyType)returnKeyType hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UITextView *textView = [[UITextView alloc]initWithFrame:frame];
    [textView setScrollEnabled:scrollEnabled];
    [textView setBackgroundColor:backgroundColor];
    [textView setTextColor:textColor];
    [textView setText:text];
    [textView setTextAlignment:textAlignment];
    [textView setFont:font];
    [textView setAutoresizingMask:autoresizingMask];//自适应高度
    [textView setKeyboardType:keyboardType];
    [textView setReturnKeyType:returnKeyType];
    [textView setHidden:hidden];
    [textView setTag:tag];
    [textView setUserInteractionEnabled:userInteractionEnabled];
    return textView;
}


#pragma mark - UITableView
+(UITableView *)tableViewInitWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor style:(NSInteger)style delegate:(id)delegate dataSource:(id)dataSource showsHorizontalScrollIndicator:(BOOL)showsHorizontalScrollIndicator showsVerticalScrollIndicator:(BOOL)showsVerticalScrollIndicator hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UITableView *tableView = [[UITableView alloc]initWithFrame:frame style:style];
    [tableView setDelegate:delegate];
    [tableView setDataSource:dataSource];
    [tableView setBackgroundColor:backgroundColor];
    [tableView setHidden:hidden];
    [tableView setTag:tag];
    [tableView setUserInteractionEnabled:userInteractionEnabled];
    [tableView setShowsHorizontalScrollIndicator:showsHorizontalScrollIndicator];
    [tableView setShowsVerticalScrollIndicator:showsVerticalScrollIndicator];
    return tableView;
}


#pragma mark - UICollectionView
+(UICollectionView *)collectionViewInitWithFrame:(CGRect)frame delegate:(id)delegate dataSource:(id)dataSource backgroundColor:(UIColor *)backgroundColor scrollEnabled:(BOOL)scrollEnabled alwaysBounceVertical:(BOOL)alwaysBounceVertical alwaysBounceHorizontal:(BOOL)alwaysBounceHorizontal showsHorizontalScrollIndicator:(BOOL)showsHorizontalScrollIndicator showsVerticalScrollIndicator:(BOOL)showsVerticalScrollIndicator collectionViewFlowLayout:(UICollectionViewFlowLayout *)collectionViewFlowLayout sectionInset:(UIEdgeInsets)sectionInset headerReference:(CGSize)headerReference footerReference:(CGSize)footerReference minimumInteritemSpacing:(CGFloat)minimumInteritemSpacing minimumLineSpacing:(CGFloat)minimumLineSpacing  scrollDirection:(NSInteger)scrollDirection hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    //UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    
    collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    //距离边界的上下左右间距
    [collectionViewFlowLayout setSectionInset:sectionInset];
    //头视图CGSize
    [collectionViewFlowLayout setHeaderReferenceSize:headerReference];
    //尾视图CGSize
    [collectionViewFlowLayout setFooterReferenceSize:footerReference];
    //竖向最大间距
    [collectionViewFlowLayout setMinimumInteritemSpacing:minimumInteritemSpacing];
    //横向最小间距
    [collectionViewFlowLayout setMinimumLineSpacing:minimumLineSpacing];
    //纵/横 排序
    [collectionViewFlowLayout setScrollDirection:scrollDirection==0?UICollectionViewScrollDirectionVertical:UICollectionViewScrollDirectionHorizontal];
    
    
    UICollectionView *newCollectionView = [[UICollectionView alloc]initWithFrame:frame collectionViewLayout:collectionViewFlowLayout];
    //RGBACOLOR(232, 232, 227, 1)
    [newCollectionView setBackgroundColor:backgroundColor];
    [newCollectionView setDelegate:delegate];
    [newCollectionView setDataSource:dataSource];
    [newCollectionView setScrollEnabled:scrollEnabled];
    //是否可以纵向滑动
    [newCollectionView setAlwaysBounceVertical:alwaysBounceVertical];
    //是否可以横向滑动
    [newCollectionView setAlwaysBounceHorizontal:alwaysBounceHorizontal];
    //是否展示横向浮动条
    [newCollectionView setShowsHorizontalScrollIndicator:showsHorizontalScrollIndicator];
    //是否展示竖向浮动条
    [newCollectionView setShowsVerticalScrollIndicator:showsVerticalScrollIndicator];
    [newCollectionView setHidden:hidden];
    [newCollectionView setTag:tag];
    [newCollectionView setUserInteractionEnabled:userInteractionEnabled];
    //注册Cell模板
    //[newCollectionView registerClass:[registerClass class] forCellWithReuseIdentifier:@"Cell"];
    //注册头视图模板
    //    [newCollectionView registerClass:[headerView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    //注册尾视图模板
    //    [newCollectionView registerClass:[footerView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView"];
    return newCollectionView;
}


#pragma mark - UISlider
+(UISlider *)sliderInitWithFrame:(CGRect)frame minimumValue:(float)minimumValue maximumValue:(float)maximumValue value:(float)value thumbImage:(UIImage *)thumbImage minimumTrackImage:(UIImage *)minimumTrackImage maximumTrackImage:(UIImage *)maximumTrackImage minimumTrackImageResizableImageWithCapInsets:(UIEdgeInsets)minimumTrackImageResizableImageWithCapInsets resizingMode1:(NSInteger)resizingMode1 maximumTrackImageResizableImageWithCapInsets:(UIEdgeInsets)maximumTrackImageResizableImageWithCapInsets resizingMode2:(NSInteger)resizingMode2 target:(id)target action:(SEL)action hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UISlider *slider = [[UISlider alloc]initWithFrame:frame];
    [slider setMinimumValue:minimumValue]; //最小值
    [slider setMaximumValue:maximumValue]; //最大值
    [slider setValue:value]; //默认值
    [slider setThumbImage:thumbImage forState:UIControlStateNormal];//滑动按钮
    minimumTrackImage = [minimumTrackImage resizableImageWithCapInsets:minimumTrackImageResizableImageWithCapInsets resizingMode:resizingMode1];
    [slider setMinimumTrackImage:minimumTrackImage forState:UIControlStateNormal];//滑动图层
    maximumTrackImage = [maximumTrackImage resizableImageWithCapInsets:maximumTrackImageResizableImageWithCapInsets resizingMode:resizingMode2];
    [slider setMaximumTrackImage:maximumTrackImage forState:UIControlStateNormal];//滑动底层
    [slider setHidden:hidden];
    [slider setTag:tag];
    [slider setUserInteractionEnabled:userInteractionEnabled];
    [slider addTarget:target action:action forControlEvents:UIControlEventValueChanged];
    return slider;
}


//UIProgressView
#pragma mark - UIProgressView
+(UIProgressView *)progressViewInitWithFrame:(CGRect)frame progressViewStyle:(NSInteger)progressViewStyle progress:(float)progress progressTintColor:(UIColor *)progressTintColor trackTintColor:(UIColor *)trackTintColor trackImage:(UIImage *)trackImage progressImage:(UIImage *)progressImage animated:(BOOL)animated tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIProgressView *progressView = [[UIProgressView alloc]init];
    //风格 感觉两种都差不多
    [progressView setProgressViewStyle:progressViewStyle];
    //设置的高度对进度条的高度没影响，整个高度=进度条的高度，进度条也是个圆角矩形
    [progressView setFrame:frame];
    //设置进度条上进度的颜色 默认是蓝色的
    [progressView setProgressTintColor:progressTintColor];
    //表示进度条未完成的，剩余的轨迹颜色,默认是灰色
    [progressView setTrackTintColor:trackTintColor];
    //设置进度条的背景图片
    [progressView setTrackImage:trackImage];
    //设置进度条上进度的背景图片
    [progressView setProgressImage:progressImage];
    //设置进度值并是否动画显示，这个相当于百分比，范围在0~1之间，不可以设置最大最小值
    [progressView setProgress:0.7 animated:animated];
    return progressView;
}


#pragma mark - 多颜色多字体的Label
+(UILabel *)labelAttributedStringInitWithFrame:(CGRect)frame text1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 numberOfLines:(int)numberOfLines textAlignment:(NSTextAlignment)textAlignment textColor1:(UIColor *)textColor1 textColor2:(UIColor *)textColor2 textColor3:(UIColor *)textColor3 font:(UIFont *)font backgroundColor:(UIColor *)backgroundColor tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled
{
    
    NSString *str = [NSString stringWithFormat:@"%@%@%@",text1,text2,text3];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@%@",text1,text2,text3]];
    NSRange range1=[[string string]rangeOfString:text1];
    [string addAttribute:NSForegroundColorAttributeName value:textColor1 range:range1];
    NSRange range2=[[string string]rangeOfString:text2];
    [string addAttribute:NSForegroundColorAttributeName value:textColor2 range:range2];
    NSRange range3=[[string string]rangeOfString:text3];
    [string addAttribute:NSForegroundColorAttributeName value:textColor3 range:range3];
    NSRange range4=[[string string]rangeOfString:str];
    [string addAttribute:NSFontAttributeName value:font range:range4];
    
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    [label setNumberOfLines:numberOfLines];
    [label setBackgroundColor:backgroundColor];
    [label setTextAlignment:textAlignment];
    [label setAttributedText:string];
    [label setTag:tag];
    [label setHidden:hidden];
    [label setUserInteractionEnabled:userInteractionEnabled];
    return label;
    
}

#pragma mark - JPushView 应用程序在前台时收到的推送消息展示UI
+(UIView *)pushViewInitWithFrame:(CGRect)frame channel:(NSString *)channel pushText:(NSString *)pushText image:(UIImage *)image backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    [view setBackgroundColor:backgroundColor];
    [view setHidden:hidden];
    [view setTag:tag];
    [view setUserInteractionEnabled:userInteractionEnabled];
    //    [view setAlpha:.7];
    
    UIImageView *imageView = [[UIImageView alloc]initWithImage:image];
    [imageView setFrame:CGRectMake(15, 10, 20, 20)];
    ViewRadius(imageView, 4);
    [view addSubview:imageView];
    
    UILabel *channelLabel = [[UILabel alloc]initWithFrame:CGRectMake(43, 8, 80, 20)];
    [channelLabel setBackgroundColor:[UIColor clearColor]];
    [channelLabel setText:channel];
    [channelLabel setTextColor:[UIColor whiteColor]];
    [channelLabel setTextAlignment:NSTextAlignmentLeft];
    [channelLabel setFont:[UIFont boldSystemFontOfSize:14]];
    [channelLabel setUserInteractionEnabled:YES];
    [view addSubview:channelLabel];
    
    UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(123, 8, 30, 20)];
    [timeLabel setBackgroundColor:[UIColor clearColor]];
    [timeLabel setText:@"现在"];
    [timeLabel setTextColor:[UIColor whiteColor]];
    [timeLabel setTextAlignment:NSTextAlignmentLeft];
    [timeLabel setFont:[UIFont systemFontOfSize:12]];
    [timeLabel setUserInteractionEnabled:YES];
    [timeLabel setAlpha:.4];
    [view addSubview:timeLabel];
    
    UILabel *pushTextLabel = [[UILabel alloc]initWithFrame:CGRectMake(43, 26, SCREEN_WIDTH-60, 35)];
    [pushTextLabel setBackgroundColor:[UIColor clearColor]];
    [pushTextLabel setText:pushText];
    [pushTextLabel setTextColor:[UIColor whiteColor]];
    [pushTextLabel setTextAlignment:NSTextAlignmentLeft];
    [pushTextLabel setFont:[UIFont systemFontOfSize:13]];
    [pushTextLabel setNumberOfLines:2];
    [pushTextLabel setUserInteractionEnabled:YES];
    [view addSubview:pushTextLabel];
    
    
    return view;
}




@end
