//
//  NewControlPackage.h
//  XPH
//
//  Created by NewProject on 16/7/7.
//  Copyright © 2016年 YAY. All rights reserved.
//
//  常用控件封装

#import <UIKit/UIKit.h>

@interface NewControlPackage : NSObject

#pragma mark - UIButton
+(UIButton *)buttonInitWithTitle:(NSString *)title Frame:(CGRect)frame backgroundImage:(UIImage *)backgroundImage backgroundImageHighlighted:(UIImage *)backgroundImageHighlighted backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor textAlignment:(UIControlContentHorizontalAlignment)textAlignment font:(UIFont *)font tag:(int)tag target:(id)target action:(SEL)action hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UILabel
//adjustsFontSizeToFitWidth 自动适应宽度
+(UILabel *)labelInitWithFrame:(CGRect)frame text:(NSString *)text numberOfLines:(int)numberOfLines  backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font tag:(int)tag hidden:(BOOL)hidden adjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UIScrollView
+(UIScrollView *)scrollViewInitWithFrame:(CGRect)frame scrollEnabled:(BOOL)scrollEnabled contentSize:(CGSize)contentSize backgroundColor:(UIColor *)backgroundColor showsHorizontalScrollIndicator:(BOOL)showsHorizontalScrollIndicator showsVerticalScrollIndicator:(BOOL)showsVerticalScrollIndicator pagingEnabled:(BOOL)pagingEnabled tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UIImageView
+(UIImageView *)imageViewInitWithFrame:(CGRect)frame image:(UIImage *)image highlightedImage:(UIImage *)highlightedImage backgroundColor:(UIColor *)backgroundColor tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UITextField
//clearButtonMode : UITextFieldViewModeWhileEditing 输入的时候显示叉叉
//clearsOnBeginEditing = YES 再次编辑时清空原有数据
//secureTextEntry = YES 密文输入
//adjustsFontSizeToFitWidth = YES 文字往后滚动
+(UITextField *)textFieldInitWithFrame:(CGRect)frame backgroundImage:(UIImage *)backgroundImage backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor placeholder:(NSString *)placeholder hidden:(BOOL)hidden tag:(int)tag font:(UIFont *)font textAlignment:(NSTextAlignment)textAlignment clearButtonMode:(UITextFieldViewMode)clearButtonMode clearsOnBeginEditing:(BOOL)clearsOnBeginEditing adjustsFontSizeToFitWidth:(BOOL)adjustsFontSizeToFitWidth secureTextEntry:(BOOL)secureTextEntry keyboardType:(UIKeyboardType)keyboardType returnKeyType:(UIReturnKeyType)returnKeyType userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UISearchBar
+(UISearchBar *)searchBarInitWithFrame:(CGRect)frame barStyle:(UIBarStyle)barStyle delegate:(id)delegate backgroundImage:(UIImage *)backgroundImage backgroundColor:(UIColor *)backgroundColor tintColor:(UIColor *)tintColor placeholder:(NSString *)placeholder hidden:(BOOL)hidden tag:(int)tag showsCancelButton:(BOOL)showsCancelButton keyboardType:(UIKeyboardType)keyboardType returnKeyType:(UIReturnKeyType)returnKeyType userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - 自定义 输入框底图1
+(UIImageView *)inputBoxInitWithFrame:(CGRect)frame lconImage:(UIImage *)lconImage backgroundImage:(UIImage *)backgroundImage backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - 自定义 输入框底图2
+(UIView *)customView1InitWithFrame:(CGRect)frame text:(NSString *)text textColor:(UIColor *)textColor textAlignment:(NSTextAlignment)textAlignment font:(UIFont *)font backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden lineHidden:(BOOL)lineHidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - 自定义 协议规则框
+(UIView *)protocolRuleBarInitWithFrame:(CGRect)frame title:(NSString *)title textColor:(UIColor *)textColor textAlignment:(UIControlContentHorizontalAlignment)textAlignment font:(CGFloat)font backgroundColor:(UIColor *)backgroundColor backgroundImage:(UIImage *)backgroundImage backgroundImageSelected:(UIImage *)backgroundImageSelected target:(id)target action:(SEL)action  hidden:(BOOL)hidden agreeBTTag:(int)agreeBTTag agreementBTTag:(int)agreementBTTag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UISwitch
+(UISwitch *)switchInitWithFrame:(CGRect)frame isOn:(BOOL)on hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UIView
+(UIView *)viewInitWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UITextView
+(UITextView *)textViewInitWithFrame:(CGRect)frame scrollEnabled:(BOOL)scrollEnabled backgroundColor:(UIColor *)backgroundColor textColor:(UIColor *)textColor text:(NSString *)text font:(UIFont *)font textAlignment:(NSTextAlignment)textAlignment autoresizingMask:(UIViewAutoresizing)autoresizingMask keyboardType:(UIKeyboardType)keyboardType returnKeyType:(UIReturnKeyType)returnKeyType hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UITableView
+(UITableView *)tableViewInitWithFrame:(CGRect)frame backgroundColor:(UIColor *)backgroundColor style:(NSInteger)style delegate:(id)delegate dataSource:(id)dataSource showsHorizontalScrollIndicator:(BOOL)showsHorizontalScrollIndicator showsVerticalScrollIndicator:(BOOL)showsVerticalScrollIndicator hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UICollectionView
+(UICollectionView *)collectionViewInitWithFrame:(CGRect)frame delegate:(id)delegate dataSource:(id)dataSource backgroundColor:(UIColor *)backgroundColor scrollEnabled:(BOOL)scrollEnabled alwaysBounceVertical:(BOOL)alwaysBounceVertical alwaysBounceHorizontal:(BOOL)alwaysBounceHorizontal showsHorizontalScrollIndicator:(BOOL)showsHorizontalScrollIndicator showsVerticalScrollIndicator:(BOOL)showsVerticalScrollIndicator collectionViewFlowLayout:(UICollectionViewFlowLayout *)collectionViewFlowLayout sectionInset:(UIEdgeInsets)sectionInset headerReference:(CGSize)headerReference footerReference:(CGSize)footerReference minimumInteritemSpacing:(CGFloat)minimumInteritemSpacing minimumLineSpacing:(CGFloat)minimumLineSpacing  scrollDirection:(NSInteger)scrollDirection hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UISlider
+(UISlider *)sliderInitWithFrame:(CGRect)frame minimumValue:(float)minimumValue maximumValue:(float)maximumValue value:(float)value thumbImage:(UIImage *)thumbImage minimumTrackImage:(UIImage *)minimumTrackImage maximumTrackImage:(UIImage *)maximumTrackImage minimumTrackImageResizableImageWithCapInsets:(UIEdgeInsets)minimumTrackImageResizableImageWithCapInsets resizingMode1:(NSInteger)resizingMode1 maximumTrackImageResizableImageWithCapInsets:(UIEdgeInsets)maximumTrackImageResizableImageWithCapInsets resizingMode2:(NSInteger)resizingMode2 target:(id)target action:(SEL)action hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - UIProgressView
+(UIProgressView *)progressViewInitWithFrame:(CGRect)frame progressViewStyle:(NSInteger)progressViewStyle progress:(float)progress progressTintColor:(UIColor *)progressTintColor trackTintColor:(UIColor *)trackTintColor trackImage:(UIImage *)trackImage progressImage:(UIImage *)progressImage animated:(BOOL)animated tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - 多颜色多字体的Label
+(UILabel *)labelAttributedStringInitWithFrame:(CGRect)frame text1:(NSString *)text1 text2:(NSString *)text2 text3:(NSString *)text3 numberOfLines:(int)numberOfLines textAlignment:(NSTextAlignment)textAlignment textColor1:(UIColor *)textColor1 textColor2:(UIColor *)textColor2 textColor3:(UIColor *)textColor3 font:(UIFont *)font backgroundColor:(UIColor *)backgroundColor tag:(int)tag hidden:(BOOL)hidden userInteractionEnabled:(BOOL)userInteractionEnabled;


#pragma mark - JPushView 应用程序在前台时收到的推送消息展示UI
+(UIView *)pushViewInitWithFrame:(CGRect)frame channel:(NSString *)channel pushText:(NSString *)pushText image:(UIImage *)image backgroundColor:(UIColor *)backgroundColor hidden:(BOOL)hidden tag:(int)tag userInteractionEnabled:(BOOL)userInteractionEnabled;




@end
