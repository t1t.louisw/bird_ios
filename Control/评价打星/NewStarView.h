//
//  NewStarView.h
//  XPH
//
//  Created by 肖雨 on 16/3/24.
//  Copyright © 2016年 YAY. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol newStarDelegate <NSObject>

@required
-(void)scoreIndex:(NSInteger)index number:(NSInteger)number;
@end

@interface NewStarView : UIView

@property(nonatomic,weak)id<newStarDelegate>delegate;

- (id)initWithFrame:(CGRect)frame EmptyImage:(NSString *)Empty StarImage:(NSString *)Star;

@property (nonatomic, strong) UIView *starBackgroundView;
@property (nonatomic, strong) UIView *starForegroundView;

@end
