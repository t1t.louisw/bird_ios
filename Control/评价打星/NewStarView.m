//
//  NewStarView.m
//  XPH
//
//  Created by 肖雨 on 16/3/24.
//  Copyright © 2016年 YAY. All rights reserved.
//

#import "NewStarView.h"

#define imageW  self.bounds.size.width/10

@implementation NewStarView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (id)initWithFrame:(CGRect)frame EmptyImage:(NSString *)Empty StarImage:(NSString *)Star{
    
    
    
    self = [super initWithFrame:frame];
    if (self) {
        
        
        
        self.starBackgroundView = [self buidlStarViewWithImageName:Empty];
        self.starForegroundView = [self buidlStarViewWithImageName:Star];
        [self addSubview:self.starBackgroundView];
        
        self.userInteractionEnabled = YES;
        
        /**点击手势*/
        UITapGestureRecognizer *tapGR=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGR:)];
        [self addGestureRecognizer:tapGR];
        
        /**滑动手势*/
        
        UIPanGestureRecognizer *panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(tapGR:)];
        [self addGestureRecognizer:panGR];
        
        
        
    }
    return self;
    
}


- (UIView *)buidlStarViewWithImageName:(NSString *)imageName
{
    CGRect frame = self.bounds;
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.clipsToBounds = YES;
    
    
    for (int j = 0; j < 5; j ++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        imageView.frame = CGRectMake(1.2*j*imageW, 0, imageW, imageW);
        [view addSubview:imageView];
    }
    
    return view;
}

-(void)tapGR:(UITapGestureRecognizer *)tapGR{
    CGPoint point =[tapGR locationInView:self];
    if (point.x<0) {
        point.x = 0;
    }
    
    int X = (int) point.x/(1.2*imageW);
    
    [self.delegate scoreIndex:self.tag number:X+1];
    
    self.starForegroundView.frame = CGRectMake(0, 0, (X+1)*1.2*imageW, imageW);
    [self addSubview:self.starForegroundView];
    
    
}



@end
