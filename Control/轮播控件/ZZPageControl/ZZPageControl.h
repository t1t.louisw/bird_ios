//
//  ZZPageControl.h
//  XPH
//
//  Created by Zick.Zhao on 15/3/24.
//  Copyright (c) 2015年 YAY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZZPageControl : UIPageControl{
    CGSize dotSize;
}

-(id) initWithFrame:(CGRect)frame dotSize:(CGSize)_dotSize;

@end
