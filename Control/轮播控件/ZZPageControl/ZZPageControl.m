//
//  ZZPageControl.m
//  XPH
//
//  Created by Zick.Zhao on 15/3/24.
//  Copyright (c) 2015年 YAY. All rights reserved.
//

#import "ZZPageControl.h"

@implementation ZZPageControl

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(id) initWithFrame:(CGRect)frame dotSize:(CGSize)_dotSize
{
    self = [super initWithFrame:frame];
    
    dotSize = _dotSize;
    
    return self;
    
}

- (void) setCurrentPage:(NSInteger)page {
    [super setCurrentPage:page];
    for (NSUInteger subviewIndex = 0; subviewIndex < [self.subviews count]; subviewIndex++) {
        UIImageView* subview = [self.subviews objectAtIndex:subviewIndex];
        [subview setFrame:CGRectMake(subview.frame.origin.x, subview.frame.origin.y,
                                     dotSize.width,dotSize.height)];
        [subview.layer setCornerRadius:dotSize.width/2];
        [subview.layer setMasksToBounds:YES];
        
    }
}

@end
