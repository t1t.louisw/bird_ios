//
//  NewGundongCell.m
//  NewProject
//
//  Created by mac on 2019/10/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewGundongCell.h"

@implementation NewGundongCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        UILabel *line = [UILabel new];
//        line.backgroundColor = NewRedColor;
//        ViewRadius(line, 2);
//        [self addSubview:line];
//
//        line.sd_layout
//        .heightIs(4)
//        .widthIs(4)
//        .leftEqualToView(self)
//        .centerYEqualToView(self);
        
        _name = [UILabel new];
        _name.textColor = [UIColor colorWithString:@"#7899FF"];
        _name.font = NewFont(12);
        [_name setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:_name];
        
        _name.sd_layout
        .centerYEqualToView(self)
        .heightIs(15)
        .leftSpaceToView(self, 1);
    }
    return self;
}
- (void)assignment:(NSString *)model {
    _name.text = model;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

