//
//  NewGundongCell.h
//  NewProject
//
//  Created by mac on 2019/10/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewGundongCell : UITableViewCell
@property (copy,nonatomic)UILabel *name;
- (void)assignment:(NSString *)model;

@end

NS_ASSUME_NONNULL_END
