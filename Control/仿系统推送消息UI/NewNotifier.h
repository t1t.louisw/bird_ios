//
//  NewNotifier.h
//  MingMen
//
//  Created by NewProject on 2017/3/26.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NewNotifierBar.h"

@interface NewNotifier : NSObject
+ (NewNotifier*)shareInstance;
/**
 *  显示一个Notifer，需要手动点击关闭
 *
 *  @param note Notifer 内容
 *
 *  @return NewNotifierBar
 */
+ (NewNotifierBar*)showNotiferRemain:(NSString*)note;
/**
 *  显示一个Notifer，需要手动点击关闭
 *
 *  @param note    Notifer 内容
 *  @param appName Notifer 自定义APP名称 默认读取CFBundleName
 *
 *  @return NewNotifierBar
 */
+ (NewNotifierBar*)showNotiferRemain:(NSString*)note
                               name:(NSString*)appName;
/**
 *  显示一个Notifer,2秒后自动关闭
 *
 *  @param note Notifer 内容
 *
 *  @return NewNotifierBar
 */
+ (NewNotifierBar*)showNotifer:(NSString*)note;
/**
 *  显示一个Notifer,delay秒后自动关闭
 *
 *  @param note  Notifer 内容
 *  @param delay 自动关闭时间
 *
 *  @return NewNotifierBar
 */
+ (NewNotifierBar*)showNotifer:(NSString *)note
                 dismissAfter:(NSTimeInterval)delay;
/**
 *  显示一个Notifer,内容，名称，图标
 *
 *  @param note    Notifer 内容
 *  @param appName APP名称
 *  @param appIcon APP图标
 *
 *  @return NewNotifierBar
 */
+ (NewNotifierBar*)showNotifer:(NSString*)note
                         name:(NSString*)appName
                         icon:(UIImage*)appIcon;
/**
 *  显示一个Notifer,内容，名称，图标，关闭时间
 *
 *  @param note    Notifer 内容
 *  @param appName APP名称
 *  @param appIcon APP图标
 *  @param delay   自动关闭时间
 *
 *  @return NewNotifierBar
 */
+ (NewNotifierBar*)showNotifer:(NSString*)note
                         name:(NSString*)appName
                         icon:(UIImage*)appIcon
                 dismissAfter:(NSTimeInterval)delay;
/**
 *  关闭Notifer
 */
+ (void)dismiss;
/**
 *  关闭Notifer
 */
- (void)dismiss;
/**
 *  延迟关闭Notifer
 *
 *  @param delay 延迟关闭时间
 */
+ (void)dismissAfter:(NSTimeInterval)delay;
/**
 *  点击Notifer的回调
 *
 *  @param notifierBarClickBlock 点击Notifer的回调
 */
+ (void)handleClickAction:(NewNotifierBarClickBlock)notifierBarClickBlock;

@end
