//
//  NewNotifierBar.h
//  MingMen
//
//  Created by NewProject on 2017/3/26.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewNotifier;
@class NewNotifierBar;
typedef void(^NewNotifierBarClickBlock)(NSString *name,NSString *detail,NewNotifier *notifier);
@interface NewNotifierBar : UIWindow
{
    NewNotifierBarClickBlock _notifierBarClickBlock;
}
- (void)show:(NSString*)note name:(NSString*)appName icon:(UIImage*)appIcon;

- (void)handleClickAction:(NewNotifierBarClickBlock)notifierBarClickBlock;

@end
