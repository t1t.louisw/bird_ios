//
//  NewNotifier.m
//  MingMen
//
//  Created by NewProject on 2017/3/26.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewNotifier.h"
#import <AudioToolbox/AudioToolbox.h>

@interface NewNotifier()
@property (nonatomic, strong) NewNotifierBar *notifierBar;
@property (nonatomic, strong) UIImage *defaultIcon;
@property (nonatomic, strong) NSString *appName;

@end

@implementation NewNotifier

+ (NewNotifier*)shareInstance {
    static dispatch_once_t onceToken;
    static NewNotifier *notifier;
    dispatch_once(&onceToken, ^ {
        notifier = [[self alloc] init];
    });
    return notifier;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifierOrientationChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
        
    }
    return self;
}
#pragma --mark getter
- (NSString*)appName{
    if (!_appName) {
        _appName =  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"]?:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
    }
    return _appName;
}
- (UIImage*)defaultIcon{
    if (!_defaultIcon) {
        
        _defaultIcon = [self loadPlistIcon] ?:[UIImage imageNamed:@"AppIcon"] ?:[UIImage imageNamed:@"AppIcon-1"] ?:[UIImage imageNamed:@"AppIcon-2"] ?:[UIImage imageNamed:@"AppIcon-3"] ?:[UIImage imageNamed:@"icon"];
    }
    return _defaultIcon;
}
- (NewNotifierBar*)notifierBar{
    if (!_notifierBar) {
        _notifierBar = [[NewNotifierBar alloc] init];
        CGRect frame = _notifierBar.frame;
        frame.origin.y = -frame.size.height;
        _notifierBar.frame = frame;
    }
    return _notifierBar;
}
+ (void)handleClickAction:(NewNotifierBarClickBlock)notifierBarClickBlock{
    [[self shareInstance].notifierBar handleClickAction:notifierBarClickBlock];
}

#pragma --mark class method
+ (NewNotifierBar*)showNotiferRemain:(NSString*)note{
    return [NewNotifier showNotiferRemain:note name:nil];
}

+ (NewNotifierBar*)showNotiferRemain:(NSString*)note
                               name:(NSString*)appName{
    return [NewNotifier showNotifer:note name:appName icon:nil dismissAfter:-1];
}

+ (NewNotifierBar*)showNotifer:(NSString*)note{
    return [NewNotifier showNotifer:note dismissAfter:4];
}

+ (NewNotifierBar*)showNotifer:(NSString*)note name:(NSString*)appName icon:(UIImage*)appIcon{
    return [NewNotifier showNotifer:note name:appName icon:appIcon dismissAfter:2];
}

+ (NewNotifierBar*)showNotifer:(NSString *)note
                 dismissAfter:(NSTimeInterval)delay{
    return [self showNotifer:note name:nil icon:nil dismissAfter:delay];
}
+ (NewNotifierBar*)showNotifer:(NSString*)note
                         name:(NSString*)appName
                         icon:(UIImage*)appIcon
                 dismissAfter:(NSTimeInterval)delay{
    NewNotifierBar *bar =  [[self shareInstance] showNotifer:note
                                                       name:appName?:[self shareInstance].appName
                                                       icon:appIcon?:[self shareInstance].defaultIcon];
    [self dismissAfter:delay];
    return bar;
}
+ (void)dismiss{
    [[self shareInstance] dismiss];
}
+ (void)dismissAfter:(NSTimeInterval)delay;
{
    if(delay<0)
    {
        [NSObject cancelPreviousPerformRequestsWithTarget:[self shareInstance]  selector:@selector(dismiss) object:nil];
    }else
    {
        [[self shareInstance] performSelector:@selector(dismiss) withObject:nil afterDelay:delay];
    }
    
}
#pragma --instance method
- (NewNotifierBar*)showNotifer:(NSString*)note name:(NSString*)appName icon:(UIImage*)appIcon{
    
    [self.notifierBar.layer removeAllAnimations];
    self.notifierBar.userInteractionEnabled = YES;
    [self.notifierBar removeFromSuperview];
    self.notifierBar = nil;
    
    //播放系统的通知声音类型 如果使用自定义的 则请关闭这个 不然自定义的声音会失效
    AudioServicesPlaySystemSound(1007);
    
    [self.notifierBar show:note name:appName icon:appIcon];
    [UIView animateWithDuration:(0.4) animations:^{
        self.notifierBar.alpha = 0.8;
        CGRect frame = _notifierBar.frame;
        frame.origin.y = 0.;
        _notifierBar.frame = frame;
    }];
    return self.notifierBar;
}

- (void)dismiss
{
    [self dismissWithAnimation:YES];
}
- (void)dismissWithAnimation:(BOOL)animated{
    [[NSRunLoop currentRunLoop] cancelPerformSelector:@selector(dismiss) target:self argument:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismiss) object:nil];
    
    if(animated){
        [UIView animateWithDuration:0.4 animations:^{
            CGRect frame = _notifierBar.frame;
            frame.origin.y = -frame.size.height;
            _notifierBar.frame = frame;
        } completion:^(BOOL finished) {
            self.notifierBar.userInteractionEnabled = NO;
            _notifierBar.hidden = YES;
        }];
    }else{
        _notifierBar.hidden = YES;
    }
}
- (void)notifierOrientationChange:(NSNotification *)notification
{
    [self dismissWithAnimation:NO];
}
#pragma --mark helper

- (UIImage*)loadPlistIcon{
    NSString *iconString = @"Icon.png";
    NSArray *icons = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIconFiles"];
    if (!icons) {
        iconString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIconFile"];
    }
    else
    {
        iconString = icons [0];
    }
    return [UIImage imageNamed:iconString];
}


@end
