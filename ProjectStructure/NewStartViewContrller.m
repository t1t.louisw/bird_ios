//
//  NewStartViewContrller.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewStartViewContrller.h"
#import "StyledPageControl.h"

#define GuidePageSize  3

@interface NewStartViewContrller ()<UIScrollViewDelegate>{
    UIScrollView *myScrollView;
    StyledPageControl *myPageControl;
    
}

@end

@implementation NewStartViewContrller

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:NewLineGrayColor];
    
    [self createSM_GuideViewWtihPageSize:GuidePageSize];
    
}

- (void) createSM_GuideViewWtihPageSize : (NSInteger) pageSize
{
    myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [myScrollView setDelegate:self];
    [myScrollView setPagingEnabled:YES];
    [myScrollView setShowsHorizontalScrollIndicator:NO];
    [myScrollView setShowsVerticalScrollIndicator:NO];
    myScrollView.contentSize = CGSizeMake(SCREEN_WIDTH * pageSize, SCREEN_HEIGHT);
    [myScrollView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:myScrollView];
    
    for (int i = 0; i < pageSize; i ++)
    {
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showIndex:)];
        UIImageView *img_view = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH * i,0,SCREEN_WIDTH,SCREEN_HEIGHT)];
        [img_view setContentMode:UIViewContentModeScaleAspectFit];
        img_view.userInteractionEnabled = YES;
        if (i == pageSize-1) {
            [img_view addGestureRecognizer:tapGes];
        }
        [img_view setBackgroundColor:NewClearColor];
        
        NSString *imgName = @"";
        if ([[Comment iphoneType] isEqualToString:@"IPhone_X"]) {
            imgName = NewStringFormat(@"Guide1_%d",i+4);
        }else{
            imgName = NewStringFormat(@"Guide1_%d",i+1);
        }
//        if (SCREEN_WIDTH == 320 && SCREEN_HEIGHT == 568) {
//            imgName = NewStringFormat(@"Guide%d_5",i+1);
//        }else if (SCREEN_WIDTH == 375 && SCREEN_HEIGHT == 667) {
//            imgName = NewStringFormat(@"Guide%d_6",i+1);
//        }else if (SCREEN_WIDTH == 414 && SCREEN_HEIGHT == 736) {
//            imgName = NewStringFormat(@"Guide%d_6+",i+1);
//        }else{
//            imgName = NewStringFormat(@"Guide%d_5",i+1);
//            [img_view setContentMode:UIViewContentModeScaleAspectFill];
//        }
        [img_view setImage:[UIImage imageNamed:imgName]];
        [myScrollView addSubview:img_view];
    }
}

- (void) createSM_GuidePageCtrWihtPageNum : (int) _pageNum currentPage : (int) _currentPage
{
    myPageControl = [[StyledPageControl alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 120.f)/2, SCREEN_HEIGHT-30.0f, 120.0f, 30.0f)];
    [myPageControl setBackgroundColor:[UIColor clearColor]];
    myPageControl.numberOfPages = _pageNum;
    myPageControl.currentPage = _currentPage;
    [myPageControl setUserInteractionEnabled:NO];
    [self.view addSubview:myPageControl];
}

- (void)showIndex:(UITapGestureRecognizer*)sender
{
//    NewLoginViewController *vc = [[NewLoginViewController alloc] init];
//    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:vc];
//    [AppDelegate shareDelegate].window.rootViewController = navi;
    [[NewInterfaceReplacement sharedInstance] replacementController];//直接到首页

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
