//
//  NewConfigureRootController.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewConfigureRootController.h"
#import "NewStartViewContrller.h"       //引导类
#import "NewInterfaceReplacement.h"
#import "NewLoginViewController.h"

#import "JPushHelper.h"

@implementation NewConfigureRootController

static NewConfigureRootController *sharedObj = nil;

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedObj = [[super allocWithZone: NULL] init];
    });

    return sharedObj;
}

+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [self sharedInstance];
}

- (id) copyWithZone:(NSZone *) zone
{
    return self;
}

#pragma mark - 程序启动根目录ViewController
- (void)setRootController {
    
//    [AppDelegate shareDelegate].window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    [AppDelegate shareDelegate].window.backgroundColor = [UIColor whiteColor];
//    [[NewInterfaceReplacement sharedInstance] replacementController];//直接到首页

//    if ([[NewUtils userDefaultsStringKey:NewToken] length]>0) {
//        [UserHelper writeUserInfoFromLocation];//回写用户信息
//        [[JPushHelper sharedInstance] addAlias];//绑定登录的账号作为推送对象
//        [[NewInterfaceReplacement sharedInstance] replacementController];//直接到首页
//    }else {
//        NewLoginViewController *vc = [[NewLoginViewController alloc] init];//去登陆
//        UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:vc];
//        [AppDelegate shareDelegate].window.rootViewController = navi;
//    }
    
    //获取系统的版本号
    NSString *currVersion = [NSString getMyApplicationVersion];
    //程序第一次启动时 会把版本号存到内存里 下次进来的时候进行版本号匹配
    //匹配成功则直接进入项目首页 否则默认进入项目介绍轮播页
    NSString *oldVersion = [NewUtils userDefaultsStringKey:NewAPPVersion];
    
    if (oldVersion.length > 0 && [oldVersion isEqualToString:currVersion]) {
        [UserHelper writeUserInfoFromLocation];
        
        if ([NewUtils userDefaultsStringKey:NewplayerId].length>0 && [NewUtils userDefaultsStringKey:NewToken].length>0 && [NewUtils userDefaultsStringKey:NewAccountNumber].length>0) {

            [[NewInterfaceReplacement sharedInstance] replacementController];//直接到首页

        }else{
            NewLoginViewController *vc = [[NewLoginViewController alloc] init];//去登陆
            UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:vc];
            [AppDelegate shareDelegate].window.rootViewController = navi;
        }
    }else{
//        NewStartViewContrller *startVc = [[NewStartViewContrller alloc] init];
//        [AppDelegate shareDelegate].window.rootViewController = startVc;
        
        NewLoginViewController *vc = [[NewLoginViewController alloc] init];//去登陆
        UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:vc];
        [AppDelegate shareDelegate].window.rootViewController = navi;
    }
     
    [NewUtils userDefaultsStringKey:NewAPPVersion Value:currVersion];
     
    //导航条颜色定制
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    [[AppDelegate shareDelegate].window makeKeyAndVisible];
}




@end
