//
//  ViewController.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBasicTabbarController : UITabBarController

+ (NewBasicTabbarController *) sharedInstance;//创建

+(void)objectDealloc;//销毁

- (void)changeRootViewControllerEvent:(NSInteger)changeIndex;

@end
