//
//  NewBasicTabbarController.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewBasicTabbarController.h"
#import "HomeRootVC.h"
#import "NewtThehallViewController.h"
#import "NewCustomerserviceViewController.h"
#import "NewMyViewController.h"
#import "NewactivityViewController.h"
@interface NewBasicTabbarController ()<UITabBarControllerDelegate>
@end

static NewBasicTabbarController *sharedObj = nil;
static dispatch_once_t onceToken;

@implementation NewBasicTabbarController

+ (NewBasicTabbarController *) sharedInstance
{
    //static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedObj = [[super allocWithZone: NULL] init];
    });
    return sharedObj;
}

+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [self sharedInstance];
}

- (id) copyWithZone:(NSZone *) zone
{
    return self;
}

+(void)objectDealloc{
    
    // 只有置成0,GCD才会认为它从未执行过.它默认为0.这样才能保证下次再次调用shareInstance的时候,再次创建对象.
    // 应用场景：切换账号、从新登陆
    onceToken = 0;
    sharedObj = nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *backView = [[UIView alloc] init];
    if (NavHeader == 88) {
        backView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 83);
    }else{
        backView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 49);
    }
    backView.backgroundColor = [UIColor colorWithString:@"#190A2E"];
    [self.tabBar insertSubview:backView atIndex:0];
    self.tabBar.opaque = YES;
    
    [self loadsView];
}


// 初始化所有子控制器
- (void)loadsView{
    [self setTabBarChildController:[[HomeRootVC alloc] init] title:@"首页" image:@"首页1" selectImage:@"首页"];
//    [self setTabBarChildController:[[NewactivityViewController alloc] init] title:@"活动" image:@"活动1" selectImage:@"活动"];
//    [self setTabBarChildController:[[NewtThehallViewController alloc] init] title:@"大厅" image:@"大厅1" selectImage:@"大厅"];
//    [self setTabBarChildController:[[NewCustomerserviceViewController alloc] init] title:@"客服" image:@"客服1" selectImage:@"客服"];
//    [self setTabBarChildController:[[NewMyViewController alloc] init] title:@"我的" image:@"我的1" selectImage:@"我的"];
}


// 添加tabbar的子viewcontroller
- (void)setTabBarChildController:(UIViewController*)controller title:(NSString*)title image:(NSString*)imageStr selectImage:(NSString*)selectImageStr{
    
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:controller];
    nav.tabBarItem.title = title;
    nav.tabBarItem.image = [[UIImage imageNamed:imageStr]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    nav.tabBarItem.selectedImage = [[UIImage imageNamed:selectImageStr]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    if (@available(iOS 13.0, *)) {

        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor colorWithString:@"#7A472C"]];

    }
    [nav.tabBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor colorWithString:@"#7A472C"]} forState:UIControlStateNormal];
    [nav.tabBarItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor colorWithString:@"#FF6D44"]} forState:UIControlStateSelected];
    
    [self addChildViewController:nav];
    
}
- (void)changeRootViewControllerEvent:(NSInteger)changeIndex{
    
    [self setSelectedIndex:changeIndex];
    //__weak typeof(self) wself = self;
    //[wself setSelectedIndex:changeIndex];
    
}


- (void)dealloc
{
    NSLog(@"Tabbar单例类销毁");
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
