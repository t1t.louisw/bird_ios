//
//  NewBasicViewController.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBasicViewController : UIViewController<UINavigationControllerDelegate>

@property (nonatomic,strong) UIView *navgationBar;
@property (nonatomic,strong) UIButton *backButton;          //返回按钮
@property (nonatomic,strong) UIView *statusView;          //状态栏


/**
 *  赋值导航条Title
 *  获取导航条Title
 */
- (void)setNavTitle:(NSString *)navTitle;
- (void)setNavTitles:(NSString *)navTitle font:(UIFont *)font;
- (NSString*)navTitle;


/**
 *  返回按钮是否隐藏
 *  Tabbar是否隐藏
 */
- (void)setBackButtonHide:(BOOL)hide;
- (void)setTabbarHide:(BOOL)isHide;


/**
 *  pushViewController , popViewController
 *  presentViewController , dismissViewController
 */
- (void)pushViewController:(id)pushVc;
- (void)popViewController;
- (void)presentViewController:(id)presentVc;
- (void)dismissViewController;


/**
 *  网络请求加载失败重载
 */
- (void) addErrorLoadingFrame:(CGRect)frame title:(NSString *)text buttonTitle:(NSString *)buttonText imageString:(NSString *)image;
- (void)hideReloadingview;
- (void)reloadingData;

@end
