//
//  NewBasicViewController.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewBasicViewController.h"
#import "UINavigationController+FDFullscreenPopGesture.h"//全局侧滑

@interface NewBasicViewController ()
{
    UILabel  *navTitleLabel; //导航标题
    UIView   *showView;      //展示视图 引用场景:网络刷新、界面重载、提示用户
}
@end

@implementation NewBasicViewController

@synthesize backButton;

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setHidesBottomBarWhenPushed:NO];
    }
    return self;
}
*/

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view setBackgroundColor:NewWhiteColor];
//    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor clearColor]}forState:UIControlStateNormal];//将title 文字的颜色改为透明
    
    [self createNavgationBar];

}

#pragma mark - 创建导航条
- (void)createNavgationBar{

     self.navgationBar = [[UIView alloc] init];
       [self.navgationBar setFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, NavHeader)];
       [self.navgationBar setBackgroundColor:NewButtonColor];
       [self.view addSubview:self.navgationBar];

       self.statusView = [[UIView alloc] init];
       [self.statusView setBackgroundColor:NewButtonColor];
       [self.navgationBar addSubview:self.statusView];
       
       self.statusView.sd_layout
       .leftEqualToView(self.navgationBar)
       .heightIs(STATUSBAR_HEIGHT)
       .rightEqualToView(self.navgationBar)
       .topEqualToView(self.navgationBar);
       
       backButton = [UIButton buttonWithType:UIButtonTypeCustom];
       [backButton setBackgroundColor:[UIColor clearColor]];
       [backButton setImage:NewImageNamed(@"返回-1") forState:UIControlStateNormal];
       [backButton addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
       [self.navgationBar addSubview:backButton];
       
       backButton.sd_layout
       .leftSpaceToView(self.navgationBar, 10)
       .heightIs(30)
       .widthIs(30)
       .bottomSpaceToView(self.navgationBar, 5);
       
       navTitleLabel = [[UILabel alloc] init];
       [navTitleLabel setBackgroundColor:NewClearColor];
       [navTitleLabel setTextAlignment:NSTextAlignmentCenter];
       [navTitleLabel setFont:NewBFont(18)];
       [navTitleLabel setTextColor:[UIColor whiteColor]];
       [navTitleLabel setSingleLineAutoResizeWithMaxWidth:0];
       [self.navgationBar addSubview:navTitleLabel];
       
       navTitleLabel.sd_layout
       .centerXEqualToView(self.navgationBar)
       .heightIs(20)
       .bottomSpaceToView(self.navgationBar, 10);
}
#pragma mark - 导航条Title赋值
- (void)setNavTitle:(NSString *)navTitle{
    if (navTitle) {
        [navTitleLabel setText:navTitle];
        [navTitleLabel setFont:NewBFont(18)];
    }else{
        [navTitleLabel setHidden:YES];
    }
}

#pragma mark - 导航条Title赋值 可自定义Title字体大小
- (void)setNavTitles:(NSString *)navTitle font:(UIFont *)font{
    if (navTitle) {
        [navTitleLabel setText:navTitle];
        [navTitleLabel setFont:font];
    }else{
        [navTitleLabel setHidden:YES];
    }
}

#pragma mark - 导航条Title取值
- (NSString*)navTitle{
    return navTitleLabel.text;
}

 

#pragma mark - backButton/Tabbar 是否隐藏
- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    self.fd_prefersNavigationBarHidden = YES;//全局侧滑的时候父类即将显示的时候加入这句话  不会出现导航白条

    //[self setHidesBottomBarWhenPushed:NO];
    
    //UIView不渗透到导航条下面
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    //NSLog(@"TabBar层级：%ld",self.navigationController.viewControllers.count);
    if (self.navigationController.viewControllers.count <= 1) {
        [self setBackButtonHide:YES];
        [self setTabbarHide:NO];
    }else{
        [self setTabbarHide:YES];
    }

}
- (void)setTabbarHide:(BOOL)isHide{

    [[NewBasicTabbarController sharedInstance].tabBar setHidden:isHide];
}



#pragma mark - 返回按钮是否隐藏
- (void)setBackButtonHide:(BOOL)hide{
    [backButton setHidden:hide];
}

#pragma mark - 跳转与返回
- (void)pushViewController:(id)pushVc
{
    [self.navigationController pushViewController:pushVc animated:YES];
}
- (void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)presentViewController:(id)presentVc
{
    [self presentViewController:presentVc animated:YES completion:nil];
}
- (void)dismissViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark 初始化展示框
- (void) addErrorLoadingFrame:(CGRect)frame title:(NSString *)text buttonTitle:(NSString *)buttonText imageString:(NSString *)image
{
    if(!showView)
    {
        [self.view addSubview:showView = [[UIView alloc] initWithFrame:frame]];
        [showView setHidden:NO];
        [showView setBackgroundColor:NewBgGrayColor];
        
        UIImage *NOimg = [UIImage imageNamed:image];
        UIImageView *tempIcon = [[UIImageView alloc]initWithImage:NOimg];
        [showView addSubview:tempIcon];
        tempIcon.sd_layout
        .leftSpaceToView(showView, (showView.width/2)-(NOimg.size.width/4.0f))
        .topSpaceToView(showView, (showView.height/2)-100.0f-(NOimg.size.height/4.0f))
        .widthIs(NOimg.size.width/2.0f)
        .heightIs(NOimg.size.height/2.0f);
        
        UILabel *tipLab = [UILabel new];
        [showView addSubview:tipLab];
        [tipLab setText:text];
        [tipLab setTextAlignment:NSTextAlignmentCenter];
        [tipLab setTextColor:NewLightGrayColor];
        [tipLab setBackgroundColor:NewClearColor];
        [tipLab setFont:NewAutoFont(16.0)];
        tipLab.sd_layout
        .leftEqualToView(showView)
        .rightEqualToView(showView)
        .topSpaceToView(tempIcon,-145.0)
        .heightIs(20.0);
        
        if (buttonText.length>0) {
            UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
            [showView addSubview:button];
            [button setTitle:buttonText forState:UIControlStateNormal];
            [button setTitleColor:NewNavigationColor forState:UIControlStateNormal];
            ViewBorderRadius(button, 16, 2, NewNavigationColor);
            [button addTarget:self action:@selector(reloadingData) forControlEvents:UIControlEventTouchUpInside];
            CGFloat width = [NewUtils heightforString:[NSString stringWithFormat:@"%@",buttonText] andHeight:34 fontSize:16];
            
            button.sd_layout
            .leftSpaceToView(showView, (showView.width/2)-((width+35)/2))
            .topSpaceToView(tipLab, 30.0)
            .widthIs(width+35)
            .heightIs(34);
        }
    }
    
    [UIView beginAnimations:@"ShowArrow" context:nil];
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    [UIView setAnimationDuration:0.25f];
    
    [UIView setAnimationDelegate:self];
    [showView setAlpha:1.0f];
    [UIView commitAnimations];
    
}

- (void)hideReloadingview
{
    [UIView animateWithDuration:0.25 animations:^{
        
        showView.hidden = YES;
        
    } completion:^(BOOL finished) {
        for (id obj in showView.subviews) {
            
            [obj removeFromSuperview];
        }
        showView = nil;
        [showView removeFromSuperview];
    }];
}

- (void)reloadingData{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
