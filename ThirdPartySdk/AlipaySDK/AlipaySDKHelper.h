//
//  AlipaySDKHelper.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlipaySDKHelper : NSObject

/*!
 * @brief 支付宝相关API封装
 * @author NewProject
 */

+ (instancetype)sharedInstance;

// NOTE: 支付回调
//typedef void (^NewAlipayCallback) (NSDictionary *dictionary);
@property(nonatomic,copy)void (^NewAlipayCallback)(NSDictionary *dictionary);

// NOTE: 授权回调
//typedef void (^NewAliauthCallback) (NSString *string);
@property(nonatomic,copy)void (^NewAliauthCallback)(NSDictionary *dictionary);

// NOTE: iOS9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options;

// NOTE: iOS9.0之前使用旧API接口
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

// NOTE: 调用订单API alipayCallback:(NewAlipayCallback)payCallback
- (void)alipayPay:(NSDictionary *)payDictionary order:(NSDictionary *)orderDictionary;

// NOTE: 调用授权API aliauthCallback:(NewAliauthCallback)authCallback
- (void)alipayAuth:(NSDictionary *)authDictionary;

@end
