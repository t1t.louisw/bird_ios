//
//  JPushHelper.m
//  SiBenServer
//
//  Created by 肖雨 on 2017/8/23.
//  Copyright © 2017年 ShangLv. All rights reserved.
//

#import "JPushHelper.h"

@implementation JPushHelper

static JPushHelper *sharedObj = nil;

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedObj = [[super allocWithZone: NULL] init];
    });
    return sharedObj;
    
    //    static NIMSDKHelper *instance = nil;
    //    static dispatch_once_t onceToken;
    //    dispatch_once(&onceToken, ^{
    //        instance = [[NIMSDKHelper alloc] init];
    //    });
    //    return instance;
}

+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [self sharedInstance];
}

- (id) copyWithZone:(NSZone *) zone
{
    return self;
}

- (void)setJPushAPNSSDK:(UIApplication *)application options:(NSDictionary *)launchOptions
{
    //打印日志
    [JPUSHService setDebugMode];
    
    // APNs注册，获取deviceToken并上报
    [self registerAPNS:application];
    
    // 初始化SDK
    [self initJPush:launchOptions];
    
    //监听注册推送通道是否成功
    [self listenerOnChannelOpened];
}
/**
 *	向APNs注册，获取deviceToken用于推送
 */
- (void)registerAPNS:(UIApplication *)application
{
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    //if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {}
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
}


#pragma mark - SDK 初始化
- (void)initJPush:(NSDictionary *)launchOptions
{
    // SDK初始化
    [JPUSHService setupWithOption:launchOptions appKey:JPushKey channel:JPushChannel apsForProduction:JPushIsProduction];
    
    /*
    //这个是根据手机唯一标识符来推送  我们使用的是别名
    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
        if(resCode == 0){
            NSLog(@"registrationID获取成功：%@",registrationID);
            
        }
        else{
            NSLog(@"registrationID获取失败，code：%d",resCode);
        }
    }];
     */
}


#pragma mark - APNs注册成功回调，将返回的deviceToken上传到JPush服务器
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //NSLog(@"Upload deviceToken to JPush server.");
    [JPUSHService registerDeviceToken:deviceToken];
}


#pragma mark - APNs注册失败回调
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"APNs注册失败: %@", error);
}


#pragma mark - 基于iOS 6 及以下的系统版本，如果 App状态为正在前台或者点击通知栏的通知消息，那么此函数将被调用，并且可通过AppDelegate的applicationState是否为UIApplicationStateActive判断程序是否在前台运行。此种情况在此函数中处理
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo{
    
    // 通知打开回执上报
    [JPUSHService handleRemoteNotification:userInfo];
    
    //app未读消息角标值-1 然后保存到极光服务器 进入此方法 则不会再程序从启的api再对角标值做操作
    application.applicationIconBadgeNumber = application.applicationIconBadgeNumber>0?application.applicationIconBadgeNumber-1:0;
    
    //应用正处理前台状态下，并且接收事件，但不会收到推送消息，因此在此处需要额外处理一下  [@"aps"][@"alert"]
    if (application.applicationState == UIApplicationStateActive) {
        
        [NewNotifier showNotifer:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
        [NewNotifier handleClickAction:^(NSString *name,NSString *detail, NewNotifier *notifier) {
            [notifier dismiss];
            //。。对收到的消息做处理
            if (userInfo) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:NewSibenServerPublicNotification object:userInfo];
                });
            }
            
        }];
        return;
    }
    
    //应用正处理前台状态下，但不接收事件，通常情况下是点击推送消息唤醒了程序
    if (application.applicationState == UIApplicationStateInactive) {
        
        if (userInfo) {
            //通过点击推送消息唤醒程序 并且该消息类型需要跳转指定界面
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter]postNotificationName:NewSibenServerPublicNotification object:userInfo];
            });
        }
    }
}



#pragma mark - iOS7~iOS9 旧的API处理收到的推送消息通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    //NSLog(@"---收到的推送消息---  %@",pushUserInfo);
    
    /*
     if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {}
     
     1.UNPushNotificationTrigger （远程通知） 远程推送的通知类型
     2.UNTimeIntervalNotificationTrigger （本地通知） 一定时间之后，重复或者不重复推送通知。我们可以设置timeInterval（时间间隔）和repeats（是否重复）。
     3.UNCalendarNotificationTrigger（本地通知） 一定日期之后，重复或者不重复推送通知 例如，你每天8点推送一个通知，只要dateComponents为8，如果你想每天8点都推送这个通知，只要repeats为YES就可以了。
     4.UNLocationNotificationTrigger （本地通知）地理位置的一种通知，
     当用户进入或离开一个地理区域来通知。在CLRegion标识符必须是唯一的。因为如果相同的标识符来标识不同区域的UNNotificationRequests，会导致不确定的行为。
     */
    
    /*
     * UIApplicationStateActive:当前应用正在前台运行，并且接收事件。这是应用正在前台运行时所处的正常状态。
     
     * UIApplicationStateInactive:当前应用正在前台运行，但是并不接收事件（当前或许正在执行其它代码）。一般每当应用要从一个状态切换到另一个不同的状态时，中途过渡会短暂停留在此状态。唯一在此状态停留时间比较长的情况是：当用户锁屏时，或者系统提示用户去响应某些（诸如电话来电、有未读短信等）事件的时候。
     
     * UIApplicationStateBackground:应用处在后台，并且还在执行代码。大多数将要进入Suspended状态的应用，会先短暂进入此状态。然而，对于请求需要额外的执行时间的应用，会在此状态保持更长一段时间。另外，如果一个应用要求启动时直接进入后台运行，这样的应用会直接从Not running状态进入Background状态，中途不会经过Inactive状态。比如没有界面的应用。注此处并不特指没有界面的应用，其实也可以是有界面的应用，只是如果要直接进入background状态的话，该应用界面不会被显示。
     
     UIApplicationState applicationState = [UIApplication sharedApplication].applicationState;
     switch (applicationState) {
     case 0:
     NSLog(@"当前应用正在前台运行，并且接收事件 UIApplicationStateActive");
     break;
     case 1:
     NSLog(@"当前应用正在前台运行，但是并不接收事件 UIApplicationStateInactive");
     break;
     case 2:
     NSLog(@"应用处在后台，并且还在执行代码 UIApplicationStateBackground");
     break;
     default:
     break;
     }
     */
    
    // 通知打开回执上报
    [JPUSHService handleRemoteNotification:userInfo];
    
    //app未读消息角标值-1 然后保存到极光服务器 进入此方法 则不会再程序从启的api再对角标值做操作
    application.applicationIconBadgeNumber = application.applicationIconBadgeNumber>0?application.applicationIconBadgeNumber-1:0;
    
    //应用正处理前台状态下，并且接收事件，但不会收到推送消息，因此在此处需要额外处理一下  [@"aps"][@"alert"]
    if (application.applicationState == UIApplicationStateActive) {
        
        [NewNotifier showNotifer:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
        [NewNotifier handleClickAction:^(NSString *name,NSString *detail, NewNotifier *notifier) {
            [notifier dismiss];
            //。。对收到的消息做处理
            if (userInfo) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:NewSibenServerPublicNotification object:userInfo];
                });
            }
            
        }];
        return;
    }
    
    //应用正处理前台状态下，但不接收事件，通常情况下是点击推送消息唤醒了程序
    if (application.applicationState == UIApplicationStateInactive) {
        
        if (userInfo) {
            //通过点击推送消息唤醒程序 并且该消息类型需要跳转指定界面
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter]postNotificationName:NewSibenServerPublicNotification object:userInfo];
            });
        }
    }
    
    /*
     UIBackgroundFetchResultNewData 成功拉取数据
     UIBackgroundFetchResultNoData 没有新数据
     UIBackgroundFetchResultFailed 拉取数据失败或者超时
     */
    completionHandler(UIBackgroundFetchResultNewData);
    
}



/*
#pragma mark - iOS10 前台 API处理收到的通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    //NSLog(@"Receive a notification in foregound.");

    //收到推送的请求
    UNNotificationRequest *request = notification.request;
    //收到推送的消息内容
    UNNotificationContent *content = request.content;
    NSDictionary *userInfo = content.userInfo;
    
    //通知打开回执上报
    [JPUSHService handleRemoteNotification:userInfo];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber>0?[UIApplication sharedApplication].applicationIconBadgeNumber-1:0;
    
    [NewNotifier showNotifer:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
    [NewNotifier handleClickAction:^(NSString *name,NSString *detail, NewNotifier *notifier) {
        [notifier dismiss];
        //对收到的消息做处理
        if (userInfo) {
            //[[NSNotificationCenter defaultCenter]postNotificationName:MMNotificationMessage object:userInfo userInfo:@{mmNotificationMessageType:mmNotificationMessageUserInfo}];
        }
    }];
    
    
    completionHandler(UNNotificationPresentationOptionNone);//通知不弹出
    
    //completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); //需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
}



#pragma mark - iOS10 未启动/后台 API处理收到的通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSString *userAction = response.actionIdentifier;
 
    if ([userAction isEqualToString:UNNotificationDefaultActionIdentifier]) {
        //收到推送的请求
        UNNotificationRequest *request = response.notification.request;
        //收到推送的消息内容
        UNNotificationContent *content = request.content;
        NSDictionary *userInfo = content.userInfo;
        
        //通知打开回执上报
        [JPUSHService handleRemoteNotification:userInfo];
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber>0?[UIApplication sharedApplication].applicationIconBadgeNumber-1:0;
        
        //对收到的消息做处理
        if (userInfo) {
            //[[NSNotificationCenter defaultCenter]postNotificationName:MMNotificationMessage object:userInfo userInfo:@{mmNotificationMessageType:mmNotificationMessageUserInfo}];
        }
    }
    
    completionHandler();
}
*/


#pragma mark - 前台 极光SDK对iOS10收到的推送消息做的处理
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    
    //收到推送的请求
    UNNotificationRequest *request = notification.request;
    //收到推送的消息内容
    UNNotificationContent *content = request.content;
    NSDictionary *userInfo = content.userInfo;
    
    //通知打开回执上报
    [JPUSHService handleRemoteNotification:userInfo];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber>0?[UIApplication sharedApplication].applicationIconBadgeNumber-1:0;
    
    [NewNotifier showNotifer:[NSString stringWithFormat:@"%@",userInfo[@"aps"][@"alert"]]];
    [NewNotifier handleClickAction:^(NSString *name,NSString *detail, NewNotifier *notifier) {
        [notifier dismiss];
        //对收到的消息做处理
        if (userInfo) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter]postNotificationName:NewSibenServerPublicNotification object:userInfo];
            });
        }
    }];
    
    
    completionHandler(UNNotificationPresentationOptionNone);//通知不弹出
    
    //completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置
}



#pragma mark - 未启动/后台 极光SDK对iOS10收到的推送消息做的处理
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    NSString *userAction = response.actionIdentifier;
    
    if ([userAction isEqualToString:UNNotificationDefaultActionIdentifier]) {
        //收到推送的请求
        UNNotificationRequest *request = response.notification.request;
        //收到推送的消息内容
        UNNotificationContent *content = request.content;
        NSDictionary *userInfo = content.userInfo;
        
        //通知打开回执上报
        [JPUSHService handleRemoteNotification:userInfo];
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber>0?[UIApplication sharedApplication].applicationIconBadgeNumber-1:0;
        
        //对收到的消息做处理
        if (userInfo) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter]postNotificationName:NewSibenServerPublicNotification object:userInfo];
            });
        }
    }

    completionHandler();  // 系统要求执行这个方法
}



#pragma mark - 本地推送通知API 处理收到的推送消息通知
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    application.applicationIconBadgeNumber = application.applicationIconBadgeNumber>0?application.applicationIconBadgeNumber-1:0;
    //应用正处理前台状态下，并且接收事件，但不会收到推送消息，因此在此处需要额外处理一下  [@"aps"][@"alert"]
    if (application.applicationState == UIApplicationStateActive) {
        
        [NewNotifier showNotifer:notification.alertBody];
        [NewNotifier handleClickAction:^(NSString *name,NSString *detail, NewNotifier *notifier) {
            [notifier dismiss];
            //。。对收到的消息做处理
            if (notification.userInfo) {
                //[[NSNotificationCenter defaultCenter]postNotificationName:MMNotificationMessage object:notification.userInfo userInfo:@{mmNotificationMessageType:mmNotificationMessageUserInfo}];
            }
            
        }];
        return;
    }
    
    //应用正处理前台状态下，但不接收事件，通常情况下是点击推送消息唤醒了程序
    if (application.applicationState == UIApplicationStateInactive) {
        
        if (notification.userInfo) {
            //通过点击推送消息唤醒程序 并且该消息类型需要跳转指定界面
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                //[[NSNotificationCenter defaultCenter]postNotificationName:MMNotificationMessage object:notification.userInfo userInfo:@{mmNotificationMessageType:mmNotificationMessageUserInfo}];
            });
        }
    }
}



#pragma mark - 监听注册推送通道是否成功
/**
 *	注册推送通道打开监听
 */
- (void)listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onChannelOpened:)
                                                 name:@"CCPDidChannelConnectedSuccess"
                                               object:nil];
}

/**
 *	推送通道打开回调
 */
- (void)onChannelOpened:(NSNotification *)notification {
    NSLog(@"推送消息通道建立成功");
}


#pragma mark - 添加推送对象
- (void)addAlias
{
    [JPUSHService setAlias:[UserEntity sharedInstance].token.length>0&&[UserEntity sharedInstance].token.length>16?[[UserEntity sharedInstance].token substringToIndex:16]:nil completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"添加推送对象 iResCode:%ld  iAlias:%@  seq:%ld", iResCode, iAlias, seq);
    } seq:[[[UserEntity sharedInstance].playerName base64Decode] integerValue]];
}


#pragma mark - 删除推送对象
- (void)deleteAlias
{
    //根据极光推送技术支持人员反馈 可以不用删除 因为每次的设置Alias都是覆盖操作
    [JPUSHService deleteAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"删除推送对象 iResCode:%ld  iAlias:%@  seq:%ld", iResCode, iAlias, seq);
    } seq:[[[UserEntity sharedInstance].playerName base64Decode] integerValue]];
}


#pragma mark - 查询推送对象
- (void)queryAlias
{
    [JPUSHService getAlias:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
        NSLog(@"查询推送对象 iResCode:%ld  iAlias:%@  seq:%ld", iResCode, iAlias, seq);
    } seq:[[[UserEntity sharedInstance].playerName base64Decode] integerValue]];
}



- (void)dealloc
{
    NSLog(@"推送消息单利类销毁");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CCPDidChannelConnectedSuccess" object:nil];
}


@end


