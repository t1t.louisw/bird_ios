//
//  JPushHelper.h
//  SiBenServer
//
//  Created by 肖雨 on 2017/8/23.
//  Copyright © 2017年 ShangLv. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JPUSHService.h"
#import "NewNotifier.h" 
#import <UserNotifications/UserNotifications.h>

@interface JPushHelper : NSObject<UNUserNotificationCenterDelegate,JPUSHRegisterDelegate>

/*!
 * @brief 极光推送相关API封装
 * @author 肖雨
 */

+ (instancetype)sharedInstance;

// 在应用启动的时候调用
- (void)setJPushAPNSSDK:(UIApplication *)application options:(NSDictionary *)launchOptions;
//基于iOS 6 及以下的系统版本，如果 App状态为正在前台或者点击通知栏的通知消息，那么此函数将被调用，并且可通过AppDelegate的applicationState是否为UIApplicationStateActive判断程序是否在前台运行。此种情况在此函数中处理
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo;
//App处于前台 收到通知(iOS 10+)
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler;
//App处于后台 触发通知动作时回调，比如点击、删除通知和点击自定义action(iOS 10+)
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler;
//App处于前台 基于iOS7~iOS9 旧的API处理收到的推送消息通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
//本地推送通知API 处理收到的推送消息通知
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification;
//APNs注册成功回调，将返回的deviceToken上传到CloudPush服务器
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
//APNs注册失败回调
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;

//添加推送对象
- (void)addAlias;
//删除推送对象
- (void)deleteAlias;
//查询推送对象
- (void)queryAlias;


@end
