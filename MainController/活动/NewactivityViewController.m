//
//  NewactivityViewController.m
//  NewProject
//
//  Created by mac on 2019/11/13.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewactivityViewController.h"
#import "SQFiltrateView.h"
#import "NewActivityCell.h"
#define Start_X          15.0f      // 第一个按钮的X坐标
#define Start_Y          5.0f     // 第一个按钮的Y坐标
#define Width_Space      10.0f      // 2个按钮之间的横间距
#define Height_Space     10.0f     // 竖间距
#define Button_Height   40    // 高
#define Button_Width     (SCREEN_WIDTH-50)/3   // 宽
#import <WebKit/WebKit.h>
#import "NewSDmodel.h"
@interface NewactivityViewController ()<UITableViewDelegate,UITableViewDataSource,WKUIDelegate,WKNavigationDelegate>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
    NSMutableDictionary *dic;//创建一个字典进行判断收缩还是展开
    NSMutableArray *_btnMutableArray2;
    NSMutableDictionary *NewDataDic;
    NSMutableArray *_dataSource;
    NSInteger classIndex;
}

@end

@implementation NewactivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"促销优惠"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self dataInitialization];
    [self NetworkRequestManagergetlistPromos];
    
}
- (void)dataInitialization {
    dataArray = NewMutableArrayInit;
    _btnMutableArray2 = NewMutableArrayInit;
    NewDataDic = NewMutableDictionaryInit;
    _dataSource = NewMutableArrayInit;
}
- (void)NetworkRequestManagergetlistPromos
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:listPromos parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            [SVProgressHUD dismiss];
        }else{
            NewDataDic = [NSMutableDictionary dictionaryWithDictionary:requestData[@"result"]];
            NSMutableArray *arr = [NSMutableArray arrayWithArray:NewDataDic[@"promos"]];
            [_dataSource removeAllObjects];
            for (NSDictionary *dic in arr) {
                NewSDmodel *model = [[NewSDmodel alloc] initWithDictionary:dic error:nil];
                [_dataSource addObject:model];
            }
            [self loadsView];
            [newtableView reloadData];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];

    }];
}
- (void)loadsView {
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, 100)];
    headView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self.view addSubview:headView];
    
    NSMutableArray *arr = [NSMutableArray arrayWithArray:NewDataDic[@"categories"]];
    [arr insertObject:@{@"name":@"全部",@"id":@"0",@"icon":@""} atIndex:0];
    for (int i = 0 ; i < arr.count; i++) {
        NSInteger index = i % 3;
        NSInteger page = i / 3;
        // 圆角按钮
        UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
         mapBtn.tag = i;//这句话不写等于废了
        mapBtn.frame = CGRectMake(index * (Button_Width + Width_Space) + Start_X, page  * (Button_Height + Height_Space)+Start_Y, Button_Width, Button_Height);
        mapBtn.titleLabel.font = NewFont(15);
        [mapBtn setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
        ViewBorderRadius(mapBtn, 2, 0, NewClearColor);
        [mapBtn setTitle:arr[i][@"name"] forState:UIControlStateNormal];
        [mapBtn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
        [headView addSubview:mapBtn];
      //按钮点击方法
        [mapBtn addTarget:self action:@selector(mapBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        mapBtn.tag = i;
        if (i == 0) {
             ViewBorderRadius(mapBtn, 5, 1, [UIColor colorWithString:@"#FF6D44"]);
             [mapBtn setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
            classIndex = 0;
         } else {
              ViewBorderRadius(mapBtn, 5, 0, NewClearColor);
             [mapBtn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
         }
        [_btnMutableArray2 addObject:mapBtn];
    }
    
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader+100, SCREEN_WIDTH, SCREEN_HEIGHT-100-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;
    
    dic = [NSMutableDictionary dictionary];
}
#pragma mark  ------ 分类选择 -------
- (void)mapBtnClick:(UIButton *)sender {
    for (UIButton *btn in _btnMutableArray2) {
        if (btn.tag == sender.tag) {
            NSLog(@"1");
            ViewBorderRadius(btn, 5, 1, [UIColor colorWithString:@"#FF6D44"]);
            [btn setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
        } else {
            NSLog(@"1");
             ViewBorderRadius(btn, 5, 0, NewClearColor);
            [btn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
        }
    }
    classIndex = sender.tag;
    NSMutableArray *arr = [NSMutableArray arrayWithArray:NewDataDic[@"categories"]];
    NSMutableArray *arr1 = [NSMutableArray arrayWithArray:NewDataDic[@"promos"]];
    
    if (sender.tag == 0) {
        //点击全部
        [self NetworkRequestManagergetlistPromos];
    }else{
        //其他
        NSString *_id = arr[sender.tag-1][@"id"];
        NSMutableArray *array = NewMutableArrayInit;
        for (int i = 0; i<arr1.count; i++) {
            if ([_id isEqualToString:arr1[i][@"promo_category"]]) {
                NewSDmodel *model= [[NewSDmodel alloc] initWithDictionary:arr1[i] error:nil];
                [array addObject:model];
            }
        }
        _dataSource = [NSMutableArray arrayWithArray:array];
        [newtableView reloadData];
    }
}

#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataSource.count;
    
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *string = [NSString stringWithFormat:@"%ld",section];
    if ([dic[string] integerValue] == 1 ) {  //打开cell返回数组的count
        return 1;
    }else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    NewActivityCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
    if (cell == nil) {
        cell = [[NewActivityCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
    if (_dataSource.count>0) {
        NewSDmodel *model = _dataSource[indexPath.section];
        cell.webViewNew.UIDelegate = self;
        cell.webViewNew.navigationDelegate = self;
        [cell assignment:model];
        NewTouchUpInside(cell.shenqingBT, shenqingBTclick:);
        cell.shenqingBT.tag = indexPath.section;
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 165;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 165)];
    UIImageView *imageV = [UIImageView new];
    [imageV setImage:NewImageNamed(@"促销海报")];
    NewSDmodel *model = _dataSource[section];

    NSString *imageUrl =  [NSString stringWithFormat:@"%@%@",@"http://player.dj002.t1t.in",model.promo_image];
    [imageV sd_setImageWithURL:NewURL(imageUrl) placeholderImage:NewImageNamed(@"")];
    [view addSubview:imageV];
        
    imageV.sd_layout
    .heightIs(150)
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 15)
    .rightSpaceToView(view, 15);
    
    UIView *backV = [UIView new];
    backV.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    [imageV addSubview:backV];
    
    backV.sd_layout
    .heightIs(30)
    .leftEqualToView(imageV)
    .rightEqualToView(imageV)
    .bottomEqualToView(imageV);
    
    UILabel *title = [UILabel new];
    title.text = [NSString stringWithFormat:@"%@",model.promoName];
    title.font = NewFont(14);
    [title setSingleLineAutoResizeWithMaxWidth:0];
    title.textColor = NewWhiteColor;
    [backV addSubview:title];
    
    title.sd_layout
    .heightIs(30)
    .leftSpaceToView(backV, 5)
    .topEqualToView(backV);

    //创建一个手势进行点击，这里可以换成button
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(action_tap:)];
    view.tag = 300 + section;
    [view addGestureRecognizer:tap];
    return view;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (void)action_tap:(UIGestureRecognizer *)tap{
    NSString *str = [NSString stringWithFormat:@"%ld",tap.view.tag - 300];
    if ([dic[str] integerValue] == 0) {//如果是0，就把1赋给字典,打开cell
        [dic setObject:@"1" forKey:str];
    }else{//反之关闭cell
        [dic setObject:@"0" forKey:str];
    }
    [newtableView reloadSections:[NSIndexSet indexSetWithIndex:tap.view.tag-300]withRowAnimation:UITableViewRowAnimationFade];//有动画的刷新
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    //修改字体大小 300%
    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '200%'" completionHandler:nil];
    
    //修改字体颜色  #9098b8
    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#FFFFFF'" completionHandler:nil];
    //修改背景颜色
    [webView evaluateJavaScript:@"document.body.style.backgroundColor=\"#230F40\"" completionHandler:nil];

    [webView evaluateJavaScript:@"document.body.scrollWidth"completionHandler:^(id _Nullable result,NSError * _Nullable error){
//        CGFloat ratio =  CGRectGetWidth(self.webViewNew111.frame) /[result floatValue];
        NSLog(@"scrollWidth宽度：%.2f",[result floatValue]);
        [webView evaluateJavaScript:@"document.body.scrollHeight"completionHandler:^(id _Nullable result,NSError * _Nullable error){
            NSLog(@"scrollHeight高度：%.2f",[result floatValue]);
        }];
    }];
}

- (void)shenqingBTclick:(UIButton *)sender {
    NewSDmodel *model = _dataSource[sender.tag];
    [self NetworkRequestManagergetapplyPromo:model.promo_cms_id];
}
- (void)NetworkRequestManagergetapplyPromo:(NSString *)promo_cms_id
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:promo_cms_id forKey:@"promo_cms_id"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:applyPromo parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] == 0) {
            [SVProgressHUD showSuccessWithStatus:requestData[@"result"][@"message"]];
        }else if([requestData[@"code"] integerValue] == 498){
            [SVProgressHUD showInfoWithStatus:requestData[@"result"][@"message"]];
        }else if([requestData[@"code"] integerValue] == 496){
            [SVProgressHUD showInfoWithStatus:requestData[@"message"]];
        }else{
            [SVProgressHUD showInfoWithStatus:requestData[@"result"][@"message"]];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];

    }];
}
@end
