//
//  NewActivityCell.m
//  NewProject
//
//  Created by mac on 2019/11/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewActivityCell.h"

@implementation NewActivityCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _webViewNew = [[WKWebView alloc] init];
        [self addSubview:_webViewNew];

        _webViewNew.sd_layout
        .leftSpaceToView(self, 15)
        .rightSpaceToView(self, 15)
        .topSpaceToView(self, 10)
        .bottomSpaceToView(self, 45);
        
        _shenqingBT = [UIButton new];
        [_shenqingBT setTitle:@"立即申请" forState:UIControlStateNormal];
        [_shenqingBT setTitleColor:[UIColor colorWithString:@"#7E7B83"] forState:UIControlStateNormal];
        _shenqingBT.titleLabel.font = NewFont(15);
        ViewBorderRadius(_shenqingBT, 15, 1, [UIColor colorWithString:@"7E7B83"]);
        [self addSubview:_shenqingBT];
        
        _shenqingBT.sd_layout
        .heightIs(30)
        .bottomSpaceToView(self, 10)
        .centerXEqualToView(self)
        .widthIs(80);
        
    }
    return self;
}
- (void)assignment:(NewSDmodel *)sdModel {
    _sdModel = sdModel;
    [_webViewNew loadHTMLString:_sdModel.promoDetails baseURL:nil];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
