//
//  NewActivityCell.h
//  NewProject
//
//  Created by mac on 2019/11/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "NewSDmodel.h"
NS_ASSUME_NONNULL_BEGIN

@interface NewActivityCell : UITableViewCell
@property (nonatomic,strong)UILabel *detailLB;
@property (nonatomic,strong)UIButton *shenqingBT;
@property (nonatomic,strong)WKWebView *webViewNew;
@property (nonatomic,strong)NewSDmodel *sdModel;

- (void)assignment:(NewSDmodel *)model;

@end

NS_ASSUME_NONNULL_END
