//
//  NewSDmodel.h
//  NewProject
//
//  Created by mac on 2019/12/30.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewSDmodel : JSONModel
@property (nonatomic,copy)NSString<Optional> *promoDetails;
@property (nonatomic,copy)NSString<Optional> *tag_as_new_flag;
@property (nonatomic,copy)NSString<Optional> *promo_check_mesg;
@property (nonatomic,copy)NSString<Optional> *promo_category;
@property (nonatomic,copy)NSString<Optional> *promoName;
@property (nonatomic,copy)NSString<Optional> *promoDescription;
@property (nonatomic,copy)NSString<Optional> *promo_code;
@property (nonatomic,copy)NSString<Optional> *promo_image;
@property (nonatomic,copy)NSString<Optional> *promo_check_player_allowed;
@property (nonatomic,copy)NSString<Optional> *promo_cms_id;
@property (nonatomic,copy)NSString<Optional> *promo_category_name;
@property (nonatomic,copy)NSString<Optional> *status;

@end

NS_ASSUME_NONNULL_END
