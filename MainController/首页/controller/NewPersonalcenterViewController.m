//
//  NewPersonalcenterViewController.m
//  NewProject
//
//  Created by mac on 2019/12/23.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewPersonalcenterViewController.h"
#import "NewMyViewCell.h"
#import "NewMyViewTypeCell.h"
#import "NewTopuppageViewController.h"
#import "NewWithdrawalViewController.h"
#import "NewTransferViewController.h"
#import "NewBillingrecordsViewController.h"
#import "MewAccountinformationViewController.h"
#import "NewMylimitViewController.h"
#import "NewSecuritycenterViewController.h"
#import "NewinvitationViewController.h"
#import "NewTrueTransferViewController.h"
#import "NewbankaccountViewController.h"
#import "NewactivityViewController.h"
#import "NewLoginViewController.h"
#import "NewViewbankcardViewController.h"
#import "NewSecurityViewController.h"
#import "NewTuichuCollectionViewCell.h"
#import "NewactivityViewController.h"

@interface NewPersonalcenterViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>{
    NSMutableArray *dataArray;
    UICollectionView *mainCollectionView;
    UICollectionViewFlowLayout *flowLayout;
    
    NSString *VipStr;
    NSMutableArray *NewdataArray;
    NSString *messagesStr;
    
    NSMutableDictionary *touzuDic;

}


@end

@implementation NewPersonalcenterViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self NetworkRequestManagergetmessage];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"个人中心"];
    [self dataInitialization];
    [self NetworkRequestManagerqueryPlayerBalance];
    [self RequestManagerUserInfo];
    [self RequestManagergetPlayerVipStatus];
    [self loadsView];
}
- (void)NetworkRequestManagergetmessage {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:@"" forKey:@"messageId"];
    [dic setValue:@"allMessage" forKey:@"messageType"];
//    [dic setValue:[NSString stringWithFormat:@"%ld",self.page] forKey:@"offset"];
//    [dic setValue:@"20" forKey:@"limit"];
//    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:Newmessage parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            
        }else{
            NSMutableArray *array = [NSMutableArray arrayWithArray:requestData[@"result"][@"messages"]];
            for (int i = 0; i<array.count; i++) {
                if ([array[i][@"status"] isEqualToString:@"6"]) {
                    messagesStr = @"显示";
                    break;
                }else{
                    messagesStr = @"不显示";
                }
            }
            [mainCollectionView reloadData];
        }
    } onFailure:^{
//        [SVProgressHUD dismiss];
    }];
}
- (void)NetworkRequestManagerqueryPlayerBalance {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:@"" forKey:@"refresh"];
//    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:queryPlayerBalance parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [NewdataArray removeAllObjects];
            [NewdataArray addObject:requestData[@"result"]];
            [mainCollectionView reloadData];
//            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)RequestManagerUserInfo{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:getPlayerProfile parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [mainCollectionView reloadData];
            if (requestData[@"result"][@"firstName"] == [NSNull new]) {
                [UserEntity sharedInstance].firstName = @"";
            }else{
                [UserEntity sharedInstance].firstName = requestData[@"result"][@"firstName"];
            }
//            [UserEntity sharedInstance].firstName = requestData[@"result"][@"firstName"] == NULL?@"":requestData[@"result"][@"firstName"];
            
            [UserHelper cacheUserInfoFromLocation];
            [UserHelper writeUserInfoFromLocation];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)RequestManagergetPlayerVipStatus{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [NetworkRequestManager requestPostWithInterfacePrefix:getPlayerVipStatus parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [mainCollectionView reloadData];
            VipStr = requestData[@"result"][@"current_level"];
            touzuDic = [NSMutableDictionary dictionaryWithDictionary:requestData[@"result"]];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}



- (void)dataInitialization{
    dataArray = NewMutableArrayInit;
    NewdataArray = NewMutableArrayInit;
    for (int i = 0; i<7; i++) {
        NSMutableDictionary *dic = NewMutableDictionaryInit;
        switch (i) {
            case 0:
                [dic setValue:@"账单记录" forKey:@"image"];
                [dic setValue:@"账单记录" forKey:@"title"];
                [dataArray addObject:dic];
                break;
                case 1:
                    [dic setValue:@"账户信息" forKey:@"image"];
                    [dic setValue:@"银行账户" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 2:
                    [dic setValue:@"站内信" forKey:@"image"];
                    [dic setValue:@"站内信" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 3:
                    [dic setValue:@"促销活动" forKey:@"image"];
                    [dic setValue:@"促销优惠" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 4:
                    [dic setValue:@"安全中心" forKey:@"image"];
                    [dic setValue:@"安全设置" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 5:
                    [dic setValue:@"邀请朋友" forKey:@"image"];
                    [dic setValue:@"邀请朋友" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 6:
                    [dic setValue:@"退出登录" forKey:@"image"];
                    [dic setValue:@"退出登录" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
            default:
                break;
        }
    }
}

- (void)loadsView{
    [self.view addSubview:mainCollectionView = [NewControlPackage collectionViewInitWithFrame:CGRectMake(0.0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) delegate:self dataSource:self backgroundColor:NewButtonColor scrollEnabled:YES alwaysBounceVertical:YES alwaysBounceHorizontal:NO showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO collectionViewFlowLayout:flowLayout sectionInset:UIEdgeInsetsMake(0, 0, 0, 0) headerReference:CGSizeMake(0, 0) footerReference:CGSizeMake(0, 0) minimumInteritemSpacing:0 minimumLineSpacing:0 scrollDirection:0 hidden:NO tag:105 userInteractionEnabled:YES]];
    [mainCollectionView registerClass:[NewMyViewCell class] forCellWithReuseIdentifier:@"NewMyViewCell"];
    [mainCollectionView registerClass:[NewMyViewTypeCell class] forCellWithReuseIdentifier:@"NewMyViewTypeCell"];
    [mainCollectionView registerClass:[NewTuichuCollectionViewCell class] forCellWithReuseIdentifier:@"NewTuichuCollectionViewCell"];
}

//单元格大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH, 260);
    }else{
        if (indexPath.row == 6) {
            return CGSizeMake(SCREEN_WIDTH, 60+80);
        }else{
            return CGSizeMake(SCREEN_WIDTH, 50);
        }
    }
}

//定义每个UICollectionView 的 margin 边距  上左下右
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return dataArray.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NewMyViewCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewMyViewCell" forIndexPath:indexPath];
        cell.backgroundColor = NewClearColor;
        NewTouchUpInside(cell.chongzhiBT, chongzhiBTclick);
        NewTouchUpInside(cell.tixianBT, tixianBTclick);
//        NewTouchUpInside(cell.zhuanzhangBT, zhuanzhangBTclick);
        NewTouchUpInside(cell.PersonalinformationBT, PersonalinformationBTclick);
        NewTouchUpInside(cell.quxiaoBT, quxiaoBTclick);
        NewTouchUpInside(cell.youhuiBT, youhuiBTclick);
        NewTouchUpInside(cell.Personalinformation111BT, PersonalinformationBTclick);        
        if (NewdataArray.count>0) {
          [cell assignment:NewdataArray[0] touzuDic:touzuDic];
        }
        return cell;
    }else if (indexPath.section == 1){
        if (indexPath.row == 6) {
            NewTuichuCollectionViewCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewTuichuCollectionViewCell" forIndexPath:indexPath];
            cell.backgroundColor = NewClearColor;
            [cell assignment:dataArray[indexPath.row]];
            return cell;
        }else{
            NewMyViewTypeCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewMyViewTypeCell" forIndexPath:indexPath];
            cell.backgroundColor = NewClearColor;
            if (indexPath.row == 2) {
                if ([messagesStr isEqualToString:@"显示"]) {
                    cell.redView.hidden = NO;
                }else{
                    cell.redView.hidden = YES;
                }
            }else{
                cell.redView.hidden = YES;
            }

            [cell assignment:dataArray[indexPath.row]];
            return cell;
        }
    }
    return [[UICollectionViewCell alloc] init];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            NewBillingrecordsViewController *vc = [NewBillingrecordsViewController new];
            NewPushViewController(vc);
        }else if (indexPath.row == 1) {
//            NewViewbankcardViewController*vc = [NewViewbankcardViewController new];
//            NewPushViewController(vc);
            NewbankaccountViewController *vc = [NewbankaccountViewController new];
            vc.Str = @"取款银行卡";
            NewPushViewController(vc);

        }else if (indexPath.row == 2) {
            NewMylimitViewController*vc = [NewMylimitViewController new];
            NewPushViewController(vc);

        }else if (indexPath.row == 3) {
            NewactivityViewController*vc = [NewactivityViewController new];
            NewPushViewController(vc);

        }else if (indexPath.row == 4){
            NewSecurityViewController*vc = [NewSecurityViewController new];
            NewPushViewController(vc);

        }else if (indexPath.row == 5){
            NewinvitationViewController*vc = [NewinvitationViewController new];
            NewPushViewController(vc);
        }else if (indexPath.row == 6){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"退出登录" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                       UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                       }];
                       UIAlertAction *skipAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                           [self tuichudenglu];
                       }];
                       [alertController addAction:cancelAction];
                       [alertController addAction:skipAction];
                       [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

- (void)chongzhiBTclick {
    NewTopuppageViewController *vc = [NewTopuppageViewController new];
    NewPushViewController(vc);
}
- (void)tixianBTclick {
    NewWithdrawalViewController *vc = [NewWithdrawalViewController new];
    NewPushViewController(vc);
}
- (void)zhuanzhangBTclick {
    NewTrueTransferViewController *vc = [NewTrueTransferViewController new];
    NewPushViewController(vc);
}

- (void)PersonalinformationBTclick{
    MewAccountinformationViewController*vc = [MewAccountinformationViewController new];
    NewPushViewController(vc);
}

- (void)youhuiBTclick{
    NewactivityViewController*vc = [NewactivityViewController new];
    NewPushViewController(vc);
}
- (void)quxiaoBTclick{
    CATransition* transition = [CATransition animation];
    transition.type = kCATransitionPush;//可更改为其他方式
    transition.subtype = kCATransitionFromRight;//可更改为其他方式
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tuichudenglu {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:logout parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"msg"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"退出登录成功"];
            [UserHelper removeUserInfoFromLocation];
            NewLoginViewController *vc = [[NewLoginViewController alloc] init];//去登陆
            UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:vc];
            [AppDelegate shareDelegate].window.rootViewController = navi;
        }
    } onFailure:^{
        
    }];
}

@end
