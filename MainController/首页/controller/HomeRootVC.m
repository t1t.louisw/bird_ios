//
//  HomeRootVC.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "HomeRootVC.h"
#import "NewPersonalcenterViewController.h"
#import "NewCustomerserviceViewController.h"
#import <WebKit/WebKit.h>
@interface HomeRootVC ()<WKUIDelegate,WKNavigationDelegate>
{
        
    WKWebView *webViewNew;

}
@end


@implementation HomeRootVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@""];
    self.view.backgroundColor = NewButtonColor;
    [self loadDhView];
    [self setupUI];
    [self NetworkRequestManagerqueryPlayerBalance];//校验是否需要重新登录
    [self addPulltoRefresh];
    //监听UIWindow隐藏
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(endFullScreen) name:UIWindowDidBecomeHiddenNotification object:nil];
    
}
- (void)addPulltoRefresh {
    __weak WKWebView *webView = webViewNew;
    __weak UIScrollView *scrollView = webViewNew.scrollView;
    // 添加下拉刷新控件
    scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [webView reload];
    }];
}

-(void)endFullScreen{
    NSLog(@"退出全屏");
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}
- (void)setupUI{
    webViewNew = [[WKWebView alloc] initWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT - NavHeader)];
    webViewNew.navigationDelegate = self;
    webViewNew.UIDelegate = self;
    [webViewNew setOpaque:NO];
    webViewNew.scrollView.backgroundColor = NewClearColor;
    webViewNew.backgroundColor = NewClearColor;

    [self.view addSubview:webViewNew];
}
- (void)NetworkRequestManagerqueryPlayerBalance {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:@"" forKey:@"refresh"];
    [NetworkRequestManager requestPostWithInterfacePrefix:queryPlayerBalance parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [self NetworkRequestManagerlistGamePlatforms];
        }
    } onFailure:^{
//        [SVProgressHUD dismiss];
    }];
}
- (void)NetworkRequestManagerlistGamePlatforms
{
    [SVProgressHUD show];
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [NetworkRequestManager requestPostWithInterfacePrefix:listGamePlatforms parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            for (int i = 0; i<[requestData[@"result"] count]; i++) {
                if ([requestData[@"result"][i][@"system_code"] isEqualToString:@"RG"]) {
                    [self NetworkRequestManagerlistGamesByPlatform:requestData[@"result"][i][@"id"]];
                    break;
                }
            }
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)NetworkRequestManagerlistGamesByPlatform:(NSString *)platform_id
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:platform_id forKey:@"platform_id"];
    [dic setValue:@"2" forKey:@"force_lang"];
    [NetworkRequestManager requestPostWithInterfacePrefix:listGamesByPlatform parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            NSString *url = [NSString stringWithFormat:@"%@/iframe/auth/login_with_token/%@/?next=%@",NewSERVER_URL,[NewUtils userDefaultsStringKey:NewToken],requestData[@"result"][@"game_types"][@"e_sports"][@"web"]];
            NSLog(@"%@",url);
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [webViewNew loadRequest:request];
        
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}

- (void)loadDhView{
    UIButton *leftBT = [UIButton new];
    [leftBT setImage:NewImageNamed(@"个人中心1") forState:UIControlStateNormal];
    NewTouchUpInside(leftBT, leftBTclick:);
    [self.navgationBar addSubview:leftBT];

    leftBT.sd_layout
    .leftSpaceToView(self.navgationBar, 5)
    .heightIs(40)
    .widthIs(40)
    .bottomSpaceToView(self.navgationBar, 0);
    
    UIButton *rightBT = [UIButton new];
    [rightBT setImage:NewImageNamed(@"客服") forState:UIControlStateNormal];
    NewTouchUpInside(rightBT, rightBTclick:);
    [self.navgationBar addSubview:rightBT];

    rightBT.sd_layout
    .rightSpaceToView(self.navgationBar, 5)
    .heightIs(40)
    .widthIs(40)
    .bottomSpaceToView(self.navgationBar, 0);
    
    UIImageView *headimage = [UIImageView new];
    [headimage setImage:NewImageNamed(@"WechatIMG96")];
    [self.navgationBar addSubview:headimage];
    
    headimage.sd_layout
    .centerXEqualToView(self.navgationBar)
    .bottomSpaceToView(self.navgationBar, 10)
    .widthIs(100)
    .heightIs(20);
}
- (void)leftBTclick:(UIButton *)sender {
    NewPersonalcenterViewController *vc = [NewPersonalcenterViewController new];
    CATransition* transition = [CATransition animation];
     transition.type = kCATransitionPush;//可更改为其他方式
     transition.subtype = kCATransitionFromLeft;//可更改为其他方式
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:vc animated:NO];
    return;
}
- (void)rightBTclick:(UIButton *)sender {
    NewCustomerserviceViewController *vc = [NewCustomerserviceViewController new];
    NewPushViewController(vc);
    return;
}

    // 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [SVProgressHUD show];
}
    // 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [SVProgressHUD dismiss];
    [webViewNew.scrollView.mj_header endRefreshing];
}
    // 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
}
    // 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [SVProgressHUD dismiss];
    [webViewNew.scrollView.mj_header endRefreshing];
}
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"加载失败1");
    [SVProgressHUD dismiss];
    [webViewNew.scrollView.mj_header endRefreshing];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
