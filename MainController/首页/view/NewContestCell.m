//
//  NewContestCell.m
//  NewProject
//
//  Created by mac on 2019/11/13.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewContestCell.h"

@implementation NewContestCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        UIView *backView1 = [UIView new];
        backView1.backgroundColor = [UIColor colorWithString:@"#462A6F"];
        ViewBorderRadius(backView1, 5 ,0.8, [UIColor colorWithString:@"#FF6D44"]);
        [self addSubview:backView1];
        
        backView1.sd_layout
        .leftEqualToView(self)
        .rightEqualToView(self)
        .topEqualToView(self)
        .bottomEqualToView(self);
        
        UIView *headView = [UIView new];
        headView.backgroundColor = [UIColor colorWithString:@"#291744"];
        [backView1 addSubview:headView];
        
        headView.sd_layout
        .heightIs(45)
        .topEqualToView(backView1)
        .leftEqualToView(backView1)
        .rightEqualToView(backView1);
        
        _image1 = [UIImageView new];
        [_image1 setImage:NewImageNamed(@"巅峰联赛")];
        [headView addSubview:_image1];
        
        _image1.sd_layout
        .heightIs(20)
        .widthIs(20)
        .centerYEqualToView(headView)
        .leftSpaceToView(headView, 13);

        
        _nameTitle1 = [UILabel new];
        _nameTitle1.text = @"游戏进行中...";
        _nameTitle1.font = NewFont(10);
        [_nameTitle1 setSingleLineAutoResizeWithMaxWidth:0];
        _nameTitle1.textColor = [UIColor colorWithString:@"#FF7942"];
        [headView addSubview:_nameTitle1];
               
        _nameTitle1.sd_layout
        .rightSpaceToView(headView, 3)
        .heightIs(15)
        .centerYEqualToView(headView);
        
        
        _nameTitle = [UILabel new];
        _nameTitle.text = @"巅峰联赛 - 南美预算赛";
        _nameTitle.font = NewFont(14);
        _nameTitle.textColor = NewWhiteColor;
        [headView addSubview:_nameTitle];
        
        _nameTitle.sd_layout
        .leftSpaceToView(_image1, 13)
        .rightSpaceToView(_nameTitle1, 10)
        .heightIs(15)
        .centerYEqualToView(headView);
        
        UIView *backView = [UIView new];
        backView.backgroundColor = [UIColor colorWithString:@"#462A6F"];
        [backView1 addSubview:backView];
        
        backView.sd_layout
        .leftEqualToView(backView1)
        .rightEqualToView(backView1)
        .topSpaceToView(backView1, 45)
        .bottomEqualToView(backView1);
        
        _image2 = [UIImageView new];
        [_image2 setImage:NewImageNamed(@"cp-1")];
        [backView addSubview:_image2];
        
        _image2.sd_layout
        .topSpaceToView(backView, 25)
        .heightIs(35)
        .widthIs(35)
        .leftSpaceToView(backView, 40);
        
        _image3 = [UIImageView new];
        [_image3 setImage:NewImageNamed(@"cp-2")];
        [backView addSubview:_image3];
        
        _image3.sd_layout
        .topSpaceToView(backView, 25)
        .heightIs(35)
        .widthIs(35)
        .rightSpaceToView(backView, 40);
        
        _scoreLabel = [UILabel new];
        _scoreLabel.text = @"2 - 1";
        _scoreLabel.font = NewBFont(22);
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        _scoreLabel.textColor = NewWhiteColor;
        [backView addSubview:_scoreLabel];
        
        _scoreLabel.sd_layout
        .centerYEqualToView(_image2)
        .heightIs(20)
        .leftSpaceToView(_image2, 5)
        .rightSpaceToView(_image3, 5);
        
        _enterGameBT = [UIButton new];
        [_enterGameBT setTitle:@"进入游戏" forState:UIControlStateNormal];
        [_enterGameBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
        [_enterGameBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
        ViewRadius(_enterGameBT, 25/2);
        _enterGameBT.titleLabel.font = NewFont(13);
        _enterGameBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
//        _enterGameBT.frame = CGRectMake(self.frame.size.width/2, 150-45-35, 90, 25);
        [backView addSubview:_enterGameBT];
        
        _enterGameBT.sd_layout
        .heightIs(25)
        .bottomSpaceToView(backView, 10)
        .centerXEqualToView(backView)
        .widthIs(90);
        
//        [_enterGameBT mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.width.mas_equalTo(90);
//            make.height.mas_equalTo(25);
//            make.centerX.equalTo(backView);
//            make.bottom.mas_equalTo(-10);
//        }];
        
    
        [self.imageView removeFromSuperview];
        [self.coverView removeFromSuperview];
    }
    return self;
}




@end
