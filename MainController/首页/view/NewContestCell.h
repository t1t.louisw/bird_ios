//
//  NewContestCell.h
//  NewProject
//
//  Created by mac on 2019/11/13.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "GKCycleScrollViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewContestCell : GKCycleScrollViewCell
@property (nonatomic, strong) UIImageView *image1;
@property (nonatomic, strong) UILabel *nameTitle1;
@property (nonatomic, strong) UILabel *nameTitle;
@property (nonatomic, strong) UIImageView *image2;
@property (nonatomic, strong) UIImageView *image3;
@property (nonatomic, strong) UILabel *scoreLabel;
@property (nonatomic, strong) UIButton *enterGameBT;

@end

NS_ASSUME_NONNULL_END
