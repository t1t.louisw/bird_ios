//
//  NewTuichuCollectionViewCell.h
//  NewProject
//
//  Created by mac on 2020/1/14.
//  Copyright © 2020 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewTuichuCollectionViewCell : UICollectionViewCell
- (void)assignment:(NSDictionary *)model;

@end

NS_ASSUME_NONNULL_END
