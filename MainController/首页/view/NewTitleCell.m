//
//  NewTitleCell.m
//  NewProject
//
//  Created by mac on 2019/11/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewTitleCell.h"

@implementation NewTitleCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#FF6D44"];
        [self addSubview:line];
        
        line.sd_layout
        .heightIs(15)
        .widthIs(3)
        .topSpaceToView(self, 7.5)
        .leftSpaceToView(self, 15);
        
        UILabel *name = [UILabel new];
        name.text = @"推荐游戏";
        name.font = NewFont(15);
        name.textColor = [UIColor colorWithString:@"#FF6D44"];
        [name setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:name];
        
        name.sd_layout
        .heightIs(15)
        .centerYEqualToView(line)
        .leftSpaceToView(line, 10);
        
        UIButton *gengduoBT = [UIButton new];
        [gengduoBT setTitle:@"更多 >" forState:UIControlStateNormal];
        [gengduoBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
        gengduoBT.titleLabel.font = NewFont(13);
        gengduoBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self addSubview:gengduoBT];
        
        gengduoBT.sd_layout
        .heightIs(15)
        .rightSpaceToView(self, 10)
        .centerYEqualToView(name)
        .widthIs(80);
    }
    return self;
}
@end
