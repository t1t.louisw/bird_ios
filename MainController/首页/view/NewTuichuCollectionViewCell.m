//
//  NewTuichuCollectionViewCell.m
//  NewProject
//
//  Created by mac on 2020/1/14.
//  Copyright © 2020 NewOrganization. All rights reserved.
//

#import "NewTuichuCollectionViewCell.h"

@implementation NewTuichuCollectionViewCell{
    UIImageView *imageV;
    UILabel *nameLB;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line];
        
        line.sd_layout
        .topEqualToView(line)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .heightIs(5);
        
        UILabel *line1 = [UILabel new];
        line1.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line1];
        
        line1.sd_layout
        .topSpaceToView(self, 50)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .heightIs(5);
                            
                    nameLB = [UILabel new];
                    nameLB.textColor = NewWhiteColor;
                    nameLB.font = NewFont(15);
        nameLB.textAlignment = NSTextAlignmentCenter;
                    [self addSubview:nameLB];
                    
                    nameLB.sd_layout
                    .heightIs(15)
                    .widthIs(SCREEN_WIDTH-30)
        .centerXEqualToView(self)
                    .topSpaceToView(line, 35/2)
        .bottomSpaceToView(line1, 35/2);
        
        UIImageView *RGlogImgae = [UIImageView new];
        [RGlogImgae setImage:NewImageNamed(@"RGLogo")];
        [self addSubview:RGlogImgae];
        
        RGlogImgae.sd_layout
        .heightIs(25)
        .widthIs(120)
        .centerXEqualToView(self)
        .bottomSpaceToView(self, 25);
    }
    return self;
}
- (void)assignment:(NSDictionary *)model {
//    [imageV setImage:NewImageNamed(model[@"image"])];
    nameLB.text = model[@"title"];
}
@end
