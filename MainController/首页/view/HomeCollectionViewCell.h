//
//  HomeCollectionViewCell.h
//  NewProject
//
//  Created by mac on 2019/11/12.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCollectionViewCell : UICollectionViewCell
- (void)assignment:(NSMutableArray *)model;

@end

NS_ASSUME_NONNULL_END
