//
//  NewGameCell.m
//  NewProject
//
//  Created by mac on 2019/11/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewGameCell.h"

@implementation NewGameCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        UIColor * randomColor= [UIColor colorWithRed:((float)arc4random_uniform(256) / 255.0) green:((float)arc4random_uniform(256) / 255.0) blue:((float)arc4random_uniform(256) / 255.0) alpha:1.0];

        UIImageView *view = [UIImageView new];
        [view setImage:NewImageNamed(@"王者")];
//        view.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:view];
        
        view.sd_layout
        .leftEqualToView(self)
        .rightEqualToView(self)
        .topEqualToView(self)
        .bottomSpaceToView(self, 30);
        
        
        UILabel *name = [UILabel new];
        name.text = @"王者荣耀";
        name.textColor = NewWhiteColor;
        name.font = NewFont(13);
        name.textAlignment = NSTextAlignmentLeft;
        [name setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:name];
        
        name.sd_layout
        .heightIs(20)
        .topSpaceToView(view, 0)
        .leftEqualToView(view);
    }
    return self;
}
@end
