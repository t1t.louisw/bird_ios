//
//  NewWebProgressLayer.h
//  Field
//
//  Created by 焦钟培 on 2017/8/7.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width

@interface NewWebProgressLayer : CAShapeLayer

+ (instancetype)layerWithFrame:(CGRect)frame;

- (void)finishedLoad;

- (void)startLoad;

- (void)closeTimer;

@end
