//
//  WebViewController.h
//  Field
//
//  Created by 焦钟培 on 2017/8/7.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : NewBasicViewController

typedef void(^WebViewControllerBlock)(NSString *pathUrl);


@property (nonatomic, copy)WebViewControllerBlock block;

@property (nonatomic, copy) NSString *url;

@property (nonatomic) BOOL requstReturn;

@property (nonatomic, copy)NSString *name;

-(void)setStatusBarColor:(UIColor *)color;

@end
