//
//  NewHTMLViewController.h
//  NewProject
//
//  Created by mac on 2020/1/6.
//  Copyright © 2020 NewOrganization. All rights reserved.
//

#import "NewBasicViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewHTMLViewController : NewBasicViewController
@property (nonatomic, copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
