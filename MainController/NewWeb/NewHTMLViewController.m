//
//  NewHTMLViewController.m
//  NewProject
//
//  Created by mac on 2020/1/6.
//  Copyright © 2020 NewOrganization. All rights reserved.
//

#import "NewHTMLViewController.h"
#import "NewWebProgressLayer.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "WebToOC.h"
@interface NewHTMLViewController ()<UIWebViewDelegate,UIScrollViewDelegate>{
    UIWebView *_webView;
    
    NewWebProgressLayer *_progressLayer;
}
@property (nonatomic, strong) JSContext *jsContext;

@end

@implementation NewHTMLViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"付款"];
    [self setupUI];
    // Do any additional setup after loading the view.
}
- (void)setupUI {
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT - NavHeader)];
    _webView.delegate = self;
    _webView.scrollView.delegate = self;
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [_webView loadHTMLString:self.url baseURL:nil];

//    [_webView loadRequest:request];
    
    _webView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_webView];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
