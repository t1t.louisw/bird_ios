//
//  NSTimer+Addition.h
//  Field
//
//  Created by 焦钟培 on 2017/8/7.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (Addition)

- (void)pause;

- (void)resume;

- (void)resumeWithTimeInterval:(NSTimeInterval)time;

+ (NSTimer *)wy_scheduledTimerWithTimeInterval:(NSTimeInterval)ti repeats:(BOOL)yesOrNo block:(void(^)(NSTimer *timer))block;

@end
