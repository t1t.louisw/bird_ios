//
//  WebViewController.m
//  Field
//
//  Created by 焦钟培 on 2017/8/7.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import "WebViewController.h"
#import "NewWebProgressLayer.h"
#import <JavaScriptCore/JavaScriptCore.h>
#import "WebToOC.h"
//#import "Comment.h"


@interface WebViewController ()<UIWebViewDelegate,UIScrollViewDelegate>
@property (nonatomic, strong) JSContext *jsContext;
@end

@implementation WebViewController {
    UIWebView *_webView;
    
    NewWebProgressLayer *_progressLayer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:self.name];
    [self setupUI];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"comment_back"] style:UIBarButtonItemStylePlain target:self action:@selector(popVC)];
}


- (void)setupUI {
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT - NavHeader)];
    _webView.delegate = self;
    _webView.scrollView.delegate = self;
    if (![self.url hasPrefix:@"http"]) {
        if ([self.name isEqualToString:@"核名系统"]) {
            self.url = @"http://www.fhi365.cn/phone/hmxt1.html";
            self.view.backgroundColor = NewGroupTableViewBackgroundColor;
        }else{
            self.url = @"http://www.fhi365.cn/phone/gn1.html";
            self.view.backgroundColor = [UIColor whiteColor];

        }
//        http://www.fhi365.cn/phone/gn.html
        //http://www.fhi365.cn/phone/hmxt1.html
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [_webView loadRequest:request];
    
    _webView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_webView];
    

}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint point = scrollView.contentOffset;
    if (point.x > 0) {
        scrollView.contentOffset = CGPointMake(0, point.y);//这里不要设置为CGPointMake(0, 0)，这样我们在文章下面左右滑动的时候，就跳到文章的起始位置，不科学
    }
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    NSString *urlStr = [NSString stringWithFormat:@"%@",url];
    self.requstReturn = YES;
//    self.block(urlStr);

    if ([urlStr rangeOfString:self.url].location != NSNotFound || [self.url rangeOfString:urlStr].location != NSNotFound) {
        _progressLayer = [NewWebProgressLayer layerWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, 3)];
        [self.view.layer addSublayer:_progressLayer];
        [_progressLayer startLoad];
    }
    return YES;
}
 - (void)hidenaction{

   //开始加载时隐藏webview 加载完后显示，原因是 因为我们要去掉头标签，，去掉的方法是在网页加载完毕进行的，，添加一个延时现实的方法 可以隐藏掉网页先显示头标签又被移除的过程。使其看起来更自然一些
   _webView.hidden =NO;

}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_progressLayer finishedLoad];
    
    CGSize size = _webView.scrollView.contentSize;

    size.width= _webView.scrollView.frame.size.width;

    _webView.scrollView.contentSize= size;
        
//    [self setNavTitle:[webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
    self.jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    self.jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        context.exception = exceptionValue;
        NSLog(@"异常信息：%@", exceptionValue);
    };
    self.jsContext[@"jsCallNativePay"] = ^(NSString *message,NSDictionary *json) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[WebToOC alloc] initWithActionType:0 clossType:message info:json];
        });
    };
    self.jsContext[@"jsCallNativePage"] = ^(NSString *message,NSDictionary *json) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[WebToOC alloc] initWithActionType:1 clossType:message info:json];
        });
    };
    self.jsContext[@"jsCallNativeClose"] = ^() {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[WebToOC alloc] initWithActionType:2 clossType:nil info:nil];
        });
    };
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"YEJIAN"]) {
        //修改文字颜色
        [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#C8C8C8'"];
        //修改body背景颜色
        [_webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('body')[0].style.background='#181a1b'"];
    }
}



- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [_progressLayer finishedLoad];
}

- (void)dealloc {
    NSLog(@"i am dealloc");
}

-(void)setStatusBarColor:(UIColor *)color{
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if (statusBar!=nil && [statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


- (void)popVC {
    if (_webView.canGoBack == YES) {
        [_webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
}







@end
