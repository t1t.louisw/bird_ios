//
//  WebToOC.h
//  iphone
//
//  Created by 焦钟培 on 2018/1/10.
//  Copyright © 2018年 Jiluai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebToOC : NSObject
- (void)initWithActionType: (NSInteger)actionType clossType: (NSString *)clossType info: (NSDictionary *)info;
@end
