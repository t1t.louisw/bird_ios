//
//  NewBillingrecordsViewController.m
//  NewProject
//
//  Created by mac on 2019/11/19.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewBillingrecordsViewController.h"
#import "SQFiltrateView.h"
#import "NewBillingrecordsCell.h"
#import "ZHG_ToolBarDatePickerView.h"
#import "NSDate+ZHGDate.h"
#import "NewgameTableViewCell.h"

@interface NewBillingrecordsViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
    UIImageView *imageV;
    UIButton *itemBT;
    BOOL isShow;
    UIView *popView;
    NSMutableArray *_btnMutableArray2;
    UIButton *startBT;
    UIButton *endBT;
    
    NSString *trans_type;//类型
    NSString *time_from;//开始时间
    NSString *time_to;//结束时间
}
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger last_page;
@end

@implementation NewBillingrecordsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"账单记录"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self dataInitialization];
    [self loadsView];
    [self addPulltoRefresh];

}
- (void)addPulltoRefresh {
    __weak __typeof(self) mySelf = self;
    newtableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        mySelf.page = 0;
        [mySelf NetworkRequestManagergetPlayerReports];
    }];
    newtableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        mySelf.page = mySelf.page + 20;
        [mySelf NetworkRequestManagergetPlayerReports];
    }];
    [newtableView.mj_header beginRefreshing];
//    [newtableView.mj_footer beginRefreshing];

}
- (void)dataInitialization {
    dataArray = NewMutableArrayInit;
    isShow = NO;
    _btnMutableArray2 = NewMutableArrayInit;
    time_from = @"2019-01-01";
    time_to = [NSDate stringFromDate:[NSDate date] formatter:@"yyyy-MM-dd"];
    _page = 0;
}
- (void)itemBTclick:(UIButton *)sender
{
    NSLog(@"");
    if (isShow == NO) {
        [UIView animateWithDuration:0.2 animations:^{
            imageV.image = [UIImage imageNamed:@"上"];
            [itemBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
            popView.hidden = NO;

        } completion:^(BOOL finished) {

        }];
        isShow = YES;
    }else{
        [UIView animateWithDuration:0.2 animations:^{
            imageV.image = [UIImage imageNamed:@"下"];
            [itemBT setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
            popView.hidden = YES;

        } completion:^(BOOL finished) {

        }];
        isShow = NO;
    }
}

- (void)loadsView {
    UIView *HeadBackView = [[UIView alloc] initWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, 44)];
    HeadBackView.backgroundColor = [UIColor colorWithString:@"#291744"];
    [self.view addSubview:HeadBackView];
        
    itemBT = [UIButton new];
    [itemBT setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
    [itemBT setTitle:@"账单类别" forState:UIControlStateNormal];
    itemBT.titleLabel.font = NewFont(15);
    itemBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    NewTouchUpInside(itemBT, itemBTclick:);
    [HeadBackView addSubview:itemBT];

    itemBT.sd_layout
    .leftSpaceToView(HeadBackView, 15)
    .centerYEqualToView(HeadBackView)
    .widthIs(65)
    .heightIs(44);
    
    imageV = [UIImageView new];
    [imageV setImage:NewImageNamed(@"下")];
    [HeadBackView addSubview:imageV];

    imageV.sd_layout
    .widthIs(6.5)
    .heightIs(4)
    .leftSpaceToView(itemBT, 3)
    .centerYEqualToView(HeadBackView);
    
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader+44, SCREEN_WIDTH, SCREEN_HEIGHT-44-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;

    popView = [[UIView alloc] initWithFrame:CGRectMake(0, NavHeader+44, SCREEN_WIDTH, SCREEN_HEIGHT-44)];
    popView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    [self.view addSubview:popView];
    if (isShow == NO) {
        popView.hidden = YES;
    }else{
        popView.hidden = NO;
    }
    
    UIView *popView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 240)];
    popView2.backgroundColor = [UIColor colorWithString:@"#291744"];
    [popView addSubview:popView2];
    
    NSArray *arr = @[@"充值",@"取款",@"游戏"];
    for (int i = 0; i<arr.count; i++) {
        // 圆角按钮
        UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        mapBtn.tag = i;//这句话不写等于废了
        CGFloat btwidth = (SCREEN_WIDTH-60)/3;
        mapBtn.frame = CGRectMake(i*(btwidth+15)+15, 10, btwidth, 40);
        [mapBtn setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
        ViewBorderRadius(mapBtn, 2, 0, NewClearColor);
        [mapBtn setTitle:arr[i] forState:UIControlStateNormal];
        [mapBtn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
        mapBtn.titleLabel.font = NewFont(15);
        mapBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        if (i == 0) {
            NSLog(@"1");
            ViewBorderRadius(mapBtn, 5, 1, [UIColor colorWithString:@"#FF6D44"]);
            [mapBtn setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
            trans_type = @"deposit";
        } else {
            NSLog(@"1");
             ViewBorderRadius(mapBtn, 5, 0, NewClearColor);
            [mapBtn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
        }
        [popView2 addSubview:mapBtn];
        mapBtn.tag = i;
//        //按钮点击方法
        [mapBtn addTarget:self action:@selector(mapBtnClick11:) forControlEvents:UIControlEventTouchUpInside];
        [_btnMutableArray2 addObject:mapBtn];
    }
    
    UILabel *timeLB = [UILabel new];
    timeLB.text = @"时间:";
    timeLB.font = NewFont(15);
    [timeLB setSingleLineAutoResizeWithMaxWidth:0];
    timeLB.textColor = [UIColor colorWithString:@"AFACB4"];
    [popView2 addSubview:timeLB];
    
    timeLB.sd_layout
    .heightIs(40)
    .topSpaceToView(popView2, 60)
    .leftSpaceToView(popView2, 15);
    
    startBT = [UIButton new];

    [startBT setTitle:time_from forState:UIControlStateNormal];
    [startBT setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
    [startBT setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
    startBT.titleLabel.font = NewFont(15);
    startBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    startBT.tag = 100;
    NewTouchUpInside(startBT, timeClick:);
    [popView2 addSubview:startBT];
    
    startBT.sd_layout
    .widthIs((SCREEN_WIDTH-45)/2)
    .heightIs(35)
    .leftSpaceToView(popView2, 15)
    .topSpaceToView(timeLB, 10);
    
    endBT = [UIButton new];
    [endBT setTitle:[NSDate stringFromDate:[NSDate date] formatter:@"yyyy-MM-dd"] forState:UIControlStateNormal];
    [endBT setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
    [endBT setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
    endBT.titleLabel.font = NewFont(15);
    endBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    ViewRadius(endBT, 5);
    endBT.tag = 101;
    NewTouchUpInside(endBT, timeClick:);
    [popView2 addSubview:endBT];
    
    endBT.sd_layout
    .widthIs((SCREEN_WIDTH-45)/2)
    .heightIs(35)
    .leftSpaceToView(startBT, 15)
    .topSpaceToView(timeLB, 10);
    
    UIButton *ChaxunBT = [UIButton new];
    [ChaxunBT setTitle:@"查询" forState:UIControlStateNormal];
    [ChaxunBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    [ChaxunBT setBackgroundColor:[UIColor colorWithString:@"##FF6D44"]];
    ChaxunBT.titleLabel.font = NewFont(15);
    ChaxunBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    ViewRadius(ChaxunBT, 5);
    NewTouchUpInside(ChaxunBT, ChaxunBTclick:);
    [popView2 addSubview:ChaxunBT];
    
    ChaxunBT.sd_layout
    .topSpaceToView(endBT, 25)
    .heightIs(44)
    .leftSpaceToView(popView2, 30)
    .rightSpaceToView(popView2, 30);
}
//存款银行卡接口
- (void)ChaxunBTclick:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^{
        imageV.image = [UIImage imageNamed:@"下"];
        [itemBT setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
        popView.hidden = YES;

    } completion:^(BOOL finished) {

    }];
    isShow = NO;
    [self NetworkRequestManagergetPlayerReports];
    
}

- (void)NetworkRequestManagergetPlayerReports
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    
    [dic setValue:trans_type forKey:@"trans_type"];//查詢種類
    [dic setValue:time_from forKey:@"time_from"];//開始時間
    [dic setValue:[NSString stringWithFormat:@"%@ 23:59:59",time_to] forKey:@"time_to"];//結束時間
    [dic setValue:@"20" forKey:@"limit"];
    [dic setValue:[NSString stringWithFormat:@"%ld",self.page] forKey:@"offset"];
    [dic setValue:@"" forKey:@"include_transfers"];
    [dic setValue:@"" forKey:@"game_platform_id"];
    __weak __typeof(self) mySelf = self;
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:getPlayerReports parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            [SVProgressHUD dismiss];
            [newtableView.mj_header endRefreshing];
            [newtableView.mj_footer endRefreshing];
        }else{
            if (mySelf.page == 0) {
                [dataArray removeAllObjects];
                dataArray = [NSMutableArray arrayWithArray:requestData[@"result"]];
            }else {
                [dataArray addObjectsFromArray:requestData[@"result"]];
            }
            [newtableView reloadData];
            [SVProgressHUD dismiss];
            [newtableView.mj_header endRefreshing];
            [newtableView.mj_footer endRefreshing];

        }
    } onFailure:^{
        [SVProgressHUD dismiss];
        [newtableView.mj_header endRefreshing];
        [newtableView.mj_footer endRefreshing];
    }];
}
#pragma mark  ------ 转账金额选择 -------
- (void)mapBtnClick11:(UIButton *)sender {
    for (UIButton *btn in _btnMutableArray2) {
        if (btn.tag == sender.tag) {
            NSLog(@"1");
            ViewBorderRadius(btn, 5, 1, [UIColor colorWithString:@"#FF6D44"]);
            [btn setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
        } else {
            NSLog(@"1");
             ViewBorderRadius(btn, 5, 0, NewClearColor);
            [btn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
        }
    }
    if ([sender.titleLabel.text isEqualToString:@"充值"]) {
        trans_type = @"deposit";
    }else if ([sender.titleLabel.text isEqualToString:@"取款"]){
        trans_type = @"withdrawal";
    }else if ([sender.titleLabel.text isEqualToString:@"游戏"]){
        trans_type = @"game";
    }
    self.page = 0;
}

- (void)timeClick:(UIButton *)sender {
    if (sender.tag == 100) {
       ZHG_ToolBarDatePickerView *view = [[ZHG_ToolBarDatePickerView alloc] initWithFrame:[UIScreen mainScreen].bounds];
       
       view.datePickerType = ZHG_CustomDatePickerView_Type_YearMonthDay;
        view.defaultDate = [NSDate dateFromString:@"2019-01-01" formatter:@"yyyy-MM-dd"];

       view.DatePickerSelectedBlock = ^(NSString *selectString, NSDate *selectedDate) {
           NSLog(@"这是选择的日期转化的字符串：%@",selectString);
//           NSString *string = [NSDate stringFromDate:selectedDate formatter:@"yyyy-MM-dd HH:mm:ss"];
//           NSLog(@"%@",string);
           [startBT setTitle:selectString forState:UIControlStateNormal];
           time_from = selectString;
       };
       
       [view show];
    }else{
               ZHG_ToolBarDatePickerView *view = [[ZHG_ToolBarDatePickerView alloc] initWithFrame:[UIScreen mainScreen].bounds];
               
               view.datePickerType = ZHG_CustomDatePickerView_Type_YearMonthDay;
                view.maxDate = [NSDate date];

               view.DatePickerSelectedBlock = ^(NSString *selectString, NSDate *selectedDate) {
                   NSLog(@"这是选择的日期转化的字符串：%@",selectString);
                   NSString *string = [NSDate stringFromDate:selectedDate formatter:@"yyyy-MM-dd HH:mm:ss"];
                   NSLog(@"%@",string);
                   [endBT setTitle:selectString forState:UIControlStateNormal];
                   time_to = selectString;
               };
               [view show];
    }
    self.page = 0;
}

#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    static NSString *Cell11 = @"Cell11";

    if ([trans_type isEqualToString:@"game"]) {
        NewgameTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell11];
        if (cell == nil) {
            cell = [[NewgameTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell11];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
        if (dataArray.count>0) {
            [cell assignment:dataArray[indexPath.row]];
        }
        return cell;
    }else{
        NewBillingrecordsCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
        if (cell == nil) {
            cell = [[NewBillingrecordsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
        if (dataArray.count>0) {
            [cell assignment:dataArray[indexPath.row] trans_type:trans_type];
        }
        return cell;
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (dataArray.count>0) {
        NSLog(@"复制");
        
        UIPasteboard *pab = [UIPasteboard generalPasteboard];

        pab.string = dataArray[indexPath.row][@"secure_id"];

        if (pab == nil) {
            [SVProgressHUD showErrorWithStatus:@"复制失败"];
            
        } else {
            [SVProgressHUD showSuccessWithStatus:@"已复制"];
        }
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *HeadBackView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    HeadBackView1.backgroundColor = [UIColor colorWithString:@"#230F40"];
    NSArray *array = [NSArray new];
    if (![trans_type isEqualToString:@"game"]) {
        array = @[@"时间",@"订单编号",@"状态",@"金额"];
        for (int i = 0; i<4; i++) {
            UILabel *title = [UILabel new];
            [title setText:array[i]];
            title.font = NewFont(14);
            title.textColor = [UIColor colorWithString:@"#AFACB4"];
            title.frame = CGRectMake(i*(SCREEN_WIDTH/4), 0, SCREEN_WIDTH/4, 44);
            title.textAlignment = NSTextAlignmentCenter;
            [HeadBackView1 addSubview:title];
        }
    }else{
        array = @[@"游戏平台",@"投注金额",@"结果"];
        for (int i = 0; i<3; i++) {
            UILabel *title = [UILabel new];
            [title setText:array[i]];
            title.font = NewFont(14);
            title.textColor = [UIColor colorWithString:@"#AFACB4"];
            title.frame = CGRectMake(i*(SCREEN_WIDTH/3), 0, SCREEN_WIDTH/3, 44);
            title.textAlignment = NSTextAlignmentCenter;
            [HeadBackView1 addSubview:title];
        }
    }
    return HeadBackView1;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

@end
