//
//  NewWithdrawalViewController.m
//  NewProject
//
//  Created by mac on 2019/11/18.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewWithdrawalViewController.h"
#import "NewHeaderScrollView.h"
#import "NewAddBankViewController.h"
@interface NewWithdrawalViewController ()<UIScrollViewDelegate>{
    UITextField *mimatextfield;
    NSMutableArray *dataArray;
    NSMutableDictionary *bankInfoDic;
    NSString *amount;
    NSString *withdrawal_password;
    NSMutableArray *NewdataArray;
    UITextField *Zhuangzhangtextfield;

    
}
@property (nonatomic, strong) NewHeaderScrollView *chongwuScrollView;

@end

@implementation NewWithdrawalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];//键盘的弹出

     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self setNavTitle:@"提现"];
    [self loadData];
    [self NetworkRequestManagerqueryPlayerBalance];
    [self NetworkRequestManagerlistPlayerWithdrawAccounts];


    
}
- (void)NetworkRequestManagerqueryPlayerBalance {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:@"" forKey:@"refresh"];
//    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:queryPlayerBalance parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [NewdataArray removeAllObjects];
            [NewdataArray addObject:requestData[@"result"]];
            Zhuangzhangtextfield.text = [NSString stringWithFormat:@"%.f",[NewdataArray[0][@"mainwallet"] floatValue]+[NewdataArray[0][@"subwallets"][@"5593"] floatValue]];
            
//            [SVProgressHUD dismiss];
        }
    } onFailure:^{
//        [SVProgressHUD dismiss];
    }];
}
//键盘的消失

//弹出键盘UIView

-(void)transformView:(NSNotification*)aNSNotification{



    //获取键盘弹出前的Rect

    NSValue*keyBoardBeginBounds=[[aNSNotification userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];

    CGRect beginRect=[keyBoardBeginBounds CGRectValue];



    //获取键盘弹出后的Rect

    NSValue*keyBoardEndBounds=[[aNSNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];

    CGRect  endRect=[keyBoardEndBounds CGRectValue];



    //获取键盘位置变化前后纵坐标Y的变化值

    CGFloat deltaY=endRect.origin.y-beginRect.origin.y;

    NSLog(@"看看这个变化的Y值:%f",deltaY);



    //在0.25s内完成self.view的Frame的变化，等于是给self.view添加一个向上移动deltaY的动画

    [UIView animateWithDuration:0.25f animations:^{

        self.view.frame = CGRectMake(0, -50, kScreenWidth, kScreenHeight);

    }];



}
//退出键盘UIView

-(void)keyboardWillHide:(NSNotification*)aNSNotification{



    [UIView beginAnimations:nil context:nil];



    [UIView setAnimationDuration:0.25];



    [UIView setAnimationCurve:7];



    self.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);



    [UIView commitAnimations];



}


- (void)NetworkRequestManagerlistPlayerWithdrawAccounts
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:listPlayerWithdrawAccounts parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0 && [requestData[@"code"] integerValue] != 298) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [dataArray removeAllObjects];
            for (NSDictionary *dic in requestData[@"result"]) {
                [dataArray addObject:dic];
            }
            [self loadUI];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)loadUI {
    UILabel *daozhangLB = [UILabel new];
    daozhangLB.text = @"到账银行卡";
    daozhangLB.font = NewFont(14);
    daozhangLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [daozhangLB setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:daozhangLB];
    
    daozhangLB.sd_layout
    .heightIs(15)
    .topSpaceToView(self, NavHeader+20)
    .leftSpaceToView(self, 15);
    
    CGFloat chongwuWidth = SCREEN_WIDTH/1.25;
    self.chongwuScrollView = [[NewHeaderScrollView alloc] init];
    self.chongwuScrollView.frame = CGRectMake(0, NavHeader+40+15, SCREEN_WIDTH, 150);
    self.chongwuScrollView.contentSize = CGSizeMake(dataArray.count*(chongwuWidth+15)+15,0);
    self.chongwuScrollView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.chongwuScrollView.delegate = self;

    [self.view addSubview:self.chongwuScrollView];
    
    if (dataArray.count>0) {
        for (int i = 0; i<dataArray.count; i++) {
            UIButton *btn = [UIButton new];
            btn.frame = CGRectMake(i*(chongwuWidth+15)+15, 0, chongwuWidth, 150);
            btn.tag = i+100;
//            [btn setBackgroundColor:[UIColor colorWithString:@"#291744"]];
            [btn setBackgroundImage:NewImageNamed(@"通用底图") forState:UIControlStateNormal];
//            NewTouchUpInside(btn, XZchongwutouchAction:);
            ViewRadius(btn, 3);
            [self.chongwuScrollView addSubview:btn];
//            if (i == 0) {
//                btn.selected = YES;
//                ViewBorderRadius(btn, 0, 0.8, NewRedColor);
//                bankInfoDic = [NSMutableDictionary dictionaryWithDictionary:dataArray[i]];
//            }
            CGFloat zhLBWidth = [NewUtils heightforString:@"账号：" andHeight:15 fontSize:16];
            UILabel *zhanghaoLB = [UILabel new];
            zhanghaoLB.frame = CGRectMake(18, 26.25, zhLBWidth, 15);
            zhanghaoLB.textColor = NewWhiteColor;
            zhanghaoLB.font = NewFont(16);
            zhanghaoLB.text = @"账号：";
            [btn addSubview:zhanghaoLB];
            
            NSString *bankNumber = [NewUtils getNewStarBankNumWitOldNum:dataArray[i][@"bankAccountNumber"]];
            
            CGFloat zhLBWidth1 = [NewUtils heightforString:bankNumber andHeight:15 fontSize:16];
            UILabel *zhanghaoLB1 = [UILabel new];
            zhanghaoLB1.frame = CGRectMake(18+zhLBWidth, 26.25, zhLBWidth1, 15);
            zhanghaoLB1.textColor = NewWhiteColor;
            zhanghaoLB1.font = NewFont(16);
            zhanghaoLB1.text = bankNumber;
            [btn addSubview:zhanghaoLB1];
            
            CGFloat zhmLBWidth = [NewUtils heightforString:@"账户名：" andHeight:15 fontSize:16];
            UILabel *zhanghumingLB = [UILabel new];
            zhanghumingLB.frame = CGRectMake(18, 67.5, zhmLBWidth, 15);
            zhanghumingLB.textColor = NewWhiteColor;
            zhanghumingLB.font = NewFont(16);
            zhanghumingLB.text = @"账户名：";
            [btn addSubview:zhanghumingLB];
            
            CGFloat zhmLBWidth1 = [NewUtils heightforString:dataArray[i][@"bankAccountFullName"] andHeight:15 fontSize:16];
            UILabel *zhanghumingLB1 = [UILabel new];
            zhanghumingLB1.frame = CGRectMake(18+zhmLBWidth, 67.5, zhmLBWidth1, 15);
            zhanghumingLB1.textColor = NewWhiteColor;
            zhanghumingLB1.font = NewFont(16);
            zhanghumingLB1.text = dataArray[i][@"bankAccountFullName"];
            [btn addSubview:zhanghumingLB1];
            
            CGFloat yhmcLBWidth = [NewUtils heightforString:@"银行名称：" andHeight:15 fontSize:16];
            UILabel *yhmcLB = [UILabel new];
            yhmcLB.frame = CGRectMake(18, 108.75, yhmcLBWidth, 15);
            yhmcLB.textColor = NewWhiteColor;
            yhmcLB.font = NewFont(16);
            yhmcLB.text = @"银行名称：";
            [btn addSubview:yhmcLB];
            
            NSString *jsonStr1 = [dataArray[i][@"bankName"] stringByReplacingOccurrencesOfString:@"_json:" withString:@""]; // 去掉空格
            NSData *data = [jsonStr1 dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
            
            CGFloat yhmcLBWidth1 = [NewUtils heightforString:json[@"2"] andHeight:15 fontSize:16];
            UILabel *yhmcLB1 = [UILabel new];
            yhmcLB1.frame = CGRectMake(18+yhmcLBWidth, 108.75, yhmcLBWidth1, 15);
            yhmcLB1.textColor = NewWhiteColor;
            yhmcLB1.font = NewFont(16);
            yhmcLB1.text = json[@"2"];
            [btn addSubview:yhmcLB1];
            
            bankInfoDic = [NSMutableDictionary dictionaryWithDictionary:dataArray.firstObject];
        }
    }else{
        UILabel *tishiLB = [[UILabel alloc] initWithFrame:CGRectMake(0, NavHeader+40+15+20, SCREEN_WIDTH, 20)];
        tishiLB.text = @"暂无取款银行卡,请先到银行账户添加!";
        tishiLB.font = NewFont(16);
        tishiLB.textColor = NewWhiteColor;
        tishiLB.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:tishiLB];
        
        UIButton *addBank = [UIButton new];
        [addBank setTitle:@"+ 添加资金账户" forState:UIControlStateNormal];
        [addBank setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
        addBank.titleLabel.font = NewFont(15);
        [addBank setBackgroundColor:[UIColor colorWithString:@"#230F40"]];
        ViewBorderRadius(addBank, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
        NewTouchUpInside(addBank, addBankclick);
        [self.view addSubview:addBank];
        
        addBank.sd_layout
        .heightIs(44)
        .topSpaceToView(tishiLB, 15)
        .leftSpaceToView(self.view, 20)
        .rightSpaceToView(self.view, 20);
        
    }
    
    UILabel *tishiLB = [UILabel new];
    tishiLB.text = @"单笔限额￥10-￥100000";
    tishiLB.font = NewFont(14);
    [tishiLB setSingleLineAutoResizeWithMaxWidth:0];
    tishiLB.textColor = [UIColor colorWithString:@"#7E7B83"];
//    [self.view addSubview:tishiLB];
    
    tishiLB.sd_layout
    .heightIs(15)
    .topSpaceToView(self.chongwuScrollView, 20)
    .leftSpaceToView(self.view, 15);
    
    UILabel *moenyLB = [UILabel new];
        moenyLB.text = @"￥";
       moenyLB.font = NewFont(17);
       [moenyLB setSingleLineAutoResizeWithMaxWidth:0];
       moenyLB.textColor = [UIColor colorWithString:@"#AFACB4"];
       [self.view addSubview:moenyLB];
       
       moenyLB.sd_layout
       .topSpaceToView(self.chongwuScrollView, 40)
       .leftSpaceToView(self.view, 40)
       .heightIs(15);
       
       UILabel *line1 = [UILabel new];
       line1.backgroundColor = [UIColor colorWithString:@"#483465"];
       [self.view addSubview:line1];
       
       line1.sd_layout
       .leftSpaceToView(self.view, 55)
       .rightSpaceToView(self.view, 55)
       .heightIs(1)
       .topSpaceToView(moenyLB, 10);
    
        Zhuangzhangtextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#230F40"] textColor:NewWhiteColor placeholder:@"请输入金额" hidden:NO tag:100 font:NewFont(20) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [Zhuangzhangtextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    Zhuangzhangtextfield.borderStyle = UITextBorderStyleRoundedRect;
//        [Zhuangzhangtextfield setValue:[UIColor colorWithString:@"#AFACB4"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr1 = [[NSMutableAttributedString alloc]initWithString:Zhuangzhangtextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    Zhuangzhangtextfield.attributedPlaceholder = arrStr1;
        [self.view addSubview:Zhuangzhangtextfield];

        Zhuangzhangtextfield.sd_layout
        .leftSpaceToView(self.view, 58)
        .bottomSpaceToView(line1, 1)
        .heightIs(30)
        .rightEqualToView(line1);
    
    UILabel *tishiLB1 = [UILabel new];
    tishiLB1.text = @"输入密码";
    tishiLB1.font = NewFont(14);
    [tishiLB1 setSingleLineAutoResizeWithMaxWidth:0];
    tishiLB1.textColor = [UIColor colorWithString:@"#7E7B83"];
    [self.view addSubview:tishiLB1];
    
    tishiLB1.sd_layout
    .heightIs(15)
    .topSpaceToView(line1, 27.5)
    .leftSpaceToView(self.view, 15);
    
    UILabel *line2 = [UILabel new];
    line2.backgroundColor = [UIColor colorWithString:@"#483465"];
    [self.view addSubview:line2];
    
    line2.sd_layout
    .leftSpaceToView(self.view, 55)
    .rightSpaceToView(self.view, 55)
    .heightIs(1)
    .topSpaceToView(tishiLB1, 50);
    
            mimatextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#230F40"] textColor:NewWhiteColor placeholder:@"请输入密码" hidden:NO tag:101 font:NewFont(20) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
            [mimatextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
        //    Zhuangzhangtextfield.borderStyle = UITextBorderStyleRoundedRect;
            mimatextfield.text = @"";
    NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:mimatextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    mimatextfield.attributedPlaceholder = arrStr;
            [self.view addSubview:mimatextfield];
            
            mimatextfield.sd_layout
            .leftSpaceToView(self.view, 58)
            .bottomSpaceToView(line2, 1)
            .heightIs(30)
            .rightEqualToView(line2);
    
    UIButton *cunkuanBT = [UIButton new];
    [cunkuanBT setTitle:@"提现" forState:UIControlStateNormal];
    [cunkuanBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];

    if (dataArray.count>0) {
        [cunkuanBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    }else{
        [cunkuanBT setBackgroundColor:[UIColor colorWithString:@"#666666"]];
    }
    cunkuanBT.titleLabel.font = NewFont(15);
    ViewRadius(cunkuanBT, 3);
    NewTouchUpInside(cunkuanBT, cunkuanBTclick:);
    [self.view addSubview:cunkuanBT];

    cunkuanBT.sd_layout
    .heightIs(44)
    .widthIs(250)
    .leftSpaceToView(self.view, (SCREEN_WIDTH-250)/2)
    .topSpaceToView(line2, 55);
    
        
}

- (void)cunkuanBTclick:(UIButton *)sender {
    if (dataArray.count>0) {
        [self NetworkRequestManagertransfer];
    }else{
        [SVProgressHUD showErrorWithStatus:@"请先添加银行卡"];
    }
}

- (void)NetworkRequestManagertransfer {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
//    [dic setValue:@"" forKey:@"transfer_to"];//目的錢包代號，可以在queryPlayerBalance 的結果中取得。0 表示主錢包
//    [dic setValue:@"" forKey:@"transfer_from"];//(選用) 來源錢包代號
//    [dic setValue:@"" forKey:@"amount"];//(選用) 金額。省略時自動轉入全部餘額
    [NetworkRequestManager requestPostWithInterfacePrefix:transfer parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
//            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
//            [SVProgressHUD showSuccessWithStatus:requestData[@"message"]];
            [self NetworkRequestManagermanualWithdraw];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}

- (void)NetworkRequestManagermanualWithdraw {
    
    
     NSMutableDictionary *dic = NewMutableDictionaryInit;
        [dic setValue:ApiKey forKey:@"api_key"];
        [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
        [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
        float aaaa = [Zhuangzhangtextfield.text floatValue];
        NSNumber * Membership_Id =  [NSNumber numberWithInt:aaaa];
        [dic setValue:Membership_Id forKey:@"amount"];//金額
    if ([Membership_Id integerValue]<100) {
        [SVProgressHUD showErrorWithStatus:@"最低提现金额不能小于100"];
        return;
    }
        [dic setValue:withdrawal_password forKey:@"withdrawal_password"];//玩家取款密碼，須在後台設定。
        [dic setValue:bankInfoDic[@"playerBankDetailsId"] forKey:@"bankDetailsId"];//玩家現有取款帳戶ID ，必須符合listPlayerWithdrawAccounts 回報的playerBankDetailsId
    //    [dic setValue:bankInfoDic[@"bankTypeId"] forKey:@"bankTypeId"];//開戶銀行ID ，必須符合queryDepositWithdrawalAvailableBank 回報的bankTypeId
    //    [dic setValue:@"" forKey:@"bankAccName"];//銀行帳戶名。必須符合玩家真實姓名
    //    [dic setValue:@"" forKey:@"bankAccNum"];//銀行帳號
    //    [dic setValue:@"" forKey:@"branch"];//支行名稱
    //    [dic setValue:@"" forKey:@"city"];//支行城市
    //    [dic setValue:@"" forKey:@"province"];//支行省份
        [SVProgressHUD show];
        [NetworkRequestManager requestPostWithInterfacePrefix:manualWithdraw parameters:dic onSuccess:^(id requestData) {
            if ([requestData[@"code"] integerValue] != 0) {
                if ([requestData[@"code"] integerValue] == 318) {
                    [SVProgressHUD showErrorWithStatus:@"账户余额不足"];
                }else if ([requestData[@"code"] integerValue] == 306){
                    [SVProgressHUD showErrorWithStatus:@"玩家還沒有任何取款銀行卡"];
                }else if ([requestData[@"code"] integerValue] == 307){
                    [SVProgressHUD showErrorWithStatus:@"依照KYC 規則，限制高風險玩家取款。"];
                }else if ([requestData[@"code"] integerValue] == 308){
                    [SVProgressHUD showErrorWithStatus:@"取款密碼錯誤"];
                }else if ([requestData[@"code"] integerValue] == 309){
                    [SVProgressHUD showErrorWithStatus:@"單筆取款金額超過上限"];
                }else if ([requestData[@"code"] integerValue] == 310){
                    [SVProgressHUD showErrorWithStatus:@"單筆取款金額低於下限"];
                }else if ([requestData[@"code"] integerValue] == 311){
                    [SVProgressHUD showErrorWithStatus:@"每日累積取款金額超過上限"];
                }else if ([requestData[@"code"] integerValue] == 312){
                    [SVProgressHUD showErrorWithStatus:@"每日累積取款次數超過上限"];
                }else if ([requestData[@"code"] integerValue] == 313){
                    [SVProgressHUD showErrorWithStatus:@"流水不足，不能取款"];
                }else if ([requestData[@"code"] integerValue] == 314){
                    [SVProgressHUD showErrorWithStatus:@"還沒有達到取款條件要求的流水，不能取款"];
                }else if ([requestData[@"code"] integerValue] == 315){
                    [SVProgressHUD showErrorWithStatus:@"受VIP 群組規則限制，同時只能發起一筆取款"];
                }else if ([requestData[@"code"] integerValue] == 316){
                    [SVProgressHUD showErrorWithStatus:@"從子錢包轉回主錢包失敗，停止操作"];
                }else if ([requestData[@"code"] integerValue] == 317){
                    [SVProgressHUD showErrorWithStatus:@"套用優惠取款限制失敗，停止操作"];
                }else if ([requestData[@"code"] integerValue] == 319){
                    [SVProgressHUD showErrorWithStatus:@"其他原因造成取款失敗"];
                }else{
                    [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
                }
            }else{
                [SVProgressHUD showSuccessWithStatus:requestData[@"message"]];
            }
        } onFailure:^{
            [SVProgressHUD dismiss];
        }];
}

#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    if (textField.tag == 100) {
        amount = textField.text;
    }else if (textField.tag == 101){
        withdrawal_password = textField.text;
    }
    NSLog(@"%@",textField.text);
}
- (void)loadData {
    dataArray = NewMutableArrayInit;
    bankInfoDic = NewMutableDictionaryInit;
    NewdataArray = NewMutableArrayInit;
}
//- (void)XZchongwutouchAction:(UIButton *)sender {
//    for (int i = 0; i<dataArray.count; i++) {
//        UIButton *btn = [self.chongwuScrollView viewWithTag:i + 100];
//        btn.selected = NO;
//        ViewBorderRadius(btn, 0, 0.8, NewClearColor);
//    }
//       sender.selected = YES;
//       ViewBorderRadius(sender, 0, 0.8, NewRedColor);
//
//    bankInfoDic = [NSMutableDictionary dictionaryWithDictionary:dataArray[sender.tag-100]];
//}

- (void)addBankclick{
    NewAddBankViewController *vc = [NewAddBankViewController new];
    vc.Str = @"取款银行卡";
    vc.bankcardYesOrNo = YES;//没绑卡
    [vc setSuccess:^(NSString *string) {
        if ([string isEqualToString:@"取款银行卡"]) {
            [self NetworkRequestManagerlistPlayerWithdrawAccounts];
        }
    }];
    NewPushViewController(vc);
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 停止类型1、停止类型2
    BOOL scrollToScrollStop = !scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
    if (scrollToScrollStop) {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        bankInfoDic = [NSMutableDictionary dictionaryWithDictionary:dataArray[page]];
        CGFloat chongwuWidth = SCREEN_WIDTH/1.25;
        [scrollView setContentOffset:CGPointMake(page*(chongwuWidth+15)+15, 0) animated:YES];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        // 停止类型3
        BOOL dragToDragStop = scrollView.tracking && !scrollView.dragging && !scrollView.decelerating;
        if (dragToDragStop) {
            CGFloat pageWidth = scrollView.frame.size.width;
            int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            NSLog(@"%d",page);
            bankInfoDic = [NSMutableDictionary dictionaryWithDictionary:dataArray[page]];
            CGFloat chongwuWidth = SCREEN_WIDTH/1.25;
            [scrollView setContentOffset:CGPointMake(page*(chongwuWidth+15)+15, 0) animated:YES];
        }
    }
}



@end
