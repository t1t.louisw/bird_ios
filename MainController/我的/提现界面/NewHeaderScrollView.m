//
//  NewHeaderScrollView.m
//  SSTUser
//
//  Created by 罗云飞 on 2019/4/29.
//  Copyright © 2019 iMac. All rights reserved.
//

#import "NewHeaderScrollView.h"
@interface NewHeaderScrollView (){
}

@end
@implementation NewHeaderScrollView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
//        self.contentSize = CGSizeMake(imageW, imageH);
        self.showsHorizontalScrollIndicator = NO;//不显示水平拖地的条
        self.showsVerticalScrollIndicator=NO;//不显示垂直拖动的条
        self.pagingEnabled = YES;//允许分页滑动
        self.bounces = NO;//到边了就不能再拖地

    }
    return self;
}

@end
