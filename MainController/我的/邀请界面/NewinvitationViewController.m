//
//  NewinvitationViewController.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewinvitationViewController.h"

@interface NewinvitationViewController (){
    UILabel *invitecodeLB;
    UILabel *invitecodeLB1;
}

@end

@implementation NewinvitationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"我的邀请码"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self dataInitialization];
    [self loadsView];
    [self RequestManagerUserInfo];
}

- (void)dataInitialization {
    
}
- (void)loadsView {
    UIImageView *imageV = [UIImageView new];
    [imageV setImage:NewImageNamed(@"邀请码")];
    imageV.userInteractionEnabled = YES;
    [self.view addSubview:imageV];
    
    imageV.sd_layout
    .widthIs(SCREEN_WIDTH-30)
    .centerXEqualToView(self.view)
    .topSpaceToView(self.view, NavHeader+30)
    .heightIs(SCREEN_WIDTH-30+15);
    
    UILabel *tishiLB = [UILabel new];
    tishiLB.text = @"您的邀请码";
    tishiLB.textColor = [UIColor colorWithString:@"#8C74B1"];
    tishiLB.font = NewFont(14);
    [tishiLB setSingleLineAutoResizeWithMaxWidth:0];
    [imageV addSubview:tishiLB];
    
    tishiLB.sd_layout
    .heightIs(15)
    .centerXEqualToView(imageV)
    .topSpaceToView(imageV, 55);
    
    invitecodeLB = [UILabel new];
    invitecodeLB.textColor = [UIColor colorWithString:@"#E6E2ED"];
    invitecodeLB.font = NewFont(30);
    [invitecodeLB setSingleLineAutoResizeWithMaxWidth:0];
    [imageV addSubview:invitecodeLB];
    
    invitecodeLB.sd_layout
    .heightIs(25)
    .centerXEqualToView(imageV)
    .topSpaceToView(tishiLB, 25);
    
    UIButton *fuzhimaBT = [UIButton new];
    [fuzhimaBT setTitle:@"复制邀请码" forState:UIControlStateNormal];
    [fuzhimaBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    fuzhimaBT.titleLabel.font = NewFont(15);
    NewTouchUpInside(fuzhimaBT, fuzhimaBTclick);
    [imageV addSubview:fuzhimaBT];
    
    fuzhimaBT.sd_layout
    .heightIs(15)
    .widthIs(80)
    .centerXEqualToView(imageV)
    .topSpaceToView(invitecodeLB, 25);
    
    UILabel *line = [UILabel new];
    line.backgroundColor = [UIColor colorWithString:@"#FF6D44"];
    [imageV addSubview:line];
    
    line.sd_layout
    .widthRatioToView(fuzhimaBT, 1)
    .centerXEqualToView(fuzhimaBT)
    .topSpaceToView(fuzhimaBT, 2)
    .heightIs(1);
    
    invitecodeLB1 = [UILabel new];
    invitecodeLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
    invitecodeLB1.font = NewFont(14);
//    [invitecodeLB1 setSingleLineAutoResizeWithMaxWidth:0];
//    invitecodeLB1.numberOfLines = 2;
    invitecodeLB1.textAlignment = NSTextAlignmentCenter;
    [imageV addSubview:invitecodeLB1];
    
    invitecodeLB1.sd_layout
    .autoHeightRatio(0)
    .centerXEqualToView(imageV)
    .leftSpaceToView(imageV, 15)
    .rightSpaceToView(imageV, 15)
    .bottomSpaceToView(imageV, (SCREEN_WIDTH-15)/3);
    
    UIButton *fuzhimaBT1 = [UIButton new];
    [fuzhimaBT1 setTitle:@"复制链接" forState:UIControlStateNormal];
    [fuzhimaBT1 setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    fuzhimaBT1.titleLabel.font = NewFont(15);
    NewTouchUpInside(fuzhimaBT1, fuzhimaBTclick1);
    [imageV addSubview:fuzhimaBT1];
       
    fuzhimaBT1.sd_layout
    .heightIs(15)
    .widthIs(64)
    .centerXEqualToView(imageV)
    .topSpaceToView(invitecodeLB1, 25);
    
    UILabel *line1 = [UILabel new];
    line1.backgroundColor = [UIColor colorWithString:@"#FF6D44"];
    [imageV addSubview:line1];
    
    line1.sd_layout
    .widthRatioToView(fuzhimaBT1, 1)
    .centerXEqualToView(fuzhimaBT)
    .topSpaceToView(fuzhimaBT1, 2)
    .heightIs(1);
    
}

- (void)fuzhimaBTclick{
    NSLog(@"复制");
    
    UIPasteboard *pab = [UIPasteboard generalPasteboard];

    pab.string = invitecodeLB.text;

    if (pab == nil) {
        [SVProgressHUD showErrorWithStatus:@"复制失败"];
        
    } else {
        [SVProgressHUD showSuccessWithStatus:@"已复制"];
    }
}
- (void)fuzhimaBTclick1{
    NSLog(@"复制");
    
    UIPasteboard *pab = [UIPasteboard generalPasteboard];

    pab.string = invitecodeLB1.text;

    if (pab == nil) {
        [SVProgressHUD showErrorWithStatus:@"复制失败"];
        
    } else {
        [SVProgressHUD showSuccessWithStatus:@"已复制"];
    }
}
- (void)RequestManagerUserInfo{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:getPlayerProfile parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            invitecodeLB.text = requestData[@"result"][@"referral_code"];
            invitecodeLB1.text = [NSString stringWithFormat:@"http://player.bird10.com/player_center/iframe_register?referralcode=%@",requestData[@"result"][@"referral_code"]];

            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
@end
