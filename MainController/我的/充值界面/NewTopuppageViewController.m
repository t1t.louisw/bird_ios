//
//  NewTopuppageViewController.m
//  NewProject
//
//  Created by mac on 2019/11/15.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewTopuppageViewController.h"
#define Start_X          15.0f      // 第一个按钮的X坐标
#define Start_Y          55.0f     // 第一个按钮的Y坐标
#define Width_Space      15.0f      // 2个按钮之间的横间距
#define Height_Space     15.0f     // 竖间距
#define Button_Height   85.0f    // 高
#define Button_Width    75    // 宽
#import "NewtopupDetailViewController.h"
#import "NewHeaderScrollView.h"
#import "NewHTMLViewController.h"

@interface NewTopuppageViewController (){
    UIScrollView *scroll;
    NSMutableArray *_btnMutableArray;
    NSMutableArray *_btnMutableArray1;
    NSMutableArray *_btnMutableArray2;
    UITextField *Zhuangzhangtextfield;
    
    NSMutableArray *NewDataArray;
    NSMutableDictionary *NewDataDic;
    NSInteger chongzhiIndex;
    NSInteger chongzhi2Index;

}
@property (nonatomic, strong) NewHeaderScrollView *chongwuScrollView;

@end

@implementation NewTopuppageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = NewButtonColor;
    [self setNavTitle:@"充值"];
    [self loadData];
    [self NetworkRequestManagerdepositPaymentCategories];
}
- (void)loadData {
    _btnMutableArray = NewMutableArrayInit;
    _btnMutableArray1 = NewMutableArrayInit;
    _btnMutableArray2 = NewMutableArrayInit;
    NewDataArray = NewMutableArrayInit;
    chongzhiIndex = 0;
    chongzhi2Index = 0;
}
- (void)NetworkRequestManagerdepositPaymentCategories
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:depositPaymentCategories parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            NSMutableArray *array1 = [NSMutableArray arrayWithArray:requestData[@"result"][@"payment_cats"][@"auto"]];//(在線支付)
            NSMutableArray *array2 = [NSMutableArray arrayWithArray:requestData[@"result"][@"payment_cats"][@"manual"]];//(公司入款)
            NSEnumerator *enumerator = [array1 reverseObjectEnumerator];
            array1 = [[NSMutableArray alloc]initWithArray: [enumerator allObjects]];
            
            //放在一个数组中便于循环添加数组
            NSArray * array = @[array1,array2];
            for (int i = 0; i < array.count; i++) {
                //循环添加
                [NewDataArray addObjectsFromArray:array[i]];
            }
            NewDataDic = [NSMutableDictionary dictionaryWithDictionary:requestData[@"result"]];
            [self loadUI];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)loadUI {
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader)];
    scroll.backgroundColor = NewButtonColor;
    [self.view addSubview:scroll];
    if (@available(iOS 11.0, *)) {
        scroll.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    scroll.contentSize = CGSizeMake(0, 610);
    
    UILabel *line = [UILabel new];
    line.backgroundColor = [UIColor colorWithString:@"#32204C"];
    [scroll addSubview:line];
    
    line.sd_layout
    .heightIs(5)
    .leftEqualToView(scroll)
    .rightEqualToView(scroll)
    .topEqualToView(scroll);
    
    UILabel *chongzhiLB = [UILabel new];
    chongzhiLB.text = @"充值方式";
    chongzhiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    chongzhiLB.font = NewFont(16);
    [chongzhiLB setSingleLineAutoResizeWithMaxWidth:0];
    [scroll addSubview:chongzhiLB];
    
    chongzhiLB.sd_layout
    .heightIs(16)
    .leftSpaceToView(scroll, 15)
    .topSpaceToView(line, 15);
    
    CGFloat chongwuWidth = 80;
    self.chongwuScrollView = [[NewHeaderScrollView alloc] init];
    self.chongwuScrollView.frame = CGRectMake(0, 40+15, SCREEN_WIDTH, Button_Height);
    self.chongwuScrollView.contentSize = CGSizeMake(NewDataArray.count*(chongwuWidth+15)+15,0);
    self.chongwuScrollView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [scroll addSubview:self.chongwuScrollView];

    for (int i = 0; i<NewDataArray.count; i++) {
        UIButton *btn = [UIButton new];
        btn.frame = CGRectMake(i*(chongwuWidth+15)+15, 0, chongwuWidth, Button_Height);
        btn.tag = i+100;
        [btn setBackgroundColor:NewRedColor];
        NewTouchUpInside(btn, XZchongwutouchAction:);
        [btn setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
        ViewBorderRadius(btn, 5, 0, NewClearColor);
        [self.chongwuScrollView addSubview:btn];
        if (i == 0) {
            NSLog(@"1");
            btn.selected = YES;
            ViewBorderRadius(btn, 5, 1, [UIColor colorWithString:@"#FF6D44"]);
            [self getupdateUI:i];
        } else {
            NSLog(@"1");
             ViewBorderRadius(btn, 5, 0, NewClearColor);
        }
        [_btnMutableArray addObject:btn];


        UIImageView *imageV = [UIImageView new];
        imageV.contentMode = UIViewContentModeScaleAspectFill;
        imageV.frame = CGRectMake((Button_Width-35)/2, 20, 35, 25);
        [btn addSubview:imageV];
        if ([NewDataArray[i][@"category_text"] isEqualToString:@"微信支付"]) {
            [imageV setImage:NewImageNamed(@"微信支付")];
        }else if ([NewDataArray[i][@"category_text"] isEqualToString:@"支付宝支付"]){
            [imageV setImage:NewImageNamed(@"支付宝")];
        }else if ([NewDataArray[i][@"category_text"] isEqualToString:@"快捷支付"]){
            [imageV setImage:NewImageNamed(@"快捷支付")];
        }else if ([NewDataArray[i][@"category_text"] isEqualToString:@"网银支付"]){
            [imageV setImage:NewImageNamed(@"网银")];
        }else{
            [imageV setImage:NewImageNamed(@"银联big")];
        }
        
        UILabel *TitleLB = [UILabel new];
        TitleLB.text = [NSString stringWithFormat:@"%@",NewDataArray[i][@"category_text"]];
        TitleLB.font = NewFont(13);
        TitleLB.textAlignment = NSTextAlignmentCenter;
        TitleLB.textColor = [UIColor colorWithString:@"#AFACB4"];
        TitleLB.frame = CGRectMake(0, 20+25+15, chongwuWidth, 15);
        [btn addSubview:TitleLB];
    }
        
}

- (void)getupdateUI:(NSInteger )index{
    //依次遍历self.view中的所有子视图
    for(id tmpView in scroll.subviews)
    {
        if([tmpView isKindOfClass:[UIView class]])
        {
            UIView *imgView = (UIView *)tmpView;
            if(imgView.tag == 1)   //判断是否满足自己要删除的子视图的条件
            {
                [imgView removeFromSuperview]; //删除子视图
                
                break;  //跳出for循环，因为子视图已经找到，无须往下遍历
            }
        }
    }
   
    NSMutableArray *array = [NSMutableArray arrayWithArray:NewDataArray[index][@"list"]];
    [_btnMutableArray1 removeAllObjects];
    //55+85+40+array.count*40
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 55+85+40, SCREEN_WIDTH, array.count*40+220)];
    backView.backgroundColor = NewClearColor;
    backView.tag = 1;
    [scroll addSubview:backView];
     for (int i = 0; i<array.count; i++) {
//            UIImageView *imageV = [UIImageView new];
//            [imageV setImage:NewImageNamed(@"银联small")];
//            imageV.contentMode = UIViewContentModeScaleAspectFill;
//            imageV.frame = CGRectMake(15, i*40, 22, 15);
//            [backView addSubview:imageV];
            
            UILabel *TitleLB = [UILabel new];
            TitleLB.text = [NSString stringWithFormat:@"%@",array[i][@"bank_name_local"]];
            TitleLB.font = NewFont(13);
            TitleLB.textAlignment = NSTextAlignmentLeft;
            TitleLB.textColor = [UIColor colorWithString:@"#FFFFFF"];
            TitleLB.frame = CGRectMake(15, i*40, 100, 15);
            TitleLB.lineBreakMode = NSLineBreakByTruncatingTail;
            [backView addSubview:TitleLB];
         
         CGFloat titlewidth = [NewUtils heightforString:[NSString stringWithFormat:@"单笔限额¥%@-¥%@",array[i][@"minDeposit"],array[i][@"maxDeposit"]] andHeight:15 fontSize:13];

            UILabel *tishiLB = [UILabel new];
            tishiLB.text = [NSString stringWithFormat:@"单笔限额¥%@-¥%@",array[i][@"minDeposit"],array[i][@"maxDeposit"]];
            tishiLB.font = NewFont(12);
            tishiLB.textColor = [UIColor colorWithString:@"#7E7B83"];
            tishiLB.frame = CGRectMake(SCREEN_WIDTH-50-titlewidth, i*40, titlewidth, 40);
            tishiLB.textAlignment = NSTextAlignmentRight;
            [backView addSubview:tishiLB];

            UIButton *chooseBT = [UIButton new];
            chooseBT.frame = CGRectMake(SCREEN_WIDTH-45, i*40, 45, 40);
            NewTouchUpInside(chooseBT, chooseBTclick:);
            [chooseBT setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
            chooseBT.tag = i;
            if (i == 0) {
                [chooseBT setImage:NewImageNamed(@"选中") forState:UIControlStateNormal];
            } else {
                [chooseBT setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
            }
//         chooseBT.backgroundColor = NewRedColor;
            [backView addSubview:chooseBT];
            [_btnMutableArray1 addObject:chooseBT];
        }
        
        UILabel *zhuanzhangLB = [UILabel new];
        zhuanzhangLB.text = @"转账金额（元）";
        zhuanzhangLB.font = NewFont(13);
        [zhuanzhangLB setSingleLineAutoResizeWithMaxWidth:0];
        zhuanzhangLB.textColor = [UIColor colorWithString:@"#AFACB4"];
        [backView addSubview:zhuanzhangLB];

        zhuanzhangLB.sd_layout
        .topSpaceToView(backView, array.count*40+20)
        .heightIs(15)
        .leftSpaceToView(backView, 15);

        UILabel *moenyLB = [UILabel new];
        moenyLB.text = @"￥";
        moenyLB.font = NewFont(17);
        [moenyLB setSingleLineAutoResizeWithMaxWidth:0];
        moenyLB.textColor = [UIColor colorWithString:@"#AFACB4"];
        [backView addSubview:moenyLB];

        moenyLB.sd_layout
        .topSpaceToView(zhuanzhangLB, 35)
        .leftSpaceToView(backView, 40)
        .heightIs(15);

        UILabel *line1 = [UILabel new];
        line1.backgroundColor = [UIColor colorWithString:@"#483465"];
        [backView addSubview:line1];

        line1.sd_layout
        .leftSpaceToView(backView, 55)
        .rightSpaceToView(backView, 55)
        .heightIs(1)
        .topSpaceToView(moenyLB, 10);
    

        Zhuangzhangtextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewButtonColor textColor:NewWhiteColor placeholder:@"请输入充值金额" hidden:NO tag:100 font:NewFont(20) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [Zhuangzhangtextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    Zhuangzhangtextfield.borderStyle = UITextBorderStyleRoundedRect;
        Zhuangzhangtextfield.text = @"";
    //    [Zhuangzhangtextfield setValue:[UIColor colorWithString:@"#AFACB4"] forKeyPath:@"_placeholderLabel.textColor"];
        NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:Zhuangzhangtextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
        Zhuangzhangtextfield.attributedPlaceholder = arrStr;
        [backView addSubview:Zhuangzhangtextfield];

        Zhuangzhangtextfield.sd_layout
        .leftSpaceToView(backView, 58)
        .bottomSpaceToView(line1, 1)
        .heightIs(30)
        .rightEqualToView(line1);
    
        
    [_btnMutableArray2 removeAllObjects];
//        NSArray *arr1 = @[@"100元",@"200元",@"500元",@"1000元"];
//        for (int i = 0; i<arr1.count; i++) {
//            NSInteger index = i % 4;
//            NSInteger page = i / 4;
//            // 圆角按钮
//            UIButton *mapBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//            mapBtn.tag = i;//这句话不写等于废了
//            CGFloat btwidth = (SCREEN_WIDTH-90)/4;
//            mapBtn.frame = CGRectMake(index * (btwidth + Width_Space) + Start_X, array.count*40+20+15+35+10+40, btwidth, 30);
//            [mapBtn setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
//            ViewBorderRadius(mapBtn, 2, 0, NewClearColor);
//            [mapBtn setTitle:arr1[i] forState:UIControlStateNormal];
//            [mapBtn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
//            mapBtn.titleLabel.font = NewFont(12);
//            mapBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
//            if (i == 0) {
//                NSLog(@"1");
//                ViewBorderRadius(mapBtn, 2, 1, [UIColor colorWithString:@"#FF6D44"]);
//                [mapBtn setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
//            } else {
//                NSLog(@"1");
//                 ViewBorderRadius(mapBtn, 2, 0, NewClearColor);
//                [mapBtn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
//            }
//            [backView addSubview:mapBtn];
//            mapBtn.tag = i;
//            //按钮点击方法
//            [mapBtn addTarget:self action:@selector(mapBtnClick11:) forControlEvents:UIControlEventTouchUpInside];
//            [_btnMutableArray2 addObject:mapBtn];
//        }

        UIButton *cunkuanBT = [UIButton new];
        [cunkuanBT setTitle:@"立即存款" forState:UIControlStateNormal];
        [cunkuanBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
        cunkuanBT.titleLabel.font = NewFont(15);
        [cunkuanBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
        ViewRadius(cunkuanBT, 3);
        NewTouchUpInside(cunkuanBT, cunkuanBTclick);
        [backView addSubview:cunkuanBT];

        cunkuanBT.sd_layout
        .heightIs(44)
        .widthIs(250)
        .leftSpaceToView(backView, (SCREEN_WIDTH-250)/2)
        .topSpaceToView(backView, array.count*40+20+15+35+10+40+35+30);
}

#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
}
#pragma mark  ------ 网银账户选择 -------
- (void)chooseBTclick:(UIButton *)sender {
    for (UIButton *btn in _btnMutableArray1) {
        if (btn.tag == sender.tag) {
            [btn setImage:NewImageNamed(@"选中") forState:UIControlStateNormal];
        } else {
            [btn setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
        }
    }
    chongzhi2Index = sender.tag;
}
#pragma mark  ------ 充值方式选择 -------
- (void)XZchongwutouchAction:(UIButton *)sender {

    for (int i = 0; i<_btnMutableArray.count; i++) {
        UIButton *btn = [self.chongwuScrollView viewWithTag:i + 100];
        btn.selected = NO;
        ViewBorderRadius(btn, 5, 0, NewClearColor);
    }
    sender.selected = YES;
    ViewBorderRadius(sender, 5, 1, [UIColor colorWithString:@"#FF6D44"]);
    [self getupdateUI:sender.tag-100];
    chongzhiIndex = sender.tag-100;
    chongzhi2Index = 0;
    
}
#pragma mark  ------ 转账金额选择 -------
- (void)mapBtnClick11:(UIButton *)sender {
    for (UIButton *btn in _btnMutableArray2) {
        if (btn.tag == sender.tag) {
            NSLog(@"1");
            ViewBorderRadius(btn, 2, 1, [UIColor colorWithString:@"#FF6D44"]);
            [btn setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
        } else {
            NSLog(@"1");
             ViewBorderRadius(btn, 2, 0, NewClearColor);
            [btn setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
        }
    }
    NSArray *arr1 = @[@"100元",@"200元",@"500元",@"1000元"];
    NSString *str = [arr1[sender.tag] stringByReplacingOccurrencesOfString:@"元" withString:@""]; // 去掉空格
    Zhuangzhangtextfield.text = str;
}

- (void)cunkuanBTclick {
    NSMutableDictionary *dic =  [NSMutableDictionary dictionaryWithDictionary:NewDataArray[chongzhiIndex]];
    NSLog(@"%@",dic);
    NSMutableArray *array1 = [NSMutableArray arrayWithArray:NewDataDic[@"payment_cats"][@"auto"]];//( 在線支付)
    NSMutableArray *array2 = [NSMutableArray arrayWithArray:NewDataDic[@"payment_cats"][@"manual"]];//( 公司入款)
    NSString *PlayStr;
    for (int i = 0; i<array2.count; i++) {
        if ([dic isEqual:array2[i]]) {
            PlayStr = @"公司入款";
        }
    }
    for (int i = 0; i<array1.count; i++) {
        if ([dic isEqual:array1[i]]) {
            PlayStr = @"在线支付";
        }
    }
    NSLog(@"%@",PlayStr);
    NSMutableDictionary *Datadic =  [NSMutableDictionary dictionaryWithDictionary:NewDataArray[chongzhiIndex][@"list"][chongzhi2Index]];

    if ([Zhuangzhangtextfield.text intValue]>[Datadic[@"maxDeposit"] intValue]  || [Zhuangzhangtextfield.text intValue]<[Datadic[@"minDeposit"] intValue]) {
        [SVProgressHUD showErrorWithStatus:@"请填写该种方式限额内的提款金额"];
        return;
    }
    if ([PlayStr isEqualToString:@"在线支付"]) {
        [self NetworkRequestManagerthirdPartyDepositForm];
    }else{
        NewtopupDetailViewController *vc = [NewtopupDetailViewController new];
        vc.dic = [NSMutableDictionary dictionaryWithDictionary:Datadic];
        vc.money = Zhuangzhangtextfield.text;
        NewPushViewController(vc);
    }
}
- (void)NetworkRequestManagerthirdPartyDepositForm
{
    NSMutableDictionary *Datadic =  [NSMutableDictionary dictionaryWithDictionary:NewDataArray[chongzhiIndex][@"list"][chongzhi2Index]];
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:Datadic[@"bankTypeId"] forKey:@"bankTypeId"];
    [dic setValue:@"" forKey:@"is_mobile"];
    [NetworkRequestManager requestPostWithInterfacePrefix:thirdPartyDepositForm parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [self NetworkRequestManagerthirdPartyDepositRequest:requestData[@"result"]];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)NetworkRequestManagerthirdPartyDepositRequest:(NSDictionary *)model
{
    NSMutableDictionary *Datadic =  [NSMutableDictionary dictionaryWithDictionary:NewDataArray[chongzhiIndex][@"list"][chongzhi2Index]];
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:Datadic[@"bankTypeId"] forKey:@"bankTypeId"];
    [dic setValue:model[@"default_fields"][@"deposit_from"] forKey:@"deposit_from"];
    [dic setValue:model[@"default_fields"][@"minDeposit"] forKey:@"minDeposit"];
    [dic setValue:model[@"default_fields"][@"maxDeposit"] forKey:@"maxDeposit"];
    [dic setValue:Zhuangzhangtextfield.text forKey:@"deposit_amount"];
    [dic setValue:@"" forKey:@"bank"];//存款銀行代號，通常只用於網銀第三方支付。除非thirdPartyDepositForm 回傳中player_input_info 內有指定本欄位，否則不需要回傳。
    [dic setValue:@"" forKey:@"is_mobile"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:thirdPartyDepositRequest parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            if ([requestData[@"result"][@"type_text"] isEqualToString:@"qrcode"]) {
                //生成url形式
                NSURL*url= [NSURL URLWithString:requestData[@"result"][@"url"]];
                [[UIApplication sharedApplication] openURL:url];
            }else if ([requestData[@"result"][@"type_text"] isEqualToString:@"form"]){
                //
                [self NetworkRequestManagerthirdPartyDepositRequest1111:requestData[@"result"]];
                
            }else if ([requestData[@"result"][@"type_text"] isEqualToString:@"URL"]){
                //url形式
                NSURL*url= [NSURL URLWithString:requestData[@"result"][@"url"]];
                [[UIApplication sharedApplication] openURL:url];
            }
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)NetworkRequestManagerthirdPartyDepositRequest1111:(NSDictionary *)model
{
    [NetworkRequestManager requestPostWithInterfacePrefix:model[@"url"] parameters:nil bodyParameters:model[@"params"] onSuccess:^(id requestData) {
        NSString *NewrequestData = [[NSString alloc]initWithData:requestData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",NewrequestData);
        NewHTMLViewController *vc = [NewHTMLViewController new];
        vc.url = NewrequestData;
        NewPushViewController(vc);
        [SVProgressHUD dismiss];
    } onFailure:^{
        [SVProgressHUD dismiss];

    }];
}
@end
