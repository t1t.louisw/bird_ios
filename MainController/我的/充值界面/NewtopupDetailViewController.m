//
//  NewtopupDetailViewController.m
//  NewProject
//
//  Created by mac on 2019/12/9.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewtopupDetailViewController.h"

@interface NewtopupDetailViewController (){
    UIScrollView *scroll;
    NSMutableDictionary *DataDic;
}

@end

@implementation NewtopupDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self setNavTitle:@"充值"];
    [self NetworkRequestManagermanualDepositForm];
    [self loadUI];
    // Do any additional setup after loading the view.
}
- (void)NetworkRequestManagermanualDepositForm
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:_dic[@"bankTypeId"] forKey:@"bankTypeId"];
    [dic setValue:@"" forKey:@"is_mobile"];
    [NetworkRequestManager requestPostWithInterfacePrefix:manualDepositForm parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            DataDic = [NSMutableDictionary dictionaryWithDictionary:requestData[@"result"]];
            [self loadUI];

        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)loadUI {
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader)];
    scroll.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self.view addSubview:scroll];
    if (@available(iOS 11.0, *)) {
        scroll.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    scroll.contentSize = CGSizeMake(0, 730);
    
    UILabel *shanghuyinhangLB = [UILabel new];
    shanghuyinhangLB.text = @"商户银行";
    shanghuyinhangLB.font = NewFont(15);
    shanghuyinhangLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [shanghuyinhangLB setSingleLineAutoResizeWithMaxWidth:0];
    [scroll addSubview:shanghuyinhangLB];
    
    shanghuyinhangLB.sd_layout
    .heightIs(15)
    .topSpaceToView(scroll, 15)
    .leftSpaceToView(scroll, 15);
    
    UIView *onebackView = [UIView new];
    onebackView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    ViewBorderRadius(onebackView, 3, 1, [UIColor colorWithString:@"#352251"]);
    [scroll addSubview:onebackView];
    
    onebackView.sd_layout
    .leftSpaceToView(scroll, 15)
    .rightSpaceToView(scroll, 15)
    .heightIs(132+44)
    .topSpaceToView(shanghuyinhangLB, 15);
    
    UILabel *zhanghaoLB = [UILabel new];
    zhanghaoLB.text = @"账号:";
    zhanghaoLB.font = NewFont(15);
    zhanghaoLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [zhanghaoLB setSingleLineAutoResizeWithMaxWidth:0];
    [onebackView addSubview:zhanghaoLB];
    
    zhanghaoLB.sd_layout
    .heightIs(44)
    .topSpaceToView(onebackView, 0)
    .leftSpaceToView(onebackView, 20);
    
    NSString *bankNumber = [NewUtils getNewStarBankNumWitOldNum:DataDic[@"account_number"]];
    
    UILabel *zhanghaoLB1 = [UILabel new];
    zhanghaoLB1.text = [NSString stringWithFormat:@"%@",bankNumber];
    zhanghaoLB1.font = NewFont(15);
    zhanghaoLB1.textColor = NewWhiteColor;
    [zhanghaoLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [onebackView addSubview:zhanghaoLB1];
    
    zhanghaoLB1.sd_layout
    .heightIs(44)
    .topSpaceToView(onebackView, 0)
    .leftSpaceToView(zhanghaoLB, 2);
    
    UIButton *fuzhiBT = [UIButton new];
    [fuzhiBT setTitle:@"复制" forState:UIControlStateNormal];
    [fuzhiBT setTitleColor:[UIColor colorWithString:@"#FFFFFF"] forState:UIControlStateNormal];
    [fuzhiBT setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
    fuzhiBT.titleLabel.font = NewFont(12);
    ViewRadius(fuzhiBT, 2);
    NewTouchUpInside(fuzhiBT, fuzhimaBTclick);
    [onebackView addSubview:fuzhiBT];
    
    fuzhiBT.sd_layout
    .heightIs(22)
    .widthIs(53)
    .rightSpaceToView(onebackView, 15)
    .centerYEqualToView(zhanghaoLB);
    
    UILabel *line = [UILabel new];
    line.backgroundColor = [UIColor colorWithString:@"#352251"];
    [onebackView addSubview:line];
    
    line.sd_layout
    .heightIs(1)
    .leftEqualToView(onebackView)
    .rightEqualToView(onebackView)
    .topSpaceToView(zhanghaoLB, 0);
    
    UILabel *zhanghumingLB = [UILabel new];
    zhanghumingLB.text = @"银行名称:";
    zhanghumingLB.font = NewFont(15);
    zhanghumingLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [zhanghumingLB setSingleLineAutoResizeWithMaxWidth:0];
    [onebackView addSubview:zhanghumingLB];
    
    zhanghumingLB.sd_layout
    .heightIs(44)
    .topSpaceToView(line, 0)
    .leftSpaceToView(onebackView, 20);
    
    UILabel *zhanghumingLB1 = [UILabel new];
    zhanghumingLB1.text = DataDic[@"bank"];
    zhanghumingLB1.font = NewFont(15);
    zhanghumingLB1.textColor = NewWhiteColor;
    [zhanghumingLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [onebackView addSubview:zhanghumingLB1];
    
    zhanghumingLB1.sd_layout
    .heightIs(44)
    .topSpaceToView(line, 0)
    .leftSpaceToView(zhanghumingLB, 2);
    
    UIButton *fuzhiBT1 = [UIButton new];
    [fuzhiBT1 setTitle:@"复制" forState:UIControlStateNormal];
    [fuzhiBT1 setTitleColor:[UIColor colorWithString:@"#FFFFFF"] forState:UIControlStateNormal];
    [fuzhiBT1 setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
    fuzhiBT1.titleLabel.font = NewFont(12);
    ViewRadius(fuzhiBT1, 2);
    NewTouchUpInside(fuzhiBT1, fuzhimaBTclick);
    [onebackView addSubview:fuzhiBT1];
    
    fuzhiBT1.sd_layout
    .heightIs(22)
    .widthIs(53)
    .rightSpaceToView(onebackView, 15)
    .centerYEqualToView(zhanghumingLB);
    
    UILabel *line1 = [UILabel new];
    line1.backgroundColor = [UIColor colorWithString:@"#352251"];
    [onebackView addSubview:line1];
    
    line1.sd_layout
    .heightIs(1)
    .leftEqualToView(onebackView)
    .rightEqualToView(onebackView)
    .topSpaceToView(zhanghumingLB, 0);
    
    UILabel *yinhangmingchengLB = [UILabel new];
    yinhangmingchengLB.text = @"支行:";
    yinhangmingchengLB.font = NewFont(15);
    yinhangmingchengLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [yinhangmingchengLB setSingleLineAutoResizeWithMaxWidth:0];
    [onebackView addSubview:yinhangmingchengLB];
    
    yinhangmingchengLB.sd_layout
    .heightIs(44)
    .topSpaceToView(line1, 0)
    .leftSpaceToView(onebackView, 20);
    
    UILabel *yinhangmingchengLB1 = [UILabel new];
    yinhangmingchengLB1.text = DataDic[@"branch"];
    yinhangmingchengLB1.font = NewFont(15);
    yinhangmingchengLB1.textColor = NewWhiteColor;
    [yinhangmingchengLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [onebackView addSubview:yinhangmingchengLB1];
    
    yinhangmingchengLB1.sd_layout
    .heightIs(44)
    .topSpaceToView(line1, 0)
    .leftSpaceToView(yinhangmingchengLB, 2);
    
    UIButton *fuzhiBT2 = [UIButton new];
    [fuzhiBT2 setTitle:@"复制" forState:UIControlStateNormal];
    [fuzhiBT2 setTitleColor:[UIColor colorWithString:@"#FFFFFF"] forState:UIControlStateNormal];
    [fuzhiBT2 setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
    fuzhiBT2.titleLabel.font = NewFont(12);
    ViewRadius(fuzhiBT2, 2);
    NewTouchUpInside(fuzhiBT2, fuzhimaBTclick);
    [onebackView addSubview:fuzhiBT2];
    
    fuzhiBT2.sd_layout
    .heightIs(22)
    .widthIs(53)
    .rightSpaceToView(onebackView, 15)
    .centerYEqualToView(yinhangmingchengLB);
    
    UILabel *line10 = [UILabel new];
    line10.backgroundColor = [UIColor colorWithString:@"#352251"];
    [onebackView addSubview:line10];
    
    line10.sd_layout
    .heightIs(1)
    .leftEqualToView(onebackView)
    .rightEqualToView(onebackView)
    .topSpaceToView(yinhangmingchengLB, 0);
    
    UILabel *tianhaoLB = [UILabel new];
       tianhaoLB.text = @"提案号:";
       tianhaoLB.font = NewFont(15);
       tianhaoLB.textColor = [UIColor colorWithString:@"#7E7B83"];
       [tianhaoLB setSingleLineAutoResizeWithMaxWidth:0];
       [onebackView addSubview:tianhaoLB];
       
       tianhaoLB.sd_layout
       .heightIs(44)
       .topSpaceToView(line10, 0)
       .leftSpaceToView(onebackView, 20);
       
       UILabel *tianhao1LB = [UILabel new];
       tianhao1LB.text = DataDic[@"secure_id"];
       tianhao1LB.font = NewFont(15);
       tianhao1LB.textColor = NewWhiteColor;
       [tianhao1LB setSingleLineAutoResizeWithMaxWidth:0];
       [onebackView addSubview:tianhao1LB];
       
       tianhao1LB.sd_layout
       .heightIs(44)
       .topSpaceToView(line10, 0)
       .leftSpaceToView(tianhaoLB, 2);
       
       UIButton *fuzhiBT10 = [UIButton new];
       [fuzhiBT10 setTitle:@"复制" forState:UIControlStateNormal];
       [fuzhiBT10 setTitleColor:[UIColor colorWithString:@"#FFFFFF"] forState:UIControlStateNormal];
       [fuzhiBT10 setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
       fuzhiBT10.titleLabel.font = NewFont(12);
       ViewRadius(fuzhiBT10, 2);
       NewTouchUpInside(fuzhiBT10, fuzhimaBTclick);
       [onebackView addSubview:fuzhiBT10];
       
       fuzhiBT10.sd_layout
       .heightIs(22)
       .widthIs(53)
       .rightSpaceToView(onebackView, 15)
       .centerYEqualToView(tianhaoLB);
    
    UILabel *shanghuyinhangLB1 = [UILabel new];
    shanghuyinhangLB1.text = @"信息录入";
    shanghuyinhangLB1.font = NewFont(15);
    shanghuyinhangLB1.textColor = [UIColor colorWithString:@"#AFACB4"];
    [shanghuyinhangLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [scroll addSubview:shanghuyinhangLB1];
    
    shanghuyinhangLB1.sd_layout
    .heightIs(15)
    .topSpaceToView(onebackView, 15)
    .leftSpaceToView(scroll, 15);
    
    UIView *twobackView = [UIView new];
    twobackView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    ViewBorderRadius(twobackView, 3, 1, [UIColor colorWithString:@"#352251"]);
    [scroll addSubview:twobackView];
    
    twobackView.sd_layout
    .leftSpaceToView(scroll, 15)
    .rightSpaceToView(scroll, 15)
    .heightIs(44)
    .topSpaceToView(shanghuyinhangLB1, 15);
    
    UILabel *dingdanbianhaoLB = [UILabel new];
    dingdanbianhaoLB.text = @"转账金额:";
    dingdanbianhaoLB.font = NewFont(15);
    dingdanbianhaoLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [dingdanbianhaoLB setSingleLineAutoResizeWithMaxWidth:0];
    [twobackView addSubview:dingdanbianhaoLB];
    
    dingdanbianhaoLB.sd_layout
    .heightIs(44)
    .centerYEqualToView(twobackView)
    .leftSpaceToView(twobackView, 10);
    
    UITextField *bankNametextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewClearColor textColor:NewWhiteColor placeholder:@"请输入转账金额" hidden:NO tag:100 font:NewFont(16) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [bankNametextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:bankNametextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#523F6D"],NSFontAttributeName:[UIFont systemFontOfSize:16]}];
    bankNametextfield.attributedPlaceholder = arrStr;
    bankNametextfield.text = _money;
    [twobackView addSubview:bankNametextfield];

    bankNametextfield.sd_layout
    .leftSpaceToView(dingdanbianhaoLB, 2)
    .heightIs(44)
    .rightSpaceToView(twobackView, 15)
    .centerYEqualToView(twobackView);
    
    UILabel *tishiLB = [UILabel new];
    tishiLB.text = [NSString stringWithFormat:@"单笔限额￥%@-￥%@",_dic[@"minDeposit"],_dic[@"maxDeposit"]];
    tishiLB.font = NewFont(12);
    [tishiLB setSingleLineAutoResizeWithMaxWidth:0];
    tishiLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [scroll addSubview:tishiLB];
    
    tishiLB.sd_layout
    .topSpaceToView(twobackView, 5)
    .leftSpaceToView(scroll, 15)
    .heightIs(15);
    
    UIButton *cunkuanBT = [UIButton new];
    [cunkuanBT setTitle:@"提交" forState:UIControlStateNormal];
    [cunkuanBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    cunkuanBT.titleLabel.font = NewFont(15);
    [cunkuanBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(cunkuanBT, 3);
    NewTouchUpInside(cunkuanBT, cunkuanBTclick:);
    [scroll addSubview:cunkuanBT];

    cunkuanBT.sd_layout
    .heightIs(44)
    .leftSpaceToView(scroll, 15)
    .rightSpaceToView(scroll, 15)
    .topSpaceToView(tishiLB, 20);
}

- (void)cunkuanBTclick:(UIButton *)sender {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:_dic[@"bankTypeId"] forKey:@"bankTypeId"];
    [dic setValue:DataDic[@"secure_id"] forKey:@"secure_id"];
    [dic setValue:_money forKey:@"amount"];
    [dic setValue:@"" forKey:@"promo_cms_id"];
    [dic setValue:@"" forKey:@"is_mobile"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:manualDepositRequest parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"谢谢你的存款"];
            NewPopViewController;
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    _money = textField.text;
}
- (void)fuzhimaBTclick{
    NSLog(@"复制");
    
    UIPasteboard *pab = [UIPasteboard generalPasteboard];

    pab.string = @"测试复制";

    if (pab == nil) {
        [SVProgressHUD showErrorWithStatus:@"复制失败"];
        
    } else {
        [SVProgressHUD showSuccessWithStatus:@"已复制"];
    }
}

@end
