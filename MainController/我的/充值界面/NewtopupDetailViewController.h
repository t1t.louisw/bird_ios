//
//  NewtopupDetailViewController.h
//  NewProject
//
//  Created by mac on 2019/12/9.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewBasicViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewtopupDetailViewController : NewBasicViewController
@property (nonatomic,copy)NSMutableDictionary *dic;
@property (nonatomic,copy)NSString *money;

@end

NS_ASSUME_NONNULL_END
