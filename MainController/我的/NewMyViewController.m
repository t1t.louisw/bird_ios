//
//  NewMyViewController.m
//  NewProject
//
//  Created by mac on 2019/11/13.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewMyViewController.h"
#import "NewMyViewCell.h"
#import "NewMyViewTypeCell.h"
#import "NewTopuppageViewController.h"
#import "NewWithdrawalViewController.h"
#import "NewTransferViewController.h"
#import "NewBillingrecordsViewController.h"
#import "MewAccountinformationViewController.h"
#import "NewMylimitViewController.h"
#import "NewSecuritycenterViewController.h"
#import "NewinvitationViewController.h"
#import "NewSecurityViewController.h"

@interface NewMyViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>{
    NSMutableArray *dataArray;
    UICollectionView *mainCollectionView;
    UICollectionViewFlowLayout *flowLayout;
}


@end

@implementation NewMyViewController
#pragma mark - backButton/Tabbar 是否隐藏
- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.navgationBar.hidden = YES;
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithString:@"#1C0F2F"];
    [self setNavTitle:@"我的"];
    [self dataInitialization];
    [self loadsView];
    // Do any additional setup after loading the view.
}
- (void)dataInitialization{
    dataArray = NewMutableArrayInit;
    for (int i = 0; i<6; i++) {
        NSMutableDictionary *dic = NewMutableDictionaryInit;
        switch (i) {
            case 0:
                [dic setValue:@"账单记录" forKey:@"image"];
                [dic setValue:@"账单记录" forKey:@"title"];
                [dataArray addObject:dic];
                break;
                case 1:
                    [dic setValue:@"账户信息" forKey:@"image"];
                    [dic setValue:@"账户信息" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 2:
                    [dic setValue:@"我的限额" forKey:@"image"];
                    [dic setValue:@"我的限额" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 3:
                    [dic setValue:@"促销活动" forKey:@"image"];
                    [dic setValue:@"促销活动" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 4:
                    [dic setValue:@"安全中心" forKey:@"image"];
                    [dic setValue:@"安全中心" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
                case 5:
                    [dic setValue:@"邀请朋友" forKey:@"image"];
                    [dic setValue:@"邀请朋友" forKey:@"title"];
                [dataArray addObject:dic];
                    break;
            default:
                break;
        }
    }
}

- (void)loadsView{
    [self.view addSubview:mainCollectionView = [NewControlPackage collectionViewInitWithFrame:CGRectMake(0.0, STATUSBAR_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT-TABBAR_HEIGHT-(STATUSBAR_HEIGHT)) delegate:self dataSource:self backgroundColor:[UIColor colorWithString:@"#1C0F2F"] scrollEnabled:YES alwaysBounceVertical:YES alwaysBounceHorizontal:NO showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO collectionViewFlowLayout:flowLayout sectionInset:UIEdgeInsetsMake(0, 0, 0, 0) headerReference:CGSizeMake(0, 0) footerReference:CGSizeMake(0, 0) minimumInteritemSpacing:0 minimumLineSpacing:0 scrollDirection:0 hidden:NO tag:105 userInteractionEnabled:YES]];
    [mainCollectionView registerClass:[NewMyViewCell class] forCellWithReuseIdentifier:@"NewMyViewCell"];
    [mainCollectionView registerClass:[NewMyViewTypeCell class] forCellWithReuseIdentifier:@"NewMyViewTypeCell"];
}
//单元格大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH, 350+150);
    }else{
        return CGSizeMake(SCREEN_WIDTH, 45);
    }
}

//定义每个UICollectionView 的 margin 边距  上左下右
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return 6;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NewMyViewCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewMyViewCell" forIndexPath:indexPath];
        cell.backgroundColor = NewClearColor;
        NewTouchUpInside(cell.chongzhiBT, chongzhiBTclick);
        NewTouchUpInside(cell.tixianBT, tixianBTclick);
        NewTouchUpInside(cell.zhuanzhangBT, zhuanzhangBTclick);

        return cell;
    }else if (indexPath.section == 1){
        NewMyViewTypeCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewMyViewTypeCell" forIndexPath:indexPath];
        cell.backgroundColor = NewClearColor;
        [cell assignment:dataArray[indexPath.row]];
        return cell;
    }
    return [[UICollectionViewCell alloc] init];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            NewBillingrecordsViewController *vc = [NewBillingrecordsViewController new];
            NewPushViewController(vc);
        }else if (indexPath.row == 1) {
            MewAccountinformationViewController*vc = [MewAccountinformationViewController new];
            NewPushViewController(vc);
        }else if (indexPath.row == 2) {
            NewMylimitViewController*vc = [NewMylimitViewController new];
            NewPushViewController(vc);
        }else if (indexPath.row == 4){
            NewSecurityViewController*vc = [NewSecurityViewController new];
            NewPushViewController(vc);
        }else if (indexPath.row == 5){
            NewinvitationViewController*vc = [NewinvitationViewController new];
            NewPushViewController(vc);
        }
        
    }
}

- (void)chongzhiBTclick {
    NewTopuppageViewController *vc = [NewTopuppageViewController new];
    NewPushViewController(vc);
}
- (void)tixianBTclick {
    NewWithdrawalViewController *vc = [NewWithdrawalViewController new];
    NewPushViewController(vc);
}
- (void)zhuanzhangBTclick {
    NewTransferViewController *vc = [NewTransferViewController new];
    NewPushViewController(vc);
}


@end
