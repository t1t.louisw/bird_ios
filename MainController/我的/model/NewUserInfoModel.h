//
//  NewUserInfoModel.h
//  NewProject
//
//  Created by mac on 2019/12/23.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewUserInfoModel : JSONModel
@property (nonatomic,copy) NSString<Optional> *referral_code;//
@property (nonatomic,copy) NSString<Optional> *vip_group;//
@property (nonatomic,copy) NSString<Optional> *lastName;//
@property (nonatomic,copy) NSString<Optional> *firstName;//
@property (nonatomic,copy) NSString<Optional> *vip_level_display;//
@property (nonatomic,copy) NSString<Optional> *withdraw_password_status;//
@property (nonatomic,copy) NSString<Optional> *imAccount2;//
@property (nonatomic,copy) NSString<Optional> *birthdate;//
@property (nonatomic,copy) NSString<Optional> *address;//
@property (nonatomic,copy) NSString<Optional> *player_id;//
@property (nonatomic,copy) NSString<Optional> *city;//
@property (nonatomic,copy) NSString<Optional> *language;//
@property (nonatomic,copy) NSString<Optional> *username;//
@property (nonatomic,copy) NSString<Optional> *contactNumber;//
@property (nonatomic,copy) NSString<Optional> *vip_level;//
@property (nonatomic,copy) NSString<Optional> *email;//
@property (nonatomic,copy) NSString<Optional> *gender;//
@property (nonatomic,copy) NSString<Optional> *imAccount;//

@end

NS_ASSUME_NONNULL_END
