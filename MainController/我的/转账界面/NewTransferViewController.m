//
//  NewTransferViewController.m
//  NewProject
//
//  Created by mac on 2019/11/19.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewTransferViewController.h"
#import "HQPickerView.h"
#import "NewTransferCell.h"

@interface NewTransferViewController ()<UITableViewDelegate,UITableViewDataSource,HQPickerViewDelegate>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
    UILabel *zhuanchuStr;
    UILabel *zhuanruStr;
    NSString *identifyID;
    UITextField *Zhuangzhangtextfield;

}

@end

@implementation NewTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"转账"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self dataInitialization];
    [self loadsView];
}

- (void)dataInitialization {
    dataArray = NewMutableArrayInit;
    for (int i = 0; i<10; i++) {
        [dataArray addObject:@""];
    }
}
- (void)loadsView {
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;
}
#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    NewTransferCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
    if (cell == nil) {
        cell = [[NewTransferCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = NewClearColor;
    if (dataArray.count>0) {
        [cell assignment:dataArray[indexPath.row]];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (dataArray.count>0) {
        
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 300;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    headView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    
    UIButton *zhuanchuBT = [UIButton new];
    zhuanchuBT.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(zhuanchuBT, 3);
    NewTouchUpInside(zhuanchuBT, zhuanchuBTclick:);
    zhuanchuBT.tag = 100;
    [headView addSubview:zhuanchuBT];
    
    zhuanchuBT.sd_layout
    .heightIs(45)
    .leftSpaceToView(headView, 15)
    .rightSpaceToView(headView, 15)
    .topSpaceToView(headView, 10);
    
    UILabel *zhuanchuLB = [UILabel new];
    zhuanchuLB.text = @"转出";
    zhuanchuLB.font = NewFont(13);
    zhuanchuLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zhuanchuLB setSingleLineAutoResizeWithMaxWidth:0];
    [zhuanchuBT addSubview:zhuanchuLB];
    
    zhuanchuLB.sd_layout
    .heightIs(15)
    .leftSpaceToView(zhuanchuBT, 12)
    .centerYEqualToView(zhuanchuBT);
    
    zhuanchuStr = [UILabel new];
    zhuanchuStr.text = @"T1  王者联盟";
    zhuanchuStr.font = NewFont(15);
    zhuanchuStr.textColor = [UIColor colorWithString:@"#E1DDE7"];
    [zhuanchuStr setSingleLineAutoResizeWithMaxWidth:0];
    [zhuanchuBT addSubview:zhuanchuStr];
    
    zhuanchuStr.sd_layout
    .heightIs(15)
    .leftSpaceToView(zhuanchuLB, 45)
    .centerYEqualToView(zhuanchuBT);
    
    UIImageView *jiantouImage = [UIImageView new];
    [jiantouImage setImage:NewImageNamed(@"个人中心右键头")];
    [zhuanchuBT addSubview:jiantouImage];
            
    jiantouImage.sd_layout
    .heightIs(11.6)
    .widthIs(6.8)
    .rightSpaceToView(zhuanchuBT, 20)
    .centerYEqualToView(zhuanchuBT);
    
    UIButton *zhuanruBT = [UIButton new];
    zhuanruBT.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(zhuanruBT, 3);
    NewTouchUpInside(zhuanruBT, zhuanchuBTclick:);
    zhuanruBT.tag = 101;
    [headView addSubview:zhuanruBT];
    
    zhuanruBT.sd_layout
    .heightIs(45)
    .leftSpaceToView(headView, 15)
    .rightSpaceToView(headView, 15)
    .topSpaceToView(zhuanchuBT, 10);
    
    UILabel *zhuanruLB = [UILabel new];
    zhuanruLB.text = @"转入";
    zhuanruLB.font = NewFont(13);
    zhuanruLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zhuanruLB setSingleLineAutoResizeWithMaxWidth:0];
    [zhuanruBT addSubview:zhuanruLB];
    
    zhuanruLB.sd_layout
    .heightIs(15)
    .leftSpaceToView(zhuanruBT, 12)
    .centerYEqualToView(zhuanruBT);
    
    zhuanruStr = [UILabel new];
    zhuanruStr.text = @"T2  王者联盟";
    zhuanruStr.font = NewFont(15);
    zhuanruStr.textColor = [UIColor colorWithString:@"#E1DDE7"];
    [zhuanruStr setSingleLineAutoResizeWithMaxWidth:0];
    [zhuanruBT addSubview:zhuanruStr];
    
    zhuanruStr.sd_layout
    .heightIs(15)
    .leftSpaceToView(zhuanruLB, 45)
    .centerYEqualToView(zhuanruBT);
    
    UIImageView *jiantouImage1 = [UIImageView new];
    [jiantouImage1 setImage:NewImageNamed(@"个人中心右键头")];
    [zhuanruBT addSubview:jiantouImage1];
            
    jiantouImage1.sd_layout
    .heightIs(11.6)
    .widthIs(6.8)
    .rightSpaceToView(zhuanruBT, 20)
    .centerYEqualToView(zhuanruBT);
    
    UILabel *zuidiLB = [UILabel new];
    zuidiLB.text = @"最低限额：";
    zuidiLB.font = NewFont(13);
    zuidiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zuidiLB setSingleLineAutoResizeWithMaxWidth:0];
    [headView addSubview:zuidiLB];
    
    zuidiLB.sd_layout
    .heightIs(12)
    .topSpaceToView(zhuanruBT, 15)
    .leftSpaceToView(headView, 30);
    
    UILabel *zuidiLB1 = [UILabel new];
    zuidiLB1.text = @"¥0.00";
    zuidiLB1.font = NewFont(13);
    zuidiLB1.textColor = [UIColor colorWithString:@"#FFFFFF"];
    [zuidiLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [headView addSubview:zuidiLB1];
    
    zuidiLB1.sd_layout
    .heightIs(12)
    .topSpaceToView(zhuanruBT, 15)
    .leftSpaceToView(zuidiLB, 1);
    
    UILabel *zuigaoLB1 = [UILabel new];
    zuigaoLB1.text = @"¥0.00";
    zuigaoLB1.font = NewFont(13);
    zuigaoLB1.textColor = [UIColor colorWithString:@"#FFFFFF"];
    [zuigaoLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [headView addSubview:zuigaoLB1];
    
    zuigaoLB1.sd_layout
    .heightIs(12)
    .topSpaceToView(zhuanruBT, 15)
    .rightSpaceToView(headView, 30);
    
    UILabel *zuigaoLB = [UILabel new];
    zuigaoLB.text = @"最高限额：";
    zuigaoLB.font = NewFont(13);
    zuigaoLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zuigaoLB setSingleLineAutoResizeWithMaxWidth:0];
    [headView addSubview:zuigaoLB];
    
    zuigaoLB.sd_layout
    .heightIs(12)
    .topSpaceToView(zhuanruBT, 15)
    .rightSpaceToView(zuigaoLB1, 1);
    
    UILabel *moenyLB = [UILabel new];
        moenyLB.text = @"￥";
       moenyLB.font = NewFont(17);
       [moenyLB setSingleLineAutoResizeWithMaxWidth:0];
       moenyLB.textColor = [UIColor colorWithString:@"#AFACB4"];
       [headView addSubview:moenyLB];
       
       moenyLB.sd_layout
       .topSpaceToView(zuidiLB, 35)
       .leftSpaceToView(headView, 40)
       .heightIs(15);
       
       UILabel *line1 = [UILabel new];
       line1.backgroundColor = [UIColor colorWithString:@"#483465"];
       [headView addSubview:line1];
       
       line1.sd_layout
       .leftSpaceToView(headView, 55)
       .rightSpaceToView(headView, 55)
       .heightIs(1)
       .topSpaceToView(moenyLB, 10);
    
        Zhuangzhangtextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#230F40"] textColor:NewWhiteColor placeholder:@"" hidden:NO tag:100 font:NewFont(20) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [Zhuangzhangtextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    Zhuangzhangtextfield.borderStyle = UITextBorderStyleRoundedRect;
        Zhuangzhangtextfield.placeholder = @"";
        Zhuangzhangtextfield.text = @"500";
        
//        [Zhuangzhangtextfield setValue:[UIColor colorWithString:@"#AFACB4"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr5 = [[NSMutableAttributedString alloc]initWithString:Zhuangzhangtextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    Zhuangzhangtextfield.attributedPlaceholder = arrStr5;
        [headView addSubview:Zhuangzhangtextfield];
        
        Zhuangzhangtextfield.sd_layout
        .leftSpaceToView(headView, 58)
        .bottomSpaceToView(line1, 1)
        .heightIs(30)
        .rightEqualToView(line1);
    
    UIButton *cunkuanBT = [UIButton new];
    [cunkuanBT setTitle:@"确定" forState:UIControlStateNormal];
    [cunkuanBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    cunkuanBT.titleLabel.font = NewFont(15);
    [cunkuanBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(cunkuanBT, 3);
    [headView addSubview:cunkuanBT];
    
    cunkuanBT.sd_layout
    .heightIs(44)
    .widthIs(250)
    .leftSpaceToView(headView, (SCREEN_WIDTH-250)/2)
    .topSpaceToView(line1, 25);
    
    UILabel *line2 = [UILabel new];
    line2.backgroundColor = [UIColor colorWithString:@"#291744"];
    [headView addSubview:line2];
    
    line2.sd_layout
    .heightIs(5)
    .bottomEqualToView(headView)
    .leftEqualToView(headView)
    .rightEqualToView(headView);

    return headView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (void)zhuanchuBTclick:(UIButton *)sender {
    if (sender.tag == 100) {
        NSArray *timeArray = @[@"英雄联盟",@"王者荣耀",@"跑跑卡丁车",@"街头篮球"];
        HQPickerView *picker = [[HQPickerView alloc]initWithFrame:self.view.bounds];
        picker.delegate = self;
        picker.customArr = timeArray;
        identifyID = @"转出";
        [self.view addSubview:picker];
    }else{
        NSArray *timeArray = @[@"英雄联盟",@"王者荣耀",@"跑跑卡丁车"];
        HQPickerView *picker = [[HQPickerView alloc]initWithFrame:self.view.bounds];
        picker.delegate = self;
        picker.customArr = timeArray;
        picker.tag = 101;
        identifyID = @"转入";
        [self.view addSubview:picker];
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectText:(NSString *)text {
    NSLog(@"%@",text);
    if ([identifyID isEqualToString:@"转出"]) {
        zhuanchuStr.text = text;
    }else{
        zhuanruStr.text = text;
    }
}
@end
