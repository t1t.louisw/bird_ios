//
//  NewTrueTransferViewController.m
//  NewProject
//
//  Created by mac on 2019/12/5.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewTrueTransferViewController.h"

@interface NewTrueTransferViewController (){
    UITextField *Zhuangzhangtextfield;
}

@end

@implementation NewTrueTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"转账"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self dataInitialization];
    [self loadsView];

}
- (void)dataInitialization {

}
- (void)loadsView {
    UIView *backview = [UIView new];
    backview.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(backview, 3);
    [self.view addSubview:backview];
    
    backview.sd_layout
    .heightIs(260)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(self.view, NavHeader+20);
    
    UILabel *tishiLB = [UILabel new];
    tishiLB.text = @"主钱包";
    tishiLB.font = NewFont(12);
    [tishiLB setSingleLineAutoResizeWithMaxWidth:0];
    tishiLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [backview addSubview:tishiLB];
    
    tishiLB.sd_layout
    .heightIs(15)
    .topSpaceToView(backview, 20)
    .leftSpaceToView(backview, 15);
    
     UILabel *moenyLB = [UILabel new];
            moenyLB.text = @"￥";
           moenyLB.font = NewFont(17);
           [moenyLB setSingleLineAutoResizeWithMaxWidth:0];
           moenyLB.textColor = [UIColor colorWithString:@"#AFACB4"];
           [backview addSubview:moenyLB];
           
           moenyLB.sd_layout
           .topSpaceToView(tishiLB, 25)
           .leftSpaceToView(backview, 25)
           .heightIs(15);
           
           UILabel *line1 = [UILabel new];
           line1.backgroundColor = [UIColor colorWithString:@"#483465"];
           [backview addSubview:line1];
           
           line1.sd_layout
           .leftSpaceToView(backview, 40)
           .rightSpaceToView(backview, 40)
           .heightIs(1)
           .topSpaceToView(moenyLB, 10);
        
            Zhuangzhangtextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:NewWhiteColor placeholder:@" " hidden:NO tag:100 font:NewFont(20) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
            [Zhuangzhangtextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
        //    Zhuangzhangtextfield.borderStyle = UITextBorderStyleRoundedRect;
            Zhuangzhangtextfield.text = @"500";
    NSMutableAttributedString *arrStr5 = [[NSMutableAttributedString alloc]initWithString:Zhuangzhangtextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    Zhuangzhangtextfield.attributedPlaceholder = arrStr5;
            [backview addSubview:Zhuangzhangtextfield];
            
            Zhuangzhangtextfield.sd_layout
            .leftSpaceToView(backview, 45)
            .bottomSpaceToView(line1, 1)
            .heightIs(30)
            .rightEqualToView(line1);
    
    UILabel *daichuliLB = [UILabel new];
    daichuliLB.text = @"待处理提款:";
    daichuliLB.font = NewFont(12);
    daichuliLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [daichuliLB setSingleLineAutoResizeWithMaxWidth:0];
    [backview addSubview:daichuliLB];
    
    daichuliLB.sd_layout
    .heightIs(12)
    .leftSpaceToView(backview, 15)
    .topSpaceToView(line1, 15);
    
    UILabel *money1 = [UILabel new];
    money1.textColor = NewWhiteColor;
    money1.font = NewFont(12);
    money1.text = @"¥0.00";
    [money1 setSingleLineAutoResizeWithMaxWidth:0];
    [backview addSubview:money1];
    
    money1.sd_layout
    .heightIs(12)
    .leftSpaceToView(daichuliLB, 0)
    .centerYEqualToView(daichuliLB);
    
    CGFloat BTwidth = (SCREEN_WIDTH-30-30-15)/2;
    
    UIButton *zhuanzhangBT = [UIButton new];
    [zhuanzhangBT setTitle:@"全部转账" forState:UIControlStateNormal];
    [zhuanzhangBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    zhuanzhangBT.titleLabel.font = NewFont(15);
    ViewBorderRadius(zhuanzhangBT, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
    [zhuanzhangBT setBackgroundColor:[UIColor colorWithString:@"#291744"]];
    [backview addSubview:zhuanzhangBT];
    
    zhuanzhangBT.sd_layout
    .heightIs(44)
    .widthIs(BTwidth)
    .topSpaceToView(daichuliLB, 30)
    .leftSpaceToView(backview, 15);
    
    UIButton *shuaxinyueBT = [UIButton new];
    [shuaxinyueBT setTitle:@"刷新余额" forState:UIControlStateNormal];
    [shuaxinyueBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    shuaxinyueBT.titleLabel.font = NewFont(15);
    ViewBorderRadius(shuaxinyueBT, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
    [shuaxinyueBT setBackgroundColor:[UIColor colorWithString:@"#291744"]];
    [backview addSubview:shuaxinyueBT];
    
    shuaxinyueBT.sd_layout
    .heightIs(44)
    .widthIs(BTwidth)
    .topSpaceToView(daichuliLB, 30)
    .leftSpaceToView(zhuanzhangBT, 15);
    
    UIButton *cunkuangBT = [UIButton new];
    [cunkuangBT setTitle:@"存款" forState:UIControlStateNormal];
    [cunkuangBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    cunkuangBT.titleLabel.font = NewFont(15);
    ViewBorderRadius(cunkuangBT, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
    [cunkuangBT setBackgroundColor:[UIColor colorWithString:@"#291744"]];
    [backview addSubview:cunkuangBT];
    
    cunkuangBT.sd_layout
    .heightIs(44)
    .widthIs(BTwidth)
    .topSpaceToView(zhuanzhangBT, 10)
    .leftSpaceToView(backview, 15);
    
    UIButton *qukuanBT = [UIButton new];
    [qukuanBT setTitle:@"取款" forState:UIControlStateNormal];
    [qukuanBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    qukuanBT.titleLabel.font = NewFont(15);
    ViewBorderRadius(qukuanBT, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
    [qukuanBT setBackgroundColor:[UIColor colorWithString:@"#291744"]];
    [backview addSubview:qukuanBT];
    
    qukuanBT.sd_layout
    .heightIs(44)
    .widthIs(BTwidth)
    .topSpaceToView(zhuanzhangBT, 10)
    .leftSpaceToView(cunkuangBT, 15);
    
    UILabel *RGlabel = [UILabel new];
    RGlabel.text = @"RG";
    RGlabel.font = NewFont(14);
    RGlabel.textColor = [UIColor colorWithString:@"#E1DDE7"];
    [RGlabel setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:RGlabel];
    
    RGlabel.sd_layout
    .heightIs(15)
    .leftSpaceToView(self.view, 30)
    .topSpaceToView(backview, 15);
    
    UILabel *money2 = [UILabel new];
    money2.text = @"1.80";
    money2.font = NewFont(14);
    money2.textColor = NewWhiteColor;
    [money2 setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:money2];
    
    money2.sd_layout
    .heightIs(15)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(backview, 15);
    
    UILabel *money3 = [UILabel new];
    money3.text = @"¥";
    money3.font = NewFont(14);
    money3.textColor = [UIColor colorWithString:@"#AFACB4"];
    [money3 setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:money3];
    
    money3.sd_layout
    .heightIs(15)
    .rightSpaceToView(money2, 1)
    .topSpaceToView(backview, 15);
    
    UIButton *zhuanruBT = [UIButton new];
    [zhuanruBT setTitle:@"一键转入" forState:UIControlStateNormal];
    [zhuanruBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    zhuanruBT.titleLabel.font = NewFont(15);
    [zhuanruBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(zhuanruBT, 3);
    [self.view addSubview:zhuanruBT];

    zhuanruBT.sd_layout
    .heightIs(44)
    .rightSpaceToView(self.view, 30)
    .leftSpaceToView(self.view, 30)
    .topSpaceToView(money3, 30);
    
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
}

@end
