//
//  MewAccountinformationViewController.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "MewAccountinformationViewController.h"
#import "NewAccountinformationCell.h"
#import "NewUserInfoModel.h"
#import "WMZDialog.h"
#import "NewCustomerserviceViewController.h"

@interface MewAccountinformationViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
    NSMutableArray *NewdataArray;
    
    NSString *xingStr;
    NSString *MingStr;
    NSString *XingbieStr;
    NSString *ShengriStr;
    NSString *ChushengdiStr;
    NSString *DianhuahaomaStr;
    NSString *WeiXinStr;
    NSString *QQStr;
    
    BOOL xingbieBool;
    BOOL ShengriBool;


}

@end

@implementation MewAccountinformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"账户信息"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#291744"];
    [self RequestManagerUserInfo];
    [self dataInitialization];
    [self loadsView];
}

- (void)RequestManagerUserInfo{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:getPlayerProfile parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [NewdataArray removeAllObjects];
            NewUserInfoModel *model = [[NewUserInfoModel alloc] initWithDictionary:requestData[@"result"] error:nil];
            [NewdataArray addObject:model];
            
            xingStr = model.firstName;
//            MingStr = model.lastName;
            XingbieStr = model.gender;
            ShengriStr = model.birthdate;
            ChushengdiStr = model.address;
            DianhuahaomaStr = model.contactNumber;
            WeiXinStr = model.imAccount2;
            QQStr = model.imAccount;
            
            if (XingbieStr == NULL) {
                xingbieBool = YES;
            }else{
                xingbieBool = NO;
            }
            if (ShengriStr == NULL) {
                ShengriBool = YES;
            }else{
                ShengriBool = NO;
            }
            
            [SVProgressHUD dismiss];
            [newtableView reloadData];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)RequestManagerUserInfo11111{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [NetworkRequestManager requestPostWithInterfacePrefix:getPlayerProfile parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [NewdataArray removeAllObjects];
            NewUserInfoModel *model = [[NewUserInfoModel alloc] initWithDictionary:requestData[@"result"] error:nil];
            [NewdataArray addObject:model];
            
            xingStr = model.firstName;
//            MingStr = model.lastName;
            XingbieStr = model.gender;
            ShengriStr = model.birthdate;
            ChushengdiStr = model.address;
            DianhuahaomaStr = model.contactNumber;
            WeiXinStr = model.imAccount2;
            QQStr = model.imAccount;
            
            [newtableView reloadData];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}

- (void)dataInitialization {
    NewdataArray = NewMutableArrayInit;
    dataArray = NewMutableArrayInit;
    for (int i = 0; i<10; i++) {
        NSMutableDictionary *dic = NewMutableDictionaryInit;
        switch (i) {
                case 0:
                    [dic setValue:@"账号" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
                case 1:
                    [dic setValue:@"姓名" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
//                case 2:
//                    [dic setValue:@"名" forKey:@"Head"];
//                    [dataArray addObject:dic];
//                    break;
                case 2:
                    [dic setValue:@"性别" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
                case 3:
                    [dic setValue:@"生日" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
//                case 5:
//                    [dic setValue:@"国籍" forKey:@"Head"];
//                    [dataArray addObject:dic];
//                    break;
                case 4:
                    [dic setValue:@"出生地" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
                case 5:
                    [dic setValue:@"电话号码" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
                case 6:
                    [dic setValue:@"微信" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
                case 7:
                    [dic setValue:@"QQ" forKey:@"Head"];
                    [dataArray addObject:dic];
                    break;
            default:
                break;
        }
    }
}

- (void)loadsView {
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;
}
#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return dataArray.count;
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    NewAccountinformationCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
    if (cell == nil) {
        cell = [[NewAccountinformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)

    cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
    cell.jiantouImage.hidden = YES;
    cell.manBT.hidden = YES;
    cell.manLB.hidden = YES;
    cell.womanBT.hidden = YES;
    cell.womanLB.hidden = YES;
    cell.headImage.hidden = YES;
    NewTouchUpInside(cell.manBT, manBTclick:);
    NewTouchUpInside(cell.womanBT, manBTclick:);
    NewTouchUpInside(cell.fuzhiBT, fuzhiBTclick);

    cell.manBT.tag = 100;
    cell.womanBT.tag = 101;

    if (dataArray.count>0 && NewdataArray.count>0) {
        [cell assignment:dataArray[indexPath.section]];
        if (indexPath.section == 2) {
            cell.jiantouImage.hidden = YES;
            cell.manBT.hidden = NO;
            cell.manLB.hidden = NO;
            cell.womanBT.hidden = NO;
            cell.womanLB.hidden = NO;
            cell.fuzhiBT.hidden = YES;
        }else if (indexPath.section == 0){
            cell.jiantouImage.hidden = YES;
            cell.manBT.hidden = YES;
            cell.manLB.hidden = YES;
            cell.womanBT.hidden = YES;
            cell.womanLB.hidden = YES;
            cell.fuzhiBT.hidden = NO;
        }else if (indexPath.section == 1){
            cell.jiantouImage.hidden = YES;
            cell.manBT.hidden = YES;
            cell.manLB.hidden = YES;
            cell.womanBT.hidden = YES;
            cell.womanLB.hidden = YES;
            cell.fuzhiBT.hidden = YES;
        }else{
            cell.jiantouImage.hidden = NO;
            cell.manBT.hidden = YES;
            cell.manLB.hidden = YES;
            cell.womanBT.hidden = YES;
            cell.womanLB.hidden = YES;
            cell.fuzhiBT.hidden = YES;

        }
        NewUserInfoModel *model = NewdataArray.lastObject;
        if (indexPath.section == 0) {
            cell.contentTitle.text = model.username;
        }else if (indexPath.section == 1) {
            cell.contentTitle.text = [NSString stringWithFormat:@"%@",xingStr==NULL?@"":xingStr];
        }
//        else if (indexPath.section == 2){
//            cell.contentTitle.text = [NSString stringWithFormat:@"%@",MingStr==NULL?@"":MingStr];
//        }
        else if (indexPath.section == 2){
            if (XingbieStr == NULL) {
                [cell.womanBT setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
                [cell.manBT setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
            }else if ([XingbieStr isEqualToString:@"男"]){
                [cell.womanBT setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
                [cell.manBT setImage:NewImageNamed(@"选中") forState:UIControlStateNormal];
            }else if ([XingbieStr isEqualToString:@"女"]){
                [cell.womanBT setImage:NewImageNamed(@"选中") forState:UIControlStateNormal];
                [cell.manBT setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
            }
        }else if (indexPath.section == 3){
            cell.contentTitle.text = ShengriStr;
        }else if (indexPath.section == 4){
            cell.contentTitle.text = ChushengdiStr;
        }else if (indexPath.section == 5){
            cell.contentTitle.text = DianhuahaomaStr;
        }else if (indexPath.section == 6){
            cell.contentTitle.text = WeiXinStr;
        }else if (indexPath.section == 7){
            cell.contentTitle.text = QQStr;
        }
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
    }else if (indexPath.section == 2 || indexPath.section == 1){
        
    }else if (indexPath.section == 3){
        if (ShengriBool == NO) {
            return;
        }else{
            [self actionTime];
        }
    }else if (indexPath.section == 5){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"手机号码需要联系客服进行修改，您是否去联系客服修改手机号码" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                   UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                   }];
                   UIAlertAction *skipAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                       [self qukefu];
                   }];
                   [alertController addAction:cancelAction];
                   [alertController addAction:skipAction];
                   [self presentViewController:alertController animated:YES completion:nil];
    }else{
        [self creatAlertController_alert:indexPath.section];
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 7) {
        return 60;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0 || section == 6) {
        return 5;
    }else{
        return 0;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5)];
    view.backgroundColor = [UIColor colorWithString:@"#291744"];
    return view;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5)];
    view.backgroundColor = NewClearColor;
    
    UIButton *baocunBT = [UIButton new];
    [baocunBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    [baocunBT setTitle:@"保存修改" forState:UIControlStateNormal];
    [baocunBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    baocunBT.titleLabel.font = NewFont(15);
    ViewRadius(baocunBT, 3);
    NewTouchUpInside(baocunBT, baocunBTclick);
    [view addSubview:baocunBT];
    
    baocunBT.sd_layout
    .topSpaceToView(view, 15)
    .bottomEqualToView(view)
    .widthIs(SCREEN_WIDTH-60)
    .leftSpaceToView(view, 30);
    return view;
}
#pragma mark 字典转化字符串
- (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
- (void)baocunBTclick {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    NSMutableDictionary *infoDic = NewMutableDictionaryInit;
    [infoDic setValue:xingStr forKey:@"firstName"];
    [infoDic setValue:DianhuahaomaStr forKey:@"contactNumber"];
    [infoDic setValue:XingbieStr forKey:@"gender"];
    [infoDic setValue:ShengriStr forKey:@"birthdate"];
    [infoDic setValue:ChushengdiStr forKey:@"address"];
    [infoDic setValue:QQStr forKey:@"imAccount"];
    [infoDic setValue:WeiXinStr forKey:@"imAccount2"];
//    [infoDic setValue:MingStr forKey:@"lastName"];
    
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:[self dictionaryToJson:infoDic] forKey:@"fields"];

    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:updatePlayerProfile parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"修改信息成功"];
            NewPopViewController;

        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}

- (void)fuzhimaBTclick{
    NewUserInfoModel *model = NewdataArray.lastObject;
    UIPasteboard *pab = [UIPasteboard generalPasteboard];
    pab.string = model.username;
    if (pab == nil) {
        [SVProgressHUD showErrorWithStatus:@"复制失败"];
    } else {
        [SVProgressHUD showSuccessWithStatus:@"复制成功"];
    }
}

- (void)manBTclick:(UIButton *)sender {
    if (xingbieBool == NO) {
        return;
    }
    if (sender.tag == 100) {
        if (XingbieStr == NULL || [XingbieStr isEqualToString:@"女"]) {
            [sender setImage:NewImageNamed(@"选中") forState:UIControlStateNormal];
        }
        XingbieStr = @"男";
        [newtableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:2], nil] withRowAnimation:UITableViewRowAnimationNone];
    }else{
        if (XingbieStr == NULL || [XingbieStr isEqualToString:@"男"]) {
            [sender setImage:NewImageNamed(@"选中") forState:UIControlStateNormal];
        }
        XingbieStr = @"女";
        [newtableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:2], nil] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)fuzhiBTclick {
    [self fuzhimaBTclick];

}
#pragma mark-----输入框------
-(void)creatAlertController_alert:(NSInteger)row {
    //跟上面的流程差不多，记得要把preferredStyle换成UIAlertControllerStyleAlert
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"修改%@",dataArray[row][@"Head"]] message:nil preferredStyle:UIAlertControllerStyleAlert];
    //可以给alertview中添加一个输入框
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = [NSString stringWithFormat:@"请输入%@",dataArray[row][@"Head"]];
        if (NewdataArray.count>0) {
            NewUserInfoModel *model = NewdataArray.lastObject;
            if (row == 1) {
                textField.text = model.firstName;
            }else if (row == 2){
                textField.text = model.lastName;
            }else if (row == 4){
                textField.text = model.address;
            }else if (row == 5){
                textField.text = model.contactNumber;
            }else if (row == 6){
                textField.text = model.imAccount2;
            }else if (row == 7){
                textField.text = model.imAccount;
            }
        }
    }];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //textFields是一个数组，获取所输入的字符串
        NSLog(@"%@",alert.textFields.lastObject.text);
        NSLog(@"点击了确定");
        if (row == 1) {
           xingStr =  alert.textFields.lastObject.text;
        }
//        else if (row == 2){
//            MingStr =  alert.textFields.lastObject.text;
//        }
        else if (row == 4){
            ChushengdiStr =  alert.textFields.lastObject.text;
        }else if (row == 5){
            DianhuahaomaStr =  alert.textFields.lastObject.text;
        }else if (row == 6){
            WeiXinStr =  alert.textFields.lastObject.text;
        }else if (row == 7){
            QQStr =  alert.textFields.lastObject.text;
        }
        [newtableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:row], nil] withRowAnimation:UITableViewRowAnimationNone];

    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"点击了取消");
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)actionTime{
    Dialog()
    .wEventOKFinishSet(^(id anyID, id otherData) {
        NSString *str = [otherData componentsJoinedByString:@"-"];//#为分隔符
        NSLog(@"选中 %@ %@",anyID,otherData);
        int time =  [self compareOneDay:[self getCurrentTime] withAnotherDay:str];
        if (time == -1) {
            NSLog(@"超过当前时间");
            [SVProgressHUD showErrorWithStatus:@"生日超过当前时间，选择有误！"];
            return ;
        }else{
            NSLog(@"未超过当前时间");
            NSDateFormatter  * formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyy-MM-dd";
            
            NSDate  * birthDate =  [formatter dateFromString:str];
            NSInteger fff =  [self ageWithDateOfBirth:birthDate];

            if (fff<18) {
                [SVProgressHUD showErrorWithStatus:@"生日应保证大于18岁"];
                return ;
            }
            ShengriStr = str;
            [newtableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:3], nil] withRowAnimation:UITableViewRowAnimationNone];
        }
    })
    //默认选中时间 不传默认是当前时间
//    .wDefaultDateSet()
    .wDateTimeTypeSet(@"yyyy-MM-dd")
    .wPickRepeatSet(NO)
    .wTypeSet(DialogTypeDatePicker)
    .wMessageColorSet([UIColor blackColor])
    .wMessageFontSet(16)
    .wShadowAlphaSet(0.0f)
    .wTitleSet(@"修改生日")
    .wStart();
}

- (NSInteger)ageWithDateOfBirth:(NSDate *)date;
{
    // 出生日期转换 年月日
    NSDateComponents *components1 = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    NSInteger brithDateYear  = [components1 year];
    NSInteger brithDateDay   = [components1 day];
    NSInteger brithDateMonth = [components1 month];
      
    // 获取系统当前 年月日
    NSDateComponents *components2 = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger currentDateYear  = [components2 year];
    NSInteger currentDateDay   = [components2 day];
    NSInteger currentDateMonth = [components2 month];
      
    // 计算年龄
    NSInteger iAge = currentDateYear - brithDateYear - 1;
    if ((currentDateMonth > brithDateMonth) || (currentDateMonth == brithDateMonth && currentDateDay >= brithDateDay)) {
        iAge++;
    }
      
    return iAge;
}
- (NSString *)getCurrentTime{
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTime=[formatter stringFromDate:[NSDate date]];
//    NSDate *date = [formatter dateFromString:dateTime];
    return dateTime;
}
- (int)compareOneDay:(NSString *)oneDay withAnotherDay:(NSString *)anotherDay
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
////    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
//    NSString *anotherDayStr = anotherDay;
    NSDate *dateA = [dateFormatter dateFromString:oneDay];
    NSDate *dateB = [dateFormatter dateFromString:anotherDay];
    NSComparisonResult result = [dateA compare:dateB];
    NSLog(@"oneDay : %@, anotherDay : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {//左边的操作对象大于右边的对象。
        return 1;
    }
    else if (result == NSOrderedAscending){//左边的操作对象小于右边的对象。
        return -1;
    }
    return 0;
    
}

- (void)qukefu {
    NewCustomerserviceViewController *vc = [NewCustomerserviceViewController new];
    NewPushViewController(vc);
}
@end
