//
//  NewAddBankViewController.m
//  NewProject
//
//  Created by mac on 2019/12/6.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewAddBankViewController.h"
#import "HQPickerView.h"

@interface NewAddBankViewController ()<HQPickerViewDelegate>{
    NSMutableArray *NewDataArray;
    UIButton *zhanghaoBT;
    NSMutableArray *bankArray;
    NSString *bankTypeId;
    
    NSString *zhanghumingStr;
    NSString *zhanghaoStr;
    NSString *kaihushengStr;
    NSString *kaihushiStr;
    NSString *fenhangmingchenStr;
    NSString *fenhangDianHuaStr;

}

@end

@implementation NewAddBankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self setNavTitle:@"添加银行账户"];
    [self loadData];
    [self NetworkRequestManagerquerqueryDepositWithdrawalAvailableBank];
}
//查詢可用存款銀行
- (void)NetworkRequestManagerquerqueryDepositWithdrawalAvailableBank
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:queryDepositWithdrawalAvailableBank parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            NSMutableArray *arr = [NSMutableArray arrayWithArray:requestData[@"result"]];
            for (int i = 0; i<arr.count; i++) {
                NSString *jsonStr1 = [arr[i][@"bankName"] stringByReplacingOccurrencesOfString:@"_json:" withString:@""]; // 去掉空格
                NSData *data = [jsonStr1 dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                if ([arr[i][@"enabled_withdrawal"] isEqualToString:@"1"] && [arr[i][@"enabled_deposit"] isEqualToString:@"0"]) {
                    [bankArray addObject:json[@"2"]];
                }
                [NewDataArray addObject:arr[i]];
            }
            [self loadsView];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)loadData {
    NewDataArray = NewMutableArrayInit;
    bankArray = NewMutableArrayInit;
}
- (void)loadsView {
    UIView *backView = [UIView new];
    backView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(backView, 5);
    [self.view addSubview:backView];
    
    backView.sd_layout
    .heightIs(305)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(self.view, NavHeader+20);
    
    UILabel *zhanhaoLB = [UILabel new];
    zhanhaoLB.text = @"银行:";
    zhanhaoLB.font = NewFont(15);
    zhanhaoLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zhanhaoLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:zhanhaoLB];
    
    zhanhaoLB.sd_layout
    .heightIs(50)
    .topEqualToView(backView)
    .leftSpaceToView(backView, 20);
    
    UILabel *xinghao = [UILabel new];
    xinghao.text = @"*";
    xinghao.font = NewFont(12);
    [xinghao setSingleLineAutoResizeWithMaxWidth:0];
    xinghao.textColor = NewRedColor;
    [backView addSubview:xinghao];
    
    xinghao.sd_layout
    .heightIs(10)
    .rightSpaceToView(zhanhaoLB, 2)
    .centerYEqualToView(zhanhaoLB);
    
//    UITextField *zhanghaotextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#E6E2ED"] placeholder:@"请选择银行" hidden:NO tag:100 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
//    [zhanghaotextfield addTarget:self action:@selector(zhanghaotextfieldclick) forControlEvents:UIControlEventTouchUpInside];
//    NSMutableAttributedString *arrStr1 = [[NSMutableAttributedString alloc]initWithString:zhanghaotextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
//    zhanghaotextfield.attributedPlaceholder = arrStr1;
//    zhanghaotextfield.userInteractionEnabled = YES;
//    [backView addSubview:zhanghaotextfield];
    
    zhanghaoBT = [UIButton new];
    [zhanghaoBT setTitle:@"请选择银行" forState:UIControlStateNormal];
    [zhanghaoBT setTitleColor:[UIColor colorWithString:@"#7E7B83"] forState:UIControlStateNormal];
    zhanghaoBT.titleLabel.font = NewFont(15);
    zhanghaoBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [zhanghaoBT setBackgroundColor:NewClearColor];
    NewTouchUpInside(zhanghaoBT, zhanghaotextfieldclick);
    [backView addSubview:zhanghaoBT];

    zhanghaoBT.sd_layout
    .leftSpaceToView(zhanhaoLB, 2)
    .heightIs(50)
    .rightSpaceToView(backView, 15)
    .topEqualToView(backView);
    
    UILabel *line = [UILabel new];
    line.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line];
    
    line.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(backView, 50)
    .rightSpaceToView(backView, 15);
    
    UILabel *zhanghumingLB = [UILabel new];
    zhanghumingLB.text = @"账户:";
    zhanghumingLB.font = NewFont(15);
    zhanghumingLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zhanghumingLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:zhanghumingLB];
    
    zhanghumingLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line, 0)
    .leftSpaceToView(backView, 20);
    
    UILabel *xinghao1 = [UILabel new];
    xinghao1.text = @"*";
    xinghao1.font = NewFont(12);
    [xinghao1 setSingleLineAutoResizeWithMaxWidth:0];
    xinghao1.textColor = NewRedColor;
    [backView addSubview:xinghao1];
    
    xinghao1.sd_layout
    .heightIs(10)
    .rightSpaceToView(zhanghumingLB, 2)
    .centerYEqualToView(zhanghumingLB);
    
    UITextField *zhanghumingtextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#E6E2ED"] placeholder:@"请输入账号" hidden:NO tag:100 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [zhanghumingtextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr2 = [[NSMutableAttributedString alloc]initWithString:zhanghumingtextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    zhanghumingtextfield.attributedPlaceholder = arrStr2;
    [backView addSubview:zhanghumingtextfield];

    zhanghumingtextfield.sd_layout
    .leftSpaceToView(zhanghumingLB, 2)
    .heightIs(50)
    .rightSpaceToView(backView, 15)
    .topSpaceToView(line, 0);
    
    UILabel *line1 = [UILabel new];
    line1.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line1];
    
    line1.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(line, 50)
    .rightSpaceToView(backView, 15);
    
    UILabel *kaihushengLB = [UILabel new];
    kaihushengLB.text = @"账户名:";
    kaihushengLB.font = NewFont(15);
    kaihushengLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [kaihushengLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:kaihushengLB];
    
    kaihushengLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line1, 0)
    .leftSpaceToView(backView, 20);
    
    UILabel *xinghao2 = [UILabel new];
    xinghao2.text = @"*";
    xinghao2.font = NewFont(12);
    [xinghao2 setSingleLineAutoResizeWithMaxWidth:0];
    xinghao2.textColor = NewRedColor;
    [backView addSubview:xinghao2];
    
    xinghao2.sd_layout
    .heightIs(10)
    .rightSpaceToView(kaihushengLB, 2)
    .centerYEqualToView(kaihushengLB);
    
    UITextField *kaihushengtextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#E6E2ED"] placeholder:@"请输入账户名" hidden:NO tag:101 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [kaihushengtextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr3 = [[NSMutableAttributedString alloc]initWithString:kaihushengtextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    kaihushengtextfield.attributedPlaceholder = arrStr3;
    kaihushengtextfield.text = [NewUtils userDefaultsStringKey:NewfirstName];
    zhanghaoStr = [NewUtils userDefaultsStringKey:NewfirstName];
    kaihushengtextfield.userInteractionEnabled = NO;
    [backView addSubview:kaihushengtextfield];

    kaihushengtextfield.sd_layout
    .leftSpaceToView(kaihushengLB, 2)
    .heightIs(50)
    .rightSpaceToView(backView, 15)
    .topSpaceToView(line1, 0);
    
    UILabel *line2 = [UILabel new];
    line2.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line2];
    
    line2.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(line1, 50)
    .rightSpaceToView(backView, 15);
    
    UILabel *kaihushiLB = [UILabel new];
    kaihushiLB.text = @"开户省:";
    kaihushiLB.font = NewFont(15);
    kaihushiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [kaihushiLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:kaihushiLB];
    
    kaihushiLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line2, 0)
    .leftSpaceToView(backView, 20);

    UITextField *kaihushitextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#E6E2ED"] placeholder:@"请输入开户省" hidden:NO tag:102 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [kaihushitextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr4 = [[NSMutableAttributedString alloc]initWithString:kaihushitextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    kaihushitextfield.attributedPlaceholder = arrStr4;
    [backView addSubview:kaihushitextfield];

    kaihushitextfield.sd_layout
    .leftSpaceToView(kaihushiLB, 2)
    .heightIs(50)
    .rightSpaceToView(backView, 15)
    .topSpaceToView(line2, 0);
    
    UILabel *line3 = [UILabel new];
    line3.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line3];
    
    line3.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(line2, 50)
    .rightSpaceToView(backView, 15);
    
    UILabel *fenhangmingchenLB = [UILabel new];
    fenhangmingchenLB.text = @"开户市:";
    fenhangmingchenLB.font = NewFont(15);
    fenhangmingchenLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [fenhangmingchenLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:fenhangmingchenLB];
    
    fenhangmingchenLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line3, 0)
    .leftSpaceToView(backView, 20);
    
    UITextField *fenhangmingchengtextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#E6E2ED"] placeholder:@"请输入开户市" hidden:NO tag:103 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [fenhangmingchengtextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr5 = [[NSMutableAttributedString alloc]initWithString:fenhangmingchengtextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    fenhangmingchengtextfield.attributedPlaceholder = arrStr5;
    [backView addSubview:fenhangmingchengtextfield];

    fenhangmingchengtextfield.sd_layout
    .leftSpaceToView(fenhangmingchenLB, 2)
    .heightIs(50)
    .rightSpaceToView(backView, 15)
    .topSpaceToView(line3, 0);
    
    UILabel *line4 = [UILabel new];
    line4.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line4];
    
    line4.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(line3, 50)
    .rightSpaceToView(backView, 15);
    
    UILabel *shoujihaomaLB = [UILabel new];
    shoujihaomaLB.text = @"分行名称:";
    shoujihaomaLB.font = NewFont(15);
    shoujihaomaLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [shoujihaomaLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:shoujihaomaLB];
    
    shoujihaomaLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line4, 0)
    .leftSpaceToView(backView, 20);
    
//    UIButton *fasongcodeBT = [UIButton new];
//    [fasongcodeBT setTitle:@"发送验证码" forState:UIControlStateNormal];
//    [fasongcodeBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
//    fasongcodeBT.titleLabel.font = NewFont(15);
//    fasongcodeBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//    [backView addSubview:fasongcodeBT];
//    
//    fasongcodeBT.sd_layout
//    .heightIs(50)
//    .rightSpaceToView(backView, 15)
//    .widthIs(80)
//    .topSpaceToView(line4, 0);
    
    UITextField *shoujihaomatextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#E6E2ED"] placeholder:@"请输入分行名称" hidden:NO tag:104 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [shoujihaomatextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr6 = [[NSMutableAttributedString alloc]initWithString:shoujihaomatextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    shoujihaomatextfield.attributedPlaceholder = arrStr6;
    [backView addSubview:shoujihaomatextfield];

    shoujihaomatextfield.sd_layout
    .leftSpaceToView(shoujihaomaLB, 2)
    .heightIs(50)
    .rightSpaceToView(backView, 15)
    .topSpaceToView(line4, 0);
    
    UILabel *line5 = [UILabel new];
    line5.backgroundColor = [UIColor colorWithString:@"#352251"];
//    [backView addSubview:line5];
    
    line5.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(line4, 50)
    .rightSpaceToView(backView, 15);
    
    UILabel *yanzhenmaLB = [UILabel new];
    yanzhenmaLB.text = @"分行电话:";
    yanzhenmaLB.font = NewFont(15);
    yanzhenmaLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [yanzhenmaLB setSingleLineAutoResizeWithMaxWidth:0];
//    [backView addSubview:yanzhenmaLB];
    
    yanzhenmaLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line5, 0)
    .leftSpaceToView(backView, 20);
    
    UITextField *yanzhengmatextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#E6E2ED"] placeholder:@"请输入分行电话" hidden:NO tag:105 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [yanzhengmatextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr7 = [[NSMutableAttributedString alloc]initWithString:yanzhengmatextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    yanzhengmatextfield.attributedPlaceholder = arrStr7;
//    [backView addSubview:yanzhengmatextfield];

    yanzhengmatextfield.sd_layout
    .leftSpaceToView(yanzhenmaLB, 2)
    .heightIs(50)
    .rightSpaceToView(backView, 15)
    .topSpaceToView(line5, 0);
    
    UIButton *tijiaoBT = [UIButton new];
    [tijiaoBT setTitle:@"提交卡号" forState:UIControlStateNormal];
    [tijiaoBT setTitleColor:[UIColor colorWithString:@"#FFFFFF"] forState:UIControlStateNormal];
    [tijiaoBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    tijiaoBT.titleLabel.font = NewFont(15);
    ViewRadius(tijiaoBT, 5);
    NewTouchUpInside(tijiaoBT, tijiaoBTclick:);
    [self.view addSubview:tijiaoBT];
    
    tijiaoBT.sd_layout
    .heightIs(44)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(backView, 20);
    
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    if (textField.tag == 100) {
        zhanghumingStr = textField.text;
    }else if (textField.tag == 101){
        zhanghaoStr = textField.text;
    }else if (textField.tag == 102){
        kaihushengStr = textField.text;
    }else if (textField.tag == 103){
        kaihushiStr = textField.text;
    }else if (textField.tag == 104){
        fenhangmingchenStr = textField.text;
    }else if (textField.tag == 105){
        fenhangDianHuaStr = textField.text;
    }
}

- (void)zhanghaotextfieldclick{
    HQPickerView *picker = [[HQPickerView alloc]initWithFrame:self.view.bounds];
    picker.delegate = self;
    picker.customArr = bankArray;
    [self.view addSubview:picker];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectText:(NSString *)text {
    NSLog(@"%@",text);
    [zhanghaoBT setTitle:text forState:UIControlStateNormal];
    [zhanghaoBT setTitleColor:[UIColor colorWithString:@"#E6E2ED"] forState:UIControlStateNormal];
    
    for (int i = 0; i<NewDataArray.count; i++) {
        NSString *jsonStr1 = [NewDataArray[i][@"bankName"] stringByReplacingOccurrencesOfString:@"_json:" withString:@""]; // 去掉空格
        NSData *data = [jsonStr1 dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
        if ([text isEqualToString:json[@"2"]]) {
            bankTypeId = NewDataArray[i][@"bankTypeId"];
        }
    }
}

- (void)tijiaoBTclick:(UIButton *)sender {
    if ([zhanghaoBT.titleLabel.text isEqualToString:@"请选择银行"]) {
        [SVProgressHUD showErrorWithStatus:@"请先选择银行卡"];
        return;
    }else if (zhanghumingStr.length<=0){
        [SVProgressHUD showErrorWithStatus:@"请填写账户名"];
        return;
    }else if (zhanghaoStr.length<=0){
        [SVProgressHUD showErrorWithStatus:@"请填写账号"];
        return;
    }else if (zhanghumingStr.length<16 || zhanghumingStr.length>19){
        [SVProgressHUD showErrorWithStatus:@"请正确填写16-19位的银行卡号"];
        return;
    }
    
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:bankTypeId forKey:@"bankTypeId"];
    [dic setValue:zhanghumingStr forKey:@"bankAccNum"];
    [dic setValue:zhanghaoStr forKey:@"bankAccName"];
    //选填
    [dic setValue:@"" forKey:@"bankAddress"];
    [dic setValue:kaihushengStr forKey:@"province"];
    [dic setValue:kaihushiStr forKey:@"city"];
    [dic setValue:fenhangmingchenStr forKey:@"branch"];
    [dic setValue:fenhangDianHuaStr forKey:@"phone"];

    NSString *URl;
    if ([_Str isEqualToString:@"存款银行卡"]) {
        URl = addPlayerDepositAccount;//增加存款银行卡
    }else{
        URl = addPlayerWithdrawAccount;//增加取款银行卡
    }
    [SVProgressHUD show];
    
    if (self.bankcardYesOrNo == YES) {
        [NetworkRequestManager requestPostWithInterfacePrefixNew:URl parameters:dic onSuccess:^(id requestData) {
            NSString *NewrequestData = [[NSString alloc]initWithData:requestData encoding:NSUTF8StringEncoding];
            NSLog(@"%@",NewrequestData);
            if ([_Str isEqualToString:@"存款银行卡"]) {
                [SVProgressHUD showSuccessWithStatus:@"成功添加存款账户"];
            }else{
                [SVProgressHUD showSuccessWithStatus:@"成功添加取款账户"];
            }
            _Success(_Str);
            NewPopViewController;
        } onFailure:^{
            [SVProgressHUD dismiss];
            if ([_Str isEqualToString:@"存款银行卡"]) {
                [SVProgressHUD showSuccessWithStatus:@"成功添加存款账户"];
            }else{
                [SVProgressHUD showSuccessWithStatus:@"成功添加取款账户"];
            }
            _Success(_Str);
            NewPopViewController;
        }];
    }else{
        [NetworkRequestManager requestPostWithInterfacePrefix:URl parameters:dic onSuccess:^(id requestData) {
            if ([requestData[@"code"] integerValue] != 0) {
                [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            }else{
                if ([_Str isEqualToString:@"存款银行卡"]) {
                    [SVProgressHUD showSuccessWithStatus:@"成功添加存款账户"];
                }else{
                    [SVProgressHUD showSuccessWithStatus:@"成功添加取款账户"];
                }
                _Success(_Str);
                NewPopViewController;
            }
        } onFailure:^{
            [SVProgressHUD dismiss];
        }];
    }

}
@end
