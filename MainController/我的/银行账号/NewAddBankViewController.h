//
//  NewAddBankViewController.h
//  NewProject
//
//  Created by mac on 2019/12/6.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewBasicViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewAddBankViewController : NewBasicViewController
@property(nonatomic,copy)NSString *Str;
@property(nonatomic,assign)BOOL bankcardYesOrNo;

@property(nonatomic,copy)void (^Success)(NSString *string);

@end

NS_ASSUME_NONNULL_END
