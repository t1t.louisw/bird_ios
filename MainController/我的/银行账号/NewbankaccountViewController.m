//
//  NewbankaccountViewController.m
//  NewProject
//
//  Created by mac on 2019/12/6.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewbankaccountViewController.h"
#import "NewbankaccountCell.h"
#import "NewAddBankViewController.h"
#import "NewBankDetailViewController.h"

@interface NewbankaccountViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
    NSMutableArray *CunKuandataArray;

}

@end

@implementation NewbankaccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];

    if ([_Str isEqualToString:@"存款银行卡"]) {
        [self NetworkRequestManagerquerqueryDepositBank];
        [self setNavTitle:@"银行账户"];
    }else{
        [self NetworkRequestManagerlistPlayerWithdrawAccounts];
        [self setNavTitle:@"银行账户"];
    }
    [self loadData];
    [self loadsView];
    // Do any additional setup after loading the view.
}
//存款银行卡接口
- (void)NetworkRequestManagerquerqueryDepositBank
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:queryDepositBank parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [CunKuandataArray removeAllObjects];
            for (NSDictionary *dic in requestData[@"result"]) {
                [CunKuandataArray addObject:dic];
            }
            [newtableView reloadData];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
//取款银行卡接口
- (void)NetworkRequestManagerlistPlayerWithdrawAccounts
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:listPlayerWithdrawAccounts parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            if ([requestData[@"code"] integerValue] == 298) {
                [SVProgressHUD showErrorWithStatus:@"当前账户尚未绑定取款银行卡，请您添加！"];
            }else{
                [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            }
        }else{
            [dataArray removeAllObjects];
            for (NSDictionary *dic in requestData[@"result"]) {
                [dataArray addObject:dic];
            }
            [newtableView reloadData];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)loadData {
    dataArray  = NewMutableArrayInit;
    CunKuandataArray = NewMutableArrayInit;
}
- (void)loadsView {
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;
}
#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_Str isEqualToString:@"存款银行卡"]) {
        return CunKuandataArray.count;
    }else{
        return dataArray.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    NewbankaccountCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
    if (cell == nil) {
        cell = [[NewbankaccountCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
    cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
    if ([_Str isEqualToString:@"存款银行卡"]) {
        if (CunKuandataArray.count>0) {
            [cell assignment:CunKuandataArray[indexPath.row] bankType:_Str];
        }
    }else{
        if (dataArray.count>0) {
            [cell assignment:dataArray[indexPath.row] bankType:_Str];
        }
    }

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NewBankDetailViewController *vc = [NewBankDetailViewController new];
    if ([_Str isEqualToString:@"存款银行卡"]) {
        vc.dic = CunKuandataArray[indexPath.row];
        vc.Str = _Str;
    }else{
        vc.dic = dataArray[indexPath.row];
        vc.Str = _Str;
    }
    NewPushViewController(vc);
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *HeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    HeaderView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    
    UILabel *tishiLB = [UILabel new];
    tishiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    tishiLB.text = @"*修改或删除银行卡信息联系在线客服";
    tishiLB.font = NewFont(13);
    [tishiLB setSingleLineAutoResizeWithMaxWidth:0];
    [HeaderView addSubview:tishiLB];
    
    tishiLB.sd_layout
    .topEqualToView(HeaderView)
    .heightIs(30)
    .leftSpaceToView(HeaderView, 20);
    
    return HeaderView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    footView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    
    UIButton *addBank = [UIButton new];
    [addBank setTitle:@"+ 添加取款银行卡" forState:UIControlStateNormal];
    [addBank setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    addBank.titleLabel.font = NewFont(15);
    [addBank setBackgroundColor:[UIColor colorWithString:@"#230F40"]];
    ViewBorderRadius(addBank, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
    NewTouchUpInside(addBank, addBankclick);
    [footView addSubview:addBank];
    
    addBank.sd_layout
    .heightIs(44)
    .topSpaceToView(footView, 15)
    .leftSpaceToView(footView, 15)
    .rightSpaceToView(footView, 15);
    
    return footView;
}

- (void)addBankclick{
    NewAddBankViewController *vc = [NewAddBankViewController new];
    vc.Str = _Str;
    if ([_Str isEqualToString:@"存款银行卡"]) {
        if (CunKuandataArray.count>0) {
            vc.bankcardYesOrNo = NO;
        }else{
            vc.bankcardYesOrNo = YES;
        }
        
    }else{
        if (dataArray.count>0) {
            vc.bankcardYesOrNo = NO;
        }else{
            vc.bankcardYesOrNo = YES;
        }
    }
    [vc setSuccess:^(NSString *string) {
        if ([string isEqualToString:@"存款银行卡"]) {
            [self NetworkRequestManagerquerqueryDepositBank];
        }else{
            [self NetworkRequestManagerlistPlayerWithdrawAccounts];
        }
    }];
    NewPushViewController(vc);
}
@end
