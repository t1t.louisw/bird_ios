//
//  NewBankDetailViewController.m
//  NewProject
//
//  Created by mac on 2019/12/9.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewBankDetailViewController.h"

@interface NewBankDetailViewController ()

@end

@implementation NewBankDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self setNavTitle:@"查看详情"];
    [self loadUI];
    // Do any additional setup after loading the view.
}

- (void)loadUI {
    UIView *backView = [UIView new];
    backView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(backView, 5);
    [self.view addSubview:backView];
    
    backView.sd_layout
    .heightIs(450)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(self.view, NavHeader+20);
    
    UIImageView *bankImage = [UIImageView new];
    [bankImage setImage:NewImageNamed(@"通用")];
    ViewRadius(bankImage, 25);
    [backView addSubview:bankImage];
    
    bankImage.sd_layout
    .heightIs(50)
    .widthIs(50)
    .leftSpaceToView(backView, 30)
    .topSpaceToView(backView, 30);
    NSString *jsonStr1 = [_dic[@"bankName"] stringByReplacingOccurrencesOfString:@"_json:" withString:@""]; // 去掉空格
    NSData *data = [jsonStr1 dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
    
    UILabel *bankName = [UILabel new];
    if ([_Str isEqualToString:@"存款银行卡"]) {
        [bankName setText:_dic[@"bankName"]];
    }else{
        [bankName setText:json[@"2"]];
    }
    bankName.font = NewFont(15);
    bankName.textColor = NewWhiteColor;
    [bankName setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:bankName];
    
    bankName.sd_layout
    .heightIs(15)
    .centerXEqualToView(bankImage)
    .topSpaceToView(bankImage, 15);
    
    UILabel *zhanhaoLB = [UILabel new];
    zhanhaoLB.text = @"账号:";
    zhanhaoLB.font = NewFont(15);
    zhanhaoLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zhanhaoLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:zhanhaoLB];
    
    zhanhaoLB.sd_layout
    .heightIs(50)
    .topSpaceToView(bankName, 35)
    .leftSpaceToView(backView, 30);
    
    UILabel *zhanhaoLB1 = [UILabel new];
    zhanhaoLB1.text = [NewUtils getNewStarBankNumWitOldNum:_dic[@"bankAccountNumber"]];
    zhanhaoLB1.font = NewFont(15);
    zhanhaoLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
    [zhanhaoLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:zhanhaoLB1];
    
    zhanhaoLB1.sd_layout
    .heightIs(50)
    .topSpaceToView(bankName, 35)
    .leftSpaceToView(zhanhaoLB, 2);
    
    UILabel *line = [UILabel new];
    line.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line];
    
    line.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(zhanhaoLB, 0)
    .rightSpaceToView(backView, 15);
    
    UILabel *zhanghumingLB = [UILabel new];
    zhanghumingLB.text = @"账户名:";
    zhanghumingLB.font = NewFont(15);
    zhanghumingLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zhanghumingLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:zhanghumingLB];
    
    zhanghumingLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line, 0)
    .leftSpaceToView(backView, 30);
    
    UILabel *zhanghumingLB1 = [UILabel new];
    zhanghumingLB1.text = [NSString stringWithFormat:@"%@",_dic[@"bankAccountFullName"]];
    zhanghumingLB1.font = NewFont(15);
    zhanghumingLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
    [zhanghumingLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:zhanghumingLB1];
    
    zhanghumingLB1.sd_layout
    .heightIs(50)
    .centerYEqualToView(zhanghumingLB)
    .leftSpaceToView(zhanghumingLB, 2);
    
    UILabel *line1 = [UILabel new];
    line1.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line1];
    
    line1.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(zhanghumingLB, 0)
    .rightSpaceToView(backView, 15);
    
    UILabel *kaihushengLB = [UILabel new];
    kaihushengLB.text = @"开户省:";
    kaihushengLB.font = NewFont(15);
    kaihushengLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [kaihushengLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:kaihushengLB];
    
    kaihushengLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line1, 0)
    .leftSpaceToView(backView, 30);
    
    UILabel *kaihushengLB1 = [UILabel new];
    kaihushengLB1.text = [NSString stringWithFormat:@"%@",_dic[@"province"]==NULL?@"":_dic[@"province"]];
    kaihushengLB1.font = NewFont(15);
    kaihushengLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
    [kaihushengLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:kaihushengLB1];
    
    kaihushengLB1.sd_layout
    .heightIs(50)
    .centerYEqualToView(kaihushengLB)
    .leftSpaceToView(kaihushengLB, 2);
    
    UILabel *line2 = [UILabel new];
    line2.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line2];
    
    line2.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(kaihushengLB, 0)
    .rightSpaceToView(backView, 15);
    
    UILabel *kaihushiLB = [UILabel new];
    kaihushiLB.text = @"开户市:";
    kaihushiLB.font = NewFont(15);
    kaihushiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [kaihushiLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:kaihushiLB];
    
    kaihushiLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line2, 0)
    .leftSpaceToView(backView, 30);
    
    UILabel *kaihushiLB1 = [UILabel new];
    kaihushiLB1.text = [NSString stringWithFormat:@"%@",_dic[@"city"]==NULL?@"":_dic[@"city"]];
    kaihushiLB1.font = NewFont(15);
    kaihushiLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
    [kaihushiLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:kaihushiLB1];
    
    kaihushiLB1.sd_layout
    .heightIs(50)
    .centerYEqualToView(kaihushiLB)
    .leftSpaceToView(kaihushiLB, 2);
    
    UILabel *line3 = [UILabel new];
    line3.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line3];
    
    line3.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(kaihushiLB, 0)
    .rightSpaceToView(backView, 15);
    
    UILabel *fenhangmingchengLB = [UILabel new];
    fenhangmingchengLB.text = @"分行名称:";
    fenhangmingchengLB.font = NewFont(15);
    fenhangmingchengLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [fenhangmingchengLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:fenhangmingchengLB];
    
    fenhangmingchengLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line3, 0)
    .leftSpaceToView(backView, 30);
    
    UILabel *fenhangmingchengLB1 = [UILabel new];
    fenhangmingchengLB1.text = [NSString stringWithFormat:@"%@",_dic[@"branch"]==NULL?@"":_dic[@"branch"]];
    fenhangmingchengLB1.font = NewFont(15);
    fenhangmingchengLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
    [fenhangmingchengLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:fenhangmingchengLB1];
    
    fenhangmingchengLB1.sd_layout
    .heightIs(50)
    .centerYEqualToView(fenhangmingchengLB)
    .leftSpaceToView(fenhangmingchengLB, 2);
    
    UILabel *line4 = [UILabel new];
    line4.backgroundColor = [UIColor colorWithString:@"#352251"];
    [backView addSubview:line4];
    
    line4.sd_layout
    .heightIs(1)
    .leftSpaceToView(backView, 15)
    .topSpaceToView(fenhangmingchengLB, 0)
    .rightSpaceToView(backView, 15);
    
    UILabel *shoujihaomaLB = [UILabel new];
    shoujihaomaLB.text = @"分行电话:";
    shoujihaomaLB.font = NewFont(15);
    shoujihaomaLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [shoujihaomaLB setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:shoujihaomaLB];
    
    shoujihaomaLB.sd_layout
    .heightIs(50)
    .topSpaceToView(line4, 0)
    .leftSpaceToView(backView, 30);
    
    UILabel *shoujihaomaLB1 = [UILabel new];
    shoujihaomaLB1.text = [NSString stringWithFormat:@"%@",_dic[@"phone"]==NULL?@"":_dic[@"phone"]];
    shoujihaomaLB1.font = NewFont(15);
    shoujihaomaLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
    [shoujihaomaLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [backView addSubview:shoujihaomaLB1];
    
    shoujihaomaLB1.sd_layout
    .heightIs(50)
    .centerYEqualToView(shoujihaomaLB)
    .leftSpaceToView(shoujihaomaLB, 2);
    
//    UILabel *line5 = [UILabel new];
//    line5.backgroundColor = [UIColor colorWithString:@"#352251"];
//    [backView addSubview:line5];
//
//    line5.sd_layout
//    .heightIs(1)
//    .leftSpaceToView(backView, 15)
//    .topSpaceToView(shoujihaomaLB, 0)
//    .rightSpaceToView(backView, 15);
//
//    UILabel *fenhangdizhiLB = [UILabel new];
//    fenhangdizhiLB.text = @"分行地址:";
//    fenhangdizhiLB.font = NewFont(15);
//    fenhangdizhiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
//    [fenhangdizhiLB setSingleLineAutoResizeWithMaxWidth:0];
//    [backView addSubview:fenhangdizhiLB];
//
//    fenhangdizhiLB.sd_layout
//    .heightIs(50)
//    .topSpaceToView(line5, 0)
//    .leftSpaceToView(backView, 30);
//
//    UILabel *fenhangdizhiLB1 = [UILabel new];
//    fenhangdizhiLB1.text = @"长沙市五一大道";
//    fenhangdizhiLB1.font = NewFont(15);
//    fenhangdizhiLB1.textColor = [UIColor colorWithString:@"#E6E2ED"];
//    [fenhangdizhiLB1 setSingleLineAutoResizeWithMaxWidth:0];
//    [backView addSubview:fenhangdizhiLB1];
//
//    fenhangdizhiLB1.sd_layout
//    .heightIs(50)
//    .centerYEqualToView(fenhangdizhiLB)
//    .leftSpaceToView(fenhangdizhiLB, 2);
    
//    UIButton *tijiaoBT = [UIButton new];
//    [tijiaoBT setTitle:@"删除账户" forState:UIControlStateNormal];
//    [tijiaoBT setTitleColor:[UIColor colorWithString:@"#FFFFFF"] forState:UIControlStateNormal];
//    [tijiaoBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
//    tijiaoBT.titleLabel.font = NewFont(15);
//    ViewRadius(tijiaoBT, 5);
//    [self.view addSubview:tijiaoBT];
//    
//    tijiaoBT.sd_layout
//    .heightIs(44)
//    .leftSpaceToView(self.view, 15)
//    .rightSpaceToView(self.view, 15)
//    .topSpaceToView(backView, 20);
}


@end
