//
//  NewViewbankcardViewController.m
//  NewProject
//
//  Created by mac on 2019/12/26.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewViewbankcardViewController.h"
#import "NewbankaccountViewController.h"

@interface NewViewbankcardViewController ()

@end

@implementation NewViewbankcardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self setNavTitle:@"查看银行卡"];
    [self loadsView];
}

- (void)loadsView {
    UIButton *CunkuanBank = [UIButton new];
    [CunkuanBank setTitle:@"查看存款银行" forState:UIControlStateNormal];
    [CunkuanBank setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    CunkuanBank.titleLabel.font = NewFont(15);
    [CunkuanBank setBackgroundColor:[UIColor colorWithString:@"#230F40"]];
    ViewBorderRadius(CunkuanBank, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
    NewTouchUpInside(CunkuanBank, CunkuanBankclick:);
    CunkuanBank.tag = 100;
    [self.view addSubview:CunkuanBank];
    
    CunkuanBank.sd_layout
    .heightIs(44)
    .topSpaceToView(self.view, NavHeader+15)
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30);
    
    UIButton *QukuanBank = [UIButton new];
    [QukuanBank setTitle:@"查看取款银行" forState:UIControlStateNormal];
    [QukuanBank setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    QukuanBank.titleLabel.font = NewFont(15);
    [QukuanBank setBackgroundColor:[UIColor colorWithString:@"#230F40"]];
    ViewBorderRadius(QukuanBank, 3, 1, [UIColor colorWithString:@"#FF6D44"]);
    NewTouchUpInside(QukuanBank, CunkuanBankclick:);
    QukuanBank.tag = 101;
    [self.view addSubview:QukuanBank];
    
    QukuanBank.sd_layout
    .heightIs(44)
    .topSpaceToView(CunkuanBank, 25)
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30);
    
}

- (void)CunkuanBankclick:(UIButton *)sender{
    if (sender.tag == 100) {
        NewbankaccountViewController *vc = [NewbankaccountViewController new];
        vc.Str = @"存款银行卡";
        NewPushViewController(vc);
    }else{
        NewbankaccountViewController *vc = [NewbankaccountViewController new];
        vc.Str = @"取款银行卡";
        NewPushViewController(vc);
    }
}
@end
