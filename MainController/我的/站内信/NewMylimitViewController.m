//
//  NewMylimitViewController.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewMylimitViewController.h"
#import "NewMylimitCell.h"
#import "NewFundslimitViewController.h"
#import "NewStandinsideletterViewController.h"
#import "NewMessageDetailViewController.h"

@interface NewMylimitViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
}
@property (nonatomic, assign) NSInteger page;

@end

@implementation NewMylimitViewController

- (void)viewWillAppear:(BOOL)animated
{
    [self NetworkRequestManagergetmessage];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"站内信"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#291744"];
    [self dataInitialization];
    [self loadsView];
    [self addPulltoRefresh];

}
- (void)addPulltoRefresh {
    __weak __typeof(self) mySelf = self;
    newtableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        mySelf.page = 0;
        [mySelf NetworkRequestManagergetmessage];
    }];
    newtableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        mySelf.page = mySelf.page + 20;
        [mySelf NetworkRequestManagergetmessage];
    }];
    [newtableView.mj_header beginRefreshing];
//    [newtableView.mj_footer beginRefreshing];

}
- (void)dataInitialization {
    dataArray = NewMutableArrayInit;
}
- (void)NetworkRequestManagergetmessage {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:@"" forKey:@"messageId"];
    [dic setValue:@"allMessage" forKey:@"messageType"];
    [dic setValue:[NSString stringWithFormat:@"%ld",self.page] forKey:@"offset"];
    [dic setValue:@"20" forKey:@"limit"];
    [SVProgressHUD show];
    __weak __typeof(self) mySelf = self;
    [NetworkRequestManager requestPostWithInterfacePrefix:Newmessage parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            [SVProgressHUD dismiss];
            [newtableView.mj_header endRefreshing];
            [newtableView.mj_footer endRefreshing];
        }else{
            if (mySelf.page == 0) {
                [dataArray removeAllObjects];
                dataArray = [NSMutableArray arrayWithArray:requestData[@"result"][@"messages"]];
            }else {
                [dataArray addObjectsFromArray:requestData[@"result"][@"messages"]];
            }
            [newtableView reloadData];
            [SVProgressHUD dismiss];
            [newtableView.mj_header endRefreshing];
            [newtableView.mj_footer endRefreshing];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)loadsView {
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;
    
    UIButton *fankuiBT = [UIButton new];
    [fankuiBT setImage:NewImageNamed(@"编辑") forState:UIControlStateNormal];
    [fankuiBT setBackgroundColor:[UIColor colorWithString:@"FF6D44"]];
    NewTouchUpInside(fankuiBT, fankuiBTclick);
    ViewRadius(fankuiBT, 30);
    [self.view addSubview:fankuiBT];
    
    fankuiBT.sd_layout
    .heightIs(60)
    .widthIs(60)
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.view, 90);
}
#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    NewMylimitCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
    if (cell == nil) {
        cell = [[NewMylimitCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
    if (dataArray.count>0) {
        [cell assignment:dataArray[indexPath.row]];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (dataArray.count>0) {
        NewMessageDetailViewController *vc = [NewMessageDetailViewController new];
        vc.dic = dataArray[indexPath.row];
        NewPushViewController(vc);
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headView = [UIView new];
    headView.backgroundColor = [UIColor colorWithString:@"#291744"];
    UILabel *zhutiLB = [UILabel new];
    zhutiLB.text = @"主题";
    zhutiLB.font = NewFont(15);
    zhutiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
//    [zhutiLB setSingleLineAutoResizeWithMaxWidth:0];
    zhutiLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:zhutiLB];
    
    zhutiLB.sd_layout
    .heightIs(15)
    .centerYEqualToView(headView)
    .leftSpaceToView(headView, 0)
    .widthIs(SCREEN_WIDTH/3);
    
    UILabel *xinxiLB = [UILabel new];
    xinxiLB.text = @"信息";
    xinxiLB.font = NewFont(15);
    xinxiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
//    [xinxiLB setSingleLineAutoResizeWithMaxWidth:0];
    xinxiLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:xinxiLB];
    
    xinxiLB.sd_layout
    .heightIs(15)
    .centerYEqualToView(headView)
    .leftSpaceToView(zhutiLB, 0)
    .widthIs(SCREEN_WIDTH/3);
    
    UILabel *timeLB = [UILabel new];
    timeLB.text = @"时间";
    timeLB.font = NewFont(15);
    timeLB.textColor = [UIColor colorWithString:@"#AFACB4"];
//    [timeLB setSingleLineAutoResizeWithMaxWidth:0];
    timeLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:timeLB];
    
    timeLB.sd_layout
    .heightIs(15)
    .centerYEqualToView(headView)
    .leftSpaceToView(xinxiLB, 0)
    .widthIs(SCREEN_WIDTH/3);
    
    return headView;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (void)fankuiBTclick {
    NewStandinsideletterViewController *vc = [NewStandinsideletterViewController new];
    NewPushViewController(vc);
}
@end
