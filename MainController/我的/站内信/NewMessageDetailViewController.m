//
//  NewMessageDetailViewController.m
//  NewProject
//
//  Created by mac on 2020/1/17.
//  Copyright © 2020 NewOrganization. All rights reserved.
//

#import "NewMessageDetailViewController.h"

@interface NewMessageDetailViewController ()

@end

@implementation NewMessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self setNavTitle:@"站内信"];
    [self NetworkRequestManagergetmessageSetRead];
    [self NetworkRequestManagergetmessage];
    [self loadsView];
}
- (void)NetworkRequestManagergetmessageSetRead {
        NSMutableDictionary *dic = NewMutableDictionaryInit;
        [dic setValue:ApiKey forKey:@"api_key"];
        [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
        [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
        [dic setValue:_dic[@"messageId"] forKey:@"messageId"];
    //    __weak __typeof(self) mySelf = self;
        [NetworkRequestManager requestPostWithInterfacePrefix:messageSetRead parameters:dic onSuccess:^(id requestData) {
            if ([requestData[@"code"] integerValue] != 0) {
                [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            }else{
                NSLog(@"");
            }
        } onFailure:^{
            [SVProgressHUD dismiss];
    }];
}
- (void)NetworkRequestManagergetmessage {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:_dic[@"messageId"] forKey:@"messageId"];
    [SVProgressHUD show];
//    __weak __typeof(self) mySelf = self;
    [NetworkRequestManager requestPostWithInterfacePrefix:Newmessage parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            NSLog(@"");
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
- (void)loadsView {
    UIView *headview = [UIView new];
    headview.backgroundColor = [UIColor colorWithString:@"#291744"];
    [self.view addSubview:headview];
    
    headview.sd_layout
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .topSpaceToView(self.view, NavHeader)
    .heightIs(80);
    
    UILabel *headlabel = [UILabel new];
    headlabel.backgroundColor = [UIColor colorWithString:@"#291744"];
    headlabel.font = NewFont(15);
    headlabel.textAlignment = NSTextAlignmentLeft;
    headlabel.text = [NSString stringWithFormat:@"主题:%@",_dic[@"subject"]];
    headlabel.textColor = [UIColor colorWithString:@"#AFACB4"];
    headlabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [headview addSubview:headlabel];
    
    headlabel.sd_layout
    .rightSpaceToView(headview, 10)
    .leftSpaceToView(headview, 10)
    .heightIs(40)
    .topEqualToView(headview);
    
    UILabel *timelabel = [UILabel new];
    timelabel.backgroundColor = [UIColor colorWithString:@"#291744"];
    timelabel.font = NewFont(15);
    timelabel.textAlignment = NSTextAlignmentLeft;
    timelabel.text = [NSString stringWithFormat:@"时间:%@",_dic[@"date"]];
    timelabel.textColor = [UIColor colorWithString:@"#AFACB4"];
    timelabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [headview addSubview:timelabel];
    
    timelabel.sd_layout
    .rightSpaceToView(headview, 10)
    .leftSpaceToView(headview, 10)
    .heightIs(40)
    .topSpaceToView(headlabel, 0);
    
    UILabel *detailabel = [UILabel new];
    detailabel.text = _dic[@"detail"];
    detailabel.font = NewFont(15);
    detailabel.textColor = [UIColor colorWithString:@"#AFACB4"];
    [self.view addSubview:detailabel];
    
    detailabel.sd_layout
    .autoHeightRatio(0)
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30)
    .topSpaceToView(headview, 20);

}
@end
