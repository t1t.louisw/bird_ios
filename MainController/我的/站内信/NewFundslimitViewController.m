//
//  NewFundslimitViewController.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewFundslimitViewController.h"

@interface NewFundslimitViewController (){
    UITextField *zuidiTextField;
    UITextField *zuigaoTextField;
    
}
@property (nonatomic, strong) NSMutableDictionary *selectDic;
@property (nonatomic, strong) UIView *selectView;

@end

@implementation NewFundslimitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"赌资限额"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self loadview];
    // Do any additional setup after loading the view.
}

- (void)loadview {
    UILabel *duziXD = [UILabel new];
    duziXD.text = @"赌资金额限定";
    duziXD.font = NewFont(15);
    duziXD.textColor = [UIColor colorWithString:@"#AFACB4"];
    [duziXD setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:duziXD];
    
    duziXD.sd_layout
    .heightIs(15)
    .leftSpaceToView(self.view, 15)
    .topSpaceToView(self.view,NavHeader+15);
    
    UIView *zuidiView = [UIView new];
    zuidiView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(zuidiView, 3);
    [self.view addSubview:zuidiView];
    
    zuidiView.sd_layout
    .heightIs(45)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(duziXD, 15);
    
    UILabel *zuidiLB = [UILabel new];
    zuidiLB.text = @"最低限额：";
    zuidiLB.font = NewFont(15);
    zuidiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zuidiLB setSingleLineAutoResizeWithMaxWidth:0];
    [zuidiView addSubview:zuidiLB];
    
    zuidiLB.sd_layout
    .heightIs(30)
    .centerYEqualToView(zuidiView)
    .leftSpaceToView(zuidiView, 15);
    
    
    UIView *zuigaoView = [UIView new];
    zuigaoView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(zuigaoView, 3);
    [self.view addSubview:zuigaoView];
    
    zuigaoView.sd_layout
    .heightIs(45)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(zuidiView, 15);
    
    UILabel *zuigaoLB = [UILabel new];
    zuigaoLB.text = @"最高限额：";
    zuigaoLB.font = NewFont(15);
    zuigaoLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [zuigaoLB setSingleLineAutoResizeWithMaxWidth:0];
    [zuigaoView addSubview:zuigaoLB];
    
    zuigaoLB.sd_layout
    .heightIs(30)
    .centerYEqualToView(zuigaoView)
    .leftSpaceToView(zuigaoView, 15);
    
    zuidiTextField = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewButtonColor textColor:NewWhiteColor placeholder:@"" hidden:NO tag:100 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [zuidiTextField addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    zuidiTextField.text = @"¥0.00";
    [zuidiView addSubview:zuidiTextField];
    
    zuidiTextField.sd_layout
    .heightIs(45)
    .centerYEqualToView(zuidiView)
    .leftSpaceToView(zuidiLB, 1)
    .rightSpaceToView(zuidiView, 15);
    
    zuigaoTextField = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewButtonColor textColor:NewWhiteColor placeholder:@"" hidden:NO tag:100 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [zuigaoTextField addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    zuigaoTextField.text = @"¥0.00";
    [zuigaoView addSubview:zuigaoTextField];
    
    zuigaoTextField.sd_layout
    .heightIs(45)
    .centerYEqualToView(zuigaoView)
    .leftSpaceToView(zuigaoLB, 1)
    .rightSpaceToView(zuigaoView, 15);
    
    UILabel *duziTimeXD = [UILabel new];
    duziTimeXD.text = @"赌资时长限定";
    duziTimeXD.font = NewFont(15);
    duziTimeXD.textColor = [UIColor colorWithString:@"#AFACB4"];
    [duziTimeXD setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:duziTimeXD];
    
    duziTimeXD.sd_layout
    .heightIs(15)
    .leftSpaceToView(self.view, 15)
    .topSpaceToView(zuigaoView,30);
    
    _selectView = [UIView new];
    _selectView.backgroundColor = [UIColor colorWithString:@"230F40"];
    [self.view addSubview:_selectView];
    
    _selectView.sd_layout
    .heightIs(45)
    .widthIs(SCREEN_WIDTH)
    .leftEqualToView(self.view)
    .topSpaceToView(self.view, NavHeader+210);
    
    NSArray *arr = @[@"1天",@"2天",@"3天",@"4天"];
    CGFloat btWidth = (SCREEN_WIDTH-60)/4;
    for (int i = 0; i<arr.count; i++) {
        UIButton *btn = [UIButton new];
        btn.frame = CGRectMake(i*(btWidth+10)+15, 0, btWidth, 45);
        btn.tag = i+100;
        [btn setBackgroundColor:[UIColor colorWithString:@"#291744"]];
        NewTouchUpInside(btn, XZchongwutouchAction:);
        [_selectView addSubview:btn];
        
        UILabel *titleLB = [UILabel new];
        titleLB.text = arr[i];
        titleLB.font = NewFont(15);
        titleLB.textColor = [UIColor colorWithString:@"#E6E2ED"];
        [titleLB setSingleLineAutoResizeWithMaxWidth:0];
        [btn addSubview:titleLB];
        
        titleLB.sd_layout
        .heightIs(30)
        .centerYEqualToView(btn)
        .centerXEqualToView(btn);
        
        UIImageView *xuanzhong = [UIImageView new];
        [xuanzhong setImage:NewImageNamed(@"选中-1")];
        xuanzhong.tag = 100;
        [btn addSubview:xuanzhong];
        
        xuanzhong.sd_layout
        .heightIs(20)
        .widthIs(20)
        .rightEqualToView(btn)
        .bottomEqualToView(btn);
        
        if (i == 0) {
            btn.selected = YES;
            ViewBorderRadius(btn, 3, 0.8, [UIColor colorWithString:@"#FF6D44"]);
            xuanzhong.hidden = NO;
        }else{
            xuanzhong.hidden = YES;
        }
    }
    UILabel *tishiLB = [UILabel new];
    tishiLB.text = @"选中此框，将锁定我的帐户限定时长";
    tishiLB.font = NewFont(13);
    tishiLB.textColor = [UIColor colorWithString:@"#AFACB4"];
    [tishiLB setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:tishiLB];
    
    tishiLB.sd_layout
    .heightIs(15)
    .leftSpaceToView(self.view, 15)
    .topSpaceToView(_selectView,20);
    
    UIButton *quedingBT = [UIButton new];
    [quedingBT setTitle:@"确定" forState:UIControlStateNormal];
    [quedingBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    [quedingBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    quedingBT.titleLabel.font = NewFont(15);
    ViewRadius(quedingBT, 3);
    [self.view addSubview:quedingBT];
    
    quedingBT.sd_layout
    .heightIs(45)
    .widthIs(250)
    .leftSpaceToView(self.view, (SCREEN_WIDTH-250)/2)
    .topSpaceToView(tishiLB, 60);
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
}
- (void)XZchongwutouchAction:(UIButton *)sender {
    UIView *view = [_selectView viewWithTag:[[_selectDic objectForKey:@"tag"] integerValue]];

    for (UIView *sub in view.subviews) {
        for (UIView *lyf in sub.subviews) {
            if ([lyf isKindOfClass:[UIImageView class]]) {
                if (((UIImageView *)lyf).tag == 100) {
                    ((UIImageView *)lyf).hidden = YES;
                }
            }
        }
    }
    for (UIView *sub in sender.subviews) {
        if ([sub isKindOfClass:[UIImageView class]]) {
            if (((UIImageView *)sub).tag == 100) {
                ((UIImageView *)sub).hidden = NO;
            }
        }
    }
    NSArray *arr = @[@"1天",@"2天",@"3天",@"4天"];

    for (int i = 0; i<arr.count; i++) {
        UIButton *btn = [self.selectView viewWithTag:i + 100];
        btn.selected = NO;
        ViewBorderRadius(btn, 3, 0, NewClearColor);
    }
    sender.selected = YES;
////    guwenID = consultantArray[sender.tag-100][@"userId"];
    ViewBorderRadius(sender, 3, 0.8, [UIColor colorWithString:@"#FF6D44"]);
    [_selectDic setObject:[NSString stringWithFormat:@"%ld",sender.tag] forKey:@"tag"];

}
@end
