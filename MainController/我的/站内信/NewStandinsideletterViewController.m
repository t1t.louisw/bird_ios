//
//  NewStandinsideletterViewController.m
//  NewProject
//
//  Created by mac on 2019/12/11.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewStandinsideletterViewController.h"

@interface NewStandinsideletterViewController ()<UITextViewDelegate>{
    NSString *yijianStr;
    NSString *ZhutiStr;

}

@end

@implementation NewStandinsideletterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"站内信"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self loadview];
    // Do any additional setup after loading the view.
}

- (void)loadview {
    UIView *sevenbackView = [UIView new];
    sevenbackView.backgroundColor = [UIColor colorWithString:@"#32204C"];
    ViewRadius(sevenbackView, 3);
    [self.view addSubview:sevenbackView];
    
    sevenbackView.sd_layout
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(44)
    .topSpaceToView(self.view, NavHeader+20);
    
    UITextField *remarktextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewClearColor textColor:NewWhiteColor placeholder:@"主题" hidden:NO tag:100 font:NewFont(16) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [remarktextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr3 = [[NSMutableAttributedString alloc]initWithString:remarktextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:16]}];
    remarktextfield.attributedPlaceholder = arrStr3;
    [sevenbackView addSubview:remarktextfield];

    remarktextfield.sd_layout
    .leftSpaceToView(sevenbackView, 10)
    .heightIs(44)
    .rightSpaceToView(sevenbackView, 10)
    .centerYEqualToView(sevenbackView);
    
    UITextView *textview = [[UITextView alloc] init];
    textview.font = NewFont(16);
    textview.textColor = [UIColor colorWithString:@"#7E7B83"];
    textview.backgroundColor = [UIColor colorWithString:@"#32204C"];
    textview.text = @"你的信";
    ViewBorderRadius(textview, 5, 1, NewClearColor);
    textview.delegate = self;
    [self.view addSubview:textview];
    
    textview.sd_layout
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(sevenbackView, 20)
    .heightIs(200);
    
    UIButton *tijiaoBT = [UIButton new];
    [tijiaoBT setTitle:@"提交" forState:UIControlStateNormal];
    [tijiaoBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    tijiaoBT.titleLabel.font = NewFont(15);
    [tijiaoBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(tijiaoBT, 5);
    NewTouchUpInside(tijiaoBT, tijiaoBT);
    [self.view addSubview:tijiaoBT];
    
    tijiaoBT.sd_layout
    .leftEqualToView(textview)
    .rightEqualToView(textview)
    .heightIs(50)
    .topSpaceToView(textview, 40);
    
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    ZhutiStr = textField.text;
}

#pragma mark - UITextView获得焦点之后，并且已经是第一响应者
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    // NSLog(@"UITextView获得焦点之后，并且已经是第一响应者");
    if ([textView.text isEqualToString:@"你的信"]) {
        textView.text = @"";
        textView.textColor = NewWhiteColor;
        yijianStr = textView.text;
    }
}

#pragma mark - UITextView失去焦点之后
-(void)textViewDidEndEditing:(UITextView *)textView
{
    //NSLog(@"UITextView失去焦点");
    if ([textView.text isEqualToString:@""] || textView.text.length == 0) {
        textView.text = @"你的信";
        textView.textColor = [UIColor colorWithString:@"#7E7B83"];
        yijianStr = textView.text;
    }
}

#pragma mark - 详细地址限制字数
-(void)textViewDidChangeSelection:(UITextView *)textView
{
        if(textView.text.length > 200){
            textView.text = [textView.text substringWithRange:NSMakeRange(0,200)];
        }
        if (textView.text.length > 0) {
            NSLog(@"textView.text:%@",textView.text);
            if ([textView.text isEqualToString:@"你的信"]) {
                yijianStr = @"";
            }else{
                yijianStr = nil;
                yijianStr = textView.text;
            }
        }
}

- (void)tijiaoBT {
    if (ZhutiStr.length<0) {
        [SVProgressHUD showErrorWithStatus:@"请填写主题"];
        return;
    }else if (yijianStr.length<0){
        [SVProgressHUD showErrorWithStatus:@"请填写信息内容"];
        return;
    }
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    [dic setValue:@"" forKey:@"messageId"];
    [dic setValue:ZhutiStr forKey:@"subject"];
    [dic setValue:yijianStr forKey:@"message"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:addMessage parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:requestData[@"message"]];
            NewPopViewController;
        }
    } onFailure:^{
        [SVProgressHUD dismiss];

    }];
}
@end
