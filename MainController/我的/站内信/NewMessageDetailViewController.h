//
//  NewMessageDetailViewController.h
//  NewProject
//
//  Created by mac on 2020/1/17.
//  Copyright © 2020 NewOrganization. All rights reserved.
//

#import "NewBasicViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewMessageDetailViewController : NewBasicViewController
@property(nonatomic,copy)NSMutableDictionary *dic;
@end

NS_ASSUME_NONNULL_END
