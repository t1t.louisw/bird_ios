//
//  NewChangePasswordViewController.m
//  NewProject
//
//  Created by mac on 2019/12/31.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewChangePasswordViewController.h"

@interface NewChangePasswordViewController (){
    NSString *ordPassWord;
    NSString *NewPassWord;
    NSString *NewPassWord1;

}

@end

@implementation NewChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:[NSString stringWithFormat:@"修改%@密码",self.str]];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self dataInitialization];
    [self loadsView];
}
- (void)dataInitialization {
    
}
- (void)loadsView {
    UIView *sevenbackView = [UIView new];
    sevenbackView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(sevenbackView, 3);
    [self.view addSubview:sevenbackView];
    
    sevenbackView.sd_layout
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(44)
    .topSpaceToView(self.view, NavHeader+20);
    
    UITextField *remarktextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewClearColor textColor:NewWhiteColor placeholder:[NSString stringWithFormat:@"请输入您的旧%@密码",_str] hidden:NO tag:100 font:NewFont(16) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [remarktextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr3 = [[NSMutableAttributedString alloc]initWithString:remarktextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:16]}];
    remarktextfield.attributedPlaceholder = arrStr3;
    [sevenbackView addSubview:remarktextfield];

    remarktextfield.sd_layout
    .leftSpaceToView(sevenbackView, 10)
    .heightIs(44)
    .rightSpaceToView(sevenbackView, 10)
    .centerYEqualToView(sevenbackView);
    
    UIView *onebackView = [UIView new];
    onebackView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(onebackView, 3);
    [self.view addSubview:onebackView];
    
    onebackView.sd_layout
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(44)
    .topSpaceToView(sevenbackView, 15);
    
    UITextField *onebacktextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewClearColor textColor:NewWhiteColor placeholder:[NSString stringWithFormat:@"请输入您的新%@密码",_str] hidden:NO tag:101 font:NewFont(16) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [onebacktextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:onebacktextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:16]}];
    onebacktextfield.attributedPlaceholder = arrStr;
    [onebackView addSubview:onebacktextfield];

    onebacktextfield.sd_layout
    .leftSpaceToView(onebackView, 10)
    .heightIs(44)
    .rightSpaceToView(onebackView, 10)
    .centerYEqualToView(onebackView);
    
    UIView *twobackView = [UIView new];
    twobackView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(twobackView, 3);
    [self.view addSubview:twobackView];
    
    twobackView.sd_layout
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .heightIs(44)
    .topSpaceToView(onebackView, 15);
    
    UITextField *twobacktextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:NewClearColor textColor:NewWhiteColor placeholder:[NSString stringWithFormat:@"请再次输入您的新%@密码",_str] hidden:NO tag:102 font:NewFont(16) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [twobacktextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr2 = [[NSMutableAttributedString alloc]initWithString:twobacktextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:16]}];
    twobacktextfield.attributedPlaceholder = arrStr2;
    [twobackView addSubview:twobacktextfield];

    twobacktextfield.sd_layout
    .leftSpaceToView(twobackView, 10)
    .heightIs(44)
    .rightSpaceToView(twobackView, 10)
    .centerYEqualToView(twobackView);
    
    UIButton *tijiaoBT = [UIButton new];
    [tijiaoBT setTitle:@"确定" forState:UIControlStateNormal];
    [tijiaoBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    tijiaoBT.titleLabel.font = NewFont(15);
    [tijiaoBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(tijiaoBT, 5);
    NewTouchUpInside(tijiaoBT, tijiaoBT);
    [self.view addSubview:tijiaoBT];
    
    tijiaoBT.sd_layout
    .leftEqualToView(twobackView)
    .rightEqualToView(twobackView)
    .heightIs(50)
    .topSpaceToView(twobackView, 30);
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    if (textField.tag == 100) {
        ordPassWord = textField.text;
    }else if (textField.tag == 101){
        NewPassWord = textField.text;
    }else if (textField.tag == 102){
        NewPassWord1 = textField.text;
    }
    NSLog(@"%@",textField.text);
}
-(void)tijiaoBT {
    if (![NewPassWord isEqualToString:NewPassWord1]) {
        [SVProgressHUD showErrorWithStatus:@"您输入的2次新密码不相同"];
        return;
    }
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
    NSString *url;

    if ([_str isEqualToString:@"登录"]) {
        [dic setValue:ordPassWord forKey:@"password_old"];
        [dic setValue:NewPassWord1 forKey:@"password"];
        url = updatePlayerPassword;
    }
//    else{
//        [dic setValue:ordPassWord forKey:@"old_password"];
//        [dic setValue:NewPassWord1 forKey:@"new_password"];
//        [dic setValue:@"0" forKey:@"force_reset"];
//        url = updatePlayerWithdrawalPassword;
//    }
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:url parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            if ([requestData[@"code"] integerValue] == 273) {
                [SVProgressHUD showErrorWithStatus:@"舊密碼為空"];
            }else if ([requestData[@"code"] integerValue] == 274){
                [SVProgressHUD showErrorWithStatus:@"新密碼為空"];
            }else if ([requestData[@"code"] integerValue] == 275){
                [SVProgressHUD showErrorWithStatus:@"新密碼格式錯誤。錯誤信息後面會附加格式錯誤代碼"];
            }else if ([requestData[@"code"] integerValue] == 276){
                [SVProgressHUD showErrorWithStatus:@"舊密碼不匹配"];
            }else if ([requestData[@"code"] integerValue] == 277){
                [SVProgressHUD showErrorWithStatus:@"新密碼不可和舊密碼重覆"];
            }
            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
            [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"修改%@密码成功",_str]];
            [self NetworkRequestManagerGetupdatePlayerWithdrawalPassword];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];

    }];
}
-(void)NetworkRequestManagerGetupdatePlayerWithdrawalPassword {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
//    [dic setValue:ordPassWord forKey:@"old_password"];
    [dic setValue:NewPassWord1 forKey:@"new_password"];
    [dic setValue:@"1" forKey:@"force_reset"];
//    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:updatePlayerWithdrawalPassword parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
//            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
//            [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"修改%@密码成功",_str]];
            NewPopViewController;
        }
    } onFailure:^{
//        [SVProgressHUD dismiss];
    }];
}
- (BOOL)checkPassword:(NSString *)password
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,20}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
}
@end
