//
//  NewRealnameViewController.m
//  NewProject
//
//  Created by mac on 2019/12/5.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewRealnameViewController.h"
#import "UIViewController+XHPhoto.h"

@interface NewRealnameViewController ()

@end

@implementation NewRealnameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self setNavTitle:@"真实姓名验证"];
    [self loadUI];
    // Do any additional setup after loading the view.
}

- (void)loadUI{
    UILabel *tishiLB = [UILabel new];
    tishiLB.text = @"  为了您的资金安全，请上传与您身份证一致的信息，否则不会通过";
    tishiLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    tishiLB.font = NewFont(12);
    tishiLB.numberOfLines = 0;
    tishiLB.lineBreakMode = NSLineBreakByWordWrapping;
    [self.view addSubview:tishiLB];
    
    tishiLB.sd_layout
    .heightIs(40)
    .topSpaceToView(self.view, NavHeader+10)
    .leftSpaceToView(self.view, 30)
    .rightSpaceToView(self.view, 30);
    
    UIButton *AddphotosBT = [UIButton new];
    [AddphotosBT setImage:NewImageNamed(@"添加照片-1") forState:UIControlStateNormal];
//    [AddphotosBT setBackgroundColor:NewRedColor];
    NewTouchUpInside(AddphotosBT, AddphotosBTclick:);
    [self.view addSubview:AddphotosBT];
    
    AddphotosBT.sd_layout
    .widthIs(SCREEN_WIDTH-120)
    .heightIs(SCREEN_WIDTH-120-40)
    .topSpaceToView(tishiLB, 45)
    .centerXEqualToView(self.view);
    
    UIButton *cunkuanBT = [UIButton new];
    [cunkuanBT setTitle:@"提交验证" forState:UIControlStateNormal];
    [cunkuanBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    cunkuanBT.titleLabel.font = NewFont(15);
    [cunkuanBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(cunkuanBT, 3);
    [self.view addSubview:cunkuanBT];

    cunkuanBT.sd_layout
    .heightIs(44)
    .widthIs(SCREEN_WIDTH-120)
    .centerXEqualToView(self.view)
    .topSpaceToView(AddphotosBT, 50);
}

- (void)AddphotosBTclick:(UIButton *)sender{
    /*
        edit:照片需要裁剪:传YES,不需要裁剪传NO(默认NO)
     */
    [self showCanEdit:NO photo:^(UIImage *photo) {
        
        [sender setBackgroundImage:photo forState:UIControlStateNormal];
        
    }];
}


@end
