//
//  NewiphonevalidationViewController.m
//  NewProject
//
//  Created by mac on 2019/12/5.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewiphonevalidationViewController.h"

@interface NewiphonevalidationViewController ()

@end

@implementation NewiphonevalidationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"手机号码验证"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self loadUI];
    // Do any additional setup after loading the view.
}

- (void)loadUI {
    UILabel *iphoneLB = [UILabel new];
    iphoneLB.text = @"手机号码";
    iphoneLB.font = NewFont(12);
    iphoneLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [iphoneLB setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:iphoneLB];
    
    iphoneLB.sd_layout
    .heightIs(12)
    .leftSpaceToView(self.view, 15)
    .topSpaceToView(self.view, NavHeader+20);
    
    UIView *iphoneNumberView = [UIView new];
    iphoneNumberView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(iphoneNumberView, 3);
    [self.view addSubview:iphoneNumberView];
    
    iphoneNumberView.sd_layout
    .topSpaceToView(iphoneLB, 15)
    .heightIs(45)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15);
    
    UITextField *iphoneNumbertextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:NewWhiteColor placeholder:@"请输入手机号码" hidden:NO tag:100 font:NewFont(20) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [iphoneNumbertextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr5 = [[NSMutableAttributedString alloc]initWithString:iphoneNumbertextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    iphoneNumbertextfield.attributedPlaceholder = arrStr5;
    [iphoneNumberView addSubview:iphoneNumbertextfield];
            
    iphoneNumbertextfield.sd_layout
    .topEqualToView(iphoneNumberView)
    .bottomEqualToView(iphoneNumberView)
    .leftSpaceToView(iphoneNumberView, 5)
    .rightSpaceToView(iphoneNumberView, 110);
    
    UIButton *getVerificationcodeBT = [UIButton new];
    [getVerificationcodeBT setBackgroundColor:[UIColor colorWithString:@"#476AD4"]];
    [getVerificationcodeBT setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerificationcodeBT setTitleColor:[UIColor colorWithString:@"#E1E5F2"] forState:UIControlStateNormal];
    getVerificationcodeBT.titleLabel.font = NewFont(15);
    getVerificationcodeBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [iphoneNumberView addSubview:getVerificationcodeBT];
    
    getVerificationcodeBT.sd_layout
    .rightEqualToView(iphoneNumberView)
    .topEqualToView(iphoneNumberView)
    .bottomEqualToView(iphoneNumberView)
    .widthIs(110);
    
    UILabel *VerificationcodeLB = [UILabel new];
    VerificationcodeLB.text = @"验证码";
    VerificationcodeLB.font = NewFont(12);
    VerificationcodeLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    [VerificationcodeLB setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:VerificationcodeLB];
    
    VerificationcodeLB.sd_layout
    .heightIs(12)
    .leftSpaceToView(self.view, 15)
    .topSpaceToView(iphoneNumberView, 30);
    
    UIView *VerificationcodeView = [UIView new];
    VerificationcodeView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(VerificationcodeView, 3);
    [self.view addSubview:VerificationcodeView];
    
    VerificationcodeView.sd_layout
    .topSpaceToView(VerificationcodeLB, 15)
    .heightIs(45)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15);
        
    UITextField *Verificationcodetextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:NewWhiteColor placeholder:@"请输入验证码" hidden:NO tag:100 font:NewFont(20) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [Verificationcodetextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:Verificationcodetextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    Verificationcodetextfield.attributedPlaceholder = arrStr;
    [VerificationcodeView addSubview:Verificationcodetextfield];
            
    Verificationcodetextfield.sd_layout
    .topEqualToView(VerificationcodeView)
    .bottomEqualToView(VerificationcodeView)
    .leftSpaceToView(VerificationcodeView, 5)
    .rightEqualToView(VerificationcodeView);
    
    UIButton *cunkuanBT = [UIButton new];
    [cunkuanBT setTitle:@"提交验证" forState:UIControlStateNormal];
    [cunkuanBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    cunkuanBT.titleLabel.font = NewFont(15);
    [cunkuanBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(cunkuanBT, 3);
    [self.view addSubview:cunkuanBT];

    cunkuanBT.sd_layout
    .heightIs(44)
    .rightSpaceToView(self.view, 15)
    .leftSpaceToView(self.view, 15)
    .topSpaceToView(VerificationcodeView, 40);
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
}
@end
