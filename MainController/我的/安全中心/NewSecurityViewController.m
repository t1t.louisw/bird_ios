//
//  NewSecurityViewController.m
//  NewProject
//
//  Created by mac on 2019/12/31.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewSecurityViewController.h"
#import "NewSecuritycenterCell.h"
#import "NewiphonevalidationViewController.h"
#import "NewChangePasswordViewController.h"
#import "NewCustomerserviceViewController.h"

@interface NewSecurityViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
}

@end

@implementation NewSecurityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"安全中心"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#291744"];
    [self dataInitialization];
    [self loadsView];
    // Do any additional setup after loading the view.
}
- (void)dataInitialization {
    dataArray = NewMutableArrayInit;
    for (int i = 0; i<2; i++) {
        NSMutableDictionary *dic = NewMutableDictionaryInit;
        switch (i) {
            case 0:
                [dic setValue:@"修改登录密码" forKey:@"headtitle"];
                [dataArray addObject:dic];
                break;
                case 1:
                    [dic setValue:@"修改绑定手机号码" forKey:@"headtitle"];
                    [dataArray addObject:dic];
                    break;
            default:
                break;
        }
    }
}
- (void)loadsView {
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;
}
#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    NewSecuritycenterCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
    if (cell == nil) {
        cell = [[NewSecuritycenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
    if (dataArray.count>0) {
        [cell assignment:dataArray[indexPath.row]];
    }
    return cell;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([dataArray[indexPath.row][@"headtitle"] isEqualToString:@""]) {
        return 10;
    }else{
        return 50;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        NewChangePasswordViewController *vc = [NewChangePasswordViewController new];
        if (indexPath.row == 0) {
            vc.str = @"登录";
        }
        NewPushViewController(vc);
    }else if (indexPath.row == 1){
        NewCustomerserviceViewController *vc = [NewCustomerserviceViewController new];
        NewPushViewController(vc);
    }

}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}
@end
