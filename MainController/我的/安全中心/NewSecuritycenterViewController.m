//
//  NewSecuritycenterViewController.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewSecuritycenterViewController.h"
#import "NewSecuritycenterCell.h"
#import "NewRealnameViewController.h"
#import "NewiphonevalidationViewController.h"

@interface NewSecuritycenterViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *dataArray;
    UITableView *newtableView;
}

@end

@implementation NewSecuritycenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"安全中心"];
    self.navgationBar.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.statusView.backgroundColor = [UIColor colorWithString:@"#230F40"];
    self.view.backgroundColor = [UIColor colorWithString:@"#291744"];
    [self dataInitialization];
    [self loadsView];
}
    
- (void)dataInitialization {
    dataArray = NewMutableArrayInit;
    for (int i = 0; i<11; i++) {
        NSMutableDictionary *dic = NewMutableDictionaryInit;
        switch (i) {
            case 0:
                [dic setValue:@"真实姓名验证" forKey:@"headtitle"];
                [dic setValue:@"已认证" forKey:@"content"];
                [dataArray addObject:dic];
                break;
                case 1:
                    [dic setValue:@"资金来源证明" forKey:@"headtitle"];
                    [dic setValue:@"未认证" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 2:
                    [dic setValue:@"存款证明" forKey:@"headtitle"];
                    [dic setValue:@"已认证" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 3:
                    [dic setValue:@"地址证明" forKey:@"headtitle"];
                    [dic setValue:@"未认证" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 4:
                    [dic setValue:@"" forKey:@"headtitle"];
                    [dic setValue:@"" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 5:
                    [dic setValue:@"手机号码验证" forKey:@"headtitle"];
                    [dic setValue:@"已认证" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 6:
                    [dic setValue:@"邮箱验证" forKey:@"headtitle"];
                    [dic setValue:@"已认证" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 7:
                    [dic setValue:@"" forKey:@"headtitle"];
                    [dic setValue:@"" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 8:
                    [dic setValue:@"登录密码" forKey:@"headtitle"];
                    [dic setValue:@"已认证" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 9:
                    [dic setValue:@"取款密码" forKey:@"headtitle"];
                    [dic setValue:@"已认证" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
                case 10:
                    [dic setValue:@"" forKey:@"headtitle"];
                    [dic setValue:@"" forKey:@"content"];
                    [dataArray addObject:dic];
                    break;
            default:
                break;
        }
    }
}
- (void)loadsView {
    [self.view addSubview:newtableView = [NewControlPackage tableViewInitWithFrame:CGRectMake(0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader) backgroundColor:[UIColor colorWithString:@"#230F40"] style:1 delegate:self dataSource:self showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO hidden:NO tag:100 userInteractionEnabled:YES]];
    newtableView.separatorStyle = NO;
}
#pragma mark-------------------------UITableView------------------------------------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//返回每段行数
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Cell = @"Cell";
    if ([dataArray[indexPath.row][@"headtitle"] isEqualToString:@""]) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor colorWithString:@"#291744"];
        return cell;
    }else{
        NewSecuritycenterCell * cell = [tableView dequeueReusableCellWithIdentifier:Cell];
        if (cell == nil) {
            cell = [[NewSecuritycenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Cell];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];//（这种是没有点击后的阴影效果)
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor colorWithString:@"#230F40"];
        if (dataArray.count>0) {
            [cell assignment:dataArray[indexPath.row]];
        }
        return cell;
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([dataArray[indexPath.row][@"headtitle"] isEqualToString:@""]) {
        return 10;
    }else{
        return 50;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        NewRealnameViewController *vc = [NewRealnameViewController new];
        NewPushViewController(vc);
    }else if (indexPath.row == 5){
        NewiphonevalidationViewController *vc = [NewiphonevalidationViewController new];
        NewPushViewController(vc);
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    footView.backgroundColor = [UIColor colorWithString:@"#291744"];
    
    UIButton *tuichuBT = [UIButton new];
    [tuichuBT setTitle:@"退出登录" forState:UIControlStateNormal];
    [tuichuBT setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
    [tuichuBT setBackgroundColor:[UIColor colorWithString:@"#230F40"]];
    tuichuBT.titleLabel.font = NewFont(15);
    tuichuBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    NewTouchUpInside(tuichuBT, tuichuBT);
    [footView addSubview:tuichuBT];
    
    tuichuBT.sd_layout
    .leftEqualToView(footView)
    .rightEqualToView(footView)
    .heightIs(50)
    .topEqualToView(footView);
    
    return footView;

}
- (void)tuichuBT {
    
}

@end
