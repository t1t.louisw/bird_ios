//
//  NewLoginViewController.m
//  NewProject
//
//  Created by mac on 2019/12/3.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewLoginViewController.h"
#import "UINavigationController+FDFullscreenPopGesture.h"
#import "NewForgotpasswordViewController.h"
#import "NewregisteredViewController.h"
#import "NewCustomerserviceViewController.h"

@interface NewLoginViewController (){
    NSString *iphoneNumberStr;
    NSString *mimaStr;

}

@end

@implementation NewLoginViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //关闭侧滑返回功能
    self.fd_interactivePopDisabled=YES;
    [self.navgationBar setHidden:YES];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.navgationBar setHidden:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    iphoneNumberStr = [NewUtils userDefaultsStringKey:NewZhanghao];
    mimaStr = [NewUtils userDefaultsStringKey:NewMima];
    [self loadUI];
    // Do any additional setup after loading the view.
}
- (void)loadUI {
    UIView *navgationBar = [[UIView alloc] init];
    [navgationBar setFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, NavHeader)];
    [navgationBar setBackgroundColor:NewButtonColor];
    [self.view addSubview:navgationBar];
    
    UILabel *navTitleLabel = [[UILabel alloc] init];
    [navTitleLabel setBackgroundColor:NewClearColor];
    [navTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [navTitleLabel setFont:NewBFont(18)];
    [navTitleLabel setTextColor:[UIColor whiteColor]];
    [navTitleLabel setSingleLineAutoResizeWithMaxWidth:0];
    navTitleLabel.text = @"登录";
    [navgationBar addSubview:navTitleLabel];
    
    navTitleLabel.sd_layout
    .centerXEqualToView(navgationBar)
    .heightIs(20)
    .bottomSpaceToView(navgationBar, 10);
    
    UIButton *rightBT = [UIButton new];
    [rightBT setImage:NewImageNamed(@"客服") forState:UIControlStateNormal];
    NewTouchUpInside(rightBT, rightBTclick:);
    [navgationBar addSubview:rightBT];

    rightBT.sd_layout
    .rightSpaceToView(navgationBar, 5)
    .heightIs(40)
    .widthIs(40)
    .bottomSpaceToView(navgationBar, 0);
    
    UIImageView *logoimageV = [UIImageView new];
    [logoimageV setImage:NewImageNamed(@"登录logo")];
    logoimageV.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:logoimageV];
    
    logoimageV.sd_layout
    .heightIs(50)
    .widthIs(200)
    .centerXEqualToView(self.view)
    .topSpaceToView(navgationBar, 55);
    
    UIView *shoujihaomaView = [UIView new];
    shoujihaomaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(shoujihaomaView, 3);
    [self.view addSubview:shoujihaomaView];
      
    shoujihaomaView.sd_layout
    .heightIs(44)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(logoimageV, 40);
      
    UITextField *phonetextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入用户名" hidden:NO tag:100 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [phonetextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:phonetextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    phonetextfield.attributedPlaceholder = arrStr;
    phonetextfield.text = iphoneNumberStr == NULL?@"":iphoneNumberStr;
    [shoujihaomaView addSubview:phonetextfield];
      
    phonetextfield.sd_layout
    .leftSpaceToView(shoujihaomaView, 10)
    .topEqualToView(shoujihaomaView)
    .bottomEqualToView(shoujihaomaView)
    .rightEqualToView(shoujihaomaView);
      
    UIView *mimaView = [UIView new];
    mimaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(mimaView, 3);
    [self.view addSubview:mimaView];
      
    mimaView.sd_layout
    .heightIs(44)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(shoujihaomaView, 25);
      
    UITextField *mimatextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入密码" hidden:NO tag:101 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [mimatextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [mimatextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr1 = [[NSMutableAttributedString alloc]initWithString:mimatextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    mimatextfield.attributedPlaceholder = arrStr1;
    mimatextfield.text = mimaStr == NULL?@"":mimaStr;
    [mimaView addSubview:mimatextfield];
      
    mimatextfield.sd_layout
    .leftSpaceToView(mimaView, 10)
    .topEqualToView(mimaView)
    .bottomEqualToView(mimaView)
    .rightEqualToView(mimaView);
    
    UIButton *forgotpasswordBT = [UIButton new];
    [forgotpasswordBT setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgotpasswordBT setTitleColor:[UIColor colorWithString:@"#AFACB4"] forState:UIControlStateNormal];
    forgotpasswordBT.titleLabel.font = NewFont(12);
    forgotpasswordBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    NewTouchUpInside(forgotpasswordBT, forgotpasswordBTclick);
    [self.view addSubview:forgotpasswordBT];
    
    forgotpasswordBT.sd_layout
    .heightIs(15)
    .topSpaceToView(mimaView, 15)
    .leftSpaceToView(self.view, 20)
    .widthIs(100);
    
    UIButton *freeregistrationBT = [UIButton new];
    [freeregistrationBT setTitle:@"免费注册" forState:UIControlStateNormal];
    [freeregistrationBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    freeregistrationBT.titleLabel.font = NewFont(12);
    freeregistrationBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    NewTouchUpInside(freeregistrationBT, freeregistrationBTclick:);
    [self.view addSubview:freeregistrationBT];
    
    freeregistrationBT.sd_layout
    .heightIs(15)
    .topSpaceToView(mimaView, 15)
    .rightSpaceToView(self.view, 20)
    .widthIs(100);
    
    UIButton *loginBT = [UIButton new];
    [loginBT setTitle:@"立即登录" forState:UIControlStateNormal];
    [loginBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    loginBT.titleLabel.font = NewFont(15);
    loginBT.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [loginBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(loginBT, 4);
    NewTouchUpInside(loginBT, loginBTclick:);
    [self.view addSubview:loginBT];
    
    loginBT.sd_layout
    .heightIs(44)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(mimaView, 75);
    
    UIImageView *RGlogImgae = [UIImageView new];
    [RGlogImgae setImage:NewImageNamed(@"RGLogo")];
    [self.view addSubview:RGlogImgae];
    
    RGlogImgae.sd_layout
    .heightIs(25)
     .widthIs(120)
    .centerXEqualToView(self.view)
    .bottomSpaceToView(self.view, 50);
    
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    if (textField.tag == 100) {
        iphoneNumberStr = textField.text;
    }else if (textField.tag == 101){
        mimaStr = textField.text;
    }
}

- (void)loginBTclick:(UIButton *)sender
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:iphoneNumberStr forKey:@"username"];
    [dic setValue:mimaStr forKey:@"password"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:login parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            if ([requestData[@"code"] integerValue] == 5) {
                [SVProgressHUD showErrorWithStatus:@"账户名或密码错误"];
            }else if ([requestData[@"code"] integerValue] == 3){
                [SVProgressHUD showErrorWithStatus:@"账户名或密码错误"];
            }else{
                [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            }
        }else{
            [SVProgressHUD showSuccessWithStatus:@"登录成功"];
            [UserEntity sharedInstance].token = requestData[@"result"][@"token"];
            [UserEntity sharedInstance].playerName = requestData[@"result"][@"playerName"];
            [UserEntity sharedInstance].playerId = requestData[@"result"][@"playerId"];
            [UserEntity sharedInstance].password = mimaStr;

            [UserEntity sharedInstance].zhanghao = requestData[@"result"][@"playerName"];
            [UserEntity sharedInstance].mima = mimaStr;
            
            
            [UserHelper cacheUserInfoFromLocation];
            [UserHelper writeUserInfoFromLocation];
            [self NetworkRequestManagerGetupdatePlayerWithdrawalPassword];//每次登陆修改取款密码  保持跟登陆密码一致
            [[NewInterfaceReplacement sharedInstance] replacementController];//直接到首页
            NSLog(@"%@",[NewUtils userDefaultsStringKey:NewAccountNumber]);
            NSLog(@"");
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
-(void)NetworkRequestManagerGetupdatePlayerWithdrawalPassword {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
//    [dic setValue:ordPassWord forKey:@"old_password"];
    [dic setValue:mimaStr forKey:@"new_password"];
    [dic setValue:@"1" forKey:@"force_reset"];
//    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:updatePlayerWithdrawalPassword parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
//            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
//            [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"修改%@密码成功",_str]];
        }
    } onFailure:^{
//        [SVProgressHUD dismiss];
    }];
}


- (BOOL)checkUsername:(NSString *)password
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,12}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
}
- (BOOL)checkPassword:(NSString *)password
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,20}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
}
- (void)forgotpasswordBTclick{
    NewForgotpasswordViewController *vc = [NewForgotpasswordViewController new];
    NewPushViewController(vc);
}

- (void)freeregistrationBTclick:(UIButton *)sender{
    NewregisteredViewController *vc = [NewregisteredViewController new];
    NewPushViewController(vc);
}
- (void)rightBTclick:(UIButton *)sender {
    NewCustomerserviceViewController *vc = [NewCustomerserviceViewController new];
    NewPushViewController(vc);
    return;
}
@end
