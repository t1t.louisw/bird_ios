//
//  NewForgotpasswordViewController.m
//  NewProject
//
//  Created by mac on 2019/12/3.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewForgotpasswordViewController.h"
#import "NewForgotpasswordTwoViewController.h"
#import "NewCustomerserviceViewController.h"
@interface NewForgotpasswordViewController (){
    UIButton *verificationCodeButton;
    NSString *username;
    NSString *iphoneNumber;
    NSString *yanzhenmaStr;
    NSString *mimaStr;
    NSString *mima1Str;
    UIButton *nextstepBT;
    
    UITextField *mima1field;

}

@end

@implementation NewForgotpasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"忘记密码"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self loadUI];
}
- (void)rightBTclick:(UIButton *)sender {
    NewCustomerserviceViewController *vc = [NewCustomerserviceViewController new];
    NewPushViewController(vc);
    return;
}
- (void)loadUI {
    UIButton *rightBT = [UIButton new];
       [rightBT setImage:NewImageNamed(@"客服") forState:UIControlStateNormal];
       NewTouchUpInside(rightBT, rightBTclick:);
       [self.navgationBar addSubview:rightBT];

       rightBT.sd_layout
       .rightSpaceToView(self.navgationBar, 5)
       .heightIs(40)
       .widthIs(40)
       .bottomSpaceToView(self.navgationBar, 0);
    
    UILabel *headtitle = [UILabel new];
    headtitle.text = @"通过手机号码找回密码";
    headtitle.font = NewFont(15);
    [headtitle setSingleLineAutoResizeWithMaxWidth:0];
    headtitle.textColor= [UIColor colorWithString:@"#AFACB4"];
    [self.view addSubview:headtitle];
    
    headtitle.sd_layout
    .heightIs(15)
    .centerXEqualToView(self.view)
    .topSpaceToView(self.view, NavHeader+45);
    
    UIView *yonghumingView = [UIView new];
    yonghumingView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(yonghumingView, 3);
    [self.view addSubview:yonghumingView];
      
    yonghumingView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(headtitle, 50);
    
        UITextField *yonghumingfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入用户名" hidden:NO tag:100 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [yonghumingfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
        NSMutableAttributedString *arrStr2 = [[NSMutableAttributedString alloc]initWithString:yonghumingfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
        yonghumingfield.attributedPlaceholder = arrStr2;
        [yonghumingView addSubview:yonghumingfield];
        
        yonghumingfield.sd_layout
        .leftSpaceToView(yonghumingView, 10)
        .topEqualToView(yonghumingView)
        .bottomEqualToView(yonghumingView)
        .rightEqualToView(yonghumingView);
    
    UILabel *tishi1 = [UILabel new];
    tishi1.text = @"忘记用户名请联系客服";
    tishi1.font = NewFont(11);
    tishi1.textColor = NewRedColor;
    [tishi1 setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:tishi1];
    
    tishi1.sd_layout
    .heightIs(15)
    .leftSpaceToView(self.view, 20)
    .topSpaceToView(yonghumingView, 2.5);
    
    
    UIView *shoujihaomaView = [UIView new];
    shoujihaomaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(shoujihaomaView, 3);
    [self.view addSubview:shoujihaomaView];
      
    shoujihaomaView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(yonghumingView, 20);
      
    UITextField *phonetextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入手机号码" hidden:NO tag:101 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [phonetextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr1 = [[NSMutableAttributedString alloc]initWithString:phonetextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    phonetextfield.attributedPlaceholder = arrStr1;
    [shoujihaomaView addSubview:phonetextfield];
    
    phonetextfield.sd_layout
    .leftSpaceToView(shoujihaomaView, 10)
    .topEqualToView(shoujihaomaView)
    .bottomEqualToView(shoujihaomaView)
    .rightEqualToView(shoujihaomaView);
    
    UIView *yanzhengmaView = [UIView new];
    yanzhengmaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(yanzhengmaView, 3);
    [self.view addSubview:yanzhengmaView];
      
    yanzhengmaView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 140)
    .topSpaceToView(shoujihaomaView, 25);
      
    UITextField *yanzhenmatextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入验证码" hidden:NO tag:102 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [yanzhenmatextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [yanzhenmatextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:yanzhenmatextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    yanzhenmatextfield.attributedPlaceholder = arrStr;
    [yanzhengmaView addSubview:yanzhenmatextfield];
      
    yanzhenmatextfield.sd_layout
    .leftSpaceToView(yanzhengmaView, 10)
    .topEqualToView(yanzhengmaView)
    .bottomEqualToView(yanzhengmaView)
    .rightEqualToView(yanzhengmaView);
        
    verificationCodeButton = [UIButton new];
    [verificationCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [verificationCodeButton setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    verificationCodeButton.titleLabel.font = NewFont(15);
    verificationCodeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    NewTouchUpInside(verificationCodeButton, touchVerificationCode:);
    [verificationCodeButton setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(verificationCodeButton, 3);
    [self.view addSubview:verificationCodeButton];
    
    verificationCodeButton.sd_layout
    .centerYEqualToView(yanzhengmaView)
    .rightSpaceToView(self.view, 15)
    .heightIs(40)
    .leftSpaceToView(yanzhengmaView, 8.5);
    
    UIView *mimaView = [UIView new];
    mimaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(mimaView, 3);
    [self.view addSubview:mimaView];
      
    mimaView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(yanzhengmaView, 20);
    
        UITextField *mimafield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入您的新密码" hidden:NO tag:103 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [mimafield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
        NSMutableAttributedString *arrStr4 = [[NSMutableAttributedString alloc]initWithString:mimafield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
        mimafield.attributedPlaceholder = arrStr4;
        [mimaView addSubview:mimafield];
        
        mimafield.sd_layout
        .leftSpaceToView(mimaView, 10)
        .topEqualToView(mimaView)
        .bottomEqualToView(mimaView)
        .rightEqualToView(mimaView);
    
    UIView *mima1View = [UIView new];
    mima1View.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(mima1View, 3);
    [self.view addSubview:mima1View];
      
    mima1View.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(mimaView, 20);
    
        mima1field = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请重新输入您的新密码" hidden:NO tag:104 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [mima1field addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
        NSMutableAttributedString *arrStr5 = [[NSMutableAttributedString alloc]initWithString:mima1field.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
        mima1field.attributedPlaceholder = arrStr5;
        [mima1View addSubview:mima1field];
        
        mima1field.sd_layout
        .leftSpaceToView(mima1View, 10)
        .topEqualToView(mima1View)
        .bottomEqualToView(mima1View)
        .rightSpaceToView(mima1View, 50);
    
//    UIButton *keshiBT = [UIButton new];
//    [keshiBT setBackgroundColor:NewRedColor];
//    NewTouchUpInside(keshiBT, keshiBTclick:);
//    keshiBT.selected = YES;
//    [mima1View addSubview:keshiBT];
//
//    keshiBT.sd_layout
//    .heightIs(30)
//    .centerYEqualToView(mima1View)
//    .widthIs(30)
//    .rightSpaceToView(mima1View, 15);
    
    nextstepBT = [UIButton new];
    [nextstepBT setTitle:@"确定" forState:UIControlStateNormal];
    [nextstepBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    nextstepBT.titleLabel.font = NewFont(15);
    [nextstepBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    NewTouchUpInside(nextstepBT, nextstepBTclick:);
    [self.view addSubview:nextstepBT];
    
    nextstepBT.sd_layout
    .heightIs(44)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(mima1View, 25);
    
    
}

//- (void)keshiBTclick:(UIButton *)sender {
//    if (sender.selected == YES) {
//        [mima1field setSecureTextEntry:NO];
//        sender.selected = NO;
//    }else{
//        [mima1field setSecureTextEntry:YES];
//        sender.selected = YES;
//    }
//}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    if (textField.tag == 100) {
        username = textField.text;
    }else if (textField.tag == 101){
        iphoneNumber = textField.text;
    }else if (textField.tag == 102){
        yanzhenmaStr = textField.text;
    }else if (textField.tag == 103){
        mimaStr = textField.text;
    }else if (textField.tag == 104){
        mima1Str = textField.text;
    }
}

- (void)nextstepBTclick:(UIButton *)sender {
    if (![self validateContactNumber:iphoneNumber]){
        [SVProgressHUD showErrorWithStatus:@"请输入正确的手机号码"];
        return;
    }else if (username.length<0){
        [SVProgressHUD showErrorWithStatus:@"请输入用户名"];
        return;
    }else if (yanzhenmaStr.length<0){
        [SVProgressHUD showErrorWithStatus:@"请输入验证码"];
        return;
    }else if (mimaStr.length<0){
        [SVProgressHUD showErrorWithStatus:@"请输入新密码"];
        return;
    }else if (mima1Str.length<0){
        [SVProgressHUD showErrorWithStatus:@"请确认新密码"];
        return;
    }else if (![mima1Str isEqualToString:mimaStr]){
        [SVProgressHUD showErrorWithStatus:@"俩次密码不相同请重新输入"];
        return;
    }else if (![username isEqualToString:mimaStr]){
        [SVProgressHUD showErrorWithStatus:@"用户名不能与密码相同"];
    }
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:username forKey:@"username"];
    [dic setValue:yanzhenmaStr forKey:@"verify_code"];
    [dic setValue:mimaStr forKey:@"password"];
    [dic setValue:mima1Str forKey:@"password_conf"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:passwordRecovSmsRecv parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            if ([requestData[@"code"] integerValue] == 160) {
                [SVProgressHUD showErrorWithStatus:@"玩家用戶名錯誤"];
            }else if ([requestData[@"code"] integerValue] == 564){
                [SVProgressHUD showErrorWithStatus:@"認證碼為空"];
            }else if ([requestData[@"code"] integerValue] == 565){
                [SVProgressHUD showErrorWithStatus:@"認證碼不對或過期"];
            }else if ([requestData[@"code"] integerValue] == 566){
                [SVProgressHUD showErrorWithStatus:@"兩次輸入的密碼(password 和password_conf 欄位) 不符合"];
            }else if ([requestData[@"code"] integerValue] == 567){
                [SVProgressHUD showErrorWithStatus:@"密碼不符合系統設定的要求(太長或太短)"];
            }else if ([requestData[@"code"] integerValue] == 568){
                [SVProgressHUD showErrorWithStatus:@"重設密碼時發生其他錯誤"];
            }else{
                [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            }
        }else{
            NSLog(@"");
            [SVProgressHUD showSuccessWithStatus:@"密码修改成功"];
            NewPopViewController;
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}
#pragma mark - 获取验证码
- (void)touchVerificationCode:(UIButton *)sender
{
    if (![self validateContactNumber:iphoneNumber]){
        [SVProgressHUD showErrorWithStatus:@"请输入正确的手机号码"];
        return;
    }else if (username.length<0){
        [SVProgressHUD showErrorWithStatus:@"请输入用户名"];
    }
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:username forKey:@"username"];
    [dic setValue:iphoneNumber forKey:@"mobile_number"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:passwordRecovSmsSend parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            if ([requestData[@"code"] integerValue] == 160) {
                [SVProgressHUD showErrorWithStatus:@"玩家用戶名錯誤"];
            }else if ([requestData[@"code"] integerValue] == 562){
                [SVProgressHUD showErrorWithStatus:@"玩家輸入的手機號碼不符合設定的手機號碼"];
            }else if ([requestData[@"code"] integerValue] == 563){
                [SVProgressHUD showErrorWithStatus:@"無法發送短信"];
            }else if ([requestData[@"code"] integerValue] == 545){
                [SVProgressHUD showErrorWithStatus:@"已達到每分鐘發送短信數上限m"];
            }else if ([requestData[@"code"] integerValue] == 546){
                [SVProgressHUD showErrorWithStatus:@"今天短信發送次數已達到上限m"];
            }else if ([requestData[@"code"] integerValue] == 544){
                [SVProgressHUD showErrorWithStatus:@"不可連續重發驗證短信，請等待n 秒鐘之後再試"];
            }else if ([requestData[@"code"] integerValue] == 531){
                [SVProgressHUD showErrorWithStatus:@"玩家沒有設定手機號碼"];
            }else{
                [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            }
        }else{
            [MBProgressHUD showSuccess:@"短信已发送" toView:self.view];
            sender.frame = CGRectMake(sender.left, sender.top, 80, sender.height);
            [self verificationCode:60 sender:sender];
            [SVProgressHUD dismiss];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];

}
- (void)verificationCode:(NSInteger)code sender:(UIButton *)sender{
    NSString *str=[NSString stringWithFormat:@"%d秒后重新发送",(int)code];
    [sender setTitle:str forState:UIControlStateNormal];
    [sender setBackgroundColor:[UIColor colorWithString:@"#666666"]];
    sender.enabled=NO;
    if (code==0) {
        sender.enabled=YES;
        [sender setTitle:@"获取验证码" forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
        sender.frame = CGRectMake(sender.left, sender.top, 66, sender.height);
        return;
    }
    code--;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self verificationCode:code sender:sender];
    });
}

- (BOOL)validateContactNumber:(NSString *)mobileNum
{
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 16[6], 17[5, 6, 7, 8], 18[0-9], 170[0-9], 19[89]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705,198
     * 联通号段: 130,131,132,155,156,185,186,145,175,176,1709,166
     * 电信号段: 133,153,180,181,189,177,1700,199
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|6[6]|7[05-8]|8[0-9]|9[89])\\d{8}$";

    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478]|9[8])\\d{8}$)|(^1705\\d{7}$)";
   
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|66|7[56]|8[56])\\d{8}$)|(^1709\\d{7}$)";

    NSString *CT = @"(^1(33|53|77|8[019]|99)\\d{8}$)|(^1700\\d{7}$)";
    
    /**
     * 大陆地区固话及小灵通
     * 区号：010,020,021,022,023,024,025,027,028,029
     * 号码：七位或八位
     */
   // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
   // NSPredicate *regextestPHS = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    
    if(([regextestmobile evaluateWithObject:mobileNum] == YES)
       || ([regextestcm evaluateWithObject:mobileNum] == YES)
       || ([regextestct evaluateWithObject:mobileNum] == YES)
       || ([regextestcu evaluateWithObject:mobileNum] == YES)) {
        return YES;
    } else {
        return NO;
    }
}

@end
