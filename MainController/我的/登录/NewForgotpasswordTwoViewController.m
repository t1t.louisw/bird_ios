//
//  NewForgotpasswordTwoViewController.m
//  NewProject
//
//  Created by mac on 2019/12/3.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewForgotpasswordTwoViewController.h"

@interface NewForgotpasswordTwoViewController ()

@end

@implementation NewForgotpasswordTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"忘记密码"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    [self loadUI];
}
- (void)loadUI {
    UILabel *headtitle = [UILabel new];
    headtitle.text = @"验证手机验证码并创建新密码";
    headtitle.font = NewFont(15);
    [headtitle setSingleLineAutoResizeWithMaxWidth:0];
    headtitle.textColor= [UIColor colorWithString:@"#AFACB4"];
    [self.view addSubview:headtitle];
    
    headtitle.sd_layout
    .heightIs(15)
    .centerXEqualToView(self.view)
    .topSpaceToView(self.view, NavHeader+45);
    
    UIView *shoujihaomaView = [UIView new];
    shoujihaomaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(shoujihaomaView, 3);
    [self.view addSubview:shoujihaomaView];
      
    shoujihaomaView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(headtitle, 50);
      
    UITextField *phonetextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入您的新密码" hidden:NO tag:100 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [phonetextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
   NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:phonetextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
   phonetextfield.attributedPlaceholder = arrStr;
    [shoujihaomaView addSubview:phonetextfield];
    
    phonetextfield.sd_layout
    .leftSpaceToView(shoujihaomaView, 10)
    .topEqualToView(shoujihaomaView)
    .bottomEqualToView(shoujihaomaView)
    .rightEqualToView(shoujihaomaView);
    
    UIView *yanzhengmaView = [UIView new];
    yanzhengmaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(yanzhengmaView, 3);
    [self.view addSubview:yanzhengmaView];
      
    yanzhengmaView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(shoujihaomaView, 25);
      
    UITextField *yanzhenmatextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请重新输入您的密码" hidden:NO tag:101 font:NewFont(15) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [yanzhenmatextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [yanzhenmatextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr1 = [[NSMutableAttributedString alloc]initWithString:yanzhenmatextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    yanzhenmatextfield.attributedPlaceholder = arrStr1;
    [yanzhengmaView addSubview:yanzhenmatextfield];
      
    yanzhenmatextfield.sd_layout
    .leftSpaceToView(yanzhengmaView, 10)
    .topEqualToView(yanzhengmaView)
    .bottomEqualToView(yanzhengmaView)
    .rightEqualToView(yanzhengmaView);
    
    UIButton *nextstepBT = [UIButton new];
    [nextstepBT setTitle:@"确定" forState:UIControlStateNormal];
    [nextstepBT setTitleColor:[UIColor colorWithString:@"#FFFFFF"] forState:UIControlStateNormal];
    nextstepBT.titleLabel.font = NewFont(15);
    [nextstepBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    NewTouchUpInside(nextstepBT, nextstepBTclick:);
    [self.view addSubview:nextstepBT];
    
    nextstepBT.sd_layout
    .heightIs(44)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(yanzhengmaView, 25);
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    if (textField.tag == 100) {
//        iphoneNumber = textField.text;
    }else if (textField.tag == 101){
//        yanzhenmaStr = textField.text;
    }
 
}
- (void)nextstepBTclick:(UIButton *)sender {
    NSLog(@"%@",@"确定");
}

@end
