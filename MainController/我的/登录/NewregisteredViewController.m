//
//  NewregisteredViewController.m
//  NewProject
//
//  Created by mac on 2019/12/4.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewregisteredViewController.h"

@interface NewregisteredViewController ()<UITextFieldDelegate>{
    NSString *UsernameStr;
    NSString *PasswordStr;
    NSString *ConfirmpasswordStr;
    NSString *IphoneNumberStr;
    NSString *NameStr;
    NSString *surnamesStr;
    NSString *InvitecodeStr;
    NSString *tuidStr;
    NSString *tuid;

    BOOL Check;
    
    UILabel *tishi1;
    UILabel *tishi2;
    UILabel *tishi3;
    UILabel *tishi4;
    UILabel *tishi5;
    UIView *passwordView;
    UIView *UserNameView;
    UIButton *verificationCodeButton;
    
}

@end

@implementation NewregisteredViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];//键盘的弹出

     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self setNavTitle:@"账户注册"];
    self.view.backgroundColor = [UIColor colorWithString:@"#230F40"];
    Check = NO;

    [self loadUI];
}
//键盘的消失

//弹出键盘UIView

-(void)transformView:(NSNotification*)aNSNotification{



    //获取键盘弹出前的Rect

    NSValue*keyBoardBeginBounds=[[aNSNotification userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];

    CGRect beginRect=[keyBoardBeginBounds CGRectValue];



    //获取键盘弹出后的Rect

    NSValue*keyBoardEndBounds=[[aNSNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];

    CGRect  endRect=[keyBoardEndBounds CGRectValue];



    //获取键盘位置变化前后纵坐标Y的变化值

    CGFloat deltaY=endRect.origin.y-beginRect.origin.y;

    NSLog(@"看看这个变化的Y值:%f",deltaY);



    //在0.25s内完成self.view的Frame的变化，等于是给self.view添加一个向上移动deltaY的动画

    [UIView animateWithDuration:0.25f animations:^{

        self.view.frame = CGRectMake(0, -50, kScreenWidth, kScreenHeight);

    }];



}
//退出键盘UIView

-(void)keyboardWillHide:(NSNotification*)aNSNotification{



    [UIView beginAnimations:nil context:nil];



    [UIView setAnimationDuration:0.25];



    [UIView setAnimationCurve:7];



    self.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);



    [UIView commitAnimations];



}
- (void)loadUI {
    UserNameView = [UIView new];
    UserNameView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(UserNameView, 3);
    [self.view addSubview:UserNameView];
      
    UserNameView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(self.view, NavHeader+25);
    
    UILabel *starV3 = [UILabel new];
    starV3.text = @"*";
    starV3.font = NewFont(18);
    [starV3 setSingleLineAutoResizeWithMaxWidth:0];
    starV3.textColor = NewRedColor;
    [UserNameView addSubview:starV3];
    
    starV3.sd_layout
    .heightIs(30)
    .widthIs(10)
    .leftSpaceToView(UserNameView, 5)
    .topSpaceToView(UserNameView, 8);
    
    UITextField *userNamefield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入用户名" hidden:NO tag:100 font:NewFont(14) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [userNamefield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    NSMutableAttributedString *arrStr6 = [[NSMutableAttributedString alloc]initWithString:userNamefield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    userNamefield.attributedPlaceholder = arrStr6;
    userNamefield.delegate = self;
    [UserNameView addSubview:userNamefield];
    
    userNamefield.sd_layout
    .leftSpaceToView(UserNameView, 20)
    .topEqualToView(UserNameView)
    .bottomEqualToView(UserNameView)
    .rightEqualToView(UserNameView);
    
    tishi1 = [UILabel new];
    tishi1.text = @"用户名以字母开头，长度必须为6-12之间由小写字母和数字组成";
    tishi1.font = NewFont(11);
    tishi1.textColor = NewWhiteColor;
    tishi1.hidden = YES;
    [self.view addSubview:tishi1];
    
    tishi1.sd_layout
    .heightIs(20)
    .leftSpaceToView(self.view, 35)
    .topSpaceToView(UserNameView, 2.5)
    .autoHeightRatio(0)
    .rightEqualToView(UserNameView);
    
    passwordView = [UIView new];
    passwordView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(passwordView, 3);
    [self.view addSubview:passwordView];

    passwordView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(UserNameView, 30);
    
    UILabel *starV4 = [UILabel new];
    starV4.text = @"*";
    starV4.font = NewFont(18);
    [starV4 setSingleLineAutoResizeWithMaxWidth:0];
    starV4.textColor = NewRedColor;
    [passwordView addSubview:starV4];
    
    starV4.sd_layout
    .heightIs(30)
    .widthIs(10)
    .leftSpaceToView(passwordView, 5)
    .topSpaceToView(passwordView, 8);
    
    UITextField *passwordfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入账户密码" hidden:NO tag:101 font:NewFont(14) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [passwordfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr7 = [[NSMutableAttributedString alloc]initWithString:passwordfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    passwordfield.attributedPlaceholder = arrStr7;
    passwordfield.delegate = self;
    [passwordView addSubview:passwordfield];
    
    passwordfield.sd_layout
    .leftSpaceToView(passwordView, 20)
    .topEqualToView(passwordView)
    .bottomEqualToView(passwordView)
    .rightEqualToView(passwordView);
    
    tishi2 = [UILabel new];
    tishi2.text = @"账户密码长度必须要为6-20之间由英文字母与数字组成!不能与您的用户名相似";
    tishi2.font = NewFont(11);
    tishi2.textColor = NewWhiteColor;
    tishi2.hidden = YES;
    [self.view addSubview:tishi2];
    
    tishi2.sd_layout
    .heightIs(20)
    .leftSpaceToView(self.view, 35)
    .topSpaceToView(passwordView, 2.5)
    .autoHeightRatio(0)
    .rightEqualToView(passwordView);
    
    UIView *confirmpasswordView = [UIView new];
    confirmpasswordView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(confirmpasswordView, 3);
    [self.view addSubview:confirmpasswordView];
      
    confirmpasswordView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(passwordView, 30);
    
    UILabel *starV5 = [UILabel new];
    starV5.text = @"*";
    starV5.font = NewFont(18);
    [starV5 setSingleLineAutoResizeWithMaxWidth:0];
    starV5.textColor = NewRedColor;
    [confirmpasswordView addSubview:starV5];
    
    starV5.sd_layout
    .heightIs(30)
    .widthIs(10)
    .leftSpaceToView(confirmpasswordView, 5)
    .topSpaceToView(confirmpasswordView, 8);
    
    UITextField *confirmpasswordfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请确认账户密码" hidden:NO tag:102 font:NewFont(14) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:YES keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [confirmpasswordfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr8 = [[NSMutableAttributedString alloc]initWithString:confirmpasswordfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    confirmpasswordfield.attributedPlaceholder = arrStr8;
    confirmpasswordfield.delegate = self;
    [confirmpasswordView addSubview:confirmpasswordfield];
    
    confirmpasswordfield.sd_layout
    .leftSpaceToView(confirmpasswordView, 20)
    .topEqualToView(confirmpasswordView)
    .bottomEqualToView(confirmpasswordView)
    .rightEqualToView(confirmpasswordView);

    tishi3 = [UILabel new];
    tishi3.text = @"确认密码不能为空，且需要与账户密码一致！";
    tishi3.font = NewFont(11);
    tishi3.textColor = NewWhiteColor;
    tishi3.hidden = YES;
    [self.view addSubview:tishi3];
    
    tishi3.sd_layout
    .heightIs(15)
    .leftSpaceToView(self.view, 35)
    .topSpaceToView(confirmpasswordView, 2.5)
    .autoHeightRatio(0)
    .rightEqualToView(confirmpasswordView);
    
     UIView *nameView = [UIView new];
        nameView.backgroundColor = [UIColor colorWithString:@"#291744"];
        ViewRadius(nameView, 3);
        [self.view addSubview:nameView];
          
        nameView.sd_layout
        .heightIs(40)
        .leftSpaceToView(self.view, 15)
        .rightSpaceToView(self.view, 15)
        .topSpaceToView(confirmpasswordView, 30);
        
        UILabel *starV = [UILabel new];
        starV.text = @"*";
        starV.font = NewFont(18);
        [starV setSingleLineAutoResizeWithMaxWidth:0];
        starV.textColor = NewRedColor;
        [nameView addSubview:starV];
        
        starV.sd_layout
        .heightIs(30)
        .widthIs(10)
        .leftSpaceToView(nameView, 5)
        .topSpaceToView(nameView, 8);
        
        UITextField *nametextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"名字" hidden:NO tag:104 font:NewFont(14) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [nametextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    [nametextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
        NSMutableAttributedString *arrStr2 = [[NSMutableAttributedString alloc]initWithString:nametextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
        nametextfield.attributedPlaceholder = arrStr2;
        nametextfield.delegate = self;
        [nameView addSubview:nametextfield];
          
        nametextfield.sd_layout
        .leftSpaceToView(nameView, 20)
        .topEqualToView(nameView)
        .bottomEqualToView(nameView)
        .rightEqualToView(nameView);
        
        tishi5 = [UILabel new];
        tishi5.text = @"为了您的资金安全,请填写真实姓名";
        tishi5.font = NewFont(11);
        tishi5.textColor = NewWhiteColor;
        tishi5.hidden = YES;
        [self.view addSubview:tishi5];
        
        tishi5.sd_layout
        .heightIs(15)
        .leftSpaceToView(self.view, 35)
        .topSpaceToView(nameView, 2.5)
        .autoHeightRatio(0)
        .rightEqualToView(nameView);
        
    UIView *shoujihaomaView = [UIView new];
    shoujihaomaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(shoujihaomaView, 3);
    [self.view addSubview:shoujihaomaView];
      
    shoujihaomaView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(nameView, 30);
    
    
    UILabel *starV6 = [UILabel new];
    starV6.text = @"*";
    starV6.font = NewFont(18);
    [starV6 setSingleLineAutoResizeWithMaxWidth:0];
    starV6.textColor = NewRedColor;
    [shoujihaomaView addSubview:starV6];
    
    starV6.sd_layout
    .heightIs(30)
    .widthIs(10)
    .leftSpaceToView(shoujihaomaView, 5)
    .topSpaceToView(shoujihaomaView, 8);
      
    UITextField *phonetextfield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入手机号码" hidden:NO tag:103 font:NewFont(14) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeNumberPad returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [phonetextfield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [phonetextfield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
   NSMutableAttributedString *arrStr = [[NSMutableAttributedString alloc]initWithString:phonetextfield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
   phonetextfield.attributedPlaceholder = arrStr;
    phonetextfield.delegate = self;
    [shoujihaomaView addSubview:phonetextfield];
      
    phonetextfield.sd_layout
    .leftSpaceToView(shoujihaomaView, 20)
    .topEqualToView(shoujihaomaView)
    .bottomEqualToView(shoujihaomaView)
    .rightEqualToView(shoujihaomaView);
    
    tishi4 = [UILabel new];
    tishi4.text = @"请输入正确的手机号码";
    tishi4.font = NewFont(11);
    tishi4.textColor = NewWhiteColor;
    tishi4.hidden = YES;
    [self.view addSubview:tishi4];
    
    tishi4.sd_layout
    .heightIs(15)
    .leftSpaceToView(self.view, 35)
    .topSpaceToView(shoujihaomaView, 2.5)
    .autoHeightRatio(0)
    .rightEqualToView(shoujihaomaView);
        
    UIView *yanzhengmaView = [UIView new];
    yanzhengmaView.backgroundColor = [UIColor colorWithString:@"#291744"];
    ViewRadius(yanzhengmaView, 3);
    [self.view addSubview:yanzhengmaView];
         
    yanzhengmaView.sd_layout
    .heightIs(40)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15+100)
    .topSpaceToView(shoujihaomaView, 30);
    
    UILabel *starV10 = [UILabel new];
    starV10.text = @"*";
    starV10.font = NewFont(18);
    [starV10 setSingleLineAutoResizeWithMaxWidth:0];
    starV10.textColor = NewRedColor;
    [yanzhengmaView addSubview:starV10];
    
    starV10.sd_layout
    .heightIs(30)
    .widthIs(10)
    .leftSpaceToView(yanzhengmaView, 5)
    .topSpaceToView(yanzhengmaView, 8);
    
        UITextField *yanzhengmafield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"请输入验证码" hidden:NO tag:109 font:NewFont(14) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
        [yanzhengmafield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    //    [Invitecodefield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
        NSMutableAttributedString *arrStr9 = [[NSMutableAttributedString alloc]initWithString:yanzhengmafield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
        yanzhengmafield.attributedPlaceholder = arrStr9;
        [yanzhengmaView addSubview:yanzhengmafield];
          
        yanzhengmafield.sd_layout
        .leftSpaceToView(yanzhengmaView, 20)
        .topEqualToView(yanzhengmaView)
        .bottomEqualToView(yanzhengmaView)
        .rightEqualToView(yanzhengmaView);
    
    verificationCodeButton = [UIButton new];
    [verificationCodeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [verificationCodeButton setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    verificationCodeButton.titleLabel.font = NewFont(15);
    verificationCodeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    NewTouchUpInside(verificationCodeButton, touchVerificationCode:);
    [verificationCodeButton setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    ViewRadius(verificationCodeButton, 3);
    [self.view addSubview:verificationCodeButton];
    
    verificationCodeButton.sd_layout
    .centerYEqualToView(yanzhengmaView)
    .rightSpaceToView(self.view, 15)
    .heightIs(40)
    .leftSpaceToView(yanzhengmaView, 0);
    
    UIView *InvitecodeView = [UIView new];
       InvitecodeView.backgroundColor = [UIColor colorWithString:@"#291744"];
       ViewRadius(InvitecodeView, 3);
       [self.view addSubview:InvitecodeView];
         
       InvitecodeView.sd_layout
       .heightIs(40)
       .leftSpaceToView(self.view, 15)
       .rightSpaceToView(self.view, 15)
       .topSpaceToView(yanzhengmaView, 30);
    
    UITextField *Invitecodefield = [NewControlPackage textFieldInitWithFrame:CGRectMake(0, 0, 0, 0) backgroundImage:nil backgroundColor:[UIColor colorWithString:@"#291744"] textColor:[UIColor colorWithString:@"#7E7B83"] placeholder:@"邀请码" hidden:NO tag:106 font:NewFont(14) textAlignment:NSTextAlignmentLeft clearButtonMode:UITextFieldViewModeNever clearsOnBeginEditing:NO adjustsFontSizeToFitWidth:NO secureTextEntry:NO keyboardType:UIKeyboardTypeDefault returnKeyType:UIReturnKeyDefault userInteractionEnabled:YES];
    [Invitecodefield addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
//    [Invitecodefield setValue:[UIColor colorWithString:@"#7E7B83"] forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *arrStr4 = [[NSMutableAttributedString alloc]initWithString:Invitecodefield.placeholder attributes:@{NSForegroundColorAttributeName : [UIColor colorWithString:@"#7E7B83"],NSFontAttributeName:[UIFont systemFontOfSize:15]}];
    Invitecodefield.attributedPlaceholder = arrStr4;
    [InvitecodeView addSubview:Invitecodefield];
      
    Invitecodefield.sd_layout
    .leftSpaceToView(InvitecodeView, 20)
    .topEqualToView(InvitecodeView)
    .bottomEqualToView(InvitecodeView)
    .rightEqualToView(InvitecodeView);
    
  
    UIButton *registeredBT = [UIButton new];
    [registeredBT setTitle:@"立即注册" forState:UIControlStateNormal];
    [registeredBT setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
    [registeredBT setTitleColor:NewWhiteColor forState:UIControlStateNormal];
    registeredBT.titleLabel.font = NewFont(15);
    ViewRadius(registeredBT, 3);
    NewTouchUpInside(registeredBT, registeredBTclick:);
    [self.view addSubview:registeredBT];
    
    registeredBT.sd_layout
    .heightIs(44)
    .leftSpaceToView(self.view, 15)
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(InvitecodeView, 30);
    
    UIButton *CheckBT = [UIButton new];
    if (Check == NO) {
        [CheckBT setBackgroundColor:NewWhiteColor];
    }else{
        [CheckBT setBackgroundColor:NewRedColor];
    }
    NewTouchUpInside(CheckBT, CheckBTclick:);
    [self.view addSubview:CheckBT];
    
    CheckBT.sd_layout
    .heightIs(12)
    .widthIs(12)
    .leftSpaceToView(self.view, 15)
    .topSpaceToView(registeredBT, 20);
    
    UILabel *tishiLB = [UILabel new];
    tishiLB.text = @"我已年满18岁并同意";
    tishiLB.textColor = [UIColor colorWithString:@"#7E7B83"];
    tishiLB.font = NewFont(12);
    [tishiLB setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:tishiLB];
    
    tishiLB.sd_layout
    .heightIs(12)
    .centerYEqualToView(CheckBT)
    .leftSpaceToView(CheckBT, 5);
    
    CGFloat lbwidth = [NewUtils heightforString:@"用户条款" andHeight:12 fontSize:12];
    UIButton *yonghutiaokuangBT = [UIButton new];
    [yonghutiaokuangBT setTitle:@"用户条款" forState:UIControlStateNormal];
    [yonghutiaokuangBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    yonghutiaokuangBT.titleLabel.font = NewFont(12);
    [self.view addSubview:yonghutiaokuangBT];
    
    yonghutiaokuangBT.sd_layout
    .heightIs(12)
    .widthIs(lbwidth)
    .leftSpaceToView(tishiLB, 1)
    .centerYEqualToView(CheckBT);
    
    UILabel *tishiLB1 = [UILabel new];
    tishiLB1.text = @"和";
    tishiLB1.textColor = [UIColor colorWithString:@"#7E7B83"];
    tishiLB1.font = NewFont(12);
    [tishiLB1 setSingleLineAutoResizeWithMaxWidth:0];
    [self.view addSubview:tishiLB1];
    
    tishiLB1.sd_layout
    .heightIs(12)
    .centerYEqualToView(CheckBT)
    .leftSpaceToView(yonghutiaokuangBT, 1);
    
    CGFloat lbwidth1 = [NewUtils heightforString:@"隐私政策" andHeight:12 fontSize:12];
    UIButton *yonghutiaokuangBT1 = [UIButton new];
    [yonghutiaokuangBT1 setTitle:@"隐私政策" forState:UIControlStateNormal];
    [yonghutiaokuangBT1 setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
    yonghutiaokuangBT1.titleLabel.font = NewFont(12);
    [self.view addSubview:yonghutiaokuangBT1];
    
    yonghutiaokuangBT1.sd_layout
    .heightIs(12)
    .widthIs(lbwidth1)
    .leftSpaceToView(tishiLB1, 1)
    .centerYEqualToView(CheckBT);
    
}


- (void)registeredBTclick:(UIButton *)sender
{

    if (![self checkUsername:UsernameStr]) {
        [SVProgressHUD showErrorWithStatus:@"用户名长度必须为6~12之间由小写字母和数字组成"];
        return;
    }else if (![self checkPassword:PasswordStr]){
        [SVProgressHUD showErrorWithStatus:@"账户密码长度必须为6~20之间由小写字母和数字组成"];
        return;
    }else if ([UsernameStr isEqualToString:PasswordStr]){
        [SVProgressHUD showErrorWithStatus:@"您输入的密码不能与您的用户名相似"];
        return;
    }else if (![PasswordStr isEqualToString:ConfirmpasswordStr]){
        [SVProgressHUD showErrorWithStatus:@"您输入的2次密码不相同"];
        return;
    }else if (![self validateContactNumber:IphoneNumberStr]){
        [SVProgressHUD showErrorWithStatus:@"请输入正确的手机号码"];
        return;
    }else if (NameStr.length<=0){
        [SVProgressHUD showErrorWithStatus:@"请输入名字"];
        return;
    }else if (tuidStr.length<=0){
        [SVProgressHUD showErrorWithStatus:@"请输入验证码"];
        return;
    }else if (Check == NO){
        [SVProgressHUD showErrorWithStatus:@"请先同意用户条款和隐私政策"];
        return;
    }
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:UsernameStr forKey:@"username"];
    [dic setValue:PasswordStr forKey:@"password"];
    [dic setValue:ConfirmpasswordStr forKey:@"cpassword"];
    [dic setValue:NameStr forKey:@"firstName"];
    [dic setValue:IphoneNumberStr forKey:@"contactNumber"];
    [dic setValue:tuidStr forKey:@"verify_code"];
    [dic setValue:tuid forKey:@"tuid"];
    [dic setValue:Check == YES?@"1":@"0" forKey:@"terms"];
    [dic setValue:InvitecodeStr==nil?@"":InvitecodeStr forKey:@"referral_code"];

    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:smsRegCreatePlayer parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            NSMutableArray *arr = [NSMutableArray arrayWithArray:requestData[@"result"]];
            NSString *string = [arr componentsJoinedByString:@","];
            [SVProgressHUD showErrorWithStatus:string];
        }else{
            [SVProgressHUD dismiss];
            [SVProgressHUD showSuccessWithStatus:@"注册成功"];
            [self loginBTclick:UsernameStr mimaStr:PasswordStr];
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
    
}

#pragma mark ---- 每次注册自动登录
- (void)loginBTclick:(NSString *)iphoneNumberStr mimaStr:(NSString *)mimaStr
{
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:iphoneNumberStr forKey:@"username"];
    [dic setValue:mimaStr forKey:@"password"];
    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:login parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
            if ([requestData[@"code"] integerValue] == 5) {
                [SVProgressHUD showErrorWithStatus:@"账户名或密码错误"];
            }else if ([requestData[@"code"] integerValue] == 3){
                [SVProgressHUD showErrorWithStatus:@"账户名或密码错误"];
            }else{
                [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
            }
        }else{
            [SVProgressHUD showSuccessWithStatus:@"登录成功"];
            [UserEntity sharedInstance].token = requestData[@"result"][@"token"];
            [UserEntity sharedInstance].playerName = requestData[@"result"][@"playerName"];
            [UserEntity sharedInstance].playerId = requestData[@"result"][@"playerId"];
            [UserEntity sharedInstance].password = mimaStr;

            [UserHelper cacheUserInfoFromLocation];
            [UserHelper writeUserInfoFromLocation];
            [self NetworkRequestManagerGetupdatePlayerWithdrawalPassword:mimaStr];//每次登陆修改取款密码  保持跟登陆密码一致
            [[NewInterfaceReplacement sharedInstance] replacementController];//直接到首页
            NSLog(@"%@",[NewUtils userDefaultsStringKey:NewAccountNumber]);
            NSLog(@"");
        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}

-(void)NetworkRequestManagerGetupdatePlayerWithdrawalPassword:(NSString *)mimaStr {
    NSMutableDictionary *dic = NewMutableDictionaryInit;
    [dic setValue:ApiKey forKey:@"api_key"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewAccountNumber] forKey:@"username"];
    [dic setValue:[NewUtils userDefaultsStringKey:NewToken] forKey:@"token"];
//    [dic setValue:ordPassWord forKey:@"old_password"];
    [dic setValue:mimaStr forKey:@"new_password"];
    [dic setValue:@"1" forKey:@"force_reset"];
//    [SVProgressHUD show];
    [NetworkRequestManager requestPostWithInterfacePrefix:updatePlayerWithdrawalPassword parameters:dic onSuccess:^(id requestData) {
        if ([requestData[@"code"] integerValue] != 0) {
//            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
        }else{
//            [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"修改%@密码成功",_str]];
        }
    } onFailure:^{
//        [SVProgressHUD dismiss];
    }];
}


- (BOOL)checkUsername:(NSString *)password
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,12}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
}
- (BOOL)checkPassword:(NSString *)password
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,20}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES%@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
}

- (BOOL)validateContactNumber:(NSString *)mobileNum
{
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 16[6], 17[5, 6, 7, 8], 18[0-9], 170[0-9], 19[89]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705,198
     * 联通号段: 130,131,132,155,156,185,186,145,175,176,1709,166
     * 电信号段: 133,153,180,181,189,177,1700,199
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|6[6]|7[05-8]|8[0-9]|9[89])\\d{8}$";

    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478]|9[8])\\d{8}$)|(^1705\\d{7}$)";
   
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|66|7[56]|8[56])\\d{8}$)|(^1709\\d{7}$)";

    NSString *CT = @"(^1(33|53|77|8[019]|99)\\d{8}$)|(^1700\\d{7}$)";
    
    /**
     * 大陆地区固话及小灵通
     * 区号：010,020,021,022,023,024,025,027,028,029
     * 号码：七位或八位
     */
   // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
   // NSPredicate *regextestPHS = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    
    if(([regextestmobile evaluateWithObject:mobileNum] == YES)
       || ([regextestcm evaluateWithObject:mobileNum] == YES)
       || ([regextestct evaluateWithObject:mobileNum] == YES)
       || ([regextestcu evaluateWithObject:mobileNum] == YES)) {
        return YES;
    } else {
        return NO;
    }
}

- (void)CheckBTclick:(UIButton *)sender {
    if (Check == NO) {
        [sender setBackgroundColor:NewRedColor];
        Check = YES;
    }else{
        [sender setBackgroundColor:NewWhiteColor];
        Check = NO;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"开始编辑");
    if (textField.tag == 100) {
        if (![self checkUsername:UsernameStr]) {
            tishi1.hidden = NO;
            tishi1.textColor = NewRedColor;
        }else{
            tishi1.hidden = YES;
            tishi1.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 101){
        if (![self checkPassword:PasswordStr]){
            tishi2.hidden = NO;
            tishi2.textColor = NewRedColor;
        }else{
            tishi2.hidden = YES;
            tishi2.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 102){
        if (![PasswordStr isEqualToString:ConfirmpasswordStr]){
            tishi3.hidden = NO;
            tishi3.textColor = NewRedColor;
        }else{
            tishi3.hidden = YES;
            tishi3.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 103){
        if (![self validateContactNumber:IphoneNumberStr]){
            tishi4.hidden = NO;
            tishi4.textColor = NewRedColor;
        }else{
            tishi4.hidden = YES;
            tishi4.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 104){
        if (NameStr.length<0){
            tishi5.hidden = NO;
            tishi5.textColor = NewRedColor;
        }else{
            tishi5.hidden = YES;
            tishi5.textColor = NewWhiteColor;
        }
    }
}
//4. 完成编辑时的代理方法
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"完成编辑");
    if (textField.tag == 100) {
        if (UsernameStr.length == 0) {
            tishi1.hidden = YES;
            tishi1.textColor = NewWhiteColor;
        }
        [self NetworkRequestManagerisPlayerExist:UsernameStr];
    }else if (textField.tag == 101){
        if (PasswordStr.length == 0) {
            tishi2.hidden = YES;
            tishi2.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 102){
        if (ConfirmpasswordStr.length == 0) {
            tishi3.hidden = YES;
            tishi3.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 103){
        if (IphoneNumberStr.length == 0) {
            tishi4.hidden = YES;
            tishi4.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 104){
        if (NameStr.length == 0) {
            tishi5.hidden = YES;
            tishi5.textColor = NewWhiteColor;
        }
    }
    
}
#pragma mark - TextField作字符输入限制
- (void)textFieldEditingChanged:(UITextField *)textField
{
    NSLog(@"%@",textField.text);
    if (textField.tag == 100) {
        UsernameStr = textField.text;
        if (![self checkUsername:UsernameStr]) {
            tishi1.hidden = NO;
            tishi1.textColor = NewRedColor;
        }else{
            tishi1.hidden = YES;
            tishi1.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 101){
        PasswordStr = textField.text;
        if (![self checkPassword:PasswordStr]){
            tishi2.hidden = NO;
            tishi2.textColor = NewRedColor;
        }else{
            tishi2.hidden = YES;
            tishi2.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 102){
        ConfirmpasswordStr = textField.text;
        if (![PasswordStr isEqualToString:ConfirmpasswordStr]){
            tishi3.hidden = NO;
            tishi3.textColor = NewRedColor;
        }else{
            tishi3.hidden = YES;
            tishi3.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 103){
        IphoneNumberStr = textField.text;
        if (![self validateContactNumber:IphoneNumberStr]){
            tishi4.hidden = NO;
            tishi4.textColor = NewRedColor;
        }else{
            tishi4.hidden = YES;
            tishi4.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 104){
        NameStr = textField.text;
        if (NameStr.length<0){
            tishi5.hidden = NO;
            tishi5.textColor = NewRedColor;
        }else{
            tishi5.hidden = YES;
            tishi5.textColor = NewWhiteColor;
        }
    }else if (textField.tag == 106){
        InvitecodeStr = textField.text;
    }else if (textField.tag == 109){
        tuidStr = textField.text;
    }
}


- (void)NetworkRequestManagerisPlayerExist:(NSString *)username{
    NSString *url = [NSString stringWithFormat:@"/%@/%@/%@",isPlayerExist,ApiKey,username];
    [NetworkRequestManager requestGetWithInterfacePrefix:url parameters:nil onSuccess:^(id requestData) {
//        if ([requestData[@"code"] integerValue] != 0) {
//            [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
//        }else{
        if ([requestData[@"success"] integerValue] != 0) {
            [SVProgressHUD showErrorWithStatus:@"用户名已存在"];
        }
//        }
    } onFailure:^{
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - 获取验证码
- (void)touchVerificationCode:(UIButton *)sender
{
    if (![self validateContactNumber:IphoneNumberStr]){
        [SVProgressHUD showErrorWithStatus:@"请输入正确的手机号码"];
        return;
    }
        NSMutableDictionary *dic = NewMutableDictionaryInit;
        [dic setValue:ApiKey forKey:@"api_key"];
        [dic setValue:IphoneNumberStr forKey:@"contactNumber"];
        [SVProgressHUD show];
        [NetworkRequestManager requestPostWithInterfacePrefix:smsRegSendSms parameters:dic onSuccess:^(id requestData) {
            if ([requestData[@"code"] integerValue] != 0) {
                if ([requestData[@"code"] integerValue] == 594) {
                    [SVProgressHUD showErrorWithStatus:@"手機號碼已由其他玩家使用"];
                }else if ([requestData[@"code"] integerValue] == 544){
                    [SVProgressHUD showErrorWithStatus:@"不可連續重發驗證短信，請等待n 秒鐘之後再試"];
                }else if ([requestData[@"code"] integerValue] == 593){
                    [SVProgressHUD showErrorWithStatus:@"玩家手機號碼格式不對或空白"];
                }else if ([requestData[@"code"] integerValue] == 545){
                    [SVProgressHUD showErrorWithStatus:@"已達到每分鐘發送短信數上限m"];
                }else if ([requestData[@"code"] integerValue] == 546){
                    [SVProgressHUD showErrorWithStatus:@"今天短信發送次數已達到上限m"];
                }else{
                    [SVProgressHUD showErrorWithStatus:requestData[@"message"]];
                }
            }else{
                [MBProgressHUD showSuccess:@"短信已发送" toView:self.view];
                sender.frame = CGRectMake(sender.left, sender.top, 80, sender.height);
                [self verificationCode:60 sender:sender];
                tuid = requestData[@"result"][@"tuid"];
                [SVProgressHUD dismiss];
            }
        } onFailure:^{
            [SVProgressHUD dismiss];
        }];
}
- (void)verificationCode:(NSInteger)code sender:(UIButton *)sender{
    NSString *str=[NSString stringWithFormat:@"%d秒",(int)code];
    [sender setTitle:str forState:UIControlStateNormal];
    [sender setBackgroundColor:[UIColor colorWithString:@"#666666"]];

    sender.enabled=NO;
    if (code==0) {
        sender.enabled=YES;
        [sender setTitle:@"获取验证码" forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];

        sender.frame = CGRectMake(sender.left, sender.top, 66, sender.height);
        return;
    }
    code--;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self verificationCode:code sender:sender];
    });
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
@end
