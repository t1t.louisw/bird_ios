//
//  NewTransferCell.h
//  NewProject
//
//  Created by mac on 2019/11/19.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewTransferCell : UITableViewCell
@property (nonatomic,strong)UILabel *name;
@property (nonatomic,strong)UILabel *jiage;
@property (nonatomic,strong)UIButton *zhuanzhangBT;

- (void)assignment:(NSDictionary *)model;

@end

NS_ASSUME_NONNULL_END
