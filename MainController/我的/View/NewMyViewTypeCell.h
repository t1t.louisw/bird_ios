//
//  NewMyViewTypeCell.h
//  NewProject
//
//  Created by mac on 2019/11/15.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewMyViewTypeCell : UICollectionViewCell
@property(nonatomic,copy)UIView *redView;
- (void)assignment:(NSDictionary *)model;
@end

NS_ASSUME_NONNULL_END
