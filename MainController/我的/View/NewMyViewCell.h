//
//  NewMyViewCell.h
//  NewProject
//
//  Created by mac on 2019/11/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYProGressView.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewMyViewCell : UICollectionViewCell
@property (nonatomic,strong)UIButton *quxiaoBT;

@property (nonatomic,strong)ZYProGressView *progress;
@property (nonatomic,strong)UIButton *chongzhiBT;
@property (nonatomic,strong)UIButton *tixianBT;
@property (nonatomic,strong)UIButton *zhuanzhangBT;

@property (nonatomic,strong)UILabel *zongjineLB1;
@property (nonatomic,strong)UIButton *PersonalinformationBT;

@property (nonatomic,strong)UIButton *youhuiBT;
@property (nonatomic,strong)UIButton *Personalinformation111BT;


- (void)assignment:(NSDictionary *)model touzuDic:(NSDictionary *)touzuDic;
@end

NS_ASSUME_NONNULL_END
