//
//  NewbankaccountCell.m
//  NewProject
//
//  Created by mac on 2019/12/6.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewbankaccountCell.h"

@implementation NewbankaccountCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _bankbackimageV = [UIImageView new];
        [_bankbackimageV setImage:NewImageNamed(@"通用底图")];
        ViewRadius(_bankbackimageV, 5);
        _bankbackimageV.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_bankbackimageV];
        
        _bankbackimageV.sd_layout
        .heightIs(120)
        .leftSpaceToView(self, 15)
        .rightSpaceToView(self, 15)
        .topSpaceToView(self, 10);
        
        _bankimageV = [UIImageView new];
        [_bankimageV setImage:NewImageNamed(@"通用")];
        ViewRadius(_bankimageV, 20);
        _bankimageV.contentMode = UIViewContentModeScaleAspectFill;
        [_bankbackimageV addSubview:_bankimageV];
        
        _bankimageV.sd_layout
        .heightIs(40)
        .leftSpaceToView(_bankbackimageV, 15)
        .widthIs(40)
        .topSpaceToView(_bankbackimageV, 22);
        
        _bankname = [UILabel new];
        _bankname.font = NewBFont(17);
        _bankname.textColor = NewWhiteColor;
        [_bankname setSingleLineAutoResizeWithMaxWidth:0];
        [_bankbackimageV addSubview:_bankname];
        
        _bankname.sd_layout
        .heightIs(18)
        .leftSpaceToView(_bankimageV, 15)
        .topSpaceToView(_bankbackimageV, 22);
        
        _nameLB = [UILabel new];
        _nameLB.font = NewFont(12);
        _nameLB.textColor = NewWhiteColor;
        _nameLB.alpha = 0.8;
        [_nameLB setSingleLineAutoResizeWithMaxWidth:0];
        [_bankbackimageV addSubview:_nameLB];
        
        _nameLB.sd_layout
        .heightIs(12)
        .leftSpaceToView(_bankimageV, 15)
        .topSpaceToView(_bankname, 10);
        
        _bankNumber = [UILabel new];
        _bankNumber.textColor = NewWhiteColor;
        _bankNumber.font = NewFont(18);
        [_bankbackimageV addSubview:_bankNumber];
        
        _bankNumber.sd_layout
        .leftSpaceToView(_bankimageV, 15)
        .rightSpaceToView(_bankbackimageV, 15)
        .heightIs(20)
        .topSpaceToView(_nameLB, 20);
        
        
    }
    return self;
}
- (void)assignment:(NSDictionary *)model bankType:(NSString *)bankType{
    if ([bankType isEqualToString:@"存款银行卡"]) {
        _bankname.text = model[@"bankName"];
        _nameLB.text = model[@"bankAccountFullName"];
        _bankNumber.text = [NewUtils getNewStarBankNumWitOldNum:model[@"bankAccountNumber"]];
    }else{
        NSString *jsonStr1 = [model[@"bankName"] stringByReplacingOccurrencesOfString:@"_json:" withString:@""]; // 去掉空格
        NSData *data = [jsonStr1 dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
        _bankname.text = json[@"2"];
        _nameLB.text = model[@"bankAccountFullName"];
        _bankNumber.text = [NewUtils getNewStarBankNumWitOldNum:model[@"bankAccountNumber"]];

    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
