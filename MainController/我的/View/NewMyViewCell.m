//
//  NewMyViewCell.m
//  NewProject
//
//  Created by mac on 2019/11/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewMyViewCell.h"

@implementation NewMyViewCell{
    UILabel *dengjiLB;
    UILabel *depositLB;
    UILabel *progressLB;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _quxiaoBT = [UIButton new];
        [_quxiaoBT setImage:NewImageNamed(@"返回-1") forState:UIControlStateNormal];
        [self addSubview:_quxiaoBT];
        
        _quxiaoBT.sd_layout
        .heightIs(30)
        .widthIs(30)
        .leftSpaceToView(self, 10)
        .topSpaceToView(self, 5);
        
        UIImageView *headImage = [UIImageView new];
        [headImage setImage:NewImageNamed(@"icon")];
        ViewRadius(headImage, 40);
        [self addSubview:headImage];
        
        headImage.sd_layout
        .heightIs(80)
        .widthIs(80)
        .topSpaceToView(self, 40)
        .leftSpaceToView(self, 20);
        
        _PersonalinformationBT = [UIButton new];
        [_PersonalinformationBT setBackgroundColor:NewClearColor];
        ViewRadius(_PersonalinformationBT, 40);
        [headImage addSubview:_PersonalinformationBT];
        
        _PersonalinformationBT.sd_layout
        .bottomEqualToView(headImage)
        .leftEqualToView(headImage)
        .rightEqualToView(headImage)
        .topEqualToView(headImage);
        
        UILabel *name = [UILabel new];
        name.text = [NewUtils userDefaultsStringKey:NewAccountNumber];
        name.textColor = NewWhiteColor;
        [name setSingleLineAutoResizeWithMaxWidth:0];
        name.font = NewFont(25);
        [self addSubview:name];
        
        name.sd_layout
        .heightIs(25)
        .leftSpaceToView(headImage, 20)
        .topSpaceToView(self, 45);
        
        UIView *dengjiView = [UIView new];
        dengjiView.backgroundColor = [UIColor colorWithString:@"#FFDE41"];
        [self addSubview:dengjiView];
        dengjiView.alpha = 0.5;
        ViewRadius(dengjiView, 15/2);
        
        dengjiView.sd_layout
        .heightIs(15)
        .leftEqualToView(name)
        .widthIs(50)
        .topSpaceToView(name, 5);
        
        UIImageView *DJimageV = [UIImageView new];
        [DJimageV setImage:NewImageNamed(@"等级")];
//        DJimageV.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:DJimageV];
        
        DJimageV.sd_layout
        .heightIs(15)
        .widthIs(15)
        .leftEqualToView(name)
        .topEqualToView(dengjiView);
        
        dengjiLB = [UILabel new];
        dengjiLB.font = NewFont(10);
        [dengjiLB setSingleLineAutoResizeWithMaxWidth:0];
        dengjiLB.textColor = [UIColor colorWithString:@"#ECECEC"];
        [self addSubview:dengjiLB];
        
        dengjiLB.sd_layout
        .heightIs(15)
        .leftSpaceToView(DJimageV, 5)
        .centerYEqualToView(DJimageV);
        
        _youhuiBT = [UIButton new];
        [_youhuiBT setBackgroundColor:NewClearColor];
        [self addSubview:_youhuiBT];
        
        _youhuiBT.sd_layout
        .bottomEqualToView(DJimageV)
        .leftEqualToView(DJimageV)
        .rightEqualToView(dengjiLB)
        .topEqualToView(DJimageV);
        

        
        UIImageView *jiantouImage = [UIImageView new];
        [jiantouImage setImage:NewImageNamed(@"个人中心右键头")];
//        jiantouImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:jiantouImage];
        
        jiantouImage.sd_layout
        .heightIs(11.6)
        .widthIs(6.8)
        .rightSpaceToView(self, 20)
        .centerYEqualToView(headImage);
        
        _Personalinformation111BT= [UIButton new];
        [_Personalinformation111BT setBackgroundColor:NewClearColor];
        [self addSubview:_Personalinformation111BT];
        
        _Personalinformation111BT.sd_layout
        .centerYEqualToView(headImage)
        .heightIs(40)
        .rightEqualToView(jiantouImage)
        .widthIs(40);
        
        CGFloat jinduWidth = SCREEN_WIDTH-kMainPageDistance-108-26.8-10;
        
        _progress = [[ZYProGressView alloc]initWithFrame:CGRectMake(108+5+5, 95+2, jinduWidth, 5)];
        _progress.bottomColor = [UIColor colorWithString:@"#41305B"];
        _progress.progressColor = [UIColor colorWithString:@"#FF6D44"];
        [self addSubview:_progress];
        
        progressLB = [UILabel new];
        progressLB.font = NewFont(10);
        progressLB.textColor = NewWhiteColor;
        [progressLB setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:progressLB];
        
        progressLB.sd_layout
        .centerYEqualToView(_progress)
        .heightIs(12)
        .leftSpaceToView(_progress, 5);
        
        depositLB = [UILabel new];
        depositLB.textColor = [UIColor colorWithString:@"#ECECEC"];
        depositLB.font = NewFont(10);
        [depositLB setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:depositLB];
        
        depositLB.sd_layout
        .heightIs(12)
        .topSpaceToView(_progress, 5)
        .leftEqualToView(_progress);
        
//        UILabel *betLB = [UILabel new];
//        betLB.text = @"下注:1.75/10.00¥";
//        betLB.textColor = [UIColor colorWithString:@"#ECECEC"];
//        betLB.font = NewFont(9);
//        [betLB setSingleLineAutoResizeWithMaxWidth:0];
//        [self addSubview:betLB];
//
//        betLB.sd_layout
//        .heightIs(12)
//        .topSpaceToView(_progress, 5)
//        .rightEqualToView(_progress);
        

        
        UIView *amountBackView = [UIView new];
        [amountBackView setBackgroundColor:[UIColor colorWithString:@"#FF6D44"]];
        ViewRadius(amountBackView, 12);
        [self addSubview:amountBackView];
        
        amountBackView.sd_layout
        .heightIs(90)
        .topSpaceToView(depositLB, 20)
        .leftSpaceToView(self, 20)
        .rightSpaceToView(self, 20);
        
        UIButton *ZQBbackBT = [UIButton new];
        [ZQBbackBT setBackgroundColor:NewClearColor];
        [amountBackView addSubview:ZQBbackBT];

        ZQBbackBT.sd_layout
        .heightIs(90)
        .topSpaceToView(amountBackView, 0)
        .leftEqualToView(amountBackView)
        .widthIs((SCREEN_WIDTH-40)/2);
        
        _zongjineLB1 = [UILabel new];
        _zongjineLB1.textColor = NewWhiteColor;
        _zongjineLB1.font = NewFont(18);
        _zongjineLB1.text = @"0.00";
        [_zongjineLB1 setSingleLineAutoResizeWithMaxWidth:0];
        [amountBackView addSubview:_zongjineLB1];
        
        _zongjineLB1.sd_layout
        .heightIs(13.5)
        .bottomSpaceToView(amountBackView, 50)
        .centerXEqualToView(ZQBbackBT);
        
        UILabel *zqbjineLB1 = [UILabel new];
        zqbjineLB1.font = NewFont(15);
        zqbjineLB1.text = @"主钱包";
        zqbjineLB1.textColor = NewWhiteColor;
        [zqbjineLB1 setSingleLineAutoResizeWithMaxWidth:0];
        [amountBackView addSubview:zqbjineLB1];
        
        zqbjineLB1.sd_layout
        .heightIs(15)
        .topSpaceToView(_zongjineLB1, 10)
        .centerXEqualToView(ZQBbackBT);
        
        UIButton *YXQBbackBT = [UIButton new];
        [YXQBbackBT setBackgroundColor:NewClearColor];
        [amountBackView addSubview:YXQBbackBT];
        
        YXQBbackBT.sd_layout
        .heightIs(90)
        .topSpaceToView(amountBackView, 0)
        .rightEqualToView(amountBackView)
        .widthIs((SCREEN_WIDTH-40)/2);
        
        _chongzhiBT = [UIButton new];
        [_chongzhiBT setBackgroundColor:NewClearColor];
        [YXQBbackBT addSubview:_chongzhiBT];
        
        _chongzhiBT.sd_layout
        .heightIs(90)
        .widthIs((SCREEN_WIDTH-40)/4)
        .leftEqualToView(YXQBbackBT)
        .topEqualToView(YXQBbackBT);
        
        UIImageView *chongzhiImage = [UIImageView new];
        [chongzhiImage setImage:NewImageNamed(@"存款")];
        chongzhiImage.contentMode = UIViewContentModeScaleAspectFill;
        [_chongzhiBT addSubview:chongzhiImage];
        
        chongzhiImage.sd_layout
        .bottomSpaceToView(_chongzhiBT, 40)
        .centerXEqualToView(_chongzhiBT)
        .heightIs(30)
        .widthIs(30);
        
        UILabel *chongzhiLB = [UILabel new];
        chongzhiLB.text = @"充值";
        chongzhiLB.font = NewFont(15);
        chongzhiLB.textColor = NewWhiteColor;
        [chongzhiLB setSingleLineAutoResizeWithMaxWidth:0];
        [_chongzhiBT addSubview:chongzhiLB];
    
        chongzhiLB.sd_layout
        .topSpaceToView(chongzhiImage, 5)
        .centerXEqualToView(_chongzhiBT)
        .heightIs(15);
        
        _tixianBT = [UIButton new];
        [_tixianBT setBackgroundColor:NewClearColor];
        [YXQBbackBT addSubview:_tixianBT];
        
        _tixianBT.sd_layout
        .heightIs(90)
        .widthIs((SCREEN_WIDTH-40)/4)
        .rightEqualToView(YXQBbackBT)
        .topEqualToView(YXQBbackBT);
        
        UIImageView *tixianImage = [UIImageView new];
        [tixianImage setImage:NewImageNamed(@"取款")];
        tixianImage.contentMode = UIViewContentModeScaleAspectFill;
        [_tixianBT addSubview:tixianImage];
        
        tixianImage.sd_layout
        .bottomSpaceToView(_tixianBT, 40)
        .centerXEqualToView(_tixianBT)
        .heightIs(30)
        .widthIs(30);
        
        UILabel *tixianLB = [UILabel new];
        tixianLB.text = @"提现";
        tixianLB.font = NewFont(15);
        tixianLB.textColor = NewWhiteColor;
        [tixianLB setSingleLineAutoResizeWithMaxWidth:0];
        [_tixianBT addSubview:tixianLB];
        
        tixianLB.sd_layout
        .topSpaceToView(tixianImage, 5)
        .centerXEqualToView(_tixianBT)
        .heightIs(15);
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line];

        line.sd_layout
        .heightIs(10)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomEqualToView(self);

        
    }
    return self;
}
- (void)assignment:(NSDictionary *)model touzuDic:(NSDictionary *)touzuDic{
    _zongjineLB1.text = [NSString stringWithFormat:@"%.2f",[model[@"mainwallet"] floatValue]+[model[@"subwallets"][@"5593"] floatValue]];
    dengjiLB.text = touzuDic[@"current_level"];
    depositLB.text = [NSString stringWithFormat:@"投注:%@¥/%@¥",touzuDic[@"current_bet"],touzuDic[@"required_bet"]];
    NSString *baifenbi = [NSString stringWithFormat:@"%.2f",[touzuDic[@"current_bet"] floatValue]/[touzuDic[@"required_bet"] floatValue]];
    progressLB.text = [NSString stringWithFormat:@"%ld%@",[baifenbi integerValue],@"%"];
    
    _progress.progressValue = [NSString stringWithFormat:@"%.2f",[baifenbi floatValue]*0.01];

}
@end
