//
//  NewTransferCell.m
//  NewProject
//
//  Created by mac on 2019/11/19.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewTransferCell.h"

@implementation NewTransferCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _name = [UILabel new];
        _name.text = @"T1 王者联盟";
        _name.font = NewFont(15);
        [_name setSingleLineAutoResizeWithMaxWidth:0];
        _name.textColor = [UIColor colorWithString:@"#AFACB4"];
        [self addSubview:_name];
        
        _name.sd_layout
        .heightIs(15)
        .centerYEqualToView(self)
        .leftSpaceToView(self, 15);
        
        _zhuanzhangBT = [UIButton new];
        [_zhuanzhangBT setTitle:@"转账" forState:UIControlStateNormal];
        [_zhuanzhangBT setBackgroundColor:[UIColor colorWithString:@"#32204C"]];
        [_zhuanzhangBT setTitleColor:[UIColor colorWithString:@"#FF6D44"] forState:UIControlStateNormal];
        _zhuanzhangBT.titleLabel.font = NewFont(14);
        ViewRadius(_zhuanzhangBT, 25/2);
        [self addSubview:_zhuanzhangBT];
        
        _zhuanzhangBT.sd_layout
        .heightIs(25)
        .rightSpaceToView(self, 15)
        .widthIs(63)
        .centerYEqualToView(self);
        
        _jiage = [UILabel new];
        _jiage.text = @"¥ 0.00";
        _jiage.textColor = [UIColor colorWithString:@"#AFACB4"];
        [_jiage setSingleLineAutoResizeWithMaxWidth:0];
        _jiage.font = NewFont(15);
        [self addSubview:_jiage];
        
        _jiage.sd_layout
        .heightIs(15)
        .centerYEqualToView(self)
        .rightSpaceToView(_zhuanzhangBT, 50);
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line];
        
        line.sd_layout
        .heightIs(1)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomEqualToView(self);
    }
    return self;
}
- (void)assignment:(NSDictionary *)model {
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
