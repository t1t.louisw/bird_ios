//
//  NewbankaccountCell.h
//  NewProject
//
//  Created by mac on 2019/12/6.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewbankaccountCell : UITableViewCell
@property (nonatomic,copy) UIImageView *bankbackimageV;
@property (nonatomic,copy) UIImageView *bankimageV;

@property (nonatomic,copy) UILabel *bankname;
@property (nonatomic,copy) UILabel *nameLB;
@property (nonatomic,copy) UILabel *bankNumber;
- (void)assignment:(NSDictionary *)model bankType:(NSString *)bankType;
@end

NS_ASSUME_NONNULL_END
