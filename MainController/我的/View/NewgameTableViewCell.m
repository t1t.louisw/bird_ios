//
//  NewgameTableViewCell.m
//  NewProject
//
//  Created by mac on 2019/12/30.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewgameTableViewCell.h"

@implementation NewgameTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _time = [UILabel new];
        _time.font = NewFont(12);
        _time.textColor = [UIColor colorWithString:@"#AFACB4"];
        _time.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_time];
        
        _time.sd_layout
        .topEqualToView(self)
        .bottomEqualToView(self)
        .widthIs(SCREEN_WIDTH/3)
        .leftEqualToView(self);
       
        _orderNumber = [UILabel new];
        _orderNumber.font = NewFont(12);
        _orderNumber.textColor = [UIColor colorWithString:@"#AFACB4"];
        _orderNumber.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_orderNumber];
        
        _orderNumber.sd_layout
        .topEqualToView(self)
        .bottomEqualToView(self)
        .widthIs(SCREEN_WIDTH/3+5)
        .leftSpaceToView(_time, 0);
        
        _stauts = [UILabel new];
        _stauts.font = NewFont(12);
        _stauts.textColor = [UIColor colorWithString:@"#AFACB4"];
        _stauts.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_stauts];
        
        _stauts.sd_layout
        .topEqualToView(self)
        .bottomEqualToView(self)
        .widthIs(SCREEN_WIDTH/3)
        .leftSpaceToView(_orderNumber, 0);
        
       
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line];
        
        line.sd_layout
        .heightIs(1)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomEqualToView(self);
    }
    return self;
}
- (void)assignment:(NSDictionary *)model{
        _time.text = [NSString stringWithFormat:@"%@",model[@"game_type"]];
        _orderNumber.text = [NSString stringWithFormat:@"¥%.2f",[model[@"bet_amount"] floatValue]];
        _stauts.text = [NSString stringWithFormat:@"¥%.2f",[model[@"bet_plus_result"] floatValue]];

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
