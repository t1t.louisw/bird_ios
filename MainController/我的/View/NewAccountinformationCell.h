//
//  NewAccountinformationCell.h
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewUserInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface NewAccountinformationCell : UITableViewCell
@property (nonatomic,strong)UILabel *headTitle;

@property (nonatomic,strong)UIButton *fuzhiBT;

@property (nonatomic,strong)UIImageView *jiantouImage;

@property (nonatomic,strong)UILabel *contentTitle;

@property (nonatomic,strong)UIImageView *headImage;

@property (nonatomic,strong)UIButton *manBT;

@property (nonatomic,strong)UILabel *manLB;

@property (nonatomic,strong)UIButton *womanBT;

@property (nonatomic,strong)UILabel *womanLB;

- (void)assignment:(NSDictionary *)model;

@end

NS_ASSUME_NONNULL_END
