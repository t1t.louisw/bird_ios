//
//  NewSecuritycenterCell.h
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewSecuritycenterCell : UITableViewCell
@property (nonatomic,strong)UILabel *headTitle;
@property (nonatomic,strong)UILabel *contentTitle;

@property (nonatomic,strong)UIImageView *jiantouImage;
- (void)assignment:(NSDictionary *)model;

@end

NS_ASSUME_NONNULL_END
