//
//  NewMylimitCell.h
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewMylimitCell : UITableViewCell
@property (nonatomic,strong)UILabel *zhutiLB;
@property (nonatomic,strong)UILabel *xinxiLB;
@property (nonatomic,strong)UILabel *timeLB;
@property (nonatomic,strong)UILabel *timeLB1;

@property (nonatomic,copy)UIView *redview;

- (void)assignment:(NSDictionary *)model;

@end

NS_ASSUME_NONNULL_END
