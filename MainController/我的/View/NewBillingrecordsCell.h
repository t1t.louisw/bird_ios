//
//  NewBillingrecordsCell.h
//  NewProject
//
//  Created by mac on 2019/11/19.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewBillingrecordsCell : UITableViewCell
@property (nonatomic,strong)UILabel *time;
@property (nonatomic,strong)UILabel *time1;

@property (nonatomic,strong)UILabel *orderNumber;
@property (nonatomic,strong)UILabel *stauts;
@property (nonatomic,strong)UILabel *amount;

- (void)assignment:(NSDictionary *)model trans_type:(NSString *)trans_type;

@end

NS_ASSUME_NONNULL_END
