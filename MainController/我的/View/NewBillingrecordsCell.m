//
//  NewBillingrecordsCell.m
//  NewProject
//
//  Created by mac on 2019/11/19.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewBillingrecordsCell.h"

@implementation NewBillingrecordsCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _time = [UILabel new];
        _time.font = NewFont(12);
        _time.textColor = [UIColor colorWithString:@"#AFACB4"];
        _time.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_time];
        
        _time.sd_layout
        .heightIs(12)
        .bottomSpaceToView(self, 25)
        .widthIs(SCREEN_WIDTH/4)
        .leftEqualToView(self);
        
        _time1 = [UILabel new];
        _time1.font = NewFont(12);
        _time1.textColor = [UIColor colorWithString:@"#AFACB4"];
        _time1.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_time1];
        
        _time1.sd_layout
        .heightIs(12)
        .topSpaceToView(self, 25)
        .widthIs(SCREEN_WIDTH/4)
        .leftEqualToView(self);
       
        _orderNumber = [UILabel new];
        _orderNumber.font = NewFont(12);
        _orderNumber.textColor = [UIColor colorWithString:@"#AFACB4"];
        _orderNumber.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_orderNumber];
        
        _orderNumber.sd_layout
        .topEqualToView(self)
        .bottomEqualToView(self)
        .widthIs(SCREEN_WIDTH/4+5)
        .leftSpaceToView(_time, 0);
        
        _stauts = [UILabel new];
        _stauts.font = NewFont(12);
        _stauts.textColor = [UIColor colorWithString:@"#AFACB4"];
        _stauts.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_stauts];
        
        _stauts.sd_layout
        .topEqualToView(self)
        .bottomEqualToView(self)
        .widthIs(SCREEN_WIDTH/4)
        .leftSpaceToView(_orderNumber, 0);
        
        _amount = [UILabel new];
        _amount.font = NewFont(12);
        _amount.textColor = [UIColor colorWithString:@"#AFACB4"];
        _amount.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_amount];
        
        _amount.sd_layout
        .topEqualToView(self)
        .bottomEqualToView(self)
        .widthIs(SCREEN_WIDTH/4)
        .leftSpaceToView(_stauts, 0);
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line];
        
        line.sd_layout
        .heightIs(1)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomEqualToView(self);
    }
    return self;
}
- (void)assignment:(NSDictionary *)model trans_type:(NSString *)trans_type{
//    trans_type = @"withdrawal";
    if ([trans_type isEqualToString:@"deposit"]) {//存款
        NSArray *array = [model[@"request_date"] componentsSeparatedByString:@" "];//分隔符
        _time.text = [NSString stringWithFormat:@"%@",array[0]];
        _time1.text = [NSString stringWithFormat:@"%@",array[1]];
        _orderNumber.text = model[@"secure_id"];
        _stauts.text = model[@"status"];
        _amount.text = [NSString stringWithFormat:@"¥%@",model[@"amount"]];
    }else if ([trans_type isEqualToString:@"withdrawal"]){//取款
        NSArray *array = [model[@"date"] componentsSeparatedByString:@" "];//分隔符
        _time.text = [NSString stringWithFormat:@"%@",array[0]];
        _time1.text = [NSString stringWithFormat:@"%@",array[1]];
        _orderNumber.text = model[@"tx_code"];
        _stauts.text = model[@"status"];
        _amount.text = [NSString stringWithFormat:@"¥%@",model[@"amount"]];
    }else if ([trans_type isEqualToString:@"game"]){//游戏
        
    }

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
