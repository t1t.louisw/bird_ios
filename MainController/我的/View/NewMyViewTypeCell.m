//
//  NewMyViewTypeCell.m
//  NewProject
//
//  Created by mac on 2019/11/15.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewMyViewTypeCell.h"

@implementation NewMyViewTypeCell{
    UIImageView *imageV;
    UILabel *nameLB;
}
- (id)initWithFrame:(CGRect)frame
    {
        self = [super initWithFrame:frame];
        if (self) {
            imageV = [UIImageView new];
//            imageV.contentMode = UIViewContentModeScaleAspectFill;
            [self addSubview:imageV];
            
            imageV.sd_layout
            .heightIs(22)
            .widthIs(20)
            .leftSpaceToView(self, 15)
            .centerYEqualToView(self);
            
            nameLB = [UILabel new];
            nameLB.textColor = NewWhiteColor;
            nameLB.font = NewFont(15);
            [nameLB setSingleLineAutoResizeWithMaxWidth:0];
            [self addSubview:nameLB];
            
            nameLB.sd_layout
            .heightIs(15)
            .leftSpaceToView(imageV, 15)
            .centerYEqualToView(self);
            
            UIImageView * jiantouImage = [UIImageView new];
            [jiantouImage setImage:NewImageNamed(@"个人中心右键头")];
            [self addSubview:jiantouImage];
                        
            jiantouImage.sd_layout
            .heightIs(11.6)
            .widthIs(6.8)
            .rightSpaceToView(self, 15)
            .centerYEqualToView(self);
            
            _redView = [UIView new];
            _redView.backgroundColor = NewRedColor;
            ViewRadius(_redView, 5/2);
            [self addSubview:_redView];
            
            _redView.sd_layout
            .heightIs(5)
            .widthIs(5)
            .centerYEqualToView(nameLB)
            .leftSpaceToView(nameLB, 10);
            
            
            UILabel *line = [UILabel new];
            line.backgroundColor = [UIColor colorWithString:@"#291744"];
            [self addSubview:line];
            
            line.sd_layout
            .bottomEqualToView(self)
            .leftEqualToView(self)
            .rightEqualToView(self)
            .heightIs(1);
                
        }
    return self;
}
- (void)assignment:(NSDictionary *)model {
    [imageV setImage:NewImageNamed(model[@"image"])];
    nameLB.text = model[@"title"];
}
@end
