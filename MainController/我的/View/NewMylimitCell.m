//
//  NewMylimitCell.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewMylimitCell.h"

@implementation NewMylimitCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIView *backView = [UIView new];
        backView.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:backView];
        
        backView.sd_layout
        .heightIs(100)
        .topSpaceToView(self, 10)
        .leftEqualToView(self)
        .rightEqualToView(self);
        
        _zhutiLB = [UILabel new];
        _zhutiLB.font = NewFont(15);
        _zhutiLB.textColor = NewWhiteColor;
        _zhutiLB.lineBreakMode = NSLineBreakByTruncatingTail;
//        _zhutiLB.numberOfLines = 2;
        _zhutiLB.textAlignment = NSTextAlignmentCenter;
        [backView addSubview:_zhutiLB];
        
        _zhutiLB.sd_layout
        .widthIs(SCREEN_WIDTH/3-10)
        .leftSpaceToView(backView, 5)
        .centerYEqualToView(backView)
        .heightIs(30);
        
        _redview = [UIView new];
        _redview.backgroundColor = NewRedColor;
        ViewRadius(_redview, 5/2);
        [backView addSubview:_redview];
        
        _redview.sd_layout
        .heightIs(5)
        .widthIs(5)
        .topSpaceToView(backView, 20)
        .leftSpaceToView(_zhutiLB, 2);
        
        _xinxiLB = [UILabel new];
        _xinxiLB.font = NewFont(12);
        _xinxiLB.textColor = [UIColor colorWithString:@"#E7E7E7"];
        _xinxiLB.lineBreakMode = NSLineBreakByTruncatingTail;
        [backView addSubview:_xinxiLB];
        
        _xinxiLB.sd_layout
        .widthIs(SCREEN_WIDTH/3-10)
        .heightIs(30)
        .centerYEqualToView(backView)
        .centerXEqualToView(backView);
        
        _timeLB = [UILabel new];
        _timeLB.font = NewFont(12);
        _timeLB.textColor = [UIColor colorWithString:@"#E7E7E7"];
        [_timeLB setSingleLineAutoResizeWithMaxWidth:0];
        _timeLB.textAlignment = NSTextAlignmentRight;
        [backView addSubview:_timeLB];
        
        _timeLB.sd_layout
        .widthIs(SCREEN_WIDTH/3-10)
         .heightIs(30)
         .centerYEqualToView(backView)
        .rightSpaceToView(backView, 5);

//
//        _timeLB1 = [UILabel new];
//        _timeLB1.text = @"12-12";
//        _timeLB1.font = NewFont(12);
//        _timeLB1.textColor = [UIColor colorWithString:@"#E7E7E7"];
//        [_timeLB1 setSingleLineAutoResizeWithMaxWidth:0];
//        [backView addSubview:_timeLB1];
//
//        _timeLB1.sd_layout
//        .topSpaceToView(backView, 51)
//        .centerXEqualToView(_timeLB)
//        .heightIs(12);
        
    }
    return self;
}
- (void)assignment:(NSDictionary *)model {
    _zhutiLB.text = model[@"subject"];
    _xinxiLB.text = model[@"detail"];
    _timeLB.text = model[@"date"];
    if ([model[@"status"] isEqualToString:@"6"]) {
        _redview.hidden = NO;
    }else{
        _redview.hidden = YES;
    }

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
