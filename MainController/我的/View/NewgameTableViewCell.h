//
//  NewgameTableViewCell.h
//  NewProject
//
//  Created by mac on 2019/12/30.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewgameTableViewCell : UITableViewCell
@property (nonatomic,strong)UILabel *time;
@property (nonatomic,strong)UILabel *orderNumber;
@property (nonatomic,strong)UILabel *stauts;

- (void)assignment:(NSDictionary *)model;
@end

NS_ASSUME_NONNULL_END
