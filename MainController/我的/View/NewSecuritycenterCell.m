//
//  NewSecuritycenterCell.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewSecuritycenterCell.h"

@implementation NewSecuritycenterCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _headTitle = [UILabel new];
        _headTitle.font = NewFont(15);
        _headTitle.textColor = [UIColor colorWithString:@"#7E7B83"];
        [_headTitle setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:_headTitle];
        
        _headTitle.sd_layout
        .heightIs(30)
        .centerYEqualToView(self)
        .leftSpaceToView(self, 17.5);
        
        _jiantouImage = [UIImageView new];
        [_jiantouImage setImage:NewImageNamed(@"个人中心右键头")];
//        jiantouImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_jiantouImage];
                
        _jiantouImage.sd_layout
        .heightIs(11.6)
        .widthIs(6.8)
        .rightSpaceToView(self, 20)
        .centerYEqualToView(self);
        
        _contentTitle = [UILabel new];
        _contentTitle.font = NewFont(15);
//        _contentTitle.textColor = [UIColor colorWithString:@"#7E7B83"];
        [_contentTitle setSingleLineAutoResizeWithMaxWidth:0];
//        [self addSubview:_contentTitle];
        
        _contentTitle.sd_layout
        .heightIs(30)
        .centerYEqualToView(self)
        .rightSpaceToView(self, 35);
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line];
        
        line.sd_layout
        .heightIs(1)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomEqualToView(self);
        
        
    }
    return self;
}
- (void)assignment:(NSDictionary *)model {
    _headTitle.text = model[@"headtitle"];
//    if ([model[@"content"] isEqualToString:@"已认证"]) {
//        _contentTitle.textColor = [UIColor colorWithString:@"#7E7B83"];
//    }else{
//        _contentTitle.textColor = [UIColor colorWithString:@"#E6E2ED"];
//    }
//    _contentTitle.text = model[@"content"];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
