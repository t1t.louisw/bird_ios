//
//  NewAccountinformationCell.m
//  NewProject
//
//  Created by mac on 2019/11/20.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewAccountinformationCell.h"

@implementation NewAccountinformationCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _headTitle = [UILabel new];
        _headTitle.font = NewFont(15);
        _headTitle.textColor = [UIColor colorWithString:@"#7E7B83"];
        [_headTitle setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:_headTitle];
        
        _headTitle.sd_layout
        .heightIs(30)
        .centerYEqualToView(self)
        .leftSpaceToView(self, 17.5);
        
        _jiantouImage = [UIImageView new];
        [_jiantouImage setImage:NewImageNamed(@"个人中心右键头")];
//        jiantouImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_jiantouImage];
                
        _jiantouImage.sd_layout
        .heightIs(11.6)
        .widthIs(6.8)
        .rightSpaceToView(self, 20)
        .centerYEqualToView(self);
        
        _fuzhiBT = [UIButton new];
        [_fuzhiBT setTitle:@"复制" forState:UIControlStateNormal];
        [_fuzhiBT setTitleColor:[UIColor colorWithString:@"#7E7B83"] forState:UIControlStateNormal];
        _fuzhiBT.titleLabel.font = NewFont(10);
        ViewBorderRadius(_fuzhiBT, 3, 1, [UIColor colorWithString:@"#7E7B83"]);
        [self addSubview:_fuzhiBT];
        
        _fuzhiBT.sd_layout
        .heightIs(20)
        .centerYEqualToView(self)
        .rightSpaceToView(self, 3)
        .widthIs(30);
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#291744"];
        [self addSubview:line];
        
        line.sd_layout
        .heightIs(1)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomEqualToView(self);
        
        _contentTitle = [UILabel new];
        _contentTitle.font = NewFont(15);
        _contentTitle.textColor = [UIColor colorWithString:@"#7E7B83"];
        [_contentTitle setSingleLineAutoResizeWithMaxWidth:0];
        [self addSubview:_contentTitle];
               
        _contentTitle.sd_layout
        .heightIs(30)
        .centerYEqualToView(self)
        .rightSpaceToView(self, 35);
        
//        _headImage = [UIImageView new];
//        [_headImage setImage:NewImageNamed(@"头像")];
//        ViewRadius(_headImage, 35/2);
//        [self addSubview:_headImage];
//        
//        _headImage.sd_layout
//        .heightIs(35)
//        .widthIs(35)
//        .rightSpaceToView(self, 35)
//        .centerYEqualToView(self);
        
        _womanLB = [UILabel new];
        _womanLB.text = @"女";
        _womanLB.textColor = [UIColor colorWithString:@"#7E7B83"];
        [_womanLB setSingleLineAutoResizeWithMaxWidth:0];
        _womanLB.font = NewFont(15);
        [self addSubview:_womanLB];
        
        _womanLB.sd_layout
        .heightIs(30)
        .rightSpaceToView(self, 35)
        .centerYEqualToView(self);
        
        _womanBT = [UIButton new];
//        [_womanBT setImage:NewImageNamed(@"未选") forState:UIControlStateNormal];
        [self addSubview:_womanBT];
        
        _womanBT.sd_layout
        .heightIs(30)
        .widthIs(30)
        .rightSpaceToView(_womanLB, 3)
        .centerYEqualToView(self);
        
        _manLB = [UILabel new];
        _manLB.text = @"男";
        _manLB.textColor = [UIColor colorWithString:@"#7E7B83"];
        [_manLB setSingleLineAutoResizeWithMaxWidth:0];
        _manLB.font = NewFont(15);
        [self addSubview:_manLB];
        
        _manLB.sd_layout
        .heightIs(30)
        .rightSpaceToView(_womanBT, 50)
        .centerYEqualToView(self);
        
        _manBT = [UIButton new];
//        [_manBT setImage:NewImageNamed(@"选中") forState:UIControlStateNormal];
        [self addSubview:_manBT];
        
        _manBT.sd_layout
        .heightIs(30)
        .widthIs(30)
        .rightSpaceToView(_manLB, 3)
        .centerYEqualToView(self);
        
    }
    return self;
}
- (void)assignment:(NSDictionary *)model{
    _headTitle.text = model[@"Head"];
//    _contentTitle.text = model[@"content"];

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
