//
//  NewCustomerserviceViewController.h
//  NewProject
//
//  Created by mac on 2019/11/13.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewBasicViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewCustomerserviceViewController : NewBasicViewController
typedef void(^WebViewControllerBlock)(NSString *pathUrl);


@property (nonatomic, copy)WebViewControllerBlock block;

@property (nonatomic, copy) NSString *url;

@property (nonatomic) BOOL requstReturn;

@property (nonatomic, copy)NSString *name;

-(void)setStatusBarColor:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
