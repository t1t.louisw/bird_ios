//
//  NewThehallCell.m
//  NewProject
//
//  Created by mac on 2019/11/14.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewThehallCell.h"

@implementation NewThehallCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat Cellheight = SCREEN_WIDTH/3;
        
        UILabel *line = [UILabel new];
        line.backgroundColor = [UIColor colorWithString:@"#352251"];
        [self addSubview:line];
        
        line.sd_layout
        .heightIs(0.4)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .bottomEqualToView(self);
        
        UILabel *line1 = [UILabel new];
        line1.backgroundColor = [UIColor colorWithString:@"#352251"];
        [self addSubview:line1];
        
        line1.sd_layout
        .heightIs(0.4)
        .leftEqualToView(self)
        .rightEqualToView(self)
        .topEqualToView(self);
        
        UILabel *line2 = [UILabel new];
        line2.backgroundColor = [UIColor colorWithString:@"#352251"];
        [self addSubview:line2];
        
        line2.sd_layout
        .widthIs(0.4)
        .rightEqualToView(self)
        .topEqualToView(self)
        .bottomEqualToView(self);
        
        UIImageView *imageV = [UIImageView new];
        [imageV setImage:NewImageNamed(@"lol")];
        imageV.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:imageV];
        
        imageV.sd_layout
        .topSpaceToView(self, (Cellheight-75)/2)
        .centerXEqualToView(self)
        .heightIs(40)
        .widthIs(40);
        
        UILabel *name = [UILabel new];
        name.text = @"英雄联盟";
        name.font = NewFont(15);
        [name setSingleLineAutoResizeWithMaxWidth:0];
        name.textColor = [UIColor colorWithString:@"#C9D8ED"];
        [self addSubview:name];
        
        name.sd_layout
        .heightIs(15)
        .centerXEqualToView(self)
        .topSpaceToView(imageV, 20);
        
        
    }
    return self;
}
@end
