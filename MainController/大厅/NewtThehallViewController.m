//
//  NewtThehallViewController.m
//  NewProject
//
//  Created by mac on 2019/11/13.
//  Copyright © 2019 NewOrganization. All rights reserved.
//

#import "NewtThehallViewController.h"
#import "NewThehallCell.h"

@interface NewtThehallViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>{
    NSMutableArray *dataArray;
    UICollectionView *mainCollectionView;
    UICollectionViewFlowLayout *flowLayout;
}

@end

@implementation NewtThehallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle:@"大厅"];
    self.view.backgroundColor = [UIColor colorWithString:@"#1C0F2F"];
    [self dataInitialization];
    [self loadsView];
    // Do any additional setup after loading the view.
}
- (void)dataInitialization {
    dataArray = NewMutableArrayInit;
    [dataArray addObject:@""];
    [dataArray addObject:@""];

}

- (void)loadsView {
    [self.view addSubview:mainCollectionView = [NewControlPackage collectionViewInitWithFrame:CGRectMake(0.0, NavHeader, SCREEN_WIDTH, SCREEN_HEIGHT-NavHeader) delegate:self dataSource:self backgroundColor:[UIColor colorWithString:@"#1C0F2F"] scrollEnabled:YES alwaysBounceVertical:YES alwaysBounceHorizontal:NO showsHorizontalScrollIndicator:NO showsVerticalScrollIndicator:NO collectionViewFlowLayout:flowLayout sectionInset:UIEdgeInsetsMake(0, 0, 0, 0) headerReference:CGSizeMake(0, 0) footerReference:CGSizeMake(0, 0) minimumInteritemSpacing:0 minimumLineSpacing:0 scrollDirection:0 hidden:NO tag:105 userInteractionEnabled:YES]];
    [mainCollectionView registerClass:[NewThehallCell class] forCellWithReuseIdentifier:@"NewThehallCell"];
}
//单元格大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SCREEN_WIDTH/3, SCREEN_WIDTH/3);

}

//定义每个UICollectionView 的 margin 边距  上左下右
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NewThehallCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewThehallCell" forIndexPath:indexPath];
    cell.backgroundColor = NewClearColor;
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
