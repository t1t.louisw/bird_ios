//
//  ZFileManagerHelper.h
//  XPH
//
//  Created by Zick.Zhao on 15/3/17.
//  Copyright (c) 2015年 YAY. All rights reserved.
//

#import <Foundation/Foundation.h>

#define zFileManager [NSFileManager defaultManager]

@interface ZFileManagerHelper : NSObject


//获取Cache目录路径
+(NSString*)dirCache;

//创建文件夹
+(BOOL)xCreateCacheDirWithPath:(NSString*)_path;

//检查文件是否存在
+ (BOOL)xCheckFileIsExists:(NSString *)_path;

//获取缓存大小
+ (NSString*) getCacheSize;

//清除缓存
+ (BOOL)cleanCache;

+ (void)flushCache;//清除图片缓存


#pragma mark **********************************************************************************

////创建UnicomeJKZY 文件目录
//- (BOOL) CBC_CreateUnicomJKZYDirectoryCacheStructure;
//
////删除非当前用户数据目录结构
//- (void) CBC_DeleteNotCurrentUserCacheDirectory;

//#pragma mark **********************************************************************************
//
////文件或目录是否存在检测
//- (BOOL) xCheckDirectoryIsExisit : (NSString *) _dirPath;
//
////检查物理文件夹是否存在该资源
//- (BOOL) xCheckFileIsExisit : (NSString *) _filePath;
//
//
////根据文件路径删除文件
//- (void) xDeleteFileWithFilePathArr : (NSArray *) _filePathArr;
//
////获取Doc文件夹下面的文件路径
//- (NSString *) xGetFilePathWithFileUnderDocumentPath : (NSString *) _filePath;
//
////获取Doc文件夹下面的文件路径
//- (NSString *) xGetFilePathWithFileUnderDocumentPathArr : (NSArray *) _filePathArr;

@end
