//
//  ZFileManagerHelper.m
//  XPH
//
//  Created by Zick.Zhao on 15/3/17.
//  Copyright (c) 2015年 YAY. All rights reserved.
//

#import "ZFileManagerHelper.h"

@implementation ZFileManagerHelper

#pragma mark FileManager Methoed

//获取Cache目录路径
+(NSString*)dirCache{
    NSArray *cacPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [cacPath objectAtIndex:0];
    return cachePath;
}

//创建文件夹
+(BOOL)xCreateCacheDirWithPath:(NSString*)_path{
    NSString *cachePath =[self dirCache];
    cachePath = [cachePath stringByAppendingPathComponent:_path];
    BOOL isDir = FALSE;
    BOOL isExists = [zFileManager fileExistsAtPath:cachePath isDirectory:&isDir];
    // 创建目录
    if (!(isExists && isDir)) {
        BOOL res =[zFileManager createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:nil];
        if (res) {
            
            NSLog(@"%@文件夹创建成功",_path);
            return YES;
        }else{
            
            NSLog(@"%@文件夹创建失败",_path);
            return NO;
        }
    }
    else{
        NSLog(@"%@文件夹已存在",_path);
        return YES;
    }
}


//判断文件夹是否存在
+ (BOOL) xCheckDirectoryIsExisit : (NSString *) _dirPath{
    // 判断存放音频、视频的文件夹是否存在，不存在则创建对应文件夹
    
    BOOL isDir = FALSE;
    
    BOOL isDirExist = [zFileManager fileExistsAtPath:_dirPath isDirectory:&isDir];
    
    return isDirExist;
}

+ (BOOL)xCheckFileIsExists:(NSString *)_path{
    
    if([zFileManager fileExistsAtPath:_path]){
        return YES;
    }else{
        return NO;
    }
}

////检查文件是否存在
//- (BOOL) CBC_CheckFileIsExisit : (NSString *) _filePath
//{
//    if (!FileManager)
//    {
//        FileManager = [NSFileManager defaultManager];
//    }
//    
//    NSString *filePath = [self xGetFilePathWithFileUnderDocumentPath:_filePath];
//    
//    if([FileManager fileExistsAtPath:filePath])
//    {
//        return YES;
//    }
//    return NO;
//}
//
////获取Cache文件夹下面的文件路径
//- (NSString *) xGetFilePathWithFileUnderDocumentPath : (NSString *) _filePath
//{
//    NSString *fileTurePath = @"";
//    NSArray *pathArr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
//    NSString *docDirectoryStr = [pathArr objectAtIndex:0];
//    fileTurePath = [docDirectoryStr stringByAppendingPathComponent:_filePath];
//    return fileTurePath;
//}
//
////获取Doc文件夹下面的文件路径
//- (NSString *) xGetFilePathWithFileUnderDocumentPathArr : (NSArray *) _filePathArr
//{
//    NSString *filePath = @"";
//    for (int i = 0; i < [_filePathArr count]; i++)
//    {
//        if (filePath.length > 0)
//        {
//            filePath = [filePath stringByAppendingPathComponent:[_filePathArr objectAtIndex:i]];
//        }
//        else
//        {
//            filePath = [_filePathArr objectAtIndex:i];
//        }
//    }
//    return filePath;
//}
//
//#pragma mark --------------------------------------------------------------------
//
////创建UnicomJKZY目录结构
//- (BOOL) xCreateUnicomJKZYDirectoryCacheStructure
//{
//    BOOL create_result = NO;
//
//    create_result = [self CBC_CreateCommonCacheDirectory];
//    create_result &= [self CBC_CreateUserCacheDirectory];
//
//    return create_result;
//}
//
////创建公用文件夹
//- (BOOL) xCreateCommonCacheDirectory
//{
//    return [self CBC_CreateDirectoryWithDirName:Cache_Common];
//}
//
////创建文件夹
//- (BOOL) xCreateDirectoryWithDirName : (NSString *) _dirName
//{
//    return [self xCheckDirectoryIsExisit:_dirName];
//}
//
////创建用户数据目录结构
//- (BOOL) CBC_CreateUserCacheDirectory
//{
//    NSString *dir_tree_path = Cache_Users;
//    BOOL create_result;
//
//    create_result = [self CBC_CreateDirectoryWithDirName:dir_tree_path];
//
//    if (create_result)
//    {
//        dir_tree_path = [self CBC_GetFilePathWithFileUnderDocumentPathArr:
//                         [[NSArray alloc] initWithObjects:
//                          dir_tree_path,
//                          CURR_RES_USERNAME,
//                          nil]];
//
//        create_result = [self CBC_CreateDirectoryWithDirName:dir_tree_path];
//
//    }
//    return create_result;
//}
//
////删除用户数据目录结构
//- (void) CBC_DeleteNotCurrentUserCacheDirectory
//{
//    NSString *path = [[Cache_BaseControl Instance] CBC_GetFilePathWithFileUnderDocumentPath:Cache_Users];
//
//    NSArray  *userArr = [FileManager  directoryContentsAtPath:path];
//
//    NSMutableArray *deleteArr = [[NSMutableArray alloc] init];
//
//    for (NSString *subStr in userArr)
//    {
//        if(![CURR_RES_USERNAME isEqualToString:subStr])
//        {
//            [deleteArr addObject:[[Cache_Users stringByAppendingString:@"/"] stringByAppendingString:subStr]];
//        }
//    }
//
//    if ([deleteArr count] > 0)
//    {
//        [self CBC_DeleteFileWithFilePathArr:deleteArr];
//    }
//
//}
//
////根据文件路径删除文件
//- (void) CBC_DeleteFileWithFilePathArr : (NSArray *) _filePathArr
//{
//    NSError *error;
//    for (int i = 0; i < [_filePathArr count]; i++)
//    {
//        NSString *filePathStr = [self CBC_GetFilePathWithFileUnderDocumentPath:(NSString *)[_filePathArr objectAtIndex:i]];
//        if([FileManager fileExistsAtPath:filePathStr])
//        {
//            if ([FileManager removeItemAtPath:filePathStr error:&error] != YES)
//            {
//                NSLog(@"Unable to delete file: %@", [error localizedDescription]);
//            }
//            else
//            {
//                NSLog(@"delete %@ success!",filePathStr);
//            }
//        }
//        else
//        {
//            NSLog(@"not exisit %@ file",filePathStr);
//        }
//    }
//}

#pragma mark cacheMethod

+ (long long) fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}

// 循环调用fileSizeAtPath来获取一个目录所占空间大小
+ (NSString*) getCacheSize{
    
    NSString *folderPath = [ZFileManagerHelper getCachePath];
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    CGFloat value = (float)folderSize;
    if ( folderSize > 1024 * 1024) {
        value = value / (1024 * 1024);
        return [NSString stringWithFormat:@"%.2f M",value];
    }
    else{
        value = value / 1024  ;
        return [NSString stringWithFormat:@"%.2f K",value];
    }
    return nil;
}

+(NSString *)getCachePath{
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];//去处需要的路径
//    NSString *path = [documentsDirectory stringByAppendingFormat:@"/Cache/"];
//    
//    if(self.hCacheDictionaryPath){
//        path = [path stringByAppendingFormat:@"%@/",self.hCacheDictionaryPath];
//    }
    return [ZFileManagerHelper dirCache];
}

+ (BOOL)cleanCache{

    [ZFileManagerHelper flushCache];
    NSString *dir = [ZFileManagerHelper getCachePath];
    NSArray *contents = [zFileManager contentsOfDirectoryAtPath:dir error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    BOOL returnFlag = false;
    while ((filename = [e nextObject])) {
       returnFlag =  [zFileManager removeItemAtPath:[dir stringByAppendingPathComponent:filename] error:NULL];
    }
    return returnFlag;
}

+ (void)flushCache
{
    [SDWebImageManager.sharedManager.imageCache clearMemory];
    [SDWebImageManager.sharedManager.imageCache clearDiskOnCompletion:nil];
}


@end
