//
//  NewPlistCache.h
//  XPH
//
//  Created by NewProject on 16/7/7.
//  Copyright © 2016年 YAY. All rights reserved.
//
// NSPlist 缓存

#import <Foundation/Foundation.h>

@interface NewPlistCache : NSObject

//保存数据到文件plist
+ (BOOL) writeAppData:(NSDictionary *)data toFile:(NSString *)fileName;

//读取数据从文件plist
+ (NSDictionary *)readAppData:(NSString *)fileName;

@end
