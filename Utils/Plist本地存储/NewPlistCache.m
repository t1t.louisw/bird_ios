//
//  NewPlistCache.m
//  XPH
//
//  Created by NewProject on 16/7/7.
//  Copyright © 2016年 YAY. All rights reserved.
//

#import "NewPlistCache.h"

@implementation NewPlistCache

+(BOOL) writeAppData:(NSDictionary *)data toFile:(NSString *)fileName
{
    // get paths from root direcory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // get documents path
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // check to see if plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
    {
        NSLog(@"Documents directory not found!");
        
        // if not in documents, get property list from main bundle
        documentsDirectory = [[NSBundle mainBundle] pathForResource:@"user" ofType:@"plist"];
    }
    
    // get the path to our plist file
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    //    NSLog(@"writeAppData appFile:%@",appFile);
    return ([data writeToFile:appFile atomically:YES]);
}


+(NSDictionary *)readAppData:(NSString *)fileName
{
    // get paths from root direcory
    NSArray *paths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    // get documents path
    NSString *documentsDirectory =[paths objectAtIndex:0];
    
    // check to see if plist exists in documents
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
    {
        NSLog(@"Documents directory not found!");
        
        // if not in documents, get property list from main bundle
        documentsDirectory = [[NSBundle mainBundle] pathForResource:@"user" ofType:@"plist"];
    }
    
    NSString *appFile =[documentsDirectory stringByAppendingPathComponent:fileName];
    
    //    NSLog(@"readAppData appFile:%@",appFile);
    
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithContentsOfFile:appFile];
    
    if(data == nil){
        data = [NSMutableDictionary dictionary];
    }
    
    //    NSLog(@"readAppData data:%@",data);
    
    return data;
}

@end
