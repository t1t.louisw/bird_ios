//
//  NewSafeObject.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewSafeObject.h"

@implementation NSArray (SafeObjectAtIndex)

- (id) safeObjectAtIndex : (NSUInteger) index
{
    if (self.count > 0 && self.count > index)
    {
        return [self objectAtIndex:index];
    }
    else
    {
        return NULL;
    }
    
    return NULL;
}

- (NSInteger)safeCount{
    if (self == nil && [self isKindOfClass:[NSNull class]]) {
        return 0;
    }
    return self.count;
}

@end

@implementation NSDictionary (SafeObjectForKey)

- (id) safeObjectForKey : (NSString *) key
{
    if (self.count > 0 && [[self allKeys] containsObject:key])
    {
        return [self objectForKey:key];
    }
    
    return nil;
}

@end

@implementation NSMutableDictionary (SafeObjectForKey)


- (void) safeSetObject:(id)value forKey:(NSString*)key
{
    if ([value isKindOfClass:[NSNull class]]) {
        value = @"";
    }
    return [self setObject:NewSafeString(value) forKey:NewSafeString(key)];
}

- (void) safeUTF8SetObject:(id)value forKey:(NSString*)key
{
    if ([value isKindOfClass:[NSNull class]]) {
        value = @"";
    }
    return [self setObject:NewSafeString([value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]) forKey:NewSafeString(key)];
}

- (void) safeJSONSetObject:(id)value forKey:(NSString*)key
{
    //成就数组转Json字符串
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:value options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    return [self setObject:NewSafeString(jsonString) forKey:NewSafeString(key)];
}

@end
