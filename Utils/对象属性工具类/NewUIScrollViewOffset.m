//
//  NewUIScrollViewOffset.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewUIScrollViewOffset.h"

@implementation UIScrollView (ScrollViewOffset)

- (CGFloat)offsetX {
    return self.contentOffset.x;
}

- (void)setOffsetX:(CGFloat)offsetX {
    self.contentOffset = CGPointMake(offsetX, self.contentOffset.y);
}

- (CGFloat)offsetY {
    return self.contentOffset.y;
}

- (void)setOffsetY:(CGFloat)offsetY {
    self.contentOffset = CGPointMake(self.contentOffset.x, offsetY);
}


- (CGFloat)contentWidth {
    return self.contentSize.width;
}

- (void)setContentWidth:(CGFloat)width {
    CGSize contentSize = self.contentSize;
    contentSize.width = width;
    self.contentSize = CGSizeMake(width, self.contentSize.height);
}

- (CGFloat)contentHeight {
    return self.contentSize.height;
}

- (void)setContentHeight:(CGFloat)height {
    CGSize contentSize = self.contentSize;
    contentSize.height = height;
    self.contentSize = CGSizeMake(self.contentSize.height, height);
}


@end
