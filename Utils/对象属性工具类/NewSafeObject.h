//
//  NewSafeObject.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (SafeObjectAtIndex)

//取数组下标值
- (id) safeObjectAtIndex : (NSUInteger) index;

//判断数组或者字典大小
- (NSInteger)safeCount;

@end

@interface NSDictionary (SafeObjectForKey)

//根据Key取Value
- (id) safeObjectForKey : (NSString *) key;

@end

@interface NSMutableDictionary (SafeObjectForKey)

//字典数据存储 判断Key Value是否为空
- (void) safeSetObject:(id)value forKey:(NSString*)key;
//字符串转UTF8编码
- (void) safeUTF8SetObject:(id)value forKey:(NSString*)key;
//数组转JSON字符串
- (void) safeJSONSetObject:(id)value forKey:(NSString*)key;

@end
