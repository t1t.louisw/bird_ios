//
//  NewUIScrollViewOffset.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIScrollView (ScrollViewOffset)


@property (nonatomic) CGFloat offsetX;

@property (nonatomic) CGFloat offsetY;

@property (nonatomic) CGFloat contentWidth;

@property (nonatomic) CGFloat contentHeight;

@end
