//
//  Comment.m
//  Field
//
//  Created by 焦钟培 on 2017/6/7.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import "Comment.h"
#import <AVFoundation/AVFoundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "AES.h"
#import <sys/utsname.h>

@implementation Comment
+ (NSString*)dictionaryToJson:(NSDictionary *)dic {
    
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}

+ (CIImage *)createQRForString:(NSString *)qrString {
    NSData *stringData = [qrString dataUsingEncoding:NSUTF8StringEncoding];
    // 创建filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // 设置内容和纠错级别
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"M" forKey:@"inputCorrectionLevel"];
    // 返回CIImage
    return qrFilter.outputImage;
}

+ (UIImage *)createNonInterpolatedUIImageFormQRForStringe:(NSString *)qrString withSize:(CGFloat) size {
    CIImage *image = [Comment createQRForString:qrString];
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    // 创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    // 保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}

+(UIImage *)addIconToQRCodeImage:(UIImage *)image withIcon:(UIImage *)icon withIconSize:(CGSize)iconSize {
    UIGraphicsBeginImageContext(image.size);
    //通过两张图片进行位置和大小的绘制，实现两张图片的合并；其实此原理做法也可以用于多张图片的合并
    
    CGFloat widthOfImage = image.size.width;
    CGFloat heightOfImage = image.size.height;
    CGFloat widthOfIcon = iconSize.width;
    CGFloat heightOfIcon = iconSize.height;
    [image drawInRect:CGRectMake(0, 0, widthOfImage, heightOfImage)];
    [icon drawInRect:CGRectMake((widthOfImage-widthOfIcon)/2, (heightOfImage-heightOfIcon)/2, widthOfIcon, heightOfIcon)];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


+(UIColor *)getColor:(NSString*)hexColor {
    unsigned int red,green,blue;
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&red];
    
    range.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&green];
    
    range.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]]scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f)alpha:1.0f];
}

+ (UIImage *)OriginImage:(UIImage *)image scaleToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);//size为CGSize类型，即你所需要的图片尺寸
    [image drawInRect:CGRectMake(0,0, size.width, size.height)];
    UIImage* scaledImage =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}


+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return theImage;
}
+ (NSData *)compressOriginalImage:(UIImage *)image toMaxDataSizeKBytes:(CGFloat)size{
    
    NSData *data=UIImageJPEGRepresentation(image, 1.0);
    NSInteger bs = data.length / (1024*1024);
    if (data.length > 1024*1024) {
        image = [Comment OriginImage:image scaleToSize:CGSizeMake(image.size.width / sqrt(bs), image.size.height / sqrt(bs))];
    }
    data=UIImageJPEGRepresentation(image, 1.0);
    if (data.length>100*1024) {
        if (data.length>1024*1024) {//1M以及以上
            data=UIImageJPEGRepresentation(image, 0.5);
        }else if (data.length>512*1024) {//0.5M-1M
            data=UIImageJPEGRepresentation(image, 0.7);
        }else if (data.length>200*1024) {//100k
            data=UIImageJPEGRepresentation(image, 0.9);
        }
    }
    return data;
}

+ (UIImage *)generateBarCode:(NSString *)code width:(CGFloat)width height:(CGFloat)height {
    // 生成二维码图片
    CIImage *barcodeImage;
    NSData *data = [code dataUsingEncoding:NSISOLatin1StringEncoding allowLossyConversion:false];
    CIFilter *filter = [CIFilter filterWithName:@"CICode128BarcodeGenerator"];
    
    [filter setValue:data forKey:@"inputMessage"];
    barcodeImage = [filter outputImage];
    
    // 消除模糊
    CGFloat scaleX = width / barcodeImage.extent.size.width; // extent 返回图片的frame
    CGFloat scaleY = height / barcodeImage.extent.size.height;
    CIImage *transformedImage = [barcodeImage imageByApplyingTransform:CGAffineTransformScale(CGAffineTransformIdentity, scaleX, scaleY)];
    
    return [UIImage imageWithCIImage:transformedImage];
}

+(NSString*)returnDataWithDictionary:(NSDictionary*)dict {
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

+(NSDictionary *)returnDictionaryWithDataPath:(NSString *)path {
    if (path == nil) {
        return nil;
    }
    NSData *jsonData = [path dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        return nil;
    }
    return dic;
}

+ (CGFloat)getLableSizeForNSString: (NSString *)str LableTextSize: (UIFont *)font isWidth: (BOOL)width wOrH: (CGFloat) wOrH{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    CGSize size;
    if (width) {
        size = [str boundingRectWithSize:CGSizeMake(MAXFLOAT, wOrH) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return size.width;
    } else {
        size = [str boundingRectWithSize:CGSizeMake(wOrH, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    }
    return size.height;
}
+ (CGFloat)getLableSizeForNSString: (NSString *)str LableTextSize: (UIFont *)font isWidth: (BOOL)width wOrH: (CGFloat) wOrH LineSpacing: (CGFloat)LineSpacing Name: (CGFloat)Name{
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setLineSpacing:LineSpacing];
    
    NSDictionary *dic = @{NSFontAttributeName:font,NSKernAttributeName:[NSNumber numberWithFloat:Name],NSParagraphStyleAttributeName:paragraph};
    
    CGSize size;
    if (width) {
        size = [str boundingRectWithSize:CGSizeMake(MAXFLOAT, wOrH) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return size.width;
    } else {
        size = [str boundingRectWithSize:CGSizeMake(wOrH, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    }
    return size.height;
}

- (UIViewController *)topViewController {
    UIViewController *resultVC;
    resultVC = [self _topViewController:[[UIApplication sharedApplication].keyWindow rootViewController]];
    while (resultVC.presentedViewController) {
        resultVC = [self _topViewController:resultVC.presentedViewController];
    }
    return resultVC;
}

- (UIViewController *)_topViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    return nil;
}

+ (UIImage *)ct_imageFromImage:(UIImage *)image inRect:(CGRect)rect isOrigin: (BOOL)isOrigin{
    
    //把像 素rect 转化为 点rect（如无转化则按原图像素取部分图片）
    CGFloat scale = [UIScreen mainScreen].scale;
    CGFloat x= rect.origin.x*scale,y=rect.origin.y*scale,w=rect.size.width*scale,h=rect.size.height*scale;
    CGRect dianRect = CGRectMake(x, y, w, h);
    
    //截取部分图片并生成新图片
    CGImageRef sourceImageRef = [image CGImage];
    if (isOrigin) {
        CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, dianRect);
        UIImage *newImage = [UIImage imageWithCGImage:newImageRef scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
        
        return newImage;
    }
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, rect);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    
    return newImage;
}

+ (UIColor *)YeJianColor:(UIColor*)YJColor YB: (UIColor *)YBColor{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"YEJIAN"]) {
        return YJColor;
    }
    return YBColor;
}

+ (UIImage *)makeImageWithView:(UIView *)view {
    CGSize s = view.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了，关键就是第三个参数。
    UIGraphicsBeginImageContextWithOptions(s, NO, [UIScreen mainScreen].scale);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (NSString *)contentTypeForImageData:(NSData *)data{
    uint8_t c;
    [data getBytes:&c length:1];
    switch (c) {
        case 0xFF:
            return @"jpeg";
        case 0x89:
            return @"png";
        case 0x47:
            return @"gif";
        case 0x49:
        case 0x4D:
            return @"tiff";
        case 0x52:
            if ([data length] < 12) {
                return nil;
            }
            NSString *testString = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(0, 12)] encoding:NSASCIIStringEncoding];
            if ([testString hasPrefix:@"RIFF"] && [testString hasSuffix:@"WEBP"]) {
                return @"webp";
            }
            return nil;
    }
    return nil;
}

+ (NSString*) base64Encode:(NSData *)data{
    static char base64EncodingTable[64] = {'A',
        'B', 'C',
        'D', 'E',
        'F', 'G',
        'H', 'I',
        'J', 'K',
        'L', 'M',
        'N', 'O',
        'P', 'Q',
        'R', 'S',
        'T', 'U',
        'V', 'W',
        'X', 'Y',
        'Z', 'a',
        'b', 'c',
        'd', 'e',
        'f', 'g',
        'h', 'i',
        'j', 'k',
        'l', 'm',
        'n', 'o',
        'p', 'q',
        'r', 's',
        't', 'u',
        'v', 'w',
        'x', 'y',
        'z', '0',
        '1', '2',
        '3', '4',
        '5', '6',
        '7', '8',
        '9', '+',
        '/'
        
    };
    NSInteger length = [data length];
    unsigned long ixtext, lentext;
    long ctremaining;
    unsigned char input[3], output[4];
    short i, charsonline =  0, ctcopy;
    const unsigned char *raw;
    NSMutableString *result;
    lentext = [data length];
    if (lentext < 1) return @"";
    result = [NSMutableString stringWithCapacity: lentext];
    raw = [data bytes];
    ixtext = 0;
    while (true) {
        ctremaining = lentext - ixtext;
        if (ctremaining <= 0) break;
        for (i = 0; i < 3; i++) {
            unsigned long ix = ixtext + i;
            if (ix < lentext) input[i] = raw[ix];
            else input[i] = 0;
        }
        output[0] = (input[0] & 0xFC) >> 2;
        output[1] = ((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4);
        output[2] = ((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6);
        output[3] = input[2] & 0x3F;
        ctcopy = 4;
        switch (ctremaining) {
            case 1:
                ctcopy = 2;
                break;
            case 2:
                ctcopy = 3;
                break;
        }
        for (i = 0; i < ctcopy; i++) [result appendString: [NSString stringWithFormat: @"%c", base64EncodingTable[output[i]]]];
        for (i = ctcopy; i < 4; i++) [result appendString: @"="];
        ixtext += 3;
        charsonline += 4;
        if ((length > 0) && (charsonline >= length)) charsonline = 0;
    }
    return result;
}



+ (NSInteger)getDifferenceByDate:(NSString *)date {
    
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString * nowStr = [dateFormatter stringFromDate:[NSDate date]];
    //获得当前时间
    NSDate *now = [dateFormatter dateFromString:nowStr ];
    
    NSDate *oldDate = [dateFormatter dateFromString:date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned int unitFlags = NSDayCalendarUnit;
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:now  toDate:oldDate  options:0];
    if ([comps day] < 0) {
        NSString *newFDate = [dateFormatter stringFromDate:now];
        NSArray *oldDateArray = [date componentsSeparatedByString:@"-"];
        NSString *year = [NSString stringWithFormat:@"%ld",[[[newFDate componentsSeparatedByString:@"-"] firstObject] integerValue]];
        date = [NSString stringWithFormat:@"%@-%@-%@",year,[oldDateArray objectAtIndex:1],[oldDateArray objectAtIndex:2]];
        NSDate *newoldDate = [dateFormatter dateFromString:date];
        
        NSDateComponents *newFcomps = [gregorian components:unitFlags fromDate:now  toDate:newoldDate  options:0];
        if ([newFcomps day] < 0) {
            NSString *newSDate = [dateFormatter stringFromDate:now];
            NSArray *oldDateArray = [date componentsSeparatedByString:@"-"];
            NSString *year = [NSString stringWithFormat:@"%ld",[[[newSDate componentsSeparatedByString:@"-"] firstObject] integerValue] + 1];
            date = [NSString stringWithFormat:@"%@-%@-%@",year,[oldDateArray objectAtIndex:1],[oldDateArray objectAtIndex:2]];
            NSDate *newSoldDate = [dateFormatter dateFromString:date];
            NSDateComponents *newScomps = [gregorian components:unitFlags fromDate:now  toDate:newSoldDate  options:0];
            return [newScomps day];
        }
        return [newFcomps day];
    }
    return [comps day];
}


/*
 系统自带弹窗
*/
+ (void)setupAlertController: (NSString *)message YesStr: (NSString *)yesStr noStr: (NSString *)noStr yes:(void (^)(UIAlertAction * _Nonnull action))yes {
    if ([message isEqualToString:@"error"]) {
        message = @"操作失败";
    }
    if ([message isEqualToString:@"success"]) {
        message = @"操作成功";
        
    }
    NSString *cancelStr = noStr;
    if (noStr.length == 0) {
        cancelStr = @"取消";
    }
    NSString *confirmStr = yesStr;
    if (confirmStr.length == 0) {
        confirmStr = @"确定";
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:cancelStr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    [actionCancel setValue:[UIColor redColor] forKey:@"titleTextColor"];
    [alert addAction:actionCancel];
    
    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:confirmStr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        yes(action);
    }];
    [alert addAction:actionConfirm];
    
    
    UIViewController *dev = [[[Comment alloc] init] topViewController];
    [dev presentViewController:alert animated:YES completion:nil];
}





+ (BOOL) validateMobile:(NSString *)mobile {
    //手机号以13， 15，18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^(13|15|14|16|18|19|17)\\d{9}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}
+ (BOOL) validatePassword:(NSString *)password {
    NSString *passWordRegex = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passwordTest evaluateWithObject:password];
}

+ (BOOL)IDCard:(NSString *)number {
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *passWordRegex = @"^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passwordTest evaluateWithObject:number];
}
+(NSDictionary *)getAttr:(NSUInteger )textSize textAlign:(NSUInteger )align textColor:(UIColor *)color type: (NSInteger)type Spacing:(NSInteger)Spacing{
    
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.alignment = align;
    
    [paragraph setLineSpacing:Spacing];
    
    NSMutableDictionary *attr = [NSMutableDictionary dictionary];
    [attr setValuesForKeysWithDictionary:@{NSFontAttributeName:[UIFont systemFontOfSize:textSize],/*NSKernAttributeName:@1,*/ NSForegroundColorAttributeName:color, NSParagraphStyleAttributeName:paragraph}];
    
    if (type != 0) {
        [attr setObject:[NSNumber numberWithFloat:type] forKey:NSStrokeWidthAttributeName];
    }
    
    
    return attr;
    
}


+ (BOOL)getIsIphone {
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"]) {
        //iPhone
        return YES;
    }
    else if([deviceType isEqualToString:@"iPod touch"]) {
        //iPod Touch
        return NO;
    }
    else if([deviceType isEqualToString:@"iPad"]) {
        //iPad
        return NO;
    }
    return NO;
}
+ (NSString *)iphoneType {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    //simulator
    if ([platform isEqualToString:@"i386"])          return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])        return @"Simulator";
    //iPhone
    if ([platform isEqualToString:@"iPhone1,1"])     return @"IPhone_1G";
    if ([platform isEqualToString:@"iPhone1,2"])     return @"IPhone_3G";
    if ([platform isEqualToString:@"iPhone2,1"])     return @"IPhone_3GS";
    if ([platform isEqualToString:@"iPhone3,1"])     return @"IPhone_4";
    if ([platform isEqualToString:@"iPhone3,2"])     return @"IPhone_4";
    if ([platform isEqualToString:@"iPhone4,1"])     return @"IPhone_4s";
    if ([platform isEqualToString:@"iPhone5,1"])     return @"IPhone_5";
    if ([platform isEqualToString:@"iPhone5,2"])     return @"IPhone_5";
    if ([platform isEqualToString:@"iPhone5,3"])     return @"IPhone_5C";
    if ([platform isEqualToString:@"iPhone5,4"])     return @"IPhone_5C";
    if ([platform isEqualToString:@"iPhone6,1"])     return @"IPhone_5S";
    if ([platform isEqualToString:@"iPhone6,2"])     return @"IPhone_5S";
    if ([platform isEqualToString:@"iPhone7,1"])     return @"IPhone_6P";
    if ([platform isEqualToString:@"iPhone7,2"])     return @"IPhone_6";
    if ([platform isEqualToString:@"iPhone8,1"])     return @"IPhone_6s";
    if ([platform isEqualToString:@"iPhone8,2"])     return @"IPhone_6s_P";
    if ([platform isEqualToString:@"iPhone8,4"])     return @"IPhone_SE";
    if ([platform isEqualToString:@"iPhone9,1"])     return @"IPhone_7";
    if ([platform isEqualToString:@"iPhone9,3"])     return @"IPhone_7";
    if ([platform isEqualToString:@"iPhone9,2"])     return @"IPhone_7P";
    if ([platform isEqualToString:@"iPhone9,4"])     return @"IPhone_7P";
    if ([platform isEqualToString:@"iPhone10,1"])    return @"IPhone_8";
    if ([platform isEqualToString:@"iPhone10,4"])    return @"IPhone_8";
    if ([platform isEqualToString:@"iPhone10,2"])    return @"IPhone_8P";
    if ([platform isEqualToString:@"iPhone10,5"])    return @"IPhone_8P";
    if ([platform isEqualToString:@"iPhone10,3"])    return @"IPhone_X";
    if ([platform isEqualToString:@"iPhone10,6"])    return @"IPhone_X";
    if ([Comment getIsIphone]) return @"IPhone_X";
    return @"NUKown";
}
+ (CGFloat)navHeader{
    
    if ([[Comment iphoneType] isEqualToString:@"IPhone_X"]) {
        return 88;
    }
    return 64;
}
+ (CGFloat)navTABBAR_HEIGHT{
    
    if ([[Comment iphoneType] isEqualToString:@"IPhone_X"]) {
        return 83;
    }
    return 49;
}

+ (NSUInteger)durationWithVideo:(NSURL *)videoUrl{
    
    NSDictionary *opts = [NSDictionary dictionaryWithObject:@(NO) forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:videoUrl options:opts]; // 初始化视频媒体文件
    NSUInteger second = 0;
    second = urlAsset.duration.value / urlAsset.duration.timescale; // 获取视频总时长,单位秒
    return second;
}


+ (NSString *)encode64:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
//    NSData *base64Data = [data base64EncodedDataWithOptions:0];
    
//    NSString *baseString = [[NSString alloc]initWithData:base64Data encoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
//    return baseString;
}

+(NSString *)getNowTimeTimestamp{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss SSS"];
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]*1000];
    return timeSp;
}


+ (NSString *) md5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(cStr, (unsigned int)strlen(cStr), md5Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH *2];
    for (int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x",md5Buffer[i]];
    }
    
    return output;

}



- (NSString *)getCode {
    NSInteger y = arc4random() % 9999;
    NSString *code = [NSString stringWithFormat:@"%ld",y];
    if (code.length == 1) {
        code = [NSString stringWithFormat:@"000%@",code];
    }
    if (code.length == 2) {
        code = [NSString stringWithFormat:@"00%@",code];
    }
    if (code.length == 3) {
        code = [NSString stringWithFormat:@"0%@",code];
    }
    return code;
}
/*转化日期*/
+(NSString *)shijian:(NSString *)time{
    // timeStampString 是服务器返回的13位时间戳
    NSString *timeStampString  = time;
    
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[timeStampString doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
}
+ (CGFloat) heightforString:(NSString *)value andWidth:(CGFloat)width fontSize:(CGFloat)fontSize{
    return [value boundingRectWithSize:CGSizeMake(width, 1000000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName] context:nil].size.height;
    
}
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height fontSize:(CGFloat)fontSize{
    return [value boundingRectWithSize:CGSizeMake(100000, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName] context:nil].size.width;
}

/*根据Key值来判断逻辑*/
+(NSString *)userDefaultsStringKey:(NSString *)key
{
    NSString *defaults = [[NSUserDefaults standardUserDefaults] valueForKey:key];
    return defaults;
}
/*存储判断NSString的条件*/
+(void)userDefaultsStringKey:(NSString *)key Value:(NSString *)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:value forKey:key];
    [defaults synchronize];
}
@end
