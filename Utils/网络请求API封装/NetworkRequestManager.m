//
//  NetworkRequestManager.m
//  MingMen
//
//  Created by NewProject on 2017/3/13.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NetworkRequestManager.h"
#include <netinet/in.h>
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "NewInterfaceReplacement.h"

@implementation NetworkRequestManager

static BOOL isNetwork;

#pragma mark - 有无网络
+(BOOL) connectedToNetwork
{
    
    return isNetwork;
    
    /* 感觉sockaddr_in 不兼容IPV6 也不会写sockaddr_in6
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    
    if (!didRetrieveFlags)
    {
        printf("Error. Could not recover network reachability flags\n");
        return NO;
    }
    
    BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
    BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
    return (isReachable && !needsConnection) ? YES : NO;
    */
}

#pragma mark - 观察网络状态
+ (void)observeNetworkStatus
{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown:
                isNetwork = YES;
                NSLog(@"未知网络");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                isNetwork = NO;
                NSLog(@"无网络");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                isNetwork = YES;
                NSLog(@"正在使用手机流量");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                isNetwork = YES;
                NSLog(@"正在使用手机WIFI");
                break;
        }
    }];
    
    [manager startMonitoring];

}



#pragma mark - GET请求
+ (void)requestGetWithInterfacePrefix:(NSString*)prefix parameters:(id)parameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed
{
    NSString *serverUrl;
//    if ([prefix containsString:@"fhi/api/sms/mobile/sendvalidatecode"] || [prefix containsString:@"fhi/api/sms/mobile/validatecode"]) {
//        serverUrl = [NSString stringWithFormat:@"%@%@",DX_URL,prefix];
//    }else{
        serverUrl = [NSString stringWithFormat:@"%@%@",SERVER_URL,prefix];
//    }
    
    NSString *httpHeaderKey = @"";
    NSString *httpHeaderValue = @"";
    
//    httpHeaderKey = @"Cookie";
//    httpHeaderValue = [NewUtils userDefaultsStringKey:NewToken];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:serverUrl parameters:parameters error:nil];
    request.timeoutInterval = 60.f;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:[NSString stringWithFormat:@"JSESSIONID=%@",httpHeaderValue] forHTTPHeaderField:httpHeaderKey];

    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager:httpHeaderValue httpHeaderKey:httpHeaderKey];
    [manager GET:serverUrl parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        //这里面是进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
//        if ([NewLogSwitch isEqualToString:@"1"] && [SERVER_URL isEqualToString:Log_SERVER_URL]) {
//            NSLog(@"GET接口链接: %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"NewNetworkRequestLink"]);
//        }
        
        if (succeed) {
            if (responseObject) {
//                if ([responseObject[@"error"] intValue] == 10020) {
//                   [[AppDelegate shareDelegate] forceLogoutLogin];
//                    [[NewInterfaceReplacement sharedInstance] replacementLogin];
//                }else{
//                }
                succeed(responseObject);

            }else{
                if (failed) {
                    failed();
                }
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failed) {
            failed();
        }
        
    }];
    
}


#pragma mark - Post请求
+ (void)requestPostWithInterfacePrefix:(NSString*)prefix parameters:(id)parameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed
{
    NSString *serverUrl;

    serverUrl = [NSString stringWithFormat:@"%@%@",SERVER_URL,prefix];
    
    NSString *httpHeaderKey = nil;
    NSString *httpHeaderValue = nil;
    
    httpHeaderKey = @"";
    httpHeaderValue = @"";
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:serverUrl parameters:parameters error:nil];
    request.timeoutInterval = 60.f;

        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];


    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager:httpHeaderValue httpHeaderKey:httpHeaderKey];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

        
    [manager POST:serverUrl parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        //这里面是进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
//        if ([NewLogSwitch isEqualToString:@"1"] && [SERVER_URL isEqualToString:Log_SERVER_URL]) {
//            NSLog(@"POST接口链接: %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"NewNetworkRequestLink"]);
//        }
        if (succeed) {
            if (responseObject) {
                if ([responseObject[@"code"] integerValue] == 161) {
                    [[AppDelegate shareDelegate] forceLogoutLogin];
                }else{
                    succeed(responseObject);
                }
            }else{
                if (failed) {
                    failed();
                }
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failed) {
            failed();
        }
    }];
}


#pragma mark - Post请求 Body上传参数
+ (void)requestPostWithInterfacePrefix:(NSString*)prefix parameters:(id)parameters bodyParameters:(id)bodyParameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed
{
    NSString *serverUrl;
        if ([prefix isEqualToString:@"https://api.onepay.solutions/payment/v3/checkOut.html"]) {
            serverUrl = prefix;
        }else{
            serverUrl = [NSString stringWithFormat:@"%@%@",SERVER_URL,prefix];
        }
//    NSString *serverUrl = [NSString stringWithFormat:@"%@%@",SERVER_URL,prefix];
    NSString *httpHeaderKey = @"";
    NSString *httpHeaderValue = @"";
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:serverUrl parameters:parameters error:nil];
    request.timeoutInterval = 60.f;
    [request setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:[NSString stringWithFormat:@"JSESSIONID=%@",httpHeaderValue] forHTTPHeaderField:httpHeaderKey];

    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:bodyParameters options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSData* bodyData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    [request setHTTPBody:bodyData];
    
//    AFHTTPSessionManager *manager = [self createAFHTTPSessionManager:httpHeaderValue httpHeaderKey:httpHeaderKey];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //设置请求的超时时间
         manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];

        [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
    
        //        if ([NewLogSwitch isEqualToString:@"1"] && [SERVER_URL isEqualToString:Log_SERVER_URL]) {
                    NSLog(@"POST接口链接: %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"NewNetworkRequestLink"]);
        //        }
            NSLog(@"");
                if (succeed) {
                    if (responseObject) {
                        succeed(responseObject);
                    }else{
                        if (failed) {
                            failed();
                        }
                    }
                }
            }] resume];

}


#pragma mark - Post请求 上传图片
+ (void)requestPostWithInterfacePrefix:(NSString*)prefix serverFileName:(NSString *)serverFileName parameters:(id)parameters arrayParameters:(id)arrayParameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed
{
    
    NSString *serverUrl = [NSString stringWithFormat:@"%@%@",SERVER_URL,prefix];
    //if ([arrayParameters isEqual:[NSArray class]]) {}
    NSArray *array = [NSArray arrayWithArray:arrayParameters];
    
    NSString *httpHeaderKey = nil;
    NSString *httpHeaderValue = nil;
    
    httpHeaderKey = @"Authorization";
    httpHeaderValue = @"";
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //设置请求的超时时间
    manager.requestSerializer.timeoutInterval = 60.f;
    //设置请求参数的类型:HTTP (AFJSONRequestSerializer,AFHTTPRequestSerializer)
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //设置服务器返回结果的类型:JSON (AFJSONResponseSerializer,AFHTTPResponseSerializer)
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //NSURLRequest添加请求头
    [manager.requestSerializer setValue:httpHeaderValue forHTTPHeaderField:httpHeaderKey];
    //设置ContentType  
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:
                                                         @"text/plain",
                                                         @"multipart/form-data",
                                                         @"application/json",
                                                         @"text/html",
                                                         @"image/jpeg",
                                                         @"image/png",
                                                         @"application/octet-stream",
                                                         @"text/json",@"text/javascript" ,nil];

//    self.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil nil];
//    1 self.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", nil];


    // 在parameters里存放照片以外的对象
    [manager POST:serverUrl parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
       
        // formData: 专门用于拼接需要上传的数据,在此位置生成一个要上传的数据体
        // 这里的_photoArr是你存放图片的数组
        for (int i = 0; i < array.count; i++) {

            UIImage *image = array[i];
            NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
          
            // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
            // 要解决此问题，
            // 可以在上传时使用当前的系统事件作为文件名
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            // 设置时间格式
            [formatter setDateFormat:@"yyyyMMddHHmmss"];
            NSString *dateString = [formatter stringFromDate:[NSDate date]];
            //拼接图片名称 当前时间+4位随机数
            NSString *fileName = [NSString  stringWithFormat:@"%@_%@", dateString,[NewUtils randomCount:4]];
            
            //该方法的参数
            //1. appendPartWithFileData：要上传的照片[二进制流]
            //2. name：对应网站上[upload.php中]处理文件的字段（比如upload）
            //3. fileName：要保存在服务器上的文件名
            //4. mimeType：上传的文件的类型
            [formData appendPartWithFileData:imageData name:serverFileName fileName:fileName mimeType:@"image/jpeg"]; //
        }
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        NSLog(@"```上传进度```%f",uploadProgress.fractionCompleted);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"```上传成功```");
        if (succeed) {
            if (responseObject) {
                
                succeed(responseObject);
                
                if ([responseObject[@"error"] isEqualToString:@"000"]) {
                    [[AppDelegate shareDelegate] forceLogoutLogin];
                }
            }else{
                if (failed) {
                    failed();
                }
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"```上传失败```");
        if (failed) {
            failed();
        }
        
    }];
}


#pragma mark - 设置AFHTTPSessionManager相关属性

+ (AFHTTPSessionManager *)createAFHTTPSessionManager:(NSString *)httpHeaderValue httpHeaderKey:(NSString *)httpHeaderKey
{
    //打开状态栏的等待菊花
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //设置请求的超时时间
    manager.requestSerializer.timeoutInterval = 30.f;
    //设置请求参数的类型:HTTP (AFJSONRequestSerializer,AFHTTPRequestSerializer)
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //设置服务器返回结果的类型:JSON (AFJSONResponseSerializer,AFHTTPResponseSerializer)
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    

    
    //NSURLRequest添加请求头
//    if (httpHeaderValue != nil) {
//
//    }
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"JSESSIONID=%@",httpHeaderValue] forHTTPHeaderField:httpHeaderKey];


    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                              @"text/html",
                                                                              @"text/json",
                                                                              @"text/plain",
                                                                              @"text/javascript",
                                                                              @"text/xml",
                                                                              @"image/*"]];
    return manager;
}

#pragma mark - 专用存取款银行请求
+ (void)requestPostWithInterfacePrefixNew:(NSString*)prefix parameters:(id)parameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed
{
    NSString *serverUrl;

    serverUrl = [NSString stringWithFormat:@"%@%@",SERVER_URL,prefix];
    
    NSString *httpHeaderKey = nil;
    NSString *httpHeaderValue = nil;
    
    httpHeaderKey = @"";
    httpHeaderValue = @"";
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:serverUrl parameters:parameters error:nil];
    request.timeoutInterval = 60.f;

        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];


        AFHTTPSessionManager *manager =  [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        manager.requestSerializer=[AFHTTPRequestSerializer serializer];

        
    [manager POST:serverUrl parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        //这里面是进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
//        if ([NewLogSwitch isEqualToString:@"1"] && [SERVER_URL isEqualToString:Log_SERVER_URL]) {
//            NSLog(@"POST接口链接: %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"NewNetworkRequestLink"]);
//        }
//        if (succeed) {
//            if (responseObject) {
//                if ([responseObject[@"code"] integerValue] == 161) {
//                    [[AppDelegate shareDelegate] forceLogoutLogin];
//                }else{
//                }
//            }else{
//                if (failed) {
//                    failed();
//                }
//            }
//        }
        succeed(responseObject);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failed) {
            failed();
        }
    }];
}


@end
