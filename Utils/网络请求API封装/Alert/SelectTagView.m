//
//  SelectTagView.m
//  iphone
//
//  Created by 焦钟培 on 2017/12/29.
//  Copyright © 2017年 Jiluai. All rights reserved.
//

#import "SelectTagView.h"

@implementation SelectTagView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init {
    self = [super init];
    if (self) {
        [self awakeFromNib];
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
}
- (IBAction)selectButtonAction:(id)sender {
    _selectStatus.selected = YES;
    self.block(self.tag);
}

@end
