//
//  GetTagView.m
//  iphone
//
//  Created by 焦钟培 on 2017/12/29.
//  Copyright © 2017年 Jiluai. All rights reserved.
//

#import "GetTagView.h"
//#import "Comment.h"
#import "SelectTagView.h"
#define MainWidth [UIScreen mainScreen].bounds.size.width
#define MainHeight [UIScreen mainScreen].bounds.size.height
@interface GetTagView ()
@property (strong, nonatomic) UIButton *backView;
@property (strong, nonatomic) UIView *tagView;
@property (strong, nonatomic) NSArray *tagArray;

@end
@implementation GetTagView

- (instancetype)initWithArray: (NSArray *)tagArray {
    self = [super init];
    if (self) {
        _tagArray = [NSArray arrayWithArray:tagArray];
        self.frame = CGRectMake(0, 0, MainWidth, MainHeight);
        self.backView = [[UIButton alloc] initWithFrame:self.frame];
        self.backView.backgroundColor = [UIColor blackColor];
        self.backView.alpha = 0.3;
        [self.backView addTarget:self action:@selector(viewClose) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.backView];
        
        
        
        self.tagView = [[UIScrollView alloc] initWithFrame:CGRectMake(30, 0, MainWidth - 60, MainWidth - 20)];
        self.tagView.center = CGPointMake(MainWidth / 2, MainHeight / 2);
        self.tagView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.tagView];
        
        UILabel *mess = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, MainWidth - 80, 40)];
        mess.text = @"  请选择提问类别  ";
        mess.backgroundColor = [UIColor whiteColor];
        mess.textAlignment = 1;
        [self.tagView addSubview:mess];
        [self creatscrollTagView];
    }
    return self;
}
- (void)creatscrollTagView {
    UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(MainWidth - 60, self.tagView.frame.origin.y - 40, 40, 40)];
    [closeBtn setImage:[UIImage imageNamed:@"删除"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(viewClose) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:closeBtn];
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 40, MainWidth - 60, MainWidth - 60)];
    scroll.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.tagView addSubview:scroll];
    
    for (int i = 0; i < _tagArray.count; i++) {
        NSDictionary *dic = [_tagArray objectAtIndex:i];
        SelectTagView * select = [[[NSBundle mainBundle]loadNibNamed:@"SelectTagView" owner:self options:nil]lastObject];
        select.tag = i + 1;
        select.block = ^(NSInteger index) {
            self.block(index);
            [self viewClose];
        };
        select.tagStr.text = [dic objectForKey:@"title"];
        select.frame =CGRectMake(10, i * 40, MainWidth - 80, 40);
        [scroll addSubview:select];
    }
    scroll.contentSize = CGSizeMake(0, 40 * _tagArray.count);
}
- (void)viewClose {
    [self removeFromSuperview];
}
- (void)viewShow {
    UIWindow *window = [[[UIApplication sharedApplication] windows] firstObject];
    [window addSubview:self];
}

@end
