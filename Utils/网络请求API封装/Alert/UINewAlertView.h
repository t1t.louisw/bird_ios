//
//  UINewAlertView.h
//  Field
//
//  Created by 焦钟培 on 2017/6/5.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINewAlertView : UIView

typedef NS_ENUM(NSUInteger,Showtype){
    ShowUp = 0,
    ShowDown = 1,
};

typedef void(^UINewAlertViewBlock)(UIButton *button);

@property (nonatomic, copy)UINewAlertViewBlock block;

@property (assign, nonatomic)Showtype showType;

@property (assign, nonatomic) BOOL autoClose;

@property (nonatomic, strong)UIColor *backViewColor;

- (instancetype)initHidenNvaiWithMessage: (NSString *)message;

- (instancetype)initNvaiWithMessage: (NSString *)message;

- (void)viewClose: (UIView *)subView;

- (void)viewShow: (UIView *)subview;
@end
