//
//  GetTagView.h
//  iphone
//
//  Created by 焦钟培 on 2017/12/29.
//  Copyright © 2017年 Jiluai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetTagView : UIView
typedef void(^GetTagViewBlock)(NSInteger index);

@property (nonatomic, copy)GetTagViewBlock block;
- (instancetype)initWithArray: (NSArray *)tagArray;
- (void)viewClose;
- (void)viewShow;
@end
