//
//  UINewAlertView.m
//  Field
//
//  Created by 焦钟培 on 2017/6/5.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import "UINewAlertView.h"
//#import "Comment.h"
@interface UINewAlertView ()
@property (assign) CGFloat originY;
@property (strong, nonatomic)UILabel *messageLable;
@end
@implementation UINewAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initNvaiWithMessage: (NSString *)message{
    self = [super init];
    if (self) {
        _autoClose = YES;
        if ([self getLableSizeForNSString:message LableTextSize:[UIFont systemFontOfSize:17] isWidth:NO wOrH:SCREEN_WIDTH - 30] > 10.0) {
            self.messageLable = [[UILabel alloc] initWithFrame:CGRectMake(15, 12, SCREEN_WIDTH - 30, [self getLableSizeForNSString:message LableTextSize:[UIFont systemFontOfSize:17] isWidth:NO wOrH:SCREEN_WIDTH])];
            _messageLable.numberOfLines = 0;
            _messageLable.textAlignment = 1;
            _messageLable.font = [UIFont systemFontOfSize:17];
            _messageLable.text = message;
            _messageLable.textColor = [UIColor blackColor];
            [self addSubview:_messageLable];
            _originY = NavHeader;
            self.frame = CGRectMake(0, _originY, SCREEN_WIDTH, [self getLableSizeForNSString:message LableTextSize:[UIFont systemFontOfSize:17] isWidth:NO wOrH:SCREEN_WIDTH - 30] + 24);
        }
        
        
    }
    return self;
}
- (instancetype)initHidenNvaiWithMessage: (NSString *)message{
    self = [super init];
    if (self) {
        _autoClose = YES;
        if ([self getLableSizeForNSString:message LableTextSize:[UIFont systemFontOfSize:17] isWidth:NO wOrH:SCREEN_WIDTH - 30] > 10.0) {
            _originY = 0;
            self.messageLable = [[UILabel alloc] initWithFrame:CGRectMake(15, 32 + NavHeader - 64, SCREEN_WIDTH - 30, [self getLableSizeForNSString:message LableTextSize:[UIFont systemFontOfSize:17] isWidth:NO wOrH:SCREEN_WIDTH - 30])];
            _messageLable.font = [UIFont systemFontOfSize:17];
            _messageLable.text = message;
            _messageLable.numberOfLines = 0;
            _messageLable.textAlignment = 1;
            _messageLable.textColor = [UIColor blackColor];
            [self addSubview:_messageLable];
            self.frame = CGRectMake(0, 0, SCREEN_WIDTH, [self getLableSizeForNSString:message LableTextSize:[UIFont systemFontOfSize:17] isWidth:NO wOrH:SCREEN_WIDTH - 30] + 44 +  NavHeader - 64);
        }
        
        
    }
    return self;
}


- (void)setAutoClose:(BOOL)autoClose {
    _autoClose = autoClose;
}


- (void)setShowType:(Showtype)showType {
    switch (showType) {
        case ShowUp:
            self.frame = CGRectMake(0, _originY, SCREEN_WIDTH, _messageLable.frame.size.height + _messageLable.frame.origin.y + 12);
            break;
        case ShowDown:
            self.frame = CGRectMake(0, SCREEN_HEIGHT - _messageLable.frame.size.height, SCREEN_WIDTH, _messageLable.frame.size.height + 24);
            break;
        default:
            break;
    }
}

- (void)viewShow: (UIView *)subview {
    [self addAlert:subview];
    if (_autoClose) {
        [self performSelector:@selector(viewClose:) withObject:subview afterDelay:1.0];
    }
}
- (void)addAlert: (UIView *)subView {
    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
    [window addSubview:self];
}

- (void)viewClose: (UIView *)subView {
    if ([subView isKindOfClass:[UIScrollView class]]) {
//        ((UIScrollView *)subView).contentOffset = CGPointMake(0, 0);
    }
    [self removeFromSuperview];
}

- (void)setBackViewColor:(UIColor *)backViewColor {
  
//    if ([backViewColor isEqual:GreenAlertColor]) {
//        self.messageLable.textColor = [UIColor blackColor];
//    } else {
        self.messageLable.textColor = [UIColor whiteColor];
//    }
    NSMutableAttributedString *text = [self getMessageForColor:backViewColor];
    _messageLable.attributedText = text;
    _backViewColor = backViewColor;
    self.backgroundColor = backViewColor;
}
- (NSMutableAttributedString *)getMessageForColor: (UIColor *)backViewColor {
    NSMutableAttributedString *attri =     [[NSMutableAttributedString alloc] init];
    NSMutableAttributedString *text =     [[NSMutableAttributedString alloc] initWithString:_messageLable.text];
    UIColor *textColor;
//    if ([backViewColor isEqual:GreenAlertColor]) {
//        textColor = [UIColor blackColor];
//    } else {
        textColor = [UIColor whiteColor];
//    }
    [text addAttribute:NSForegroundColorAttributeName value:textColor range:NSMakeRange(0, _messageLable.text.length)];
    [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(0, _messageLable.text.length)];
    
//    NSTextAttachment *attch = [[NSTextAttachment alloc] init];
//    if ([backViewColor isEqual:GreenAlertColor] || [backViewColor isEqual:RedAlertColor]) {
//        attch.image = [UIImage imageNamed:@"alert_erre.png"];
//    } else {
//        attch.image = [UIImage imageNamed:@"alert_success.png"];
//    }
//
//    attch.bounds = CGRectMake(0, -4, 20, 20);
//    NSAttributedString *string = [NSAttributedString attributedStringWithAttachment:attch];
//    [attri appendAttributedString:string];
    [attri appendAttributedString:text];
    return attri;
}

- (CGFloat)getLableSizeForNSString: (NSString *)str LableTextSize: (UIFont *)font isWidth: (BOOL)width wOrH: (CGFloat) wOrH{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    CGSize size;
    if (width) {
        size = [str boundingRectWithSize:CGSizeMake(MAXFLOAT, wOrH) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return size.width;
    } else {
        size = [str boundingRectWithSize:CGSizeMake(wOrH, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    }
    return size.height;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    self.block(nil);
}

@end
