//
//  SelectTagView.h
//  iphone
//
//  Created by 焦钟培 on 2017/12/29.
//  Copyright © 2017年 Jiluai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectTagView : UIView
typedef void(^SelectTagViewBlock)(NSInteger index);

@property (nonatomic, copy)SelectTagViewBlock block;
@property (strong, nonatomic) IBOutlet UILabel *tagStr;
@property (strong, nonatomic) IBOutlet UIButton *selectStatus;
@end
