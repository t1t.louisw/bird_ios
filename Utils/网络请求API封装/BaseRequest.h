//
//  BaseRequest.h
//  Field
//
//  Created by 焦钟培 on 2017/6/14.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "WLURL.h"
#import "MJRefresh.h"
//#import "MJExtension.h"
#import "SVProgressHUD.h"
#import "AFNetworking.h"
#import "AFNetworkReachabilityManager.h"
//#import "MJExtension.h"

/**
 *  请求的类型
 */
typedef NS_ENUM(NSInteger,requestType) {
    /**
     *  get请求（默认）
     */
    requestGet,
    /**
     *  post请求
     */
    requestPost,
    /**
     *  put请求
     */
    requestPATCH,
    /**
     *  PATCH请求
     */
    requestPUT,
    /**
     *  delete请求
     */
    requestDELETE
};

@interface BaseRequest : NSObject

@property (strong, nonatomic) AFHTTPSessionManager *manager;
/**
 *  关闭弹窗
 */
@property (nonatomic) BOOL isCloesTC;
/**
 *  请求的url
 */
@property (strong, nonatomic) NSString *requestUrl;
/**
 *  请求类型，get post
 */
@property (assign, nonatomic) requestType requestType;

/**
 *  关闭加载
 */
@property (nonatomic) BOOL isCloesHud;

/**
 *  关闭判断
 */
@property (nonatomic) BOOL isCloesGetToken;

/**
 请求(带字典参数)
 
 @param parameters 字典参数
 @param success    成功
 @param failure    失败
 */

- (void)toRequestparameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;

//上传图片
- (void)uploadRequestparameters:(NSDictionary *)parameters fileName:(NSString *)fileName images:(NSArray *)images success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure;


@end
