//
//  BaseRequest.m
//  Field
//
//  Created by 焦钟培 on 2017/6/14.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import "BaseRequest.h"
#import "Comment.h"
#import "UINewAlertView.h"
#import "AES.h"
//显示
#define kShowHUD(view) [MBProgressHUD showHUDAddedTo:view animated:YES]
//隐藏
#define kHideHUD(view) [MBProgressHUD hideHUDForView:view animated:YES]

#define kTimeOutInterval 20

typedef enum {
    NotReachable = 0,  //无连接
    ReachableViaWiFi,  //使用3G/GPRS网络
    ReachableViaWWAN  //使用WiFi网络
} NetworkStatus;

@implementation BaseRequest

- (instancetype)init
{
    self = [super init];
    if (self) {
        _requestType = requestGet;
        _isCloesTC = NO;
        [self getManager];
    }
    return self;
}
/**
 请求(带参数字典)
 
 @param parameters 参数字典
 @param success    成功
 @param failure    失败
 */

- (void)toRequestparameters:(NSDictionary *)parameters success:(void (^)(id))success failure:(void (^)(NSError *))failure{
//    self.requestUrl = [self.requestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    __block   BOOL network =  YES;
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
         switch (status) {
             case AFNetworkReachabilityStatusNotReachable: {
                 network = NO;
                 break;
             }
             default:
                 break;
         }
     }];
    
    
    if (_requestType == requestGet) {//默认get请求
        
        [self get:_requestUrl parameters:parameters success:^(id responseObject) {
              if (success) {
                  success(responseObject);
              }
          } failure:^(NSError *error) {
              if (failure) {
                  failure(error);
              }
          }];
        
    } else if (_requestType == requestPost) { //post
        [self post:_requestUrl parameters:parameters success:^(id responseObject) {
               if (success) {
                   success(responseObject);
               }
           } failure:^(NSError *error) {
               if (failure) {
                   failure(error);
               }
           }];
    } else if (_requestType == requestPUT) { //put
        [self put:_requestUrl parameters:parameters success:^(id responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    } else if (_requestType == requestDELETE) {
        [self delete:_requestUrl parameters:parameters success:^(id responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    } else {
        [self PATCH:_requestUrl parameters:parameters success:^(id responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    }
}

- (void)gotoLoginVC {
    
}

/**
 上传图片
 
 @param parameters 参数
 @param images     图片集合UIImage
 @param success    成功
 @param failure    失败
 */
- (void)uploadRequestparameters:(NSDictionary *)parameters fileName:(NSString *)fileName images:(NSArray *)images success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    if (!_isCloesHud) {
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    }

    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    [securityPolicy setAllowInvalidCertificates:YES];
    [securityPolicy setValidatesDomainName:NO];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // 超时时间
    [manager setSecurityPolicy:securityPolicy];
    manager.requestSerializer.timeoutInterval = 180;
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserToken"];
    
    [manager.requestSerializer setValue:@"v1" forHTTPHeaderField:@"version"];
    
    
    if (token.length > 0) {
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", token] forHTTPHeaderField:@"Authorization"];
        
    }
    
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"multipart/form-data",@"application/x-www-form-urlencoded",@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    [manager POST:_requestUrl parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSInteger imgCount = 0;
        for (NSData *imageData in images) {
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            
            formatter.dateFormat = @"yyyy-MM-ddHH:mm:ss:SSS";
            
            NSString *fileName = [NSString stringWithFormat:@"%@%@.png",[formatter stringFromDate:[NSDate date]],@(imgCount)];
            
            NSString *photoname = [BaseRequest defaultPhotoName:fileName];
            [imageData writeToFile:photoname atomically: NO];
            NSURL *filePath = [NSURL fileURLWithPath:photoname];
            [formData appendPartWithFileURL:filePath name:fileName error:nil];
        }
        imgCount++;
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        CGFloat progres = (CGFloat)uploadProgress.completedUnitCount / (CGFloat)uploadProgress.totalUnitCount;//这里是返回的上传图片进度,一定要用CGFloat进行接收
        if (progres) {
            
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];

        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
      
        [SVProgressHUD dismiss];

        if (failure && !self->_isCloesTC) {
            if (error.code == -1011) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSString *errMess = [serializedData objectForKey:@"message"];
                if (errMess.length > 0) {
                    UIViewController *top = [[Comment alloc] topViewController];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:errMess preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"确定"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    }];
                    [alert addAction:actionCancel];
                    [top presentViewController:alert animated:YES completion:nil];
                } else {
                    UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                    newAlert.backViewColor = NewRedColor;
                    newAlert.showType = ShowUp;
                    [newAlert viewShow:nil];
                }
            } else if(error.code == -1001) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络请求超时！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else if (error.code == -1009) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络连接已断开！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            }
        }
        failure(error);
    }];
}
+(NSString *)defaultPhotoName: (NSString *)imageName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *photoDirectory = [documentsDirectory stringByAppendingPathComponent:@"image"];
    if ( ! [[NSFileManager defaultManager] fileExistsAtPath:photoDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:photoDirectory withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    //return [voiceDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%.0f.ogg", [[NSDate date] timeIntervalSince1970]]];
    return [photoDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",imageName]];
    
}
/**
*  post请求
*
*  @param url        请求的url
*  @param parameters 请求的参数
*  @param success    成功后的block
*  @param failure    失败后的block
*/
- (void)post:(NSString *)url parameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {

    
    if (!_isCloesHud) {
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    }
    [self.manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [SVProgressHUD dismiss];
        if (failure && !self->_isCloesTC) {
            if (error.code == -1011) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSString *errMess = [serializedData objectForKey:@"message"];
                if (errMess.length > 0) {
                    UIViewController *top = [[Comment alloc] topViewController];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:errMess preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"确定"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        failure(error);
                    }];
                    [alert addAction:actionCancel];
                    [top presentViewController:alert animated:YES completion:nil];
                } else {
                    UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                    newAlert.backViewColor = NewRedColor;
                    newAlert.showType = ShowUp;
                    [newAlert viewShow:nil];
                    failure(error);

                }
            } else if(error.code == -1001) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"网络请求超时！"];
                newAlert.backViewColor = [UIColor redColor];
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
                failure(error);

            } else if (error.code == -1009) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络连接已断开！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
                failure(error);

            } else {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
                failure(error);

            }
        } else {
            
            failure(error);
        }
    }];
}


/**
 *  get请求
 *
 *  @param url        请求的url
 *  @param parameters 请求的参数
 *  @param success    成功后的block
 *  @param failure    失败后的block
 */
- (void)get:(NSString *)url parameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    if (!_isCloesHud) {
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    }

    [self.manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];

        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        NSLog(@"%@",error);
        if (failure && !self->_isCloesTC) {
            if (error.code == -1011) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSString *errMess = [serializedData objectForKey:@"message"];
                if (errMess.length > 0) {
                    UIViewController *top = [[Comment alloc] topViewController];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:errMess preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"确定"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    }];
                    [alert addAction:actionCancel];
                    [top presentViewController:alert animated:YES completion:nil];
                } else {
                    UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                    newAlert.backViewColor = NewRedColor;
                    newAlert.showType = ShowUp;
                    [newAlert viewShow:nil];
                }
               
            } else if(error.code == -1001) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络请求超时,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else if (error.code == -1009) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络连接已断开"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            }
        }
        failure(error);
    }];
    
}


/**
 *  put请求
 *
 *  @param url        请求的url
 *  @param parameters 请求的参数
 *  @param success    成功后的block
 *  @param failure    失败后的block
 */
- (void)put:(NSString *)url parameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    if (!_isCloesHud) {
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    }
    [self.manager PUT:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         [SVProgressHUD dismiss];
        if (failure && !self->_isCloesTC) {
            if (error.code == -1011) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSString *errMess = [serializedData objectForKey:@"message"];
                if (errMess.length > 0) {
                    UIViewController *top = [[Comment alloc] topViewController];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:errMess preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"确定"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    }];
                    [alert addAction:actionCancel];
                    [top presentViewController:alert animated:YES completion:nil];
                } else {
                    UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                    newAlert.backViewColor = NewRedColor;
                    newAlert.showType = ShowUp;
                    [newAlert viewShow:nil];
                }
            } else if(error.code == -1001) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络请求超时,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else if (error.code == -1009) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络连接已断开"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            }
        }
        failure(error);
    }];
}


/**
 *  PATCH请求
 *
 *  @param url        请求的url
 *  @param parameters 请求的参数
 *  @param success    成功后的block
 *  @param failure    失败后的block
 */
- (void)PATCH:(NSString *)url parameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    if (!_isCloesHud) {
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    }
    [self.manager PATCH:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        if (failure && !self->_isCloesTC) {
            if (error.code == -1011) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSString *errMess = [serializedData objectForKey:@"message"];
                if (errMess.length > 0) {
                    UIViewController *top = [[Comment alloc] topViewController];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:errMess preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"确定"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    }];
                    [alert addAction:actionCancel];
                    [top presentViewController:alert animated:YES completion:nil];
                } else {
                    UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                    newAlert.backViewColor = NewRedColor;
                    newAlert.showType = ShowUp;
                    [newAlert viewShow:nil];
                }
            } else if(error.code == -1001) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络请求超时,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else if (error.code == -1009) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络连接已断开"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            }
        }
        failure(error);
    }];
}



/**
 *  delete请求
 *
 *  @param url        请求的url
 *  @param parameters 请求的参数
 *  @param success    成功后的block
 *  @param failure    失败后的block
 */
- (void)delete:(NSString *)url parameters:(NSDictionary *)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError *error))failure {
    if (!_isCloesHud) {
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    }
    [self.manager DELETE:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [SVProgressHUD dismiss];
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        if (failure && !self->_isCloesTC) {
            if (error.code == -1011) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                NSString *errMess = [serializedData objectForKey:@"message"];
                if (errMess.length > 0) {
                    UIViewController *top = [[Comment alloc] topViewController];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:errMess preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"确定"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    }];
                    [alert addAction:actionCancel];
                    [top presentViewController:alert animated:YES completion:nil];
                } else {
                    UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                    newAlert.backViewColor = NewRedColor;
                    newAlert.showType = ShowUp;
                    [newAlert viewShow:nil];
                }
            } else if(error.code == -1001) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络请求超时,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else if (error.code == -1009) {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  网络连接已断开"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            } else {
                UINewAlertView * newAlert = [[UINewAlertView alloc] initHidenNvaiWithMessage:@"  服务器维护中,请稍后再试！"];
                newAlert.backViewColor = NewRedColor;
                newAlert.showType = ShowUp;
                [newAlert viewShow:nil];
            }
        }
        failure(error);
    }];
}



- (void)getManager {
    AFSecurityPolicy *securityPolicy = [[AFSecurityPolicy alloc] init];
    [securityPolicy setAllowInvalidCertificates:YES];
    [securityPolicy setValidatesDomainName:NO];
    self.manager = [AFHTTPSessionManager manager];
    // 超时时间
    [self.manager setSecurityPolicy:securityPolicy];
    [self.manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    self.manager.requestSerializer.timeoutInterval = kTimeOutInterval;
    [self.manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"];
//    [self.manager.requestSerializer setValue:@"v1" forHTTPHeaderField:@"version"];
//
    if (token.length > 0) {
        [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", token] forHTTPHeaderField:@"Authorization"];
    }
    [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//     [self.manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", token] forHTTPHeaderField:@"Authorization"];
//    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    self.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"multipart/form-data",@"application/x-www-form-urlencoded",@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
//    self.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"multipart/form-data",@"application/x-www-form-urlencoded",@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    [self.manager.requestSerializer setValue:@"application/json;" forHTTPHeaderField:@"Content-Type"];
//   self.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/xml"];
}

@end
