//
//  Comment.h
//  Field
//
//  Created by 焦钟培 on 2017/6/7.
//  Copyright © 2017年 焦钟培. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UINewAlertView.h"
//#import "Color.h"
#import "NewControlPackage.h"
#import "WXSDKHelper.h"

#define kStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define kNavBarHeight 44.0
//注意：请直接获取系统的tabbar高度，若没有用系统tabbar，建议判断屏幕高度；之前判断  状态栏高度的方法不妥，如果正在通话状态栏会变高，导致判断异常，下面只是一个例子，请勿直接使用！
#define kTopHeight (kStatusBarHeight + kNavBarHeight)

//#define NavHeader [Comment navHeader]
//#define TABBAR_HEIGHT           NavHeader == 64?49:83        //菜单栏高
//#define STATUSBAR_HEIGHT        NavHeader == 64?20:44        //状态栏

#define kFrameWidth [[UIScreen mainScreen] bounds].size.width

#define kFrameHeight [[UIScreen mainScreen] bounds].size.height

#define ShopId [[NSUserDefaults standardUserDefaults] objectForKey:@"shop_id"]

#define fitScreenWidth(width)          width*(kFrameWidth/375)
#define fitScreenHeight(height)        height*(kFrameHeight/667)

#define SCREEN_WIDTH            [[UIScreen mainScreen] bounds].size.width  //屏幕宽度
#define SCREEN_HEIGHT           [[UIScreen mainScreen] bounds].size.height //屏幕高度

/**
 *  自定义颜色
 */
#define NewRGBColor(R,G,B,A)    [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]//16进制颜色值
//iOS10 苹果官方提供的sRGB
#define NewS1RGBColor(R,G,B,A)  [UIColor colorWithDisplayP3Red:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
#define NewS2RGBColor(R,G,B,A)  [UIColor initWithDisplayP3Red:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
//+ (UIColor *)colorWithDisplayP3Red:(CGFloat)displayP3Red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha NS_AVAILABLE_IOS(10_0);
//- (UIColor *)initWithDisplayP3Red:(CGFloat)displayP3Red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha NS_AVAILABLE_IOS(10_0);
//16进制颜色转换
//#define UIColorFromRGBA(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define NewClearColor         [UIColor clearColor] //无
#define NewWhiteColor         [UIColor whiteColor] //白
#define NewBlackColor         [UIColor blackColor] //黑
#define NewPurpleColor        [UIColor purpleColor]//紫
#define NewRedColor           [UIColor redColor]   //红
#define NewYellowColor        [UIColor yellowColor]//黄
#define NewOrangeColor        [UIColor orangeColor]//橙
#define NewBlueColor          [UIColor blueColor]//蓝
#define NewLightGrayColor     [UIColor lightGrayColor]//深灰
#define NewCellLineColor      NewRGBColor(200, 199, 204, 1) //单元格的Cell横线颜色 宽度约为0.6
#define NewIphoneNumberColor  NewRGBColor(0, 136, 232, 1) //电话字体颜色
#define NewGroupTableViewBackgroundColor [UIColor groupTableViewBackgroundColor] //浅灰

//#define NewButtonColor        [UIColor colorWithRed:255.0/255.0 green:150.0/255.0 blue:38.0/255.0 alpha:1.0] //常用按钮浅橙色  UIColorFromRGBA(0xff9626)
#define NewButtonColor        [UIColor colorWithString:@"#230F40"] //常用按钮浅橙色  UIColorFromRGBA(0xff9626)
#define NewNavigationColor    [UIColor colorWithRed:236.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0] //导航条灰色  UIColorFromRGBA(0xf8c392)
//#define NewNavigationColor   [UIColor colorWithString:@"#FF0000"]//导航条
#define NewThemeColor         [UIColor colorWithRed:239.0/255.0 green:51.0/255.0 blue:83.0/255.0 alpha:1.0]
//#define NewLineGrayColor      [UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0] //灰线

#define NewLineGrayColor   [Comment getColor:@"EEEEEE"]
#define NewBgGrayColor        [UIColor colorWithRed:239.0/255 green:239.0/255 blue:244.0/255 alpha:1.0] //灰色背景
#define NewGrayColor          [UIColor colorWithRed:105/255.0 green:105/255.0 blue:105/255.0 alpha:1.0] //灰
#define NewGray2Color         [UIColor colorWithRed:171/255.0 green:171/255.0 blue:171/255.0 alpha:1.0] //浅灰
#define NewDarkGrayColor      [UIColor colorWithRed:46/255.0 green:46/255.0 blue:46/255.0 alpha:1.0] //深灰
#define NewImgBgColor         [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]
#define NewNoDataColor         [UIColor colorWithRed:167.0/255.0 green:174.0/255.0 blue:186.0/255.0 alpha:1.0] //无数据字体颜色



/**
 *  View 圆角和加边框
 */
#define ViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

/**
 *  View 圆角
 */
#define ViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]


/**
 *  字体
 */
#define NewFont(fontSize)   [UIFont systemFontOfSize:fontSize]//细体字号
#define NewBFont(fontSize)  [UIFont boldSystemFontOfSize:fontSize]//粗体字号
#define ChangeProportion (SCREEN_WIDTH/320.0)
#define NewAutoFont(fontSize)  ChangeProportion>1?NewFont(fontSize+2):NewFont(fontSize)//根据屏幕宽度返回细字体
#define NewBAutoFont(fontSize) ChangeProportion>1?NewBFont(fontSize+2):NewBFont(fontSize)//根据屏幕宽度返回粗字体


/**
 *  字符串拼接
 */
#define NewStringFormat(xfmt, ...) [NSString stringWithFormat:xfmt, ##__VA_ARGS__]

/**
 *  字符串比较
 */
#define NewEqualString(xParameter1, xParameter2) [xParameter1 isEqualToString:xParameter2]

/**
 *  对象初始化
 */
#define NewInit(xClass) [[xClass alloc] init]
#define NewInitWithName(xClass,initName) xClass *initName = NewInit(xClass)

/**
 *  MutableArrayInit 可变数组初始化
 */
#define NewMutableArrayInit [NSMutableArray array]

/**
 *  MutableDictionaryInit 可变字典初始化
 */
#define NewMutableDictionaryInit [NSMutableDictionary dictionary]

/**
 *  对象初始化且添加Frame
 */
#define NewInitWithParameter(xClass,initName,xInitFrame) xClass *initName = [[xClass alloc] initWithFrame:xInitFrame]

/**
 *  Frame
 */
#define NewFrame(xmx,xmy,xw,xh) CGRectMake(xmx,xmy,xw,xh)

/**
 *  Button点击事件
 */
#define NewTouchUpInside(touchName,touchAction) [touchName addTarget:self action:@selector(touchAction) forControlEvents:UIControlEventTouchUpInside]

/**
 *  UIImage初始化
 */
#define NewImageNamed(imageNameString) [UIImage imageNamed:imageNameString]

/**
 *  UIImageView初始化且添加图片
 */
#define NewImgInit(xClass,initName,pImg) xClass *initName = [[xClass alloc] initWithImage:pImg]

/**
 *  SafeRelease MRC状态下手动释放内存
 */
#define SafeRelease(A) [A release];A=nil;

/**
 *  SafeString
 */
#define NewSafeString(source) (([source isKindOfClass:[NSNull class]]?@"":source) == nil?@"":(NSString*)source)

/**
 *  StringSizeWith
 *
 *  @param xString 自定义字体以及字体大小
 *  @param xFont
 *
 *  @return StringSize
 */
#define NewStringSizeWith(xString,xFont) [xString sizeWithFont:xFont]

#define NewStringSize(xString,xFont) [xString sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:xFont, NSFontAttributeName, nil]]



/**
 *  Push Present
 */
#define NewPushViewController(xClass) [self.navigationController pushViewController:xClass animated:YES] //普通push
#define NewPopViewController [self.navigationController popViewControllerAnimated:YES] //普通返回到上一级
#define NewPopToRootViewController [self.navigationController popToRootViewControllerAnimated:YES] //返回到最顶层

#define NewPresentViewController(xClass) [self presentViewController:xClass animated:YES completion:nil] //普通present
#define NewDismissViewController [self dismissViewControllerAnimated:YES completion:nil] //普通dismiss返回


#define NewPresentNavigationController(xClass) [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:xClass] animated:YES completion:nil] //带导航条的present
#define NewDismissNavigationController [self.navigationController dismissViewControllerAnimated:YES completion:nil] //带导航条的dismiss返回



/**
 *  DEBUG输出日志
 */

/*
 #ifdef DEBUG
 #define NSLog(...) NSLog(@"%s 第%d行 \n%@",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
 
 #else
 
 #define NSLog(...)
 #endif
 */

/*
 #ifdef DEBUG
 #define NSLog(...) NSLog(__VA_ARGS__)
 #define debugMethod() NSLog(@"%s",__func__)
 ////
 
 #else
 
 #define NSLog(...)
 #define debugMethod()
 #endif
 */


/*
 #ifdef DEBUG
 #define LRString [NSString stringWithFormat:@"%s", __FILE__].lastPathComponent
 #define LRLog(...) printf("%s: %s 第%d行: %s\n\n",[[NSString lr_stringDate] UTF8String], [LRString UTF8String] ,__LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String]);
 
 #else
 #define LRLog(...)
 #endif
 */

/**
 *  NSURL
 */
#define NewURL(NewImageUrl)     [NSURL URLWithString:NewImageUrl]


/**
 *  用户默认头像
 */
#define NewUserImage            [UIImage imageNamed:@"NewDefaultHeadPortrait"]


/**
 *  个人中心头视图
 */
#define NewDefaultHead          [UIImage imageNamed:@"NewDefaultHead"]


/**
 *  用户未激活提示语
 */
#define NewNoActivedTitle       @"很抱歉，您的账号尚未激活！"
#define NewNoActivedHUD         [SVProgressHUD showInfoWithStatus:NewNoActivedTitle]


/**
 *  默认连接服务器失败/无数据提示语
 */
#define NewConnectServerErrorTitle      @"很遗憾，网络连接失败了！"
#define NewConnectServerReloadTitle     @"重新加载"
#define NewConnectServerNoDataTitle     @"没有数据哦！"
#define NewConnectServerRefreshTitle    @"刷新"
#define NewConnectServerNoMoreDataTitle @"数据已经加载完毕"


/**
 *  默认网络错误图片
 */
#define NewetworkErrorImage             @"NewNetworkError"


/**
 *  默认无网络数据图片
 */
#define NewNoDataErrorImage             @"无数据"


/**
 *  默认无视频图片
 */
#define NewNoVideoErrorImage            @"NewNoVideoError"

/**
 *  默认的加载失败之后呈现的图片
 */
#define NewImageError_J [UIImage imageNamed:@"imageError_J"]  //正
#define NewImageError_T [UIImage imageNamed:@"imageError_T"]  //横
#define NewImageError_V [UIImage imageNamed:@"imageError_V"]  //竖
#define NewImageError_C [UIImage imageNamed:@"imageError_C"]  //圆
#define NewImageError_Avatar [UIImage imageNamed:@"imageError_Avatar"]  //头像
#define NewImageError_Special [UIImage imageNamed:@"imageError_Special"]  //专题

#define NewShareURL         @"" //微信分享的默认链接使用

@interface Comment : NSObject

+ (NSString*_Nullable)dictionaryToJson:(NSDictionary *_Nullable)dic;

//生成条形码
+ (UIImage *_Nullable)generateBarCode:(NSString *_Nullable)code width:(CGFloat)width height:(CGFloat)height;

//生成二维码
+ (UIImage *_Nullable)createNonInterpolatedUIImageFormQRForStringe:(NSString *_Nullable)qrString withSize:(CGFloat) size;

//图片合成
+(UIImage *_Nullable)addIconToQRCodeImage:(UIImage *_Nullable)image withIcon:(UIImage *_Nullable)icon withIconSize:(CGSize)iconSize;

+(UIColor *_Nullable)getColor:(NSString*_Nullable)hexColor;

+ (UIImage *_Nullable)OriginImage:(UIImage *_Nullable)image scaleToSize:(CGSize)size;

+ (UIImage *_Nullable)imageWithColor:(UIColor *_Nullable)color;

//图片压缩
+ (NSData *)compressOriginalImage:(UIImage *)image toMaxDataSizeKBytes:(CGFloat)size;

+(NSDictionary *_Nullable)returnDictionaryWithDataPath:(NSString *_Nullable)path;

+(NSString*_Nullable)returnDataWithDictionary:(NSDictionary*_Nullable)dict;

+ (CGFloat)getLableSizeForNSString: (NSString *_Nullable)str LableTextSize: (UIFont *_Nullable)font isWidth: (BOOL)width wOrH: (CGFloat) wOrH;

+ (CGFloat)getLableSizeForNSString: (NSString *_Nullable)str LableTextSize: (UIFont *_Nullable)font isWidth: (BOOL)width wOrH: (CGFloat) wOrH LineSpacing: (CGFloat)LineSpacing Name: (CGFloat)Name;

- (UIViewController *_Nullable)topViewController;

//剪切图片;
+ (UIImage *_Nullable)ct_imageFromImage:(UIImage *_Nullable)image inRect:(CGRect)rect isOrigin: (BOOL)isOrigin;

+ (UIColor *_Nullable)YeJianColor:(UIColor*_Nullable)YJColor YB: (UIColor *_Nullable)YBColor;

+ (UIImage *_Nullable)makeImageWithView:(UIView *_Nullable)view;

+ (NSString *_Nullable)contentTypeForImageData:(NSData *_Nullable)data;

+ (NSString*) base64Encode:(NSData *)data;

+ (NSInteger)getDifferenceByDate:(NSString *_Nullable)date;

+ (CGFloat)navHeader;
+ (CGFloat)navTABBAR_HEIGHT;

+ (void)setupAlertController: (NSString *_Nullable)message YesStr: (NSString *_Nullable)yesStr noStr: (NSString *_Nullable)noStr yes:(void (^_Nullable)(UIAlertAction * _Nonnull action))yes;

+ (BOOL) validateMobile:(NSString *_Nullable)mobile;
+ (BOOL) validatePassword:(NSString *_Nonnull)password;
+ (BOOL)IDCard:(NSString *_Nullable)number;

+(NSDictionary *_Nullable)getAttr:(NSUInteger )textSize textAlign:(NSUInteger )align textColor:(UIColor *_Nullable)color type: (NSInteger)type Spacing:(NSInteger)Spacing;

+ (BOOL)getIsIphone;

//base64
+ (NSString *_Nullable)encode64:(NSString *_Nullable)string;
//时间
+(NSString *_Nullable)getNowTimeTimestamp;
//md5
+ (NSString *_Nonnull) md5:(NSString *_Nullable)str;

- (NSDictionary *_Nullable)AESData:(NSString *_Nullable)data;
- (NSArray *_Nullable)AESDataFromYan:(NSString *_Nullable)data;
- (NSString *_Nonnull)getCode;

//网络请求code
+ (BOOL)requestResultForCode:(NSString *_Nullable)code;

+ (NSString *)shijian:(NSString *)time;

/**
 @method 获取指定宽度width,字体大小fontSize,字符串value的高度
 @param value 待计算的字符串
 @param fontSize 字体的大小
 @param width 限制字符串显示区域的宽度
 @result float 返回的高度
 */
+ (CGFloat) heightforString:(NSString *)value andWidth:(CGFloat)width fontSize:(CGFloat)fontSize;
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height fontSize:(CGFloat)fontSize;

/*根据Key值来判断逻辑*/
+(NSString *)userDefaultsStringKey:(NSString *)key;
+(void)userDefaultsStringKey:(NSString *)key Value:(NSString *)value;

+ (NSString *)iphoneType;


@end
