//
//  NetworkRequestManager.h
//  MingMen
//
//  Created by NewProject on 2017/3/13.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkRequestManager : NSObject 

typedef void(^ResponseTask)(id json);
typedef void(^CallbackTask)(void);

//有无网络
+ (BOOL)connectedToNetwork;


//观察网络状态(此方法在整个项目中只需要调用一次)
+ (void)observeNetworkStatus;


/**
 *  异步GET请求
 *
 *  @param prefix       请求的接口标识
 *  @param parameters   上传参数
 *  @param succeed      成功回调
 *  @param failed       失败回调
 */
+ (void)requestGetWithInterfacePrefix:(NSString*)prefix parameters:(id)parameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed;


/**
 *  异步POST请求
 *
 *  @param prefix       请求的接口标识
 *  @param parameters   上传参数
 *  @param succeed      成功回调
 *  @param failed       失败回调
 */
+ (void)requestPostWithInterfacePrefix:(NSString*)prefix parameters:(id)parameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed;


/**
 *  异步POST请求:以body方式,支持数组
 *
 *  @param prefix           请求的接口标识
 *  @param parameters       普通上传参数
 *  @param bodyParameters   Body上传参数
 *  @param succeed          成功回调
 *  @param failed           失败回调
 */
+ (void)requestPostWithInterfacePrefix:(NSString*)prefix parameters:(id)parameters bodyParameters:(id)bodyParameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed;


/**
 *  异步POST请求,上传图片,支持数组
 *
 *  @param prefix           请求的接口标识
 *  @param parameters       普通上传参数
 *  @param serverFileName   服务器存放文件的名称
 *  @param arrayParameters  文件数组
 *  @param succeed          成功回调
 *  @param failed           失败回调
 */
+ (void)requestPostWithInterfacePrefix:(NSString*)prefix serverFileName:(NSString *)serverFileName parameters:(id)parameters arrayParameters:(id)arrayParameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed;



+ (void)requestPostWithInterfacePrefixNew:(NSString*)prefix parameters:(id)parameters onSuccess:(ResponseTask)succeed onFailure:(CallbackTask)failed;
@end
