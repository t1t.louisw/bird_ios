//
//  NewInterfaceReplacement.h
//  MingMen
//
//  Created by NewProject on 2017/5/19.
//  Copyright © 2017年 NewProject. All rights reserved.
//
//  此单利类用于界面更替 例如程序控制类与登录界面的互动

#import <Foundation/Foundation.h>
#import "LeftSlideViewController.h"

@interface NewInterfaceReplacement : NSObject

@property (strong, nonatomic) LeftSlideViewController *LeftSlideVC;
@property (strong, nonatomic) UINavigationController *mainNavigationController;

+ (NewInterfaceReplacement *) sharedInstance;

/*切换登录界面*/
- (void)replacementLogin;

/*切换根控制器*/
- (void)replacementController;

@end
