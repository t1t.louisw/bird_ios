//
//  NewUtils.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewUtils.h"

#import <CommonCrypto/CommonDigest.h>

#import <UIKit/UIKit.h>
#import <math.h>
#import <net/if.h>
#import <net/if_dl.h>
#import <mach/mach.h>
#import <mach/mach_host.h>
#import <sys/socket.h>
#import <sys/sysctl.h>
#import <sys/types.h>
#import <ifaddrs.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <net/if.h>
#include <ifaddrs.h>
#import <dlfcn.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <sys/utsname.h>

@implementation NewUtils


/*
static NSString* gAccount          = @"";//账号
static NSString* gPassword         = @"";//密码
static NSString* gAuthorid         = @"";//账号ID
static NSString* gUserName         = @"";//用户名称
static NSString* gUserHeadImage    = @"";//用户头像
static NSString* gUdid             = @"";//UDID
static NSString* gAppkey           = @"";//appkey
static NSString* gReg_time         = @"";//注册时间
static NSString* gMobile_phone     = @"";//手机号
static NSString* gShoppingCartNumber = @"0";//购物车数量
static BOOL gShoppingCartFlag      = NO;
*/


/*
//用户名称
+(void)setUserName:(NSString*)userName
{
    gUserName = userName;
}
+(NSString*)getUserName
{
    return gUserName;
}


//用户头像
+(void)setUserHeadImage:(NSString*)userHeadImage
{
    gUserHeadImage = userHeadImage;
}
+(NSString*)getUserHeadImage
{
    return gUserHeadImage;
}



//账号
+(void)setAccount:(NSString*)account
{
    gAccount = account;
}

+(NSString*)getAccount
{
    return gAccount;
}




//密码
+(void)setPassword:(NSString*)password
{
    gPassword = password;
}

+(NSString*)getPassword
{
    return gPassword;
}




//账号ID
+(void)setAuthorid:(NSString*)authorid
{
    gAuthorid = authorid;
}

+(NSString*)getAuthorid
{
    return gAuthorid;
}




//UDID
+(void)set_UDID:(NSString*)udid
{
    gUdid = udid;
}

+ (NSString*)get_UDID
{
    return gUdid;//
}


//验证码
+(void)set_Appkey:(NSString *)appkey
{
    gAppkey = appkey;
}

+(NSString*)get_Appkey
{
    return gAppkey;
}


//注册时间
+(void)set_reg_time:(NSString *)time
{
    gReg_time = time;
}

+(NSString*)get_reg_time
{
    return gReg_time;
}

//手机号
+(void)set_mobile_phone:(NSString *)phone
{
    gMobile_phone = phone;
}

+(NSString*)get_mobile_phone
{
    return gMobile_phone;
}



//购物车数量
+(void)setShoppingCartNumber:(NSString *)number
{
    gShoppingCartNumber = number;
}

+(NSString *)getShoppingCartNumber
{
    return gShoppingCartNumber;
}


//购物车跳转登录
+(void)setShoppingCartLogin:(BOOL)flag
{
    gShoppingCartFlag = flag;
}

+(BOOL)getShoppingCartLogin
{
    return gShoppingCartFlag;
}



//农博汇项目请求接口的实时时间
+ (NSString *)get_req_time
{
    //年月日时分秒毫秒
    NSTimeInterval curTime = [[NSDate date] timeIntervalSince1970];
    double dtime = (double)curTime;
    NSDateFormatter* format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyyMMddHHmmss"];
    NSString* timeString = [format stringFromDate:[NSDate dateWithTimeIntervalSince1970:dtime]];
    
    
//    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
//    NSTimeInterval a=[dat timeIntervalSince1970]*1000;
//    NSString *timeString = [NSString stringWithFormat:@"%f", a];//／／转为字符型
    
    
//    NSDate *date = [NSDate date];
//    NSString *timeString = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
    
    return timeString;
}

//公用接口的请求时间
+(NSString*)get_publicInterface_time
{
    NSDate *date = [NSDate date];
    return [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
}
*/

/*验证正则表达式*/
+ (BOOL)matchRegularExpression:(NSString*)text match:(NSString*)match
{
    NSError *error;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:match options:0 error:&error];
    
    if (regex != nil)
    {
        NSTextCheckingResult *firstMatch = [regex firstMatchInString:text options:0 range:NSMakeRange(0, [text length])];
        
        if (firstMatch)
        {
            return YES;
        }
    }
    
    return NO;
    
}


/**
 @method 获取指定宽度width,字体大小fontSize,字符串value的高度
 @param value 待计算的字符串
 @param fontSize 字体的大小
 @param width 限制字符串显示区域的宽度
 @result float 返回的高度
 */
+ (CGFloat) heightforString:(NSString *)value andWidth:(CGFloat)width fontSize:(CGFloat)fontSize{
    return [value boundingRectWithSize:CGSizeMake(width, 1000000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName] context:nil].size.height;
    
}
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height fontSize:(CGFloat)fontSize{
    return [value boundingRectWithSize:CGSizeMake(100000, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:fontSize] forKey:NSFontAttributeName] context:nil].size.width;
}
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height XfontSize:(CGFloat)XfontSize{
    return [value boundingRectWithSize:CGSizeMake(100000, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:XfontSize] forKey:NSFontAttributeName] context:nil].size.width;
}
+ (CGFloat) heightforString:(NSString *)value andWidth:(CGFloat)width AutofontSize:(CGFloat)AutofontSize{
    return [value boundingRectWithSize:CGSizeMake(width, 1000000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:NewAutoFont(AutofontSize) forKey:NSFontAttributeName] context:nil].size.height;
}
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height AutofontSize:(CGFloat)AutofontSize{
    return [value boundingRectWithSize:CGSizeMake(100000, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:NewAutoFont(AutofontSize) forKey:NSFontAttributeName] context:nil].size.width;
}


+(BOOL)luhn:(NSString*)bank//for checkBankCard
{
    int s1 = 0;
    int s2 = 0;
    
    NSString* reverse = [NewUtils reverseString:bank];
    NSInteger length = reverse.length;
    NSRange range;
    
    for (int i=0; i<length; i++)
    {
        range = NSMakeRange (i, 1);
        int digit = [[reverse substringWithRange:range] intValue];
        if(i % 2 == 0)//this is for odd digits, they are 1-indexed in the algorithm
        {
            s1 += digit;
        }
        else//add 2 * digit for 0-4, add 2 * digit - 9 for 5-9
        {
            s2 += 2 * digit;
            if(digit >= 5)
            {
                s2 -= 9;
            }
        }
    }
    
    return (s1 + s2) % 10 == 0;
}

+(NSString*)reverseString:(NSString*)str
{
    NSUInteger len = [str length];
    NSMutableString *reverse  = [NSMutableString stringWithCapacity:len];
    while(len>0)
    {
        //从后取一个字符
        unichar c = [str characterAtIndex:--len];
        NSString *s = [NSString stringWithFormat:@"%C",c];
        [reverse appendString:s];
    }
    return reverse;
}


/*银行卡号码验证*/
+(BOOL)checkBankCard:(NSString*)bank
{
    NSString* mach = @"^(^\\d{16}$|^\\d{19}$)$";
    
    if (bank.length <= 0 || ![NewUtils matchRegularExpression:bank match:mach])
    {
        return NO;
    }
    
    return [NewUtils luhn:bank];
}


/*身份证的判断*/
+(BOOL)checkIdentity:(NSString*)identity
{
    if (identity.length <= 0)
    {
        return false;
    }
    
    if([NewUtils checkCid:identity] == 0)
    {
        return true;
    }
    
    return false;
}


+(int) checkCid:(NSString*)identity
{
    /**
     * 0：合法 1：非法格式 2：非法地区 3：非法生日 4：非法校验
     * */
    NSString* match = @"^(^\\d{15}$|^\\d{18}$|^\\d{17}(\\d|X|x))$";
    if (![NewUtils matchRegularExpression:identity match:match])
    {
        return 1;
    }
    
    //NSArray* areas = [NSArray arrayWithObjects:@"", @"安微",@"",nil];
    
    NSArray* areas = [NSArray arrayWithObjects:@"", @"", @"", @"", @"", @"", @"", @"", @"", @"", @"", @"北京", @"天津", @"河北", @"山西", @"内蒙古", @"", @"",@"", @"", @"", @"辽宁", @"吉林", @"黑龙江", @"", @"", @"", @"", @"", @"", @"", @"上海", @"江苏", @"浙江", @"安微", @"福建", @"江西", @"山东", @"", @"",@"", @"河南", @"湖北", @"湖南", @"广东", @"广西", @"海南", @"", @"", @"", @"重庆", @"四川", @"贵州", @"云南", @"西藏", @"", @"", @"", @"", @"", @"", @"陕西",@"甘肃", @"青海", @"宁夏", @"新疆", @"", @"", @"", @"", @"", @"台湾", @"", @"", @"", @"", @"", @"", @"", @"", @"", @"香港", @"澳门", @"",@"", @"", @"", @"", @"", @"", @"", @"国外", @"", @"", @"", @"", @"", @"", @"", @"", nil];
    
    // 地区
    NSRange range = NSMakeRange (0, 2);
    int areacode = [[identity substringWithRange:range] intValue];
    NSString* string = [areas objectAtIndex:areacode];
    
    if (string.length == 0)
    {
        return 2;
    }
    
    // 生日
    NSString* dateStrSrc = nil;
    
    if (identity.length == 18)
    {
        range = NSMakeRange (6, 8);
        dateStrSrc = [identity substringWithRange:range];
    }
    else
    {
        range = NSMakeRange (6, 6);
        dateStrSrc = [NSString stringWithFormat:@"%d%@",19,[identity substringWithRange:range]];
    }
    
    //range = NSMakeRange (0, 4);
    // NSString* str = [dateStrSrc substringWithRange:range];
    //range = NSMakeRange (4, 2);
    NSString* str1 = nil;//[dateStrSrc substringWithRange:range];
    //range = NSMakeRange (6, 2);
    NSString* str2 = nil;//[dateStrSrc substringWithRange:range];
    //NSString* dateStr = [NSString stringWithFormat:@"%@-%@-%@",str,str1,str2];
    // 校验位比对
    if (identity.length == 18)
    {
        NSMutableArray* wi = [NSMutableArray arrayWithCapacity:17];
        
        for (int i=0; i<17; i++)
        {
            int k = pow(2,(17-i));
            int v = k % 11;
            str2 = [NSString stringWithFormat:@"%d",v];
            [wi addObject:str2];
        }
        
        int sum = 0;
        int t = 0;
        int t2 = 0;
        
        // 进行加权求和
        for (int i = 0; i < 17; i++)
        {
            range = NSMakeRange (i, 1);
            t = [[identity substringWithRange:range] intValue];
            t2 = [[wi objectAtIndex:i] intValue];
            sum += t * t2;
        }
        
        // 取模运算，得到模值
        int code = sum % 11;
        NSString* checkCode = @"10X98765432";
        range = NSMakeRange (code, 1);
        NSString* check = [checkCode substringWithRange:range];
        range = NSMakeRange (17, 1);
        str1 = [identity substringWithRange:range];
        if (NSOrderedSame != [str1 compare:check options:NSCaseInsensitiveSearch])
        {
            return 4;
        }
    }
    // 男女
    if (identity.length == 18)
    {
        range = NSMakeRange (16, 1);
        str1 = [identity substringWithRange:range];
        
        if ([str1 intValue]%2 == 1)
        {
            //NSLog(@"sex = 男");
        }
        else
        {
            //NSLog(@"sex = 女");
        }
    }
    else
    {
        range = NSMakeRange (13, 1);
        str1 = [identity substringWithRange:range];
        
        if ([str1 intValue]%2 == 1)
        {
            //NSLog(@"sex = 男");
        }
        else
        {
            //NSLog(@"sex = 女");
        }
    }
    
    return 0;
}

+(BOOL)checkMobilePhone:(NSString*) mobilePhone
{
    if (mobilePhone.length <= 0)
    {
        return NO;
    }else if (mobilePhone.length == 11)
    {
        return YES;
    }
    
    
    
    if (mobilePhone.length <= 0)
    {
        return NO;
    }
    
    NSArray * mobilePhoneStarts = [NSArray arrayWithObjects:@"106|移动", @"130|联通", @"131|联通", @"132|联通", @"133|电信", @"134|移动", @"135|移动", @"136|移动", @"137|移动", @"138|移动", @"139|移动",@"145|联通", @"147|移动", @"150|移动", @"152|移动", @"153|电信", @"155|联通", @"156|联通", @"157|移动", @"158|移动", @"159|移动",@"180|电信", @"181|电信", @"182|移动", @"183|移动",@"184|移动", @"185|联通", @"186|联通", @"187|移动", @"188|移动", @"189|电信",@"151|移动",@"177|电信",nil];
    
    NSString* regex1MobilePhone = @"^\\d{11}$";
    NSString* regex2MobilePhone = @"^(86)\\d{11}$";
    NSString* regex3MobilePhone = @"^(\\+86)\\d{11}$";
    
    NSRange range = NSMakeRange (0, 3);
    if ([NewUtils matchRegularExpression:mobilePhone match:regex1MobilePhone])
    {
        NSString* temp1 = nil;
        NSString* temp2 = nil;
        
        for (NSString* mobilePhoneStart in mobilePhoneStarts)
        {
            temp1 = [mobilePhoneStart substringWithRange:range];
            temp2 = [mobilePhone substringWithRange:range];
            if (NSOrderedSame == [temp1 compare:temp2 options:NSCaseInsensitiveSearch])
            {
                return YES;
            }
        }
        
        
    }
    
    if ([NewUtils matchRegularExpression:mobilePhone match:regex2MobilePhone])
    {
        NSString* temp1 = nil;
        NSString* temp2 = nil;
        for (NSString* mobilePhoneStart in mobilePhoneStarts)
        {
            temp1 = [mobilePhoneStart substringWithRange:range];
            range = NSMakeRange (2, 3);
            temp2 = [mobilePhone substringWithRange:range];
            
            if (NSOrderedSame == [temp1 compare:temp2 options:NSCaseInsensitiveSearch])
            {
                return YES;
            }
        }
    }
    
    if ([NewUtils matchRegularExpression:mobilePhone match:regex3MobilePhone])
    {
        NSString* temp1 = nil;
        NSString* temp2 = nil;
        
        for (NSString* mobilePhoneStart in mobilePhoneStarts)
        {
            temp1 = [mobilePhoneStart substringWithRange:range];
            range = NSMakeRange (3, 3);
            temp2 = [mobilePhone substringWithRange:range];
            
            if (NSOrderedSame == [temp1 compare:temp2 options:NSCaseInsensitiveSearch])
            {
                return YES;
            }
        }
    }
    
    return NO;
}


/*数字检测*/
+ (BOOL)checkInterNum:(NSString *)num
{
    NSScanner* scan = [NSScanner scannerWithString:num];
    
    int val;
    
    return [scan scanInt:&val] && [scan isAtEnd];
}


/*检测中文*/
+ (BOOL)checkName:(NSString *)name
{
    if (name.length <= 0)
    {
        return NO;
    }
    
    NSString* mach = @"^[\u4e00-\u9fa5]([\u4e00-\u9fa5]){1,6}$";
    
    return [NewUtils matchRegularExpression:name match:mach];
}


/*判断密码 特殊符号跟中文不通过*/
+ (BOOL)checkPassword:(NSString *)password
{
    if (password.length <= 0)
    {
        return NO;
    }
    
    NSString* mach = @"^[a-zA-Z0-9]{6,20}$";
    
    return [NewUtils matchRegularExpression:password match:mach];
}

/*判断密码 进支持数字、字母、下划线 其它不通过*/
+ (BOOL)checkPassword2:(NSString *)password
{
    NSInteger len=password.length;
    for(int i=0;i<len;i++)
    {
        unichar a=[password characterAtIndex:i];
        
        /*
         isalnum(int);// 判断是否是数字或者字母
         isalpha(int);//判断是否是英文字符
         isdigit(int);//判断是否是数字 0~9
         islower(int);//判断是否是小写字母
         isupper(int);//判断是否是大写字母
         isxdigit(int);//是否是16进制数字
         tolower(int);//转成小写
         toupper(int);//转成大写
         digittoint(int);//把十六进制数字字符转换成整型
         ishexnumber(int);//等价 isxdigit
         isnumber(int);//等价isdigit
         ((a >= 0x4e00 && a <= 0x9fa6)) ;//是否为中文
         */
        
        if(!((isalpha(a))
             ||(isalnum(a))
             ||((a=='_'))
             )) {
            return NO;
        }
        
        //        if(!((isalpha(a))
        //             ||(isalnum(a))
        //             ||((a=='_'))
        //             ||((a >= 0x4e00 && a <= 0x9fa6))
        //             )) {
        //            return NO;
        //        }
    }
    return YES;
}



/*特殊符号*/
+(BOOL)cheeckNotChina:(NSString*)number
{
    
    if (number.length <= 0)
    {
        return NO;
    }
    
    NSString* mach = @"/^([\u4E00-\u9FA5]|\\w)*$/";
    
    return [NewUtils matchRegularExpression:number match:mach];
    
}


+ (BOOL)checkEmail:(NSString *)email
{
    if (email.length <= 0)
    {
        return NO;
    }
    
    NSString* mach = @"^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
    
    return [NewUtils matchRegularExpression:email match:mach];
}


/**
 压图片质量
 压图片质量和缩小图片大小相结合
 @param image image
 @return Data
 */
+ (NSData *)zipImageWithImage:(UIImage *)image
{
    if (!image) {
        return nil;
    }
    CGFloat maxFileSize = 32*1024;
    CGFloat compression = 0.9f;
    NSData *compressedData = UIImageJPEGRepresentation(image, compression);
    while ([compressedData length] > maxFileSize) {
        compression *= 0.9;
        compressedData = UIImageJPEGRepresentation([NewUtils compressImage:image newWidth:image.size.width*compression], compression);
    }
    return compressedData;
}

/**
 *  等比缩放本图片大小
 *
 *  @param newImageWidth 缩放后图片宽度，像素为单位
 *
 *  @return self-->(image)
 */
+ (UIImage *)compressImage:(UIImage *)image newWidth:(CGFloat)newImageWidth
{
    if (!image) return nil;
    float imageWidth = image.size.width;
    float imageHeight = image.size.height;
    float width = newImageWidth;
    float height = image.size.height/(image.size.width/width);
    
    float widthScale = imageWidth /width;
    float heightScale = imageHeight /height;
    
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    
    if (widthScale > heightScale) {
        [image drawInRect:CGRectMake(0, 0, imageWidth /heightScale , height)];
    }
    else {
        [image drawInRect:CGRectMake(0, 0, width , imageHeight /widthScale)];
    }
    
    // 从当前context中创建一个改变大小后的图片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    
    return newImage;
}



/*图片质量进行压缩*/
- (NSData *)returnsTheSizeAccordingToThePicture:(UIImage *)image
{
    NSData *imageData1=UIImagePNGRepresentation(image);
    NSData *imageData;
    
    NSInteger sum=[self setCalculatingPictureSize:imageData1];
    switch (sum) {
        case 0:{//少于300KB
            imageData=UIImageJPEGRepresentation(image, 1.0f);
        }
            break;
        case 1:{//超过300kb少于1000kb
            imageData=UIImageJPEGRepresentation(image, 0.8f);
        }
            break;
        case 2:{//超过1000KB少于2000kb
            imageData=UIImageJPEGRepresentation(image, 0.65f);
        }
            break;
        case 3:{//超过20000KB
            imageData=UIImageJPEGRepresentation(image, 0.5f);
        }
            break;
        default:
            break;
    }
    
    return imageData;
}

//用于计算图片大小
- (NSInteger)setCalculatingPictureSize:(NSData *)image
{
    NSInteger sum=0;
    CGFloat scale=(image.length/1024);
    if (scale>300&&scale<=1000) {
        sum=1;
    }else if(scale>1000&&scale<=2000){
        sum=2;
    }else if (scale>2000){
        sum=3;
    }
    
    return sum;
}


/**
 * 图片压缩到指定大小
 * @param targetSize 目标图片的大小
 * @param sourceImage 源图片
 * @return 目标图片
 */
+ (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize withSourceImage:(UIImage *)sourceImage
{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}


/*图片拉伸*/
+ (UIImage *)stretchImage:(UIImage *)img edgeInsets:(UIEdgeInsets)inset
{
    if ([img respondsToSelector:@selector(resizableImageWithCapInsets:resizingMode:)])
    {
        img = [img resizableImageWithCapInsets:inset resizingMode:UIImageResizingModeStretch];
    }
    else if ([img respondsToSelector:@selector(resizableImageWithCapInsets:)])
    {
        img = [img resizableImageWithCapInsets:inset];
    }
    
    return img;
}

/*字符串转DATE*/
+(NSString*)dataToString:(NSData*)data
{
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

/*DATA转字符串*/
+(NSData*)stringToData:(NSString*)string
{
    return [string dataUsingEncoding: NSASCIIStringEncoding];
}

/*转化日期*/
+ (NSDateComponents *)componentsOfDate:(NSDate *)date
{
    NSDate *aDate = date ? date : [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
    
    return [calendar components:unitFlags fromDate:aDate];
}

/*转化日期 精确到秒 NSDate - NSString*/
+ (NSString *)dateToFromString:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddHHmmssSSS"];//@"yyyy-MM-dd HH:mm:ss:SSS"
    return [formatter stringFromDate:date];
}

/*转化日期 精确到秒 NSString - NSDate*/
+ (NSDate *)stringToFromDate:(NSString *)string
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddHHmmssSSS"];//@"yyyy-MM-dd HH:mm:ss:SSS"
    return [formatter dateFromString:string];
}

/*转化日期 精确到天 NSString - NSDate*/
+ (NSDate *)stringToFromDateDay:(NSString *)string
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    [dateFormatter setMonthSymbols:[NSArray arrayWithObjects:@"01",@"02",@"03",@"04",@"05",@"06",@"07",@"08",@"09",@"10",@"11",@"12", nil]];
    NSDate * valueDate = [dateFormatter dateFromString:string];
    return valueDate;
}


/*时间转换 精确到天*/
+ (NSString *)returnTransformationTime:(NSString *)string
{
    // timeStampString 是服务器返回的13位时间戳
    NSString *timeStampString  = string;
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[timeStampString doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString       = [formatter stringFromDate: date];
    //NSLog(@"服务器返回的时间戳对应的时间是:%@",dateString);
    return dateString;
}

/*时间转换 精确到秒*/
+ (NSString *)returnTransformationDetailedTime:(NSString *)string
{
    // timeStampString 是服务器返回的13位时间戳
    NSString *timeStampString  = string;
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[timeStampString doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString       = [formatter stringFromDate: date];
    //NSLog(@"服务器返回的时间戳对应的时间是:%@",dateString);
    return dateString;
}



/*获取当前时间 年、月、日、分、秒*/
+ (NSString *)returnDetailedTime:(NSString *)string
{
    // 获取代表公历的NSCalendar对象
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // 获取当前日期
    NSDate* dt = [NSDate date];
    // 定义一个时间字段的旗标，指定将会获取指定年、月、日、时、分、秒的信息
    unsigned unitFlags = NSCalendarUnitYear |
    NSCalendarUnitMonth |  NSCalendarUnitDay |
    NSCalendarUnitHour |  NSCalendarUnitMinute |
    NSCalendarUnitSecond | NSCalendarUnitWeekday;
    // 获取不同时间字段的信息
    NSDateComponents* comp = [gregorian components: unitFlags
                                          fromDate:dt];
    // 获取各时间字段的数值
    //NSLog(@"现在是%ld年" , comp.year);
    //NSLog(@"现在是%ld月 " , comp.month);
    //NSLog(@"现在是%ld日" , comp.day);
    //NSLog(@"现在是%ld时" , comp.hour);
    //NSLog(@"现在是%ld分" , comp.minute);
    //NSLog(@"现在是%ld秒" , comp.second);
    //NSLog(@"现在是星期%ld" , comp.weekday);
    
    if ([string isEqualToString:@"year"]) {
        return [NSString stringWithFormat:@"%ld",comp.year];
    }else if ([string isEqualToString:@"month"]){
        return [NSString stringWithFormat:@"%ld",comp.month];
    }else if ([string isEqualToString:@"day"]){
        return [NSString stringWithFormat:@"%ld",comp.day];
    }else if ([string isEqualToString:@"hour"]){
        return [NSString stringWithFormat:@"%ld",comp.hour];
    }else if ([string isEqualToString:@"minute"]){
        return [NSString stringWithFormat:@"%ld",comp.minute];
    }else if ([string isEqualToString:@"second"]){
        return [NSString stringWithFormat:@"%ld",comp.second];
    }else if ([string isEqualToString:@"weekday"]){
        return [NSString stringWithFormat:@"%ld",comp.weekday];
    }else {
        return [NSString stringWithFormat:@"%ld年%ld月%ld日 周%ld %ld时%ld分%ld秒",comp.year,comp.month,comp.day,comp.weekday,comp.hour,comp.minute,comp.second];
    }
    
    
    /*
     // 再次创建一个NSDateComponents对象
     NSDateComponents* comp2 = [[NSDateComponents alloc]
     init];
     // 设置各时间字段的数值
     comp2.year = 2013;
     comp2.month = 4;
     comp2.day = 5;
     comp2.hour = 18;
     comp2.minute = 34;
     // 通过NSDateComponents所包含的时间字段的数值来恢复NSDate对象
     NSDate *date = [gregorian dateFromComponents:comp2];
     NSLog(@"获取的日期为：%@" , date);
     */
}


+(int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *oneDayStr = [dateFormatter stringFromDate:oneDay];
    NSString *anotherDayStr = [dateFormatter stringFromDate:anotherDay];
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    NSComparisonResult result = [dateA compare:dateB];
    NSLog(@"date1 : %@, date2 : %@", oneDay, anotherDay);
    if (result == NSOrderedDescending) {
        //NSLog(@"前者大");
        return 1;
    }
    else if (result ==NSOrderedAscending){
        //NSLog(@"后者大");
        return -1;
    }
    //NSLog(@"相等");
    return 0;
}


/*返回1970年距离至今的时间差*/
+ (NSString *)returnTime
{
    NSDate *date = [NSDate date];
    return [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
}



/*返回两个时间的时间差*/
+ (NSString *)returnTimeDifference:(NSString *)starTime endTiem:(NSString *)endTime
{
    //要转换的字符串
    NSString * dateString1 = starTime;
    NSString * dateString2 = endTime;
    //字符串转NSDate格式的方法
    NSDate * valueDate1 = [NewUtils stringToFromDateDay:dateString1];
    NSDate * valueDate2 = [NewUtils stringToFromDateDay:dateString2];
    //计算两个中间差值(秒)
    NSTimeInterval time = [valueDate2 timeIntervalSinceDate:valueDate1];
    
    //开始时间和结束时间的中间相差的时间
    int days;
    days = ((int)time)/(3600*24);  //一天是24小时*3600秒
    return [NSString stringWithFormat:@"%i",days];
}




/*判断数字输入错误的集合*/
+ (NSInteger)doTextFieldDelegate:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger result = 0;
    NSInteger length = [textField.text length] + [string length];
    
    if (range.length > 0)
    {
        length--;
    }
    
    if (length > 0)
    {
        if (textField.text.length <=0)
        {
            if (string.length >=2)
            {
                if ([[string substringToIndex:1] isEqualToString:@"0"])
                {
                    result = 6;
                }
            }
        }
        
        if ([textField.text isEqualToString:@"0"])
        {
            if (![string isEqualToString:@"."])
            {
                result = 5;
            }
        }
        
        if(length > 8)//大于八位数
        {
            result = 3;
        }
        else
        {
            NSRange textRange = [textField.text rangeOfString:@"."];
            NSArray* textArr = [textField.text componentsSeparatedByString:@"."];
            
            if (textRange.length > 0)//如果已经有小数点时
            {
                if ([[textArr objectAtIndex:1] length]>=2 && string.length>0)//只能保留二位小数
                {
                    result = 1;
                }
            }
            else //如果还没有小数点时
            {
                if ([string isEqualToString:@"."])//输入小数点时
                {
                    if ([textField.text length] <= 0)//第一位不能输入小数点
                    {
                        result = 2;
                    }
                }
            }
        }
    }
    
    return result;
}

/*拨打电话*/
+(void)callTel:(NSString*)tel
{
    NSMutableString * str=[[NSMutableString alloc] initWithCapacity:50];
    [str appendString:@"tel://"];
    [str appendString:tel];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}


/*弹框*/
+ (void)alertWithMsg:(NSString*)msg delegate:(id<UIAlertViewDelegate>)sender andTag:(NSInteger)tag title:(NSString *)title cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles1:(NSString *)otherButtonTitles1 otherButtonTitles2:(NSString *)otherButtonTitles2 otherButtonTitles3:(NSString *)otherButtonTitles3 otherButtonTitles4:(NSString *)otherButtonTitles4 otherButtonTitles5:(NSString *)otherButtonTitles5 otherButtonTitles6:(NSString *)otherButtonTitles6
{
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:title message:msg delegate:sender cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles1,otherButtonTitles2,otherButtonTitles3,otherButtonTitles4,otherButtonTitles5,otherButtonTitles6, nil];
    view.tag = tag;
    [view show];
}


+ (NSString *) getSysInfoByName:(char *)typeSpecifier
{
    size_t size;
    sysctlbyname(typeSpecifier, NULL, &size, NULL, 0);
    char *answer = malloc(size);
    sysctlbyname(typeSpecifier, answer, &size, NULL, 0);
    NSString *results = [NSString stringWithCString:answer encoding: NSUTF8StringEncoding];
    free(answer);
    return results;
}

//
+ (NSString *) platform
{
    return [NewUtils getSysInfoByName:"hw.machine"];
}

/*iphone/iPad类型及适配*/
+ (NSString *) simplePlatformString
{
    NSString *platform = [NewUtils platform];
    // NSLog(@"手机型号 == %@",platform);
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G (A1203)";
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G (A1241/A1324)";
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS (A1303/A1325)";
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4 (A1332)";
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4 (A1332)";
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4 (A1349)";
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S (A1387/A1431)";
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5 (A1428)";
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5 (A1429/A1442)";
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c (A1456/A1532)";
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c (A1507/A1516/A1526/A1529)";
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s (A1453/A1533)";
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s (A1457/A1518/A1528/A1530)";
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus (A1522/A1524)";
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6 (A1549/A1586)";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6 Plus s";
    
    if ([platform isEqualToString:@"iPod1,1"])   return @"iPod Touch 1G (A1213)";
    if ([platform isEqualToString:@"iPod2,1"])   return @"iPod Touch 2G (A1288)";
    if ([platform isEqualToString:@"iPod3,1"])   return @"iPod Touch 3G (A1318)";
    if ([platform isEqualToString:@"iPod4,1"])   return @"iPod Touch 4G (A1367)";
    if ([platform isEqualToString:@"iPod5,1"])   return @"iPod Touch 5G (A1421/A1509)";
    
    if ([platform isEqualToString:@"iPad1,1"])   return @"iPad 1G (A1219/A1337)";
    
    if ([platform isEqualToString:@"iPad2,1"])   return @"iPad 2 (A1395)";
    if ([platform isEqualToString:@"iPad2,2"])   return @"iPad 2 (A1396)";
    if ([platform isEqualToString:@"iPad2,3"])   return @"iPad 2 (A1397)";
    if ([platform isEqualToString:@"iPad2,4"])   return @"iPad 2 (A1395+New Chip)";
    if ([platform isEqualToString:@"iPad2,5"])   return @"iPad Mini 1G (A1432)";
    if ([platform isEqualToString:@"iPad2,6"])   return @"iPad Mini 1G (A1454)";
    if ([platform isEqualToString:@"iPad2,7"])   return @"iPad Mini 1G (A1455)";
    
    if ([platform isEqualToString:@"iPad3,1"])   return @"iPad 3 (A1416)";
    if ([platform isEqualToString:@"iPad3,2"])   return @"iPad 3 (A1403)";
    if ([platform isEqualToString:@"iPad3,3"])   return @"iPad 3 (A1430)";
    if ([platform isEqualToString:@"iPad3,4"])   return @"iPad 4 (A1458)";
    if ([platform isEqualToString:@"iPad3,5"])   return @"iPad 4 (A1459)";
    if ([platform isEqualToString:@"iPad3,6"])   return @"iPad 4 (A1460)";
    
    if ([platform isEqualToString:@"iPad4,1"])   return @"iPad Air (A1474)";
    if ([platform isEqualToString:@"iPad4,2"])   return @"iPad Air (A1475)";
    if ([platform isEqualToString:@"iPad4,3"])   return @"iPad Air (A1476)";
    if ([platform isEqualToString:@"iPad4,4"])   return @"iPad Mini 2G (A1489)";
    if ([platform isEqualToString:@"iPad4,5"])   return @"iPad Mini 2G (A1490)";
    if ([platform isEqualToString:@"iPad4,6"])   return @"iPad Mini 2G (A1491)";
    
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
    return platform;
}

/*对一段字符串的中间部分进行隐藏 以***代替*/
+ (NSString *)formatCardNumber:(NSString *)cardNumber
{
    if (cardNumber.length > 9)
    {
        NSString *strLeft = [cardNumber substringToIndex:6];
        NSString *strRight = [cardNumber substringFromIndex:(cardNumber.length - 4)];
        return [NSString stringWithFormat:@"%@***%@",strLeft,strRight];
    }
    else
    {
        return cardNumber;
    }
}




/*存储BOOL判断的条件*/
+(void)userDefaultsKey:(NSString *)key Value:(BOOL)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:value forKey:key];
    [defaults synchronize];
}

/*根据Key值来判断逻辑*/
+(BOOL)userDefaultsKey:(NSString *)key
{
    BOOL defaults = [[NSUserDefaults standardUserDefaults] boolForKey:key];
    return defaults;
}


/*存储判断NSString的条件*/
+(void)userDefaultsStringKey:(NSString *)key Value:(NSString *)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:value forKey:key];
    [defaults synchronize];
}

/*根据Key值来判断逻辑*/
+(NSString *)userDefaultsStringKey:(NSString *)key
{
    NSString *defaults = [[NSUserDefaults standardUserDefaults] valueForKey:key];
    return defaults;
}



//创建package签名
+(NSString*) createMd5Sign:(NSMutableDictionary*)dict
{
    NSMutableString *contentString  =[NSMutableString string];
    NSArray *keys = [dict allKeys];
    //按字母顺序排序
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    //拼接字符串
    for (NSString *categoryId in sortedArray) {
        if (   ![[dict objectForKey:categoryId] isEqualToString:@""]
            && ![categoryId isEqualToString:@"sign"]
            && ![categoryId isEqualToString:@"key"]
            )
        {
            [contentString appendFormat:@"%@=%@&", categoryId, [dict objectForKey:categoryId]];
        }
        
    }
    
    //添加key字段 商户API密钥
    [contentString appendFormat:@"key=%@", @"8ae1da0fe37c98412768453f82490da2"];
    //得到MD5 sign签名
    NSString *md5Sign =[NewUtils md5:contentString];
    
    
    return md5Sign;
}
//md5 encode
+(NSString *) md5:(NSString *)str
{
    const char *cStr = [str UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (unsigned int)strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02X", digest[i]];
    
    return output;
}


+(NSString *)stringTransformation:(NSString *)string changeType:(NSString *)type
{
    //全部大写
    if ([type isEqualToString:@"A"]) {
        return [string uppercaseString];
    }
    //全部小写
    else if ([type isEqualToString:@"a"]) {
        return [string lowercaseString];
    }
    //开头大写，其余小写
    else if ([type isEqualToString:@"Ab"]) {
        return [string capitalizedString];
    }
    else {
        return string;
    }
}

+ (NSString*)randomCount:(NSInteger)randomCount{
    
    NSInteger count = randomCount;
    char chars[] = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ";
    char codes[count];
    
    for(int i=0;i<count; i++){
        codes[i]= chars[arc4random()%62];
    }
    
    NSString *text = [[NSString alloc] initWithBytes:codes
                                              length:count encoding:NSUTF8StringEncoding];
    return text;
}

+ (NSString *)typeForImageData:(NSData *)data {
    
    
    if (data.length > 4) {
        const unsigned char * bytes = [data bytes];
        
        if (bytes[0] == 0xff &&
            bytes[1] == 0xd8 &&
            bytes[2] == 0xff)
        {
            return @"jpeg";
        }
        
        if (bytes[0] == 0x89 &&
            bytes[1] == 0x50 &&
            bytes[2] == 0x4e &&
            bytes[3] == 0x47)
        {
            return @"png";
        }
    }
    
    return nil;
    
    /*
    uint8_t c;
    
    [data getBytes:&c length:1];
    
    switch (c) {
            
        case 0xFF:
            
            return @"jpeg";
            
        case 0x89:
            
            return @"png";
            
        case 0x47:
            
            return @"gif";
            
        case 0x49:
            return nil;
            
        case 0x4D:
            
            return @"tiff";
            
    }
    
    return nil;
    */
}

+(NSString *)resourcesFileName:(NSString *)fileName ofType:(NSString *)type

{
    
    if ([fileName isEqualToString:@""] || fileName == nil) {
        
        return nil;
        
    }
    
    return [[NSBundle mainBundle] pathForResource:fileName ofType:type];
    
    /*
     提取路径中的文件名
     
     方法一：通过函数
     - (NSString *)displayNameAtPath:(NSString *)path
     需要主意这个函数返回的是文件名称的显示字符串，适当的进行本地化。如果手机中选择的是英语则显示英文名，中文则显示中文名
     
     方法二：lastPathComponent
     - (NSString *)lastPathComponent
     */
    
}


//根据文件名来获取文件路径 取所有母缓存文件路径
+ (NSString *)resourcesFileName:(NSString *)fileName
{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                        NSUserDomainMask,
                                                        YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:fileName];
}



/*
 
//根据文件路径获取文件名称
[[NewUtils resourcesFileName:@"支付宝实名认证"] lastPathComponent]
 
 
//resource目录下 取你自己APP下的缓存路径
+ (NSString *)resourcesFileName3:(NSString *)fileName
{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
}


//Cache目录下
+ (NSString *)resourcesFileName4:(NSString *)fileName
{
    NSArray *cache = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [cache objectAtIndex:0];
    //NSString *voiceName = [cachePath stringByAppendingPathComponent:[sender stringByAppendingString:@".wav"]];
}


_array_FileName=[NSMutableArray array];
_array_FilePath=[NSMutableArray array];
[self getFile];

-(void)getFile{
    NSString *DocPath=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSFileManager *file_manager=[[NSFileManager alloc]init];
    NSArray *subPaths=[file_manager subpathsAtPath:DocPath];
    for (NSString *path in subPaths) {
        NSArray *arry=[path componentsSeparatedByString:@"."];
        NSString *last=[arry lastObject];
        //NSLog(@"filepath:%@  ----- fileName:%@",path,[path lastPathComponent]);
        if ([last isEqualToString:@"pdf"]) {
            [_array_FilePath addObject:[DocPath stringByAppendingPathComponent:path]];
            [_array_FileName addObject:[path lastPathComponent]];
            NSLog(@"filepath:%@  ----- fileName:%@",path,[path lastPathComponent]);
        }
    }
}
*/


//获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

//获取当前屏幕中present出来的viewcontroller
- (UIViewController *)getPresentedViewController
{
    UIViewController *appRootVC = [[UIApplication sharedApplication] keyWindow].rootViewController;
    UIViewController *topVC = appRootVC;
    if (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    
    return topVC;
}

//删除整个共享文件夹Document
+ (void)deleteDocumentFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}


+ (NSString *)iphoneType {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    //simulator
    if ([platform isEqualToString:@"i386"])          return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])        return @"Simulator";
    //iPhone
    if ([platform isEqualToString:@"iPhone1,1"])     return @"IPhone_1G";
    if ([platform isEqualToString:@"iPhone1,2"])     return @"IPhone_3G";
    if ([platform isEqualToString:@"iPhone2,1"])     return @"IPhone_3GS";
    if ([platform isEqualToString:@"iPhone3,1"])     return @"IPhone_4";
    if ([platform isEqualToString:@"iPhone3,2"])     return @"IPhone_4";
    if ([platform isEqualToString:@"iPhone4,1"])     return @"IPhone_4s";
    if ([platform isEqualToString:@"iPhone5,1"])     return @"IPhone_5";
    if ([platform isEqualToString:@"iPhone5,2"])     return @"IPhone_5";
    if ([platform isEqualToString:@"iPhone5,3"])     return @"IPhone_5C";
    if ([platform isEqualToString:@"iPhone5,4"])     return @"IPhone_5C";
    if ([platform isEqualToString:@"iPhone6,1"])     return @"IPhone_5S";
    if ([platform isEqualToString:@"iPhone6,2"])     return @"IPhone_5S";
    if ([platform isEqualToString:@"iPhone7,1"])     return @"IPhone_6P";
    if ([platform isEqualToString:@"iPhone7,2"])     return @"IPhone_6";
    if ([platform isEqualToString:@"iPhone8,1"])     return @"IPhone_6s";
    if ([platform isEqualToString:@"iPhone8,2"])     return @"IPhone_6s_P";
    if ([platform isEqualToString:@"iPhone8,4"])     return @"IPhone_SE";
    if ([platform isEqualToString:@"iPhone9,1"])     return @"IPhone_7";
    if ([platform isEqualToString:@"iPhone9,3"])     return @"IPhone_7";
    if ([platform isEqualToString:@"iPhone9,2"])     return @"IPhone_7P";
    if ([platform isEqualToString:@"iPhone9,4"])     return @"IPhone_7P";
    if ([platform isEqualToString:@"iPhone10,1"])    return @"IPhone_8";
    if ([platform isEqualToString:@"iPhone10,4"])    return @"IPhone_8";
    if ([platform isEqualToString:@"iPhone10,2"])    return @"IPhone_8P";
    if ([platform isEqualToString:@"iPhone10,5"])    return @"IPhone_8P";
    if ([platform isEqualToString:@"iPhone10,3"])    return @"IPhone_X";
    if ([platform isEqualToString:@"iPhone10,6"])    return @"IPhone_X";
    if ([NewUtils getIsIphone]) return @"IPhone_X";
    return @"NUKown";
}
+ (CGFloat)navHeader{
    
    if ([[NewUtils iphoneType] isEqualToString:@"IPhone_X"]) {
        return 88;
    }
    return 64;
}
+ (BOOL)getIsIphone {
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"]) {
        //iPhone
        return YES;
    }
    else if([deviceType isEqualToString:@"iPod touch"]) {
        //iPod Touch
        return NO;
    }
    else if([deviceType isEqualToString:@"iPad"]) {
        //iPad
        return NO;
    }
    return NO;
}
+ (CGFloat)navTABBAR_HEIGHT{
    
    if ([[NewUtils iphoneType] isEqualToString:@"IPhone_X"]) {
        return 83;
    }
    return 49;
}
+(NSString *)getNewStarBankNumWitOldNum:(NSString *)bankCardNumber{
    NSString *bankNum = bankCardNumber;
    NSMutableString *mutableStr;
    if (bankNum.length) {
        mutableStr = [NSMutableString stringWithString:bankNum];
        for (int i = 0 ; i < mutableStr.length; i ++) {
            if (i>3&&i<mutableStr.length - 4) {
                [mutableStr replaceCharactersInRange:NSMakeRange(i, 1) withString:@"*"];
            }
        }
        NSString *text = mutableStr;
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        
        NSString *newString = @"";
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        return newString;
    }
    return bankNum;
}
@end
