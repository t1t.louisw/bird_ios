//
//  UserHelper.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "UserHelper.h"

@implementation UserHelper


//#pragma mark - 判断是否登录
+ (BOOL)isLogin
{
    if ([UserEntity sharedInstance].playerName && [UserEntity sharedInstance].playerName.length > 0) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark - 退出登录
+ (void)exitLogin
{
    //清除用户信息
    [self removeUserInfoFromLocation];
}


#pragma mark - 缓存用户信息到本地
+ (void)cacheUserInfoFromLocation
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[UserEntity sharedInstance].token forKey:NewToken];
    [defaults setValue:[UserEntity sharedInstance].playerName forKey:NewAccountNumber];
    [defaults setValue:[UserEntity sharedInstance].playerId forKey:NewplayerId];
    [defaults setValue:[UserEntity sharedInstance].firstName forKey:NewfirstName];
    [defaults setValue:[UserEntity sharedInstance].password forKey:Newpassword];
    
    [defaults setValue:[UserEntity sharedInstance].mima forKey:NewMima];
    [defaults setValue:[UserEntity sharedInstance].zhanghao forKey:NewZhanghao];

    

    [defaults synchronize];
    NSLog(@"%@",[NewUtils userDefaultsStringKey:NewAccountNumber]);
    NSLog(@"%@",[NewUtils userDefaultsStringKey:NewplayerId]);
    NSLog(@"%@",[NewUtils userDefaultsStringKey:NewToken]);
    NSLog(@"%@",[NewUtils userDefaultsStringKey:NewfirstName]);
    NSLog(@"%@",[NewUtils userDefaultsStringKey:Newpassword]);

}


#pragma mark - 回写用户信息到用户信息单利类
+ (void)writeUserInfoFromLocation
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [UserEntity sharedInstance].token = [defaults valueForKey:NewToken];
    [UserEntity sharedInstance].playerName = [defaults valueForKey:NewAccountNumber];
    [UserEntity sharedInstance].playerId = [defaults valueForKey:NewplayerId];
    [UserEntity sharedInstance].firstName = [defaults valueForKey:NewfirstName];
    [UserEntity sharedInstance].password = [defaults valueForKey:Newpassword];
    

    [UserEntity sharedInstance].zhanghao = [defaults valueForKey:NewZhanghao];
    [UserEntity sharedInstance].mima = [defaults valueForKey:NewMima];

    NSLog(@"111");
    NSLog(@"%@",[UserEntity sharedInstance].token);
    NSLog(@"%@",[UserEntity sharedInstance].playerName);
    NSLog(@"%@",[UserEntity sharedInstance].playerId);
    NSLog(@"%@",[UserEntity sharedInstance].firstName);
}


#pragma mark - 移除单利用户信息以及本地用户信息
+ (void)removeUserInfoFromLocation
{
    [UserEntity sharedInstance].token = @"";
    [UserEntity sharedInstance].playerName = @"";
    [UserEntity sharedInstance].playerId = @"";
    [UserEntity sharedInstance].firstName = @"";
    [UserEntity sharedInstance].password = @"";

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NewToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NewAccountNumber];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NewplayerId];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:NewfirstName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Newpassword];

}

@end
