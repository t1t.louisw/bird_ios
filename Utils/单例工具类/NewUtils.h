//
//  NewUtils.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include "UserEntity.h"

@interface NewUtils : NSObject

/*
+(void)setAccount:(NSString*)account;//账号
+(void)setPassword:(NSString*)password;//密码
+(void)setAuthorid:(NSString*)authorid;//账号ID
+(void)setUserName:(NSString*)userName;//用户名称
+(void)setUserHeadImage:(NSString*)userHeadImage;//用户头像
+(void)set_UDID:(NSString*)udid;//UDID
+(void)set_Appkey:(NSString *)appkey;//验证码
+(void)set_reg_time:(NSString *)time;//注册时间
+(void)set_mobile_phone:(NSString *)phone;//手机号

+(void)setShoppingCartNumber:(NSString *)number;//购物车数量
+(void)setShoppingCartLogin:(BOOL)flag;//购物车跳转登录


+(NSString*)getAccount;//账号
+(NSString*)getPassword;//密码
+(NSString*)getAuthorid;//账号ID
+(NSString*)getUserName;//用户名称
+(NSString*)getUserHeadImage;//用户头像
+(NSString*)get_UDID;//UDID
+(NSString*)get_Appkey;//验证码
+(NSString*)get_reg_time;//注册时间
+(NSString*)get_mobile_phone;//手机号

+(NSString *)getShoppingCartNumber;//购物车数量
+(BOOL)getShoppingCartLogin;//购物车跳转登录

+(NSString*)get_req_time;//请求接口的实时时间
+(NSString*)get_publicInterface_time;//公用接口的请求时间
*/
 
+(BOOL)checkBankCard:(NSString*)bank;//判断银行卡
+(BOOL)checkIdentity:(NSString*)identity;//判断身份证
+(BOOL)checkMobilePhone:(NSString*)mobilePhone;//判断电话号码
+(BOOL)checkInterNum:(NSString*)num;//判断数字
+(BOOL)checkName:(NSString*)name;//判断姓名
+(BOOL)checkPassword:(NSString*)password;//判断密码 特殊符号跟中文不通过
+(BOOL)checkPassword2:(NSString *)password;//判断密码 进支持数字、字母、下划线 其它不通过
+(BOOL)cheeckNotChina:(NSString*)number;//判断特殊符号
+(BOOL)checkEmail:(NSString*)email;//判断邮箱

/*验证正则表达式*/
+ (BOOL)matchRegularExpression:(NSString*)text match:(NSString*)match;

/**
 @method 获取指定宽度width,字体大小fontSize,字符串value的高度
 @param value 待计算的字符串
 @param fontSize 字体的大小
 @param width 限制字符串显示区域的宽度
 @result float 返回的高度
 */
+ (CGFloat) heightforString:(NSString *)value andWidth:(CGFloat)width fontSize:(CGFloat)fontSize;
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height fontSize:(CGFloat)fontSize;
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height XfontSize:(CGFloat)XfontSize;
+ (CGFloat) heightforString:(NSString *)value andWidth:(CGFloat)width AutofontSize:(CGFloat)AutofontSize;
+ (CGFloat) heightforString:(NSString *)value andHeight:(CGFloat)height AutofontSize:(CGFloat)AutofontSize;

/*类型转换*/
+(NSString*) dataToString:(NSData*)data;
+(NSData*) stringToData:(NSString*)string;

/*时间转化*/
+ (NSDateComponents *)componentsOfDate:(NSDate *)date;

/*转化日期 精确到秒 NSDate - NSString*/
+ (NSString *)dateToFromString:(NSDate *)date;

/*转化日期 精确到秒 NSString - NSDate*/
+ (NSDate *)stringToFromDate:(NSString *)string;

/*转化日期 精确到天 NSString - NSDate*/
+ (NSDate *)stringToFromDateDay:(NSString *)string;

/*返回1970年距离至今的时间差*/
+ (NSString *)returnTime;

/*返回两个时间的时间差*/
+ (NSString *)returnTimeDifference:(NSString *)starTime endTiem:(NSString *)endTime;

/*时间转换 精确到天*/
+ (NSString *)returnTransformationTime:(NSString *)string;

/*时间转换 精确到秒*/
+ (NSString *)returnTransformationDetailedTime:(NSString *)string;

/*获取当前时间 年、月、日、分、秒*/
+ (NSString *)returnDetailedTime:(NSString *)string;

/*判断时间大小*/
+(int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;

/*压图片质量和缩小图片大小相结合*/
+ (NSData *)zipImageWithImage:(UIImage *)image;

/*图片压缩到指定大小*/
+ (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize withSourceImage:(UIImage *)sourceImage;

/*图片质量进行压缩*/
- (NSData *)returnsTheSizeAccordingToThePicture:(UIImage *)image;

/*图片拉伸处理*/
+(UIImage*)stretchImage:(UIImage *)img edgeInsets:(UIEdgeInsets)inset;

/*获取图片格式*/
+ (NSString *)typeForImageData:(NSData *)data;

/*根据文件名称+格式来返回文件路径*/
+(NSString *)resourcesFileName:(NSString *)fileName ofType:(NSString *)type;
+ (NSString *)resourcesFileName:(NSString *)fileName;
//+ (NSString *)resourcesFileName3:(NSString *)fileName;
//+ (NSString *)resourcesFileName4:(NSString *)fileName;
//+ (NSString *)resourcesFileName5:(NSString *)fileName;

/*判断数字输入错误的集合*/
+(NSInteger)doTextFieldDelegate:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

/*弹框*/
+ (void)alertWithMsg:(NSString*)msg delegate:(id<UIAlertViewDelegate>)sender andTag:(NSInteger)tag title:(NSString *)title cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles1:(NSString *)otherButtonTitles1 otherButtonTitles2:(NSString *)otherButtonTitles2 otherButtonTitles3:(NSString *)otherButtonTitles3 otherButtonTitles4:(NSString *)otherButtonTitles4 otherButtonTitles5:(NSString *)otherButtonTitles5 otherButtonTitles6:(NSString *)otherButtonTitles6;


/*拨打电话*/
+(void)callTel:(NSString*)tel;

/*iphone/iPad类型及适配*/
//+(NSString *) simplePlatformString;

/*对一段字符串的中间部分进行隐藏 以***代替*/
+(NSString *)formatCardNumber:(NSString *)cardNumber;

/*存储判断BOOL的条件*/
+(void)userDefaultsKey:(NSString *)key Value:(BOOL)value;
/*根据Key值来判断逻辑*/
+(BOOL)userDefaultsKey:(NSString *)key;
/*存储判断NSString的条件*/
+(void)userDefaultsStringKey:(NSString *)key Value:(NSString *)value;
/*根据Key值来判断逻辑*/
+(NSString *)userDefaultsStringKey:(NSString *)key;

+(NSString*) createMd5Sign:(NSMutableDictionary*)dict;
+(NSString *) md5:(NSString *)str;

/*大小写转换*/
+(NSString *)stringTransformation:(NSString *)string changeType:(NSString *)type;

//随机数
+ (NSString*)randomCount:(NSInteger)count;

//删除整个共享文件夹Document
+ (void)deleteDocumentFolder;

//获取导航高度
+ (CGFloat)navHeader;
+ (CGFloat)navTABBAR_HEIGHT;
+(NSString *)getNewStarBankNumWitOldNum:(NSString *)bankCardNumber;


@end
