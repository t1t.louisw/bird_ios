//
//  UserHelper.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserHelper : NSObject


/**
 *  判断是否登录
 */
+ (BOOL)isLogin;


/**
 *  自动登录
 */
//+ (void)automaticLogon;


/**
 *  退出登录
 */
+ (void)exitLogin;


/**
 *  缓存用户信息到本地
 */
+ (void)cacheUserInfoFromLocation;


/**
 *  回写用户信息到用户信息单利类
 */
+ (void)writeUserInfoFromLocation;


/**
 *  移除单利用户信息以及本地用户信息
 */
+ (void)removeUserInfoFromLocation;


@end
