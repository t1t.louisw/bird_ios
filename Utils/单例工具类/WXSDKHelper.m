//
//  WXSDKHelper.m
//  MingMen
//
//  Created by 德玛西亚 on 2017/8/18.
//  Copyright © 2017年 德玛西亚. All rights reserved.
//

#import "WXSDKHelper.h"
//#import "NewLoginViewController.h"
@implementation WXSDKHelper

static WXSDKHelper *sharedObj = nil;

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedObj = [[super allocWithZone: NULL] init];
    });
    return sharedObj;
}

+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [self sharedInstance];
}

- (id) copyWithZone:(NSZone *) zone
{
    return self;
}

#pragma mark - 初始化微信SDK
-  (void)setWXSDK
{
    [WXApi registerApp:weixinAPPID enableMTA:YES];
   
    //向微信注册支持的文件类型 例如word/pdf之类的  暂时没用到
    //UInt64 typeFlag = MMAPP_SUPPORT_TEXT | MMAPP_SUPPORT_PICTURE | MMAPP_SUPPORT_LOCATION | MMAPP_SUPPORT_VIDEO |MMAPP_SUPPORT_AUDIO | MMAPP_SUPPORT_WEBPAGE | MMAPP_SUPPORT_DOC | MMAPP_SUPPORT_DOCX | MMAPP_SUPPORT_PPT | MMAPP_SUPPORT_PPTX | MMAPP_SUPPORT_XLS | MMAPP_SUPPORT_XLSX | MMAPP_SUPPORT_PDF;
    
    //[WXApi registerAppSupportContentFlag:typeFlag];
}


#pragma mark - 调用微信分享API
- (void)wxShare:(NSDictionary *)shareDictionary
{    
    //创建发送对象实例
    SendMessageToWXReq *sendReq = [[SendMessageToWXReq alloc] init];
    sendReq.bText = NO;//不使用文本信息
    sendReq.scene = [shareDictionary[@"scene"] intValue];//0 = 好友列表 1 = 朋友圈 2 = 收藏
    
    //创建分享内容对象
    WXMediaMessage *urlMessage = [WXMediaMessage message];
    urlMessage.title = shareDictionary[@"title"];//分享标题
    urlMessage.description = shareDictionary[@"description"];//分享描述
    
    UIImage *image = shareDictionary[@"image"]!=nil?shareDictionary[@"image"]:NewImageError_J;
    [urlMessage setThumbImage:image];//分享图片,使用SDK的setThumbImage方法可压缩图片大小
    
    //创建多媒体对象
    WXWebpageObject *webObj = [WXWebpageObject object];
    webObj.webpageUrl = [shareDictionary[@"webpageUrl"] length]>0?shareDictionary[@"webpageUrl"]:NewShareURL;//分享链接
    
    //完成发送对象实例
    urlMessage.mediaObject = webObj;
    sendReq.message = urlMessage;
    
    //发送分享信息
    [WXApi sendReq:sendReq];
}


#pragma mark - 调用微信支付API
- (void)wxPay:(NSDictionary *)orderDictionary
{
    //已安装较新版本的微信
    if([WXApi isWXAppInstalled] == YES && [WXApi isWXAppSupportApi] == YES)
    {
        //NSString    *appid, *partnerId, *prepayId;//这三个字段需要我们自己的服务器配置
        //NSString    *time_stamp, *nonce_str;
        //设置支付参数
        //time_t now;
        //time(&now);
        //time_stamp  = [NSString stringWithFormat:@"%ld", now];
        //nonce_str	= [time_stamp md5];
        
        PayReq *request = [[PayReq alloc] init];
        //微信号和AppID组成的唯一标识
        request.openID = orderDictionary[@"appId"];
        //商户号
        request.partnerId = orderDictionary[@"partnerId"];
        //预支付订单ID
        request.prepayId = orderDictionary[@"prepayId"];
        //商家 财付通签名 这个是固定写死
        request.package = orderDictionary[@"package"];//@"Sign=WXPay";
        //随机串
        request.nonceStr = orderDictionary[@"nonceStr"];//nonce_str;
        //请求时间
        request.timeStamp = [orderDictionary[@"timeStamp"] intValue];//[time_stamp intValue];
        
        /*
        //商家 微信开发平台签名 64F0562166BFA93EC49F05C70E587ABB 9C6819BB8D3D88DDD4FF7A0927FB7315
        // 构造参数列表
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:orderDictionary[@"appId"] forKey:@"appid"];
        //[params setObject:request.openID forKey:@"openID"];
        [params setObject:orderDictionary[@"nonceStr"] forKey:@"noncestr"];
        [params setObject:@"Sign=WXPay" forKey:@"package"];
        [params setObject:orderDictionary[@"partnerId"] forKey:@"partnerid"];
        [params setObject:orderDictionary[@"timeStamp"] forKey:@"timestamp"];
        [params setObject:orderDictionary[@"prepayId"] forKey:@"prepayid"];
        */
        
        request.sign = orderDictionary[@"sign"];//[NewUtils createMd5Sign:params];
        
        NSLog(@"\n商户号:%@\n预支付订单ID:%@\n商家签名:%@\n随机字符串:%@\n请求时间:%u\nSign签名:%@",request.partnerId,request.prepayId,request.package,request.nonceStr,(unsigned int)request.timeStamp,request.sign);
        
        [WXApi sendReq:request];
        
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"请去AppStore下载最新版本微信APP"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - 调用微信支付API
- (void)wxlog:(NSDictionary *)orderDictionary
{
    //已安装较新版本的微信
    if([WXApi isWXAppInstalled] == YES && [WXApi isWXAppSupportApi] == YES)
    {
                SendAuthReq *req = [[SendAuthReq alloc] init];
                req.state = @"App";//用于保持请求和回调的状态，授权请求或原样带回
                req.scope = @"snsapi_userinfo";//授权作用域：获取用户个人信息
            
                [WXApi sendReq:req];//发起微信授权请求
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"请去AppStore下载最新版本微信APP"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - 下面三个方法都是处理微信通过URL启动App时传递的数据
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  [WXApi handleOpenURL:url delegate:self];
}

// NOTE: iOS9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    return [WXApi handleOpenURL:url delegate:self];
}


// NOTE: iOS9.0之前使用旧API接口
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [WXApi handleOpenURL:url delegate:self];
}



//- (BOOL)application:(UIApplication *)application
//continueUserActivity:(NSUserActivity *)userActivity

#pragma mark - 微信回调方法
-(void)onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[SendAuthResp class]]){//判断是否为授权登录类

        SendAuthResp *req = (SendAuthResp *)resp;

        if([req.state isEqualToString:@"App"]){//微信授权成功

//            req.code //获得code
            NSLog(@"%@",req.code);
            _NewWXlogSdkCallback(req.code);
        }
    }
    //微信支付回调
    if ([resp isKindOfClass:[PayResp class]])
    {
        PayResp *response = (PayResp *)resp;
        
        switch (response.errCode) {
            case WXSuccess: {
                
//                _NewWXSdkCallback?_NewWXSdkCallback([NSString stringWithFormat:@"%d",response.errCode]):nil;
                _NewWXSdkCallback(@"成功");
            
                break;
            }
                /*
                 case WXErrCodeCommon: {
                 //普通错误类型
                 break;
                 }
                 case WXErrCodeUserCancel: {
                 //用户点击取消并返回
                 break;
                 }
                 case WXErrCodeSentFail: {
                 //发送失败
                 break;
                 }
                 case WXErrCodeAuthDeny: {
                 //授权失败
                 break;
                 }
                 case WXErrCodeUnsupport: {
                 //微信不支持
                 break;
                 }
                 */
            default: {
                
//                _NewWXSdkCallback?_NewWXSdkCallback([NSString stringWithFormat:@"%d",response.errCode]):nil;
                _NewWXSdkCallback(@"失败");
                
                break;
            }
        }
    }
}





@end
