//
//  NewInterfaceReplacement.m
//  MingMen
//
//  Created by NewProject on 2017/5/19.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NewInterfaceReplacement.h"
#import "HomeRootVC.h"
#import "LeftSortsViewController.h"

@implementation NewInterfaceReplacement

static NewInterfaceReplacement *sharedObj = nil;

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedObj = [[super allocWithZone: NULL] init];
    });
    return sharedObj;
}

+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [self sharedInstance];
}

- (id) copyWithZone:(NSZone *) zone
{
    return self;
}

- (void)replacementLogin
{
    //删除用户单例类以及本地缓存保存的数据
    [UserHelper exitLogin];
    
    //销毁之前的单利对象
    [NewBasicTabbarController objectDealloc];
    
    //跳转登录界面
//    NewLoginViewController *startVc = [[NewLoginViewController alloc] init];
//    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:startVc];
////    [AppDelegate shareDelegate].window.rootViewController = navi;
////    UIViewController *root = [UIApplication sharedApplication].keyWindow.rootViewController;
//    [navi presentViewController:startVc animated:YES completion:nil];
//    [SVProgressHUD showWithStatus:@"请先登录"];

    
//    [SVProgressHUD dismissWithDelay:2.0 completion:^{
//        UIViewController *topRootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
//        while (topRootViewController.presentedViewController)
//        {
//        topRootViewController = topRootViewController.presentedViewController;
//        }
////        NewLoginViewController *loginVC = [NewLoginViewController new];
//        UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:loginVC];
////        navc.modalPresentationStyle = UIModalPresentationFullScreen;
//        
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请您先登录" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            [topRootViewController.navigationController popViewControllerAnimated:YES];
//        }];
//        UIAlertAction *skipAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [topRootViewController presentViewController:navc animated:YES completion:nil];
//        }];
//        [alertController addAction:cancelAction];
//        [alertController addAction:skipAction];
//        [topRootViewController presentViewController:alertController animated:YES completion:nil];
//    }];

}

- (void)replacementController
{
    //进入程序主控制界面
//    [[AppDelegate shareDelegate].window setRootViewController:[NewBasicTabbarController sharedInstance]];
    HomeRootVC *mainVC = [[HomeRootVC alloc] init];
    self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:mainVC];
    LeftSortsViewController *leftVC = [[LeftSortsViewController alloc] init];
    self.LeftSlideVC = [[LeftSlideViewController alloc] initWithLeftView:leftVC andMainView:self.mainNavigationController];
    [AppDelegate shareDelegate].window.rootViewController = self.LeftSlideVC;
}


@end
