//
//  WXSDKHelper.h
//  MingMen
//
//  Created by 德玛西亚 on 2017/8/18.
//  Copyright © 2017年 德玛西亚. All rights reserved.
//

#import <Foundation/Foundation.h>
//微信支付、分享
#import "WXApi.h"

@interface WXSDKHelper : NSObject<WXApiDelegate>

/*!
 * @brief 微信相关API封装
 * @author 德玛西亚
 */

+ (instancetype)sharedInstance;


// NOTE: 支付回调
//typedef void (^NewWXpayCallback) (NSString *string);
@property(nonatomic,copy)void (^NewWXSdkCallback)(NSString *string);
@property(nonatomic,copy)void (^NewWXlogSdkCallback)(NSString *string);

// NOTE: 注册微信
- (void)setWXSDK;

// NOTE: 回调方法
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url;

// NOTE: iOS9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options;

// NOTE: iOS9.0之前使用旧API接口
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

// NOTE: 调用支付API
- (void)wxPay:(NSDictionary *)orderDictionary;

// NOTE: 调用分享API
- (void)wxShare:(NSDictionary *)shareDictionary;

// NOTE: 调用登录API
- (void)wxlog:(NSDictionary *)shareDictionary;

@end
