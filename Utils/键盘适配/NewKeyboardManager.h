//
//  NewKeyboardManager.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewKeyboardManager : NSObject

+ (instancetype)sharedInstance;
- (void)keyboardManager;

@end
