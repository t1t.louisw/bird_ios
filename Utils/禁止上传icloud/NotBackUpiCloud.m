//
//  NotBackUpiCloud.m
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#import "NotBackUpiCloud.h"

#import "sys/xattr.h"                   //设置禁止云同步

@implementation NotBackUpiCloud

static NotBackUpiCloud *sharedObj = nil;

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedObj = [[super allocWithZone: NULL] init];
    });
    return sharedObj;
}

+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [self sharedInstance];
}

- (id) copyWithZone:(NSZone *) zone
{
    return self;
}

/*
 *写个宏方法 document内的文件夹设置不备iCloud备份
 
#define AddSkipBackupAttributeToItemAtPath(filePathString) \
{ \
NSURL* URL= [NSURL fileURLWithPath: filePathString]; \
if([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]){ \
[URL setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: nil]; \
} \
}

在需要的地方这样调用
AddSkipBackupAttributeToItemAtPath(MAP_FILE_DATA_PATH);
*/

#pragma mark - 禁止上传icloud
- (void)addNotBackUpiCloud{
    
    //被注释掉的是遍历NSDocumentDirectory目录下所有文件不被备份上传 但是有个隐患就是如果这个方法在没被更新之前又新增的文件还是被上传到iCloud
    //为此我把整个NSDocumentDirectory文件夹都设置成不能备份上传到iCloud
    //还有补充一点NSLibraryDirectory目录本身就不会被备份上传到iCloud
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString* docPath = [documentPaths objectAtIndex:0];
    NSURL *filePath = [NSURL fileURLWithPath:docPath];
    [self addSkipBackupAttributeToItemAtURL:filePath];
    
    /*
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSArray *libPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    
    
    NSString *docPath = [docPaths objectAtIndex:0];
    
    NSString *libPath = [libPaths objectAtIndex:0];
    
    [self fileList:docPath];
    
    [self fileList:libPath];
    */
}

/*
- (void)fileList:(NSString*)directory{
    
    NSError *error = nil;
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    
    NSArray *fileList = [fileManager contentsOfDirectoryAtPath:directory error:&error];
    
    for (NSString* each in fileList) {
        
        NSMutableString* path = [[NSMutableString alloc]initWithString:directory];
        
        [path appendFormat:@"/%@",each];
        
        NSURL *filePath = [NSURL fileURLWithPath:path];
        
        [self addSkipBackupAttributeToItemAtURL:filePath];
        
        [self fileList:path];
        
    }
    
}
*/


#pragma mark - 设置禁止云同步
- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL{
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES] forKey:NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
    
    /*
    double version = [[UIDevice currentDevice].systemVersion doubleValue];//判定系统版本。
    
    if(version >=5.1f){
        
        NSError *error = nil;
        
        BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                        
                                      forKey: NSURLIsExcludedFromBackupKey error: &error];
        
        if(!success){
            
            NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
            
        }
        
        return success;
        
    }
    
    const char* filePath = [[URL path] fileSystemRepresentation];
    
    const char* attrName = "com.apple.MobileBackup";
    
    u_int8_t attrValue = 1;
    
    
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    
    return result == 0;
    */
}



@end
