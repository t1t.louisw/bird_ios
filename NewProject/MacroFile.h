//
//  MacroFile.h
//  MingMen
//
//  Created by NewProject on 2017/8/18.
//  Copyright © 2017年 NewProject. All rights reserved.
//

#ifndef MacroFile_h
#define MacroFile_h


#endif /* MacroFile_h */





/*
 -----------------------------------------------------------------------------------
 ------------------------环境切换  版本号  客服电话  客户端来源---------------------------
 -----------------------------------------------------------------------------------
 */

#define NewAppID            @"" //服务平台每个项目的appKey 签名密钥  开发环境

#define NewDeviceClass      @"iphone" //客户端来源

#define NewSecurityCode     @"" //安全码

#define NewVersionNumber    @"1.0" //当前项目版本号

#define NewServiceTelephone @"" //服务电话

#define NewMailbox          @"" //邮箱

#define NewShareURL         @"" //微信分享的默认链接使用

#define NewImageFileName    @"file" //服务器存放图片文件名称

#define SERVER_URL         @"http://player.dj002.t1t.in/api/player_center/" //生产环境

#define NewSERVER_URL         @"http://player.dj002.t1t.in" //

#define DX_URL         @"https://cloud.fhi365.cn" //短信接口地址

//#define SERVER_URL         @"http://192.168.0.115:8080" //通用开发环境


//http://192.168.0.111:8080/user/login4IOS

#define Log_SERVER_URL     @"" //这有在这个IP的情况下并且将日志开关打开才能打印接口地址

#define NewLogSwitch        @"1"//日志打印开关，0是关，1是开

#define weixinAPPID     @"wx8b18955b9556dc27"

#define ApiKey   @"ea443b05c7067089bd2716f47257ee73"
//#define SERVER_URL         @"" //发布环境

//#define SERVER_HTML5_URL   @"" //H5开发环境
//#define SERVER_HTML5_URL   @"" //H5发布环境

//#define OSSUpload_URL      @"" //上传图片/视屏/文件URL

//苹果内购 沙盒测试环境验证
//#define Pay_SANDBOX        @"https://sandbox.itunes.apple.com/verifyReceipt"
//苹果内购 正式环境验证
//#define Pay_AppStore       @"https://buy.itunes.apple.com/verifyReceipt"


/*
 -----------------------------------------------------------------------------------
 --------------------------------------用户信息--------------------------------------
 -----------------------------------------------------------------------------------
 */
#define NewAccountNumber    @"newAccountNumber" //账号
#define NewToken            @"newToken" //令牌 登录有效期
#define NewfirstName             @"newfirstName" //
#define NewplayerId             @"newplayerId" //
#define Newpassword             @"newpassword" //


#define NewZhanghao             @"NewZhanghao" //
#define NewMima                  @"NewMima" //

/*
 -----------------------------------------------------------------------------------
 -------------------------------------工具类key值------------------------------------
 -----------------------------------------------------------------------------------
 */

//项目版本key
#define NewAPPVersion                        @"NewAPPVersion"
#define NewLocationCue                       @"NewLocationCue"

//订单管理通知Key
#define NewOrderManageNotification           @"NewOrderManageNotification"

//订单推送通知Key
#define NewSibenServerPublicNotification     @"NewSibenServerPublicNotificationKey"



/*
 -----------------------------------------------------------------------------------
 ---------------------------------第三方SDK对应的key值--------------------------------
 -----------------------------------------------------------------------------------
 */

//微信
#define     WXKey            @""

//极光推送
#define     JPushKey          @""
#define     JPushChannel      @""//发布渠道. 可选.
#define     JPushIsProduction 1    //是否生产环境. 如果为开发状态,设置为 NO; 如果为生产状态,应改为 YES.


/*
 -----------------------------------------------------------------------------------
 --------------------------------------Java接口标识--------------------------------------
 -----------------------------------------------------------------------------------
 */
#define     createPlayer          @"createPlayer"//注册接口

#define     smsRegCreatePlayer          @"smsRegCreatePlayer"//注册接口

#define     isPlayerExist          @"isPlayerExist"//檢查玩家存在與否

#define     login          @"login"//登录接口
#define     logout          @"logout"//退出登录接口
#define     getPlayerProfile          @"getPlayerProfile"//取得玩家信息
#define     listPlayerWithdrawAccounts          @"listPlayerWithdrawAccounts"//查询玩家取款银行卡
#define     queryDepositBank          @"queryDepositBank"//查询玩家存款银行卡


#define     manualWithdraw          @"manualWithdraw"//人工取款
#define     queryPlayerBalance          @"queryPlayerBalance"//读取余额

#define     updatePlayerProfile          @"updatePlayerProfile"//修改玩家信息
#define     depositPaymentCategories          @"depositPaymentCategories"//存款支付分類列表
#define     thirdPartyDepositForm          @"thirdPartyDepositForm"//第三方支付

#define     thirdPartyDepositRequest          @"thirdPartyDepositRequest"//

#define     manualDepositForm          @"manualDepositForm"//
#define     manualDepositRequest          @"manualDepositRequest"//
#define     queryDepositWithdrawalAvailableBank          @"queryDepositWithdrawalAvailableBank"//

#define     addPlayerDepositAccount          @"addPlayerDepositAccount"//增加存款银行卡

#define     addPlayerWithdrawAccount          @"addPlayerWithdrawAccount"//增加取款银行卡
#define     getPlayerReports          @"getPlayerReports"//查詢玩家報表
#define     listPromos          @"listPromos"//列出玩家可用優惠
#define     applyPromo          @"applyPromo"//玩家申請優惠
#define     addMessage          @"addMessage"//發送站內信息
#define     Newmessage          @"message"//讀取站內信息
#define     messageSetRead          @"messageSetRead"//

#define     updatePlayerPassword          @"updatePlayerPassword"//修改玩家密碼
#define     updatePlayerWithdrawalPassword          @"updatePlayerWithdrawalPassword"//更改玩家取款密碼

#define     listGamePlatforms          @"listGamePlatforms"//列出系統內已接入的遊戲平台

#define     listGamesByPlatform          @"listGamesByPlatform"//列出遊戲平台下的遊戲大廳網址，或遊戲種類，可用於進一步查詢。

#define      listGamesByPlatformGameType          @"listGamesByPlatformGameType"

#define      transfer          @"transfer"

#define      getPlayerVipStatus          @"getPlayerVipStatus"


#define      smsRegSendSms          @"smsRegSendSms"

#define      passwordRecovSmsSend          @"passwordRecovSmsSend"

#define      passwordRecovSmsRecv          @"passwordRecovSmsRecv"

/*
 -----------------------------------------------------------------------------------
 --------------------------------------PHP接口标识--------------------------------------
 -----------------------------------------------------------------------------------
 */


/*
 -----------------------------------------------------------------------------------
 --------------------------------------常用的宏--------------------------------------
 -----------------------------------------------------------------------------------
 */


//第三方代码计算出的获取唯一设备标识符
#define NewUDID [OpenUDID value]

//短信验证码时长
#define SMSCodeTime 60.0f

//WebView加载超时计时时间
#define WebViewOverTimeMark 60.0f

//录音音量 1~10
#define NewAudioVolume  10.0f

//轮播图片时长
#define CarouselTime    3.0f

//普通轮播图片高度
#define headwheelHeight fitScreenHeight(179)

#define homeHeadImageHEIGHT (((SCREEN_WIDTH * 500 ) / 1080)-36) //首页轮播图片高度

/**
 *  Version
 */
#define ISIOS7 ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)//系统版本号
#define isPad  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)//判断iPhone和iPad

/**
 *  屏幕高度
 */
#define NavHeader [NewUtils navHeader]

#define SCREEN_WIDTH            [[UIScreen mainScreen] bounds].size.width  //屏幕宽度
#define SCREEN_HEIGHT           [[UIScreen mainScreen] bounds].size.height //屏幕高度
#define NAV_HEIGHT              44.0        //导航栏高
#define STATUSBAR_HEIGHT        NavHeader == 64?20:44        //状态栏
#define TABBAR_HEIGHT           [NewUtils navTABBAR_HEIGHT]  //菜单栏高

#define SLIDER_HEIGHT           44.0        //滑动条导航栏高度
#define ViewStartY              (NAV_HEIGHT+STATUSBAR_HEIGHT) //导航条高度
#define SELF_VIEW_HEIGHT        (SCREEN_HEIGHT-ViewStartY) //不算导航条的高度
#define fitScreenWidth(width)          width*(SCREEN_WIDTH/375)
#define fitScreenHeight(height)        height*(SCREEN_HEIGHT/667)

#define ICONS_WIDTH             16.0        //小图标宽
#define ICONS_HEIGHT            22.0        //小图标高
#define SLCommonButton_WIDTH        (SCREEN_WIDTH-100)  //常用按钮宽度
#define SLCommonButton_HEIGHT       fitScreenHeight(42) //常用按钮高度
#define SLCommonButton_Radius       fitScreenHeight(6)  //常用按钮切角
#define SL_OrderCellTitle_HEIGHT    fitScreenHeight(50) //订单标题高度
#define SL_OrderCellText_HEIGHT     fitScreenHeight(44) //订单详细高度

//弹窗Frame
#define PopupOneHeaderViewHeight fitScreenHeight(44)
#define PopupOneCellHeight fitScreenHeight(40)
#define PopupOneTextFiledHeight fitScreenHeight(50)
#define PopupOneX fitScreenHeight(60)
#define PopupOneWIDTH (SCREEN_WIDTH-(PopupOneX*2))

/**
 *  自定义颜色
 */
#define NewRGBColor(R,G,B,A)    [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]//16进制颜色值
//iOS10 苹果官方提供的sRGB
#define NewS1RGBColor(R,G,B,A)  [UIColor colorWithDisplayP3Red:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
#define NewS2RGBColor(R,G,B,A)  [UIColor initWithDisplayP3Red:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]
//+ (UIColor *)colorWithDisplayP3Red:(CGFloat)displayP3Red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha NS_AVAILABLE_IOS(10_0);
//- (UIColor *)initWithDisplayP3Red:(CGFloat)displayP3Red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha NS_AVAILABLE_IOS(10_0);
//16进制颜色转换
//#define UIColorFromRGBA(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define NewClearColor         [UIColor clearColor] //无
#define NewWhiteColor         [UIColor whiteColor] //白
#define NewBlack2Color        [UIColor blackColor] //黑
#define NewPurpleColor        [UIColor purpleColor]//紫
#define NewRedColor           [UIColor redColor]   //红
#define NewYellowColor        [UIColor yellowColor]//黄
#define NewOrangeColor        [UIColor orangeColor]//橙
#define NewBlueColor          [UIColor blueColor]//蓝
#define NewLightGrayColor     [UIColor lightGrayColor]//深灰
//#define NewCellLineColor      NewRGBColor(200, 199, 204, 1) //单元格的Cell横线颜色 宽度约为0.6
#define NewCellLineColor    [UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] //单元格的Cell横线颜色 宽度约为0.6
#define NewIphoneNumberColor  NewRGBColor(0, 136, 232, 1) //电话字体颜色
#define NewGroupTableViewBackgroundColor [UIColor groupTableViewBackgroundColor] //浅灰

#define NewButtonColor        [UIColor colorWithString:@"#1278D0"] //常用按钮浅橙色  UIColorFromRGBA(0xff9626)
#define NewNavigationColor    [UIColor colorWithRed:230.0/255.0 green:130.0/255.0 blue:30.0/255.0 alpha:1.0] //导航条橙色  UIColorFromRGBA(0xf8c392)
#define NewThemeColor         [UIColor colorWithRed:239.0/255.0 green:51.0/255.0 blue:83.0/255.0 alpha:1.0]
#define NewLineGrayColor      [UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0] //灰线
#define NewBgGrayColor        [UIColor colorWithRed:239.0/255 green:239.0/255 blue:244.0/255 alpha:1.0] //灰色背景
#define NewGrayColor          [UIColor colorWithRed:137/255.0 green:137/255.0 blue:137/255.0 alpha:1.0] //灰 常用
#define NewBlackColor         [UIColor blackColor] //黑 常用
#define NewTabBarColor        [UIColor colorWithRed:173/255.0 green:172/255.0 blue:180/255.0 alpha:1.0] //菜单栏浅灰
#define NewDarkGrayColor      [UIColor colorWithRed:46/255.0 green:46/255.0 blue:46/255.0 alpha:1.0] //深灰
#define NewImgBgColor         [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0]

//常用字体颜色
#define NewCommonColor

/**
 *  View 圆角和加边框
 */
#define ViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

/**
 *  View 圆角
 */
#define ViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]


/**
 *  字体
 */
#define NewFont(fontSize)   [UIFont systemFontOfSize:fontSize]//细体字号
#define NewBFont(fontSize)  [UIFont boldSystemFontOfSize:fontSize]//粗体字号
#define ChangeProportion (SCREEN_WIDTH/320.0)
#define NewAutoFont(fontSize)  ChangeProportion>1?NewFont(fontSize+2):NewFont(fontSize)//根据屏幕宽度返回细字体
#define NewBAutoFont(fontSize) ChangeProportion>1?NewBFont(fontSize+2):NewBFont(fontSize)//根据屏幕宽度返回粗字体


/**
 *  字符串拼接
 */
#define NewStringFormat(xfmt, ...) [NSString stringWithFormat:xfmt, ##__VA_ARGS__]

/**
 *  字符串比较
 */
#define NewEqualString(xParameter1, xParameter2) [xParameter1 isEqualToString:xParameter2]

/**
 *  对象初始化
 */
#define NewInit(xClass) [[xClass alloc] init]
#define NewInitWithName(xClass,initName) xClass *initName = NewInit(xClass)

/**
 *  MutableArrayInit 可变数组初始化
 */
#define NewMutableArrayInit [NSMutableArray array]

/**
 *  MutableDictionaryInit 可变字典初始化
 */
#define NewMutableDictionaryInit [NSMutableDictionary dictionary]

/**
 *  对象初始化且添加Frame
 */
#define NewInitWithParameter(xClass,initName,xInitFrame) xClass *initName = [[xClass alloc] initWithFrame:xInitFrame]

/**
 *  Frame
 */
#define NewFrame(xmx,xmy,xw,xh) CGRectMake(xmx,xmy,xw,xh)

/**
 *  Button点击事件
 */
#define NewTouchUpInside(touchName,touchAction) [touchName addTarget:self action:@selector(touchAction) forControlEvents:UIControlEventTouchUpInside]

/**
 *  UIImage初始化
 */
#define NewImageNamed(imageNameString) [UIImage imageNamed:imageNameString]

/**
 *  UIImageView初始化且添加图片
 */
#define NewImgInit(xClass,initName,pImg) xClass *initName = [[xClass alloc] initWithImage:pImg]

/**
 *  SafeRelease MRC状态下手动释放内存
 */
#define SafeRelease(A) [A release];A=nil;

/**
 *  SafeString
 */
#define NewSafeString(source) (([source isKindOfClass:[NSNull class]]?@"":source) == nil?@"":(NSString*)source)

/**
 *  StringSizeWith
 *
 *  @param xString 自定义字体以及字体大小
 *  @param xFont
 *
 *  @return StringSize
 */
#define NewStringSizeWith(xString,xFont) [xString sizeWithFont:xFont]

#define NewStringSize(xString,xFont) [xString sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:xFont, NSFontAttributeName, nil]]



/**
 *  Push Present
 */
#define NewPushViewController(xClass) [self.navigationController pushViewController:xClass animated:YES] //普通push
#define NewPopViewController [self.navigationController popViewControllerAnimated:YES] //普通返回到上一级
#define NewPopToRootViewController [self.navigationController popToRootViewControllerAnimated:YES] //返回到最顶层

#define NewPresentViewController(xClass) [self presentViewController:xClass animated:YES completion:nil] //普通present
#define NewDismissViewController [self dismissViewControllerAnimated:YES completion:nil] //普通dismiss返回


#define NewPresentNavigationController(xClass) [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:xClass] animated:YES completion:nil] //带导航条的present
#define NewDismissNavigationController [self.navigationController dismissViewControllerAnimated:YES completion:nil] //带导航条的dismiss返回



/**
 *  DEBUG输出日志
 */

/*
 #ifdef DEBUG
 #define NSLog(...) NSLog(@"%s 第%d行 \n%@",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
 
 #else
 
 #define NSLog(...)
 #endif
 */

/*
 #ifdef DEBUG
 #define NSLog(...) NSLog(__VA_ARGS__)
 #define debugMethod() NSLog(@"%s",__func__)
 ////
 
 #else
 
 #define NSLog(...)
 #define debugMethod()
 #endif
 */


/*
 #ifdef DEBUG
 #define LRString [NSString stringWithFormat:@"%s", __FILE__].lastPathComponent
 #define LRLog(...) printf("%s: %s 第%d行: %s\n\n",[[NSString lr_stringDate] UTF8String], [LRString UTF8String] ,__LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String]);
 
 #else
 #define LRLog(...)
 #endif
 */

/**
 *  NSURL
 */
#define NewURL(NewImageUrl)     [NSURL URLWithString:NewImageUrl]


/**
 *  用户默认头像
 */
#define NewUserImage            [UIImage imageNamed:@"NewDefaultHeadPortrait"]


/**
 *  个人中心头视图
 */
#define NewDefaultHeaderImage   [UIImage imageNamed:@"个人中心_头视图"]


/**
 *  用户未激活提示语
 */
#define NewNoActivedTitle       @"很抱歉，您的账号尚未激活！"
#define NewNoActivedHUD         [SVProgressHUD showInfoWithStatus:NewNoActivedTitle]


/**
 *  默认连接服务器失败/无数据提示语
 */
#define NewConnectServerErrorTitle      @"很遗憾，网络连接失败了！"
#define NewConnectServerReloadTitle     @"重新加载"
#define NewConnectServerNoDataTitle     @"没有数据哦！"
#define NewConnectServerRefreshTitle    @"刷新"
#define NewConnectServerNoMoreDataTitle @"没有更多数据了"


/**
 *  默认网络错误图片
 */
#define NewetworkErrorImage             @"NewNetworkError"


/**
 *  默认无网络数据图片
 */
#define NewNoDataErrorImage             @"NewNoDataError"


/**
 *  默认无视频图片
 */
#define NewNoVideoErrorImage            @"NewNoVideoError"


/**
 *  默认头像
 */
#define NewDefaultAvatarImage            @"NewDefaultAvatar"


/**
 *  默认的加载失败之后呈现的图片
 */
#define NewImageError_J [UIImage imageNamed:@"imageError_J"]  //正
#define NewImageError_T [UIImage imageNamed:@"imageError_T"]  //横
#define NewImageError_V [UIImage imageNamed:@"imageError_V"]  //竖
#define NewImageError_C [UIImage imageNamed:@"imageError_C"]  //圆
#define NewImageError_Avatar [UIImage imageNamed:@"imageError_Avatar"]  //头像
#define NewImageError_Special [UIImage imageNamed:@"imageError_Special"]  //专题


/**
 *  沙盒目录文件
 
 //获取temp
 #define NewPathTemp NSTemporaryDirectory()
 //获取沙盒 Document
 #define NewPathDocument [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
 //获取沙盒 Cache
 #define NewPathCache [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]
 */


/**
 *  GCD 的宏定义  使用的时候 -> xDISPATCH_ONCE_BLOCK(^{这里面写代码});
 
 //GCD - 一次性执行
 #define NewDISPATCH_ONCE_BLOCK(onceBlock) static dispatch_once_t onceToken; dispatch_once(&onceToken, onceBlock);
 //GCD - 在Main线程上运行
 #define NewDISPATCH_MAIN_THREAD(mainQueueBlock) dispatch_async(dispatch_get_main_queue(), mainQueueBlock);
 //GCD - 开启异步线程
 #define NewDISPATCH_GLOBAL_QUEUE_DEFAULT(globalQueueBlock) dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), globalQueueBlocl);
 */


