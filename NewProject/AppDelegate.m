//
//  AppDelegate.h
//  NewProject
//
//  Created by 肖雨 on 2017/9/17.
//  Copyright © 2017年 NewOrganization. All rights reserved.
//

#import "AppDelegate.h"

#import "NewInterfaceReplacement.h"     //当Token失效时 让用户选择强制退出
#import "NotBackUpiCloud.h"             //设置禁止云同步
#import "UncaughtExceptionHandler.h"    //初始化程序异常
#import "NewConfigureRootController.h"  //控制类初始化
#import "NewKeyboardManager.h"          //键盘适配
#import "WXSDKHelper.h"                 //微信支付
#import "AlipaySDKHelper.h"             //支付宝支付
#import "JPushHelper.h"                 //极光推送

#import "UIWindow+PazLabs.h"
#import "WXApi.h"
#import "NewLoginViewController.h"
@interface AppDelegate ()
{
    UIAlertView *alertViewLog;
}
@end

@implementation AppDelegate

+ (AppDelegate *) shareDelegate
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    /**
     *  配置公共通知对象
     */
//    [self sibenServerPublicNotification];
    
    
    /**
     *  禁止上传icloud
     */
//    [[NotBackUpiCloud sharedInstance] addNotBackUpiCloud];
    
    
    /**
     *  初始化异常 捕捉程序崩溃原因 弹窗提示
     */
//    InstallUncaughtExceptionHandler();
    
    
    /**
     *  观察网络状态
     */
//    [NetworkRequestManager observeNetworkStatus];
    
    
    /**
     *  初始化极光推送
     */
    
//    [[JPushHelper sharedInstance]setJPushAPNSSDK:application options:launchOptions];
    
    
    /**
     *  配置微信SDK
     */
//    [[WXSDKHelper sharedInstance] setWXSDK];
    

    /**
     *  设置根目录reWriteUserInfoFromLocation
     */
    [[NewConfigureRootController sharedInstance] setRootController];
    
    
    /**
     *  键盘适配
     */
    [[NewKeyboardManager sharedInstance] keyboardManager];

//    NSLog(@"%@",[NewUtils userDefaultsStringKey:NewAccountNumber]);

//    [NSThread sleepForTimeInterval:1.5];

    return YES;
}


#pragma mark - APNs注册成功回调，将返回的deviceToken上传到CloudPush服务器
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
//    [[JPushHelper sharedInstance] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}


#pragma mark - APNs注册失败回调
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
//    [[JPushHelper sharedInstance] application:application didFailToRegisterForRemoteNotificationsWithError:error];
}


#pragma mark - 基于iOS6及以下的系统版本 旧的API处理收到的推送消息通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    //收到通知消息
//    [[JPushHelper sharedInstance] application:application didReceiveRemoteNotification:userInfo];
}


#pragma mark - APP处于前台 基于iOS10 新的API处理收到的推送消息通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    //应用在前台收到通知
//    [[JPushHelper sharedInstance] userNotificationCenter:center willPresentNotification:notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler];
}


#pragma mark - APP处于后台/未启动 新的API处理收到的推送消息通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    
    //点击通知进入应用
//    [[JPushHelper sharedInstance] userNotificationCenter:center didReceiveNotificationResponse:response withCompletionHandler:(void (^)())completionHandler];
}


#pragma mark -  基于iOS7~iOS9 旧的API处理收到的推送消息通知
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
//    [[JPushHelper sharedInstance] application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:(void (^)())completionHandler];
}


#pragma mark - 本地推送通知API 处理收到的推送消息通知
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
//    [[JPushHelper sharedInstance] application:application didReceiveLocalNotification:notification];
}




#pragma mark -  基于iOS9以前 旧的API处理第三方跳转本应用收到的URL与值
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //NSLog(@"欢迎回来 Url : %@",[url.absoluteString urlDecodedString]);
    
    if ([url.host isEqualToString:@"safepay"]) {
        
        //如果APP处于后台，则通过此方法拿到支付宝的回调信息
        return [[AlipaySDKHelper sharedInstance]application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        
    }else if ([url.host isEqualToString:@"pay"]) {
        
        //处理微信回调信息
        return [[WXSDKHelper sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        
    }else if ([url.host isEqualToString:@"oauth"]){
        
        return [[WXSDKHelper sharedInstance] application:application openURL:url options:annotation];
    }

    /*
     else {
     
     //其它通过项目协议唤醒APP
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     [[NSNotificationCenter defaultCenter]postNotificationName:MMNotificationMessage object:url userInfo:@{mmNotificationMessageType:mmNotificationMessageUrl}];
     });
     }
     */
    return YES;
}

#pragma mark - 基于iOS9以后 新的API处理第三方跳转本应用收到的URL与值
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options
{
    //NSLog(@"欢迎回来 Url : %@",[url.absoluteString urlDecodedString]);
    
    if ([url.host isEqualToString:@"safepay"]) {
        
        //如果APP处于后台，则通过此方法拿到支付宝的回调信息
        return [[AlipaySDKHelper sharedInstance]application:application openURL:url options:options];
        //
    }else if ([url.host isEqualToString:@"pay"]) {
        
        //处理微信回调信息
        return [[WXSDKHelper sharedInstance] application:application openURL:url options:options];
        
    }else if ([url.host isEqualToString:@"oauth"]){
        
        return [[WXSDKHelper sharedInstance] application:application openURL:url options:options];
    }
    /*
     else {
     
     //其它通过项目协议唤醒APP
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     [[NSNotificationCenter defaultCenter]postNotificationName:MMNotificationMessage object:url userInfo:@{mmNotificationMessageType:mmNotificationMessageUrl}];
     });
     }
     */
    return YES;
}

#pragma mark - 处理微信通过URL启动App时传递的数据
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    //处理微信回调信息
    return [[WXSDKHelper sharedInstance] application:application handleOpenURL:url];
}


#pragma mark - 应用程序进入非活跃状态（接听电话）
- (void)applicationWillResignActive:(UIApplication *)application {
    
    //NSLog(@"应用程序进入非活跃状态（接听电话）");

}


#pragma mark - 应用程序进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    //NSLog(@"应用程序进入后台");
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


#pragma mark - 应用程序进入前台
- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    //NSLog(@"应用程序进入前台");
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


#pragma mark - 应用程序启动（重启）
- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    //NSLog(@"应用程序启动");
    //[JPUSHService setBadge:0];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


#pragma mark - 应用程序终止时
- (void)applicationWillTerminate:(UIApplication *)application {
    
    //NSLog(@"应用程序终止时");
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
    //释放公共通知对象
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NewSibenServerPublicNotification object:nil];
}

#pragma mark - 强制退出登录
- (void)forceLogoutLogin
{
    if (!alertViewLog) {
        alertViewLog = [[UIAlertView alloc]initWithTitle:@"系统提示" message:@"登录已过期，请重新登录！此功能用于被挤下线或者退出登录" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertViewLog show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    [alertViewLog removeFromSuperview];
//    alertViewLog = nil;
    
    //删除已绑定的推送对象
//    [[JPushHelper sharedInstance] deleteAlias];
    [UserHelper removeUserInfoFromLocation];
    NewLoginViewController *vc = [[NewLoginViewController alloc] init];//去登陆
    UINavigationController* navi = [[UINavigationController alloc] initWithRootViewController:vc];
    [AppDelegate shareDelegate].window.rootViewController = navi;
    //销毁本地数据
//    [[NewInterfaceReplacement sharedInstance] replacementLogin];
    
}

#pragma mark - 配置公共通知对象
- (void)sibenServerPublicNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotificationMethod:) name:NewSibenServerPublicNotification object:nil];
}

#pragma mark - 公共通知 收到通知之后的处理
- (void)receiveNotificationMethod:(NSNotification *)notication
{
    if (notication.object) {
        
        /*
         //用户端需要处理的Code
         1.客户端下单成功
         2.服务端修改订单状态
         3.客户端退款
         
         //服务端需要处理的Code
         4.服务端下单成功
         5.客户端用户进行评价
         6.账号被挤下线
         */
        
        switch ([notication.object[@"msgType"] integerValue]) {
            case 1:
                [self orderEvaluate];
                break;
            case 2:
                [self orderEvaluate];
                break;
            case 3:
                [self orderEvaluate];
                break;
            case 6:
                [[AppDelegate shareDelegate] forceLogoutLogin];
                break;
                
            default:
                break;
        }
    }
}

//客户评价订单成功 服务端跳转到订单列表 全部 界面
- (void)orderEvaluate
{
    [[[self.window visibleViewController] navigationController] popToRootViewControllerAnimated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NewBasicTabbarController sharedInstance] changeRootViewControllerEvent:1];
        [[NSNotificationCenter defaultCenter]postNotificationName:NewOrderManageNotification object:@{@"clientStatus":@"-1"} userInfo:nil];
    });
    
    //    [[NSNotificationCenter defaultCenter]postNotificationName:NewSibenServerPublicNotification object:@{@"msgType":@"1"}];
    //    return;
}

@end
