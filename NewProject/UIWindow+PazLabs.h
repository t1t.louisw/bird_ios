//
//  UIWindow+PazLabs.h
//  SiBenClient
//
//  Created by 肖雨 on 2017/9/15.
//  Copyright © 2017年 ShangLv. All rights reserved.
//

#import <UIKit/UIKit.h>

//@interface UIWindow_PazLabs : UIWindow
//
//- (UIViewController *) visibleViewController;
//
//@end

@interface UIWindow (PazLabs)

- (UIViewController *) visibleViewController;

@end
