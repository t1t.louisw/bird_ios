//
//  AppDelegate.h
//  NewProject
//
//  Created by 肖雨 on 2017/9/17.
//  Copyright © 2017年 NewOrganization. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (AppDelegate *) shareDelegate;

- (void)forceLogoutLogin;//强制退出登录

@end

